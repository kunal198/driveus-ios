//
//  ProfileView.h
//  LinkRider
//
//  Created by Pham Diep on 3/27/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

IB_DESIGNABLE
@interface ProfileView : UIView
    
    @property (strong, nonatomic) IBOutlet UIView *view;
    @property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
    @property (weak, nonatomic) IBOutlet UILabel *lblName;
    @property (weak, nonatomic) IBOutlet UILabel *lblPhoneNum;
    
    @property (weak, nonatomic) IBOutlet EDStarRating *starRatingView;
@property (weak, nonatomic) IBOutlet UIView *viewStarIfIp5;
@property (weak, nonatomic) IBOutlet UILabel *lblStarValue;
 
- (void)setProfileWithAvatarLink:(NSString *)imgAvatarLink phoneNumber:(NSString *)phoneNumber starValue:(NSString *)up_count down_value:(NSString *)up_down  name:(NSString *)name type:(int)type;

@property (strong, nonatomic) IBOutlet UIImageView *imgThumbDown;
@property (strong, nonatomic) IBOutlet UIView *bgViewOfThumbs;


@property (strong, nonatomic) IBOutlet UILabel *up_count;

@property (strong, nonatomic) IBOutlet UILabel *up_down;



@end
