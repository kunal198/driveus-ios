//
//  ProfileView.m
//  LinkRider
//
//  Created by Pham Diep on 3/27/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import "ProfileView.h"
#import "UIImageView+WebCache.h"

@implementation ProfileView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
    
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // load view frame XIB
        [self commonSetup];
    }
    return self;
}
    
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // load view frame XIB
        [self commonSetup];
    }
    return self;
}
    
#pragma mark - setup view
    
- (UIView *)loadViewFromNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    
    //  An exception will be thrown if the xib file with this class name not found,
    UIView *view = [[bundle loadNibNamed:NSStringFromClass([self class])  owner:self options:nil] firstObject];
    return view;
}
    
- (void)commonSetup {
    UIView *nibView = [self loadViewFromNib];
    nibView.frame = self.bounds;
    // the autoresizingMask will be converted to constraints, the frame will match the parent view frame
    nibView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    // Adding nibView on the top of our view
    [self insertSubview:nibView atIndex:0];
   // [self.view setBackgroundColor:COLOR_BG_PROFILEVIEW];
  //  [self setProfileWithAvatarLink:@"" phoneNumber:@"" starValue:@"" name:@""];
}




    
- (void)setProfileWithAvatarLink:(NSString *)imgAvatarLink phoneNumber:(NSString *)phoneNumber starValue:(NSString *)up_count down_value:(NSString *)up_down  name:(NSString *)name type:(int)type{
    
    
    [_imgAvatar setImageWithURL:[NSURL URLWithString:imgAvatarLink] placeholderImage:HOLDER_AVATAR];
    _lblName.text = name;
    _lblPhoneNum.text = phoneNumber;
    
    if (type == 1) {
        self.imgThumbDown.hidden = true;
        self.up_down.hidden = true;
    }else if(type == 2){
        self.bgViewOfThumbs.hidden = true;
    }else{
        self.bgViewOfThumbs.hidden = false;
        self.imgThumbDown.hidden = false;
        self.up_down.hidden = false;
    }
    
        self.up_count.text = up_count;
        self.up_down.text = up_down;
        
    if (IS_iPhone5) {
        _viewStarIfIp5.hidden = NO;
        _starRatingView.hidden = YES;
        //_lblStarValue.text = [NSString stringWithFormat:@"%.1f", [starValue floatValue]/2];
    }else {
        _viewStarIfIp5.hidden = YES;
        _starRatingView.hidden = NO;
    _starRatingView.starImage = [[UIImage imageNamed:@"ic_star_NotSelect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _starRatingView.starHighlightedImage = [[UIImage imageNamed:@"ic_star"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [_starRatingView setMaxRating:5.0];
    [_starRatingView setEditable:NO];
    [_starRatingView setDisplayMode:EDStarRatingDisplayHalf];
  //  [_starRatingView setRating:[starValue floatValue]/2];
        [_starRatingView setNeedsDisplay];
    }

    
}

@end
