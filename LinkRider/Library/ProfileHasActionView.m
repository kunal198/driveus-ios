//
//  ProfileHasActionView.m
//  LinkRider
//
//  Created by Pham Diep on 3/27/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import "ProfileHasActionView.h"
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>

@implementation ProfileHasActionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
    
- (instancetype)initWithFrame:(CGRect)frame {
        self = [super initWithFrame:frame];
        if (self) {
            // load view frame XIB
            [self commonSetup];
        }
        return self;
    }
    
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // load view frame XIB
        [self commonSetup];
    }
    return self;
}
    
#pragma mark - setup view
    
- (UIView *)loadViewFromNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    
    //  An exception will be thrown if the xib file with this class name not found,
    UIView *view = [[bundle loadNibNamed:NSStringFromClass([self class])  owner:self options:nil] firstObject];
    return view;
}
    
- (void)commonSetup {
    UIView *nibView = [self loadViewFromNib];
    nibView.frame = self.bounds;
    // the autoresizingMask will be converted to constraints, the frame will match the parent view frame
    nibView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    // Adding nibView on the top of our view
    [self insertSubview:nibView atIndex:0];
  //  [self.view setBackgroundColor:COLOR_BG_PROFILEVIEW];
  
   // [self setProfileWithAvatarLink:@"" phoneNumber:@"" starValue:@"" name:@""];
    
    
}
    
- (void)setProfileWithAvatarLink:(NSString*)imgAvatarLink phoneNumber:(NSString* )phoneNumber starValue:(NSString* )upvalue  downvalue :(NSString*)downvalue name:(NSString* )name
{
    _up_count.text = upvalue;
    _down_count.text = downvalue;
    
    [_imgAvatar setImageWithURL:[NSURL URLWithString:imgAvatarLink] placeholderImage:HOLDER_AVATAR];

    _lblName.text = name;
    _phoneNumber = phoneNumber;
   
}

- (IBAction)onCall:(id)sender {
    [Util callPhoneNumber:_phoneNumber];
}
- (IBAction)onSMS:(id)sender {
    [Util sendSms:_phoneNumber];
}

@end
