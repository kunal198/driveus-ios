//
//  ProfileHasActionView.h
//  LinkRider
//
//  Created by Pham Diep on 3/27/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//
IB_DESIGNABLE
#import <UIKit/UIKit.h>

@interface ProfileHasActionView : UIView
    @property (strong, nonatomic) IBOutlet UIView *view;
    @property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
    @property (weak, nonatomic) IBOutlet UILabel *lblName;
    @property (weak, nonatomic) IBOutlet UILabel *lblStar;
    @property (strong, nonatomic) NSString *phoneNumber;
- (void)setProfileWithAvatarLink:(NSString*)imgAvatarLink phoneNumber:(NSString* )phoneNumber starValue:(NSString* )upvalue  downvalue :(NSString*)downvalue name:(NSString* )name;

@property (strong, nonatomic) IBOutlet UILabel *up_count;

@property (strong, nonatomic) IBOutlet UILabel *down_count;





@end
