

#import <Foundation/Foundation.h>
#import "Trips.h"
@interface Global : NSObject
extern NSString *deviceTokenString;
extern User *gUser;
extern Trips *gTrip;
extern NSDictionary *notificationsDic;
extern NSArray* gArrState;
extern LocationManager *gShareLocation;

@end
