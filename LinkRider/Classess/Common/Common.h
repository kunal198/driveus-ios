

#ifndef LinkRider_Common_h
#define LinkRider_Common_h

/*. Replace 27.72.88.241:8888 by your Domain  .*/



#define BASE_URL                   @"http://beta.brstdev.com/DrivAS/api/"
//#define BASE_URL                @"http://hicomsolutions.hicommart.com/taxinear/api/"
#define STRIPE_URL              @"http://hicomsolutions.hicommart.com/taxinear/stripe/web/index.php"
//#define BASE_URL                @"http://27.72.88.241:8888/hicom-taxinear-dev/api/"
//#define STRIPE_URL              @"http://27.72.88.241:8888/hicom-taxinear-dev/stripe/web/index.php"

//#define kPayPalEnvironment      PayPalEnvironmentProduction
#define kPayPalEnvironment      PayPalEnvironmentSandbox

/*. Replace ARR3YZ29JFSRJVIto4SXEqW3s9IddLWdEz5QM8e8VniV-OimdTQDE8VWOBmENwFgAq9P2wZA202z8YYd by your PayPal's key .*/
#define kPayPalClientId         @"ARR3YZ29JFSRJVIto4SXEqW3s9IddLWdEz5QM8e8VniV-OimdTQDE8VWOBmENwFgAq9P2wZA202z8YYd"

/*. Replace sk_test_6Bfoaswa7K2SBzX4EYrRBwWi by your stripe's secret key .*/
//AIzaSyB4u35HaWUTA8-rcLW-B28HFw5GW_MQm9c
#define kStripSecretKey         @"sk_test_w8jox949HgMUQfUSuk8HOqn1"
//#define GOOGLE_API_KEY        @"AIzaSyA7tCJxBc94SPWQAboLneSmOPLQNGSNXP4"
#define GOOGLE_API_KEY          @"AIzaSyCqVT7gFmAy7oZEs-aa5nyDWN6x0-Gsqp4"

//#define GOOGLE_API_KEY         @"AIzaSyCaBp2oucdfBCPSVkoxoQgZ7fB-Qz4Gvas"
#define APP_IN_TEST_MODE        NO
#define MAP_ZOOM_DEFAULT        16
#define APP_URL_TO_SHARE        @"http://hicomsolutions.com"
#define REDEEM_URL              @"http://hicomsolutions.com"

#define API_LOGIN               @"login" 
#define API_REGISTER            @"register"
#define VEHICLE_REGISTER        @"vehicleRegister"
#define SHOW_USER_INFO          @"showUserInfo"
#define UPDATE_PROFILE          @"updateProfile"
#define SEARCH_DRIVER           @"searchDriver"
#define CREATE_REQUEST          @"createRequest"
#define CREATE_FutureREQUEST    @"futureRequest"
#define REGISTER_NOMAL          @"signup"

#define kFakeLocationLat        -37.812090
#define kFakeLocationLong       144.962455
#define UPDATE_LOCATION_DISTANCE_MODE    kCLLocationAccuracyNearestTenMeters

#define HOLDER_AVATAR           [UIImage imageNamed:@"no_image.png"]

#define COLOR_BG_PROFILEVIEW [UIColor \
colorWithRed:((float)((0xe8c03a & 0xFF0000) >> 16))/255.0 \
green:((float)((0xe8c03a & 0xFF00) >> 8))/255.0 \
blue:((float)(0xe8c03a & 0xFF))/255.0 alpha:1.0]

#define COLOR_DIRECTION [UIColor \
colorWithRed:((float)((0x727272 & 0xFF0000) >> 16))/255.0 \
green:((float)((0x727272 & 0xFF00) >> 8))/255.0 \
blue:((float)(0x727272 & 0xFF))/255.0 alpha:1.0]

#define COLOR_BG_NAVIGATION [UIColor \
colorWithRed:((float)((0x06fbe9 & 0xFF0000) >> 16))/255.0 \
green:((float)((0x06fbe9 & 0xFF00) >> 8))/255.0 \
blue:((float)(0x06fbe9 & 0xFF))/255.0 alpha:1.0]
////
#define COLOR_BG_BUTTONACTIVE [UIColor \
colorWithRed:((float)((0x02645c & 0xFF0000) >> 16))/255.0 \
green:((float)((0x02645c & 0xFF00) >> 8))/255.0 \
blue:((float)(0x02645c & 0xFF))/255.0 alpha:1.0]

#define COLOR_BACKGROUND [UIColor \
colorWithRed:((float)((0xf7f7f7 & 0xFF0000) >> 16))/255.0 \
green:((float)((0xf7f7f7 & 0xFF00) >> 8))/255.0 \
blue:((float)(0xf7f7f7 & 0xFF))/255.0 alpha:1.0]


#define COLOR_TITLE_TEXT [UIColor \
colorWithRed:((float)((0x252525 & 0xFF0000) >> 16))/255.0 \
green:((float)((0x252525 & 0xFF00) >> 8))/255.0 \
blue:((float)(0x252525 & 0xFF))/255.0 alpha:1.0]

#define COLOR_TEXT_SUB_1 [UIColor \
colorWithRed:((float)((0x252525 & 0xFF0000) >> 16))/255.0 \
green:((float)((0x252525 & 0xFF00) >> 8))/255.0 \
blue:((float)(0x252525 & 0xFF))/255.0 alpha:1.0]

#define COLOR_RATE [UIColor \
colorWithRed:((float)((0xe8c03a & 0xFF0000) >> 16))/255.0 \
green:((float)((0xe8c03a & 0xFF00) >> 8))/255.0 \
blue:((float)(0xe8c03a & 0xFF))/255.0 alpha:1.0]



#endif
