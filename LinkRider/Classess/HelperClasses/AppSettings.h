

#import <Foundation/Foundation.h>
typedef enum {
    SceneMapViewController = 1,
    SceneWaitingViewController = 2,
    SceneOrderConfirmViewController = 3,
    SceneRatingTripViewController = 4,
    SceneOnlineViewController = 5,
    SceneOnlineConfirmViewController = 6,
    SceneOnlineStartTripViewController = 7,
    
} CurrentScene;

@interface AppSettings : NSObject

+ (instancetype)share;

- (void)getDataFromServer;

@property (nonatomic) int currentScene;
@property (nonatomic, strong) NSArray *arrSceneName;

@property (assign, nonatomic) NSInteger maxTimeSendRequest;
@property (assign, nonatomic) NSInteger timeToSendRequestAgain;
@property (strong, nonatomic) NSString* adminPhoneNumber;
@property (strong, nonatomic) NSString* adminEmail;
@property (assign, nonatomic) float minReedemAmount;
@property (assign, nonatomic) float minTransferAmount;
@property (assign, nonatomic) float driverShareBonus;
@property (assign, nonatomic) float passengerShareBonus;
@property (assign, nonatomic) float pricePerKmWith1Seat;
@property (assign, nonatomic) float pricePerKmWith2Seat;
@property (assign, nonatomic) float pricePerKmWith3Seat;
@property (assign, nonatomic) float pricePerMinuteWith1Seat;
@property (assign, nonatomic) float pricePerMinuteWith2Seat;
@property (assign, nonatomic) float pricePerMinuteWith3Seat;
@property (assign, nonatomic) float startFare1Seat;
@property (assign, nonatomic) float startFare2Seat;
@property (assign, nonatomic) float startFare3Seat;
@property (assign, nonatomic) float estimate_fare_speed;
@property (assign, nonatomic) float driverEarn;


@end
