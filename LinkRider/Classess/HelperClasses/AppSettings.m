

#import "AppSettings.h"
#import "ModelManager.h"

@implementation AppSettings

+ (instancetype)share {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
        [sharedMyModel getDataFromServer];
        [sharedMyModel setCurrentScene:SceneMapViewController];
        [sharedMyModel setArrSceneName:@[@"MapViewController", @"WaitingViewController", @"OrderConfirmViewController", @"RatingTripViewController", @"OnlineViewController", @"OnlineConfirmViewController", @"OnlineStartTripViewController"]];
    });
    if (![sharedMyModel pricePerKmWith1Seat]) {
        [sharedMyModel getDataFromServer];
    }
    return sharedMyModel;
}

- (void)getDataFromServer{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [ModelManager generalSettingWithSuccess:^(NSDictionary *dicReturn) {
            _timeToSendRequestAgain = [Validator getSafeInt:dicReturn[@"time_to_send_request_again"]];
            _maxTimeSendRequest = [Validator getSafeInt:dicReturn[@"max_time_send_request"]] + 1;
            _adminPhoneNumber = [Validator getSafeString:dicReturn[@"admin_phone_number"]];
            _adminEmail = [Validator getSafeString:dicReturn[@"admin_email"]];
            _minReedemAmount = [Validator getSafeFloat:dicReturn[@"min_redeem_amount"]];
            _minTransferAmount = [Validator getSafeFloat:dicReturn[@"min_transfer_amount"]];
            _driverShareBonus = [Validator getSafeFloat:dicReturn[@"driver_share_bonus"]];
            _passengerShareBonus = [Validator getSafeFloat:dicReturn[@"passenger_share_bonus"]];
            _pricePerKmWith1Seat = [Validator getSafeFloat:dicReturn[@"ppk_of_link_i"]];
            _pricePerKmWith2Seat = [Validator getSafeFloat:dicReturn[@"ppk_of_link_ii"]];
            _pricePerKmWith3Seat = [Validator getSafeFloat:dicReturn[@"ppk_of_link_iii"]];
            _pricePerMinuteWith1Seat = [Validator getSafeFloat:dicReturn[@"ppm_of_link_i"]];
            _pricePerMinuteWith2Seat = [Validator getSafeFloat:dicReturn[@"ppm_of_link_ii"]];
            _pricePerMinuteWith3Seat = [Validator getSafeFloat:dicReturn[@"ppm_of_link_iii"]];
            _startFare1Seat = [Validator getSafeFloat:dicReturn[@"sf_of_link_i"]];
            _startFare2Seat = [Validator getSafeFloat:dicReturn[@"sf_of_link_ii"]];
            _startFare3Seat = [Validator getSafeFloat:dicReturn[@"sf_of_link_iii"]];
            
            _estimate_fare_speed = [Validator getSafeFloat:dicReturn[@"estimate_fare_speed"]];
            _driverEarn = [Validator getSafeFloat:dicReturn[@"driver_earn"]];
            
        } andFailure:^(NSString *err) {
            
        }];

    });
   
}

@end
