//
//  AboutViewController.m
//  LinkRider
//
//  Created by Brst981 on 01/05/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self customSetup];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    // [self.navigationController.navigationBar setBarTintColor:[Util colorWithHexString:@"#333333" alpha:1]];
    //[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    //[self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Raleway-Medium" size:20], NSFontAttributeName, nil]];
    // self.navigationController.navigationBar.translucent = YES;
    
    if ( revealViewController )
    {
        
        //        [self.revealButtonItem setTarget: self.revealViewController];
        //        [self.revealButtonItem setAction: @selector(revealToggle:)];
        //[self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
        
        //    SWRevealViewController *revealViewController = self.revealViewController;
        if ( revealViewController )
        {
            [_Reveal_toggle addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        }
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        
        
    }
    
    //    [self.menu setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
    //                                                   [UIFont fontAwesomeFontOfSize:24], NSFontAttributeName,
    //                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
    //                                                   nil]
    //                                         forState:UIControlStateNormal];
    //
    //    [self.menu setTitle:@"\uf0c9"];
    
 
    
}


@end
