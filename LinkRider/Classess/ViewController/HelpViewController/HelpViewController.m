//
//  HelpViewController.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

   // [self setNeedsStatusBarAppearanceUpdate];
    // Do any additional setup after loading the view.
}
//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [self setText];
}
-(void)setText{
    SWRevealViewController *revealViewController = self.revealViewController;
    //[self.navigationController.navigationBar setBarTintColor:[Util colorWithHexString:@"#333333" alpha:1]];
    //[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    //[self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Raleway-Medium" size:20], NSFontAttributeName, nil]];
//    [self.revealBtn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                            [UIFont fontAwesomeFontOfSize:24], NSFontAttributeName,
//                                            [UIColor whiteColor], NSForegroundColorAttributeName,
//                                            nil]
//                                  forState:UIControlStateNormal];
//    
//    [self.revealBtn setTitle:@"\uf0c9"];
    if ( revealViewController )
    {
        
        //        [self.revealButtonItem setTarget: self.revealViewController];
        //        [self.revealButtonItem setAction: @selector(revealToggle:)];
        //[self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
        
        //    SWRevealViewController *revealViewController = self.revealViewController;
        if ( revealViewController )
        {
            [revealBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        }
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        
        
    }
   [self.lblHeaderTitle setTitle:[Util localized:@"header_help"]];
    self.lblHelp.text = [Util localized:@"help_description"];
    [self.lblHelp sizeToFit];
    
}

@end
