//
//  HelpViewController.h
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController

{
    
    IBOutlet UIButton *revealBtn;
}
@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;


@property (weak, nonatomic) IBOutlet UILabel *lblHelp;

@end
