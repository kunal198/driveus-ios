//
//  RegisterVC.h
//  LinkRider
//
//  Created by Pham Diep on 10/26/16.
//  Copyright © 2016 Fruity Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"

@interface RegisterVC : UIViewController<UIKeyboardViewControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
UIKeyboardViewController *keyboard;
    UIActionSheet *selectPhoto;
    UIImagePickerController *pickerAvatar;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *icName;
@property (weak, nonatomic) IBOutlet UILabel *icEmail;
@property (weak, nonatomic) IBOutlet UILabel *icPassword;
@property (weak, nonatomic) IBOutlet UILabel *icPhone;
@property (weak, nonatomic) IBOutlet UILabel *icAddress;
@property (weak, nonatomic) IBOutlet UILabel *icPostCode;


//@property (weak, nonatomic) IBOutlet UILabel *lblName;
//@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
//@property (weak, nonatomic) IBOutlet UILabel *lblPassword;
//@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
//@property (weak, nonatomic) IBOutlet UILabel *lblCountry;
//@property (weak, nonatomic) IBOutlet UILabel *lblCity;
//@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
//@property (weak, nonatomic) IBOutlet UILabel *lblPostCode;

@property (weak, nonatomic) IBOutlet UITextField *tfCountry;
@property (weak, nonatomic) IBOutlet UITextField *tfCity;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfPostCode;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UILabel *lblAccount;
@property (weak, nonatomic) IBOutlet UITextField *txtAccount;
@property (weak, nonatomic) IBOutlet UILabel *icAccount;

//@property (weak, nonatomic) IBOutlet UITextField *tfName;
//@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
//@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
//@property (strong, nonatomic) IBOutlet UITextField *tfconfirmpasswrd;
//@property (weak, nonatomic) IBOutlet UITextField *tfPhone;

@property (strong, nonatomic) IBOutlet UITextField *tfName;

@property (strong, nonatomic) IBOutlet UITextField *tfEmail;
@property (strong, nonatomic) IBOutlet UITextField *tfPassword;
@property (strong, nonatomic) IBOutlet UITextField *tfconfirmpasswrd;
@property (strong, nonatomic) IBOutlet UITextField *tfPhone;


@end
