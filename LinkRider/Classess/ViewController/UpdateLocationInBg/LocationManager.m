//
//  LocationShareModel.m
//  Location
//
//  Created by Rick
//  Copyright (c) 2014 Location. All rights reserved.
//

#import "LocationManager.h"
#import "ModelManager.h"
#import <UIKit/UIKit.h>

@interface LocationManager ()

@end

@implementation LocationManager{
    NSTimer *timer;
    __block UIBackgroundTaskIdentifier bgTask;
    BOOL calculateDistanceOn;
}

//Class method to make sure the share model is synch across the app
+ (instancetype)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
        [sharedMyModel startMonitoringLocation];
    });
    return sharedMyModel;
}


- (void)startCalculateDistanceTravel{
    calculateDistanceOn = YES;
    _updateOn = YES;
    _distanceTraveled = [Validator getSafeFloat:[Util objectForKey:kSaveDistance]];
    
}

- (void)stopCalculateDistanceTravel{
    calculateDistanceOn = NO;
    _distanceTraveled = 0;
}

#pragma mark - CLLocationManager

- (void)startMonitoringLocation {
    BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
    if (locationAllowed==NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Service Disabled"
                                                        message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    self.anotherLocationManager = [[CLLocationManager alloc]init];
    _anotherLocationManager.delegate = self;
    _anotherLocationManager.pausesLocationUpdatesAutomatically = YES;
    _anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.anotherLocationManager.distanceFilter = kDistanceFilter;
    self.anotherLocationManager.headingFilter = kHeadingFilter;
    if(IS_OS_8_OR_LATER) {
        [_anotherLocationManager requestAlwaysAuthorization];
    }
    
    [_anotherLocationManager startUpdatingLocation];
    
    if (APP_IN_TEST_MODE) {
        self.myLocation = CLLocationCoordinate2DMake(kFakeLocationLat, kFakeLocationLong);
    }else{
        self.myLocation = _anotherLocationManager.location.coordinate;
    }
}

- (void)restartMonitoringLocation {
    [timer invalidate];
    [_anotherLocationManager stopUpdatingLocation];
    [_anotherLocationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    _anotherLocationManager.pausesLocationUpdatesAutomatically = YES;
    [_anotherLocationManager startUpdatingLocation];
    [_anotherLocationManager stopMonitoringSignificantLocationChanges];
}

- (void)enterForegroundApp:(UIApplication*)app{
    //    NSLog(@"Enter Foreground");
    [timer invalidate];
    [app endBackgroundTask:bgTask];
    bgTask = UIBackgroundTaskInvalid;
    [_anotherLocationManager setAllowsBackgroundLocationUpdates:NO];
}

- (void)enterBackgroundApp:(UIApplication*)app{
    
    [_anotherLocationManager stopUpdatingLocation];
    
//    if (!_updateOn) {
//        return;
//    }
    
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"begin background taskwith expriration handler");
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    [self startTrackingBg];
    timer = [NSTimer scheduledTimerWithTimeInterval:TIME_UPDATE
                                             target:self
                                           selector:@selector(startTrackingBg)
                                           userInfo:nil
                                            repeats:YES];
}

-(void)startTrackingBg {
    
    [_anotherLocationManager startUpdatingLocation];
    [_anotherLocationManager stopMonitoringSignificantLocationChanges];
    [_anotherLocationManager setAllowsBackgroundLocationUpdates:YES];
    _anotherLocationManager.pausesLocationUpdatesAutomatically = NO;
    NSLog(@"App is running in background");
}
//starts automatically with locationManager
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
        NSLog(@"App Received new location Location");
    if (APP_IN_TEST_MODE) {
        
        [Util setObject:[NSString stringWithFormat:@"%f", 1000.0] forKey:kSaveDistance];
        [self.delegate didUpdateLocationFrom:_myLocation toLocation:_myLocation];
        
        return;
        
    }
    
    [self.delegate didUpdateLocationFrom:_myLocation toLocation:newLocation.coordinate];
    
    if (!_updateOn) {
        _myLocation = newLocation.coordinate;
        [_anotherLocationManager stopUpdatingLocation];
        return;
    }
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        if (calculateDistanceOn) {
            CLLocationDistance distanceVar = [newLocation distanceFromLocation:[[CLLocation alloc] initWithLatitude:_myLocation.latitude longitude:_myLocation.longitude]];
            
            if (_myLocation.latitude == 0) {
                self.myLocation = newLocation.coordinate;
                [self updateLocationWithLat:_myLocation.latitude andLong:_myLocation.longitude];
                return;
            }
            
            if ( distanceVar > RANGE_TO_UPDATE) {
                
                
                _distanceTraveled += distanceVar;
                [Util setObject:[NSString stringWithFormat:@"%f", _distanceTraveled] forKey:kSaveDistance];
                
                
             
                    self.myLocation = newLocation.coordinate;
                
                [self updateLocationWithLat:_myLocation.latitude andLong:_myLocation.longitude];
                
            }
        }else{
          
                self.myLocation = newLocation.coordinate;
            
            [self updateLocationWithLat:_myLocation.latitude andLong:_myLocation.longitude];
        }
        
    });
    
}
#pragma mark - CLLocationManager Delegate

- (void)updateLocationWithLat:(float)lat andLong:(float)longtitude{
    NSLog(@"Will push new location to server");
    if (gUser.driver.isOnline) {
        [ModelManager updateCoordinatewithLat:lat andLong:longtitude withSuccess:^{
            NSLog(@"Did push new location to server");
        } failure:^(NSString *err) {
            
        }];
    }
       
}


@end
