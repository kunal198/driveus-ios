//
//  RegisterVC.m
//  LinkRider
//
//  Created by Pham Diep on 10/26/16.
//  Copyright © 2016 Fruity Solution. All rights reserved.
//

#import "RegisterVC.h"
#import "StateObj.h"
#import "UIView+Toast.h"
#import "MBProgressHUD.h"
#import "ModelManager.h"

@interface RegisterVC ()<UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation RegisterVC{
    UIPickerView *pickerCountry;
    StateObj *stateObjThumb;
    BOOL isSelectedImg;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
    isSelectedImg = false;
    [self customSetup];
    // Do any additional setup after loading the view.
}


- (IBAction)Back_Action:(id)sender

{
     [self.navigationController popViewControllerAnimated:YES];
    
}





- (void)customSetup
{
//    stateObjThumb = [[StateObj alloc] initWithDict:@{}];
//    if (gArrState.count == 0) {
//        _tfCountry.enabled = NO;
//    }else{
//        _tfCountry.enabled = YES;
//        stateObjThumb = [gArrState firstObject];
//        _tfCountry.text = stateObjThumb.stateName;
//        for (StateObj *objStateThumb in gArrState) {
//            if ([objStateThumb.stateId isEqualToString: gUser.stateId]) {
//                stateObjThumb = objStateThumb;
//                
//                break;
//            }
//        }
//    }

//    [self.navigationController setNavigationBarHidden:NO];
//    [self.navigationController.navigationBar setBarTintColor:[Util colorWithHexString:@"#333333" alpha:1]];
//    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
//    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Raleway-Medium" size:20], NSFontAttributeName, nil]];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.backItem.title = @"";
//    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    _tfPassword.secureTextEntry = YES ;
    _tfconfirmpasswrd.secureTextEntry = YES;
    
    
    
    [self configTextField:_tfName];
    [self configTextField:_tfEmail];
    [self configTextField:_tfPassword];
    [self configTextField:self.tfconfirmpasswrd];
    [self configTextField:_tfPhone];
    //[self configTextField:_tfCountry];
    //[self configTextField:_tfCity];
   /// [self configTextField:_tfAddress];
    //[self configTextField:_tfPostCode];
    //[self configTextField:_txtAccount];
    
    pickerCountry = [[UIPickerView alloc] init];
    pickerCountry.delegate =self;
    pickerCountry.dataSource = self;
   // _tfCountry.inputView = pickerCountry;
//   [_tfCountry addTarget:self action:@selector(textFieldShouldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
//    self.icPhone.text =  @"\ue670";
//    self.icEmail.text =  @"\ue639";
//    self.icAddress.text =  @"\ue638";
//    self.icName.text = @"\ue605";
//    self.icPostCode.text = @"\ue647";
//    self.icPassword.text = @""; //[NSString fontAwesomeIconStringForIconIdentifier:@"fa-key"];
    self.icAccount.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-money"];

//    [_imgAvatar setImage:HOLDER_AVATAR];
//    _imgAvatar.layer.cornerRadius = 40;
//    _imgAvatar.layer.masksToBounds = YES;
//    UITapGestureRecognizer *tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelectAvatar:)];
//    [_imgAvatar addGestureRecognizer:tapAvatar];
}

-(void)configTextField:(UITextField*)text{
    text.layer.borderColor = [UIColor whiteColor].CGColor;
    text.layer.borderWidth = 1.0;
  [text setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 15, 0)];
    text.leftView = paddingView;
    text.leftViewMode = UITextFieldViewModeAlways;

}

#pragma mark- Uipickerview Datasource (state, city)
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
           return gArrState.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
   
        StateObj* objState = gArrState[row];
        return objState.stateName;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
  
        stateObjThumb = gArrState[row];
        _tfCountry.text = stateObjThumb.stateName;

}


- (IBAction)onSelectAvatar:(id)sender {
    selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                             delegate:self
                                    cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                    otherButtonTitles: [Util localized:@"lbl_take_photo"],
                   [Util localized:@"lbl_select_from_galery"], nil];
    [selectPhoto showInView:self.view];
}




- (IBAction)onRegister:(id)sender {
    
//    if (!isSelectedImg) {
//        [self.view makeToast:@"Choose avatar by tapping on the image!"];
//        return;
//    }

    if (_tfEmail.text.length == 0) {
        [self.view makeToast:@"You must enter Email to continue!"];
        return;
    }
    if (![Validator validateEmail:_tfEmail.text]) {
        [self.view makeToast:@"Please insert your correct email!"];
        return;
    }
    if (_tfPassword.text.length == 0) {
        [self.view makeToast:@"You must enter Password to continue!"];
        return;
    }
    
    if (self.tfconfirmpasswrd.text.length == 0) {
        [self.view makeToast:@"confirm Password does not match with Pasword!"];
        return;
    }
    
    if ([_tfPassword isEqual: self.tfconfirmpasswrd])
    {
        [self.view makeToast:@"confirm Password does not match with Pasword"];
        return;
    }
    
    if (_tfName.text.length == 0) {
        [self.view makeToast:@"You must enter Name to continue!"];
        return;
    }
    if (_tfPhone.text.length == 0) {
        [self.view makeToast:@"You must enter  Phone number to continue!"];
        return;
    }
    if (_tfPhone.text.length < 10) {
        [self.view makeToast:@"You must enter correct Phone number!"];
        return;
    }
//    if (_tfCountry.text.length == 0) {
//        [self.view makeToast:@"You must select Country to continue!"];
//        return;
//    }
//    if (_tfCity.text.length == 0) {
//        [self.view makeToast:@"You must enter your City's name to continue!"];
//        return;
//    }
//    if (_tfAddress.text.length == 0) {
//        [self.view makeToast:@"You must enter Address to continue!"];
//        return;
//    }
//    if (_tfPostCode.text.length == 0) {
//        [self.view makeToast:@"You must enter Postcode to continue!"];
//        return;
//    }
//    if (_txtAccount.text.length == 0) {
//        [self.view makeToast:@"You mus enter Pauout Paypal to continue!"];
//        return;
//    }
    
    User *userTempObj = [[User alloc] init];
    userTempObj.name = _tfName.text;
    userTempObj.email = _tfEmail.text;
    userTempObj.password = _tfPassword.text;
    userTempObj.phone = [Validator getSafeString:_tfPhone.text];
//userTempObj.stateId = stateObjThumb.stateId;
   // userTempObj.city = [Validator getSafeString:_tfCity.text];
////userTempObj.address = [Validator getSafeString:_tfAddress.text];
    //userTempObj.descriptions = [Validator getSafeString:_tfPostCode.text];
   // userTempObj.cart_value = [Validator getSafeString:_txtAccount.text];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    UIImage *img = [UIImage imageNamed:@"no_image.png"];
    [ModelManager registerNormalWithUser:userTempObj avatar:img withSuccess:^(NSString *successStr) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Congratulation"
                                                                       message:@"Registered successfully."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 
                                                                  [self.navigationController popViewControllerAnimated:YES];
                                                              }];
        [alert addAction:firstAction];
    
        [self presentViewController:alert animated:YES completion:nil];
    } failure:^(NSString *err) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [Util showMessage:err withTitle:@"Error"];
    }];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex<2) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self showImagePickerWithSelection:buttonIndex];
        }];
    }
    
}
-(void)showImagePickerWithSelection:(NSInteger)selection{
    pickerAvatar = [[UIImagePickerController alloc]init];
    pickerAvatar.delegate = self;
    pickerAvatar.allowsEditing = YES;
    switch (selection) {
        case 1:
            pickerAvatar.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            break;
        case 0:
            pickerAvatar.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            break;
        default:
            break;
    }
    [self presentViewController:pickerAvatar animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _imgAvatar.image = chosenImage;
    isSelectedImg = true;
    
}


@end
