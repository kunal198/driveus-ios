//
//  RegisterAsDriverViewController.h
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"
@interface RegisterAsDriverViewController : UIViewController<UIKeyboardViewControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate,UITextFieldDelegate>{
    UIKeyboardViewController *keyboard;
    
    UIActionSheet *selectPhoto;
    UIImagePickerController *pickerCar;
    UIImage *mainImage;
    UIImage *subImage;
    UIImage *documentPath;
    UIImage *License_documentPath;
    UIDatePicker *yearPicker;
    UIDatePicker *dobPicker;
   
    
    
    BOOL TLCLicense;
    BOOL drivingLicense;
    BOOL insuranceselecting;
    BOOL refistrationselecting ;
    BOOL Permitselecting;
    BOOL isDocumentSelecting;
    BOOL isImgProfile1Selecting;
    
    IBOutlet UIButton *reveal_toggle;
    
    
    UIImage *document_img;
    NSString *document;
    UIImage *Licence_img;
    NSString *Licence_Document;
    
    UIImage *insurance_img;
    NSString *insurance_Document;
    
    
    UIImage *Refistration_img;
    NSString *Refistration_Document;
    
    UIImage *Permit_img;
    NSString *Permit_Document;
    
    
    UIImage *maincar_img;
    NSString *maincar_Document;
    
    UIImage *subcar_img;
    NSString *subcar_Document;
    
    
    
    
    
}
@property (retain)UIDocumentInteractionController *documentController;
@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealBtn;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSave;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *mainSubview;
@property (weak, nonatomic) IBOutlet UILabel *lblTapToUpdateAv;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMainCar;
@property (weak, nonatomic) IBOutlet UIButton *btnAddSubCar;
@property (weak, nonatomic) IBOutlet UILabel *icAvatar;
@property (weak, nonatomic) IBOutlet UILabel *icName;
@property (weak, nonatomic) IBOutlet UILabel *icCarPlate;
@property (weak, nonatomic) IBOutlet UILabel *icBrandOfCar;
@property (weak, nonatomic) IBOutlet UILabel *icModelOfCar;
@property (weak, nonatomic) IBOutlet UILabel *icYear;
@property (weak, nonatomic) IBOutlet UILabel *icEmail;
@property (weak, nonatomic) IBOutlet UILabel *icStatus;
@property (weak, nonatomic) IBOutlet UILabel *icAccount;
@property (weak, nonatomic) IBOutlet UILabel *icDocument;
@property (weak, nonatomic) IBOutlet UILabel *icDoB;
@property (weak, nonatomic) IBOutlet UILabel *icCarType;
@property (strong, nonatomic) IBOutlet UIImageView *driver_licenseimage;
@property (strong, nonatomic) IBOutlet UIImageView *TLc_image;
@property (weak, nonatomic) IBOutlet UILabel *lblAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCarPlate;
@property (weak, nonatomic) IBOutlet UILabel *lblBrandOfCar;
@property (weak, nonatomic) IBOutlet UILabel *lblModelOfCar;
@property (weak, nonatomic) IBOutlet UILabel *lblYear;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblAccount;
@property (weak, nonatomic) IBOutlet UILabel *lblDocument;
@property (weak, nonatomic) IBOutlet UILabel *lblDoB;
@property (weak, nonatomic) IBOutlet UILabel *lblCarType;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtCarPlate;
@property (weak, nonatomic) IBOutlet UITextField *txtBrandOfCar;
@property (weak, nonatomic) IBOutlet UITextField *txtModelOfCar;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtStatus;
@property (weak, nonatomic) IBOutlet UITextField *txtDoB;
@property (weak, nonatomic) IBOutlet UIButton *btnDocument;
@property (weak, nonatomic) IBOutlet UITextField *tfCarType;
@property (strong, nonatomic) IBOutlet UITextField *tfPassword;
@property (strong, nonatomic) IBOutlet UITextField *tfconfirmpasswrd;
@property (strong, nonatomic) IBOutlet UITextField *tfPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtAccount;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile1;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile2;
@property (strong, nonatomic) IBOutlet UIImageView *insurance_cert;
@property (strong, nonatomic) IBOutlet UIImageView *refistration_image;
@property (strong, nonatomic) IBOutlet UIImageView *permit_;




    
    


@end
