//
//  RegisterAsDriverViewController.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "RegisterAsDriverViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIView+Toast.h"
#import "OnlineViewController.h"
#import "ViewController.h"
@interface RegisterAsDriverViewController ()

@end

@implementation RegisterAsDriverViewController{
    //outlet
    
    IBOutlet UITextField *txtCarAvailable;
    
    
    UIPickerView *carBrandPicker;
    UIPickerView *carModalPicker;
    UIPickerView *carTypePicker;
    UIPickerView *carAvailablePicker;

    NSDictionary* letterValues ;
    NSArray *carAvailableArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
//[self customsetting];
    [self.btnSave addTarget:self action:@selector(onSave) forControlEvents:UIControlEventTouchUpInside];
  
   // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   // [self getData];
    [self setText];
    _txtYear.delegate= self;
    isDocumentSelecting = NO;
    // Do any additional setup after loading the view.
    carAvailableArray = [NSArray arrayWithObjects:@"Yellow/Green Taxi",@"Black", nil];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    //[self.navigationItem setHidesBackButton:NO animated:YES];
    self.navigationController.navigationBarHidden = YES;
   
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
}
-(void)viewDidLayoutSubviews{
    CGRect rect = self.mainSubview.frame;
    rect.size.height = self.imgProfile1.frame.size.height+self.imgProfile1.frame.origin.y+20;
    self.mainSubview.frame = rect;
    self.scrollView.contentSize = CGSizeMake(320, self.imgProfile1.frame.size.height+self.imgProfile1.frame.origin.y+20);
}
-(void)setText

{
    self.icAvatar.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-user-plus"];
    self.icName.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-user"];
    self.icCarPlate.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-mobile"];
    self.icBrandOfCar.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-glass"];
    self.icModelOfCar.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-trophy"];
    self.icYear.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-calendar"];
    self.icEmail.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-envelope-o"];
    self.icStatus.text =  [NSString fontAwesomeIconStringForIconIdentifier:@" fa-bar-chart"];
    self.icDocument.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-file-pdf-o"];
    self.icAccount.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-money"];
    self.icDoB.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-calendar"];
    self.icCarType.text = [NSString fontAwesomeIconStringForIconIdentifier:@"fa-car"];
    
    self.txtBrandOfCar.delegate = self;
    
    

    self.lblAvatar.text = [Util localized:@"lbl_avatar"];
    self.lblName.text = [Util localized:@"lbl_id_driver"];
    self.lblCarPlate.text = [Util localized:@"lbl_car_plate"];
    self.lblBrandOfCar.text = [Util localized:@"lbl_brand_of_car"];
    self.lblModelOfCar.text = [Util localized:@"lbl_model_of_car"];
    self.lblYear.text = [Util localized:@"lbl_year"];
    self.lblEmail.text = [Util localized:@"lbl_email"];
    self.lblCarType.text = [Util localized:@"lbl_carType"];

    
    self.lblStatus.text = [Util localized:@"lbl_status"];
    self.lblDocument.text = [Util localized:@"lbl_document"];
    self.lblAccount.text = [Util localized:@"lbl_account"];
    [self.btnDocument setTitle:[Util localized:@"lbl_select_document"] forState:UIControlStateNormal];
    self.lblDoB.text = [Util localized:@"lbl_date_of_birth"];
    
    carBrandPicker = [[UIPickerView alloc] init];
    carBrandPicker.delegate =self;
    carBrandPicker.dataSource = self;
    self.txtBrandOfCar.inputView = carBrandPicker;
    
    carModalPicker = [[UIPickerView alloc] init];
    carModalPicker.delegate =self;
    carModalPicker.dataSource = self;
    
    self.txtModelOfCar.inputView = carModalPicker;
    
    
    carTypePicker = [[UIPickerView alloc] init];
    carTypePicker.delegate =self;
    carTypePicker.dataSource = self;
    _tfCarType.inputView = carTypePicker;
    
    letterValues = carsInfoDic;
    
    carAvailablePicker = [[UIPickerView alloc] init];
    carAvailablePicker.delegate =self;
    carAvailablePicker.dataSource = self;
    txtCarAvailable.inputView = carAvailablePicker;
    
    
    
    
    dobPicker = [[UIDatePicker alloc]init];
    dobPicker.datePickerMode = UIDatePickerModeDate;
    [dobPicker addTarget:self action:@selector(incidentDoBValueChanged:) forControlEvents:UIControlEventValueChanged];
    [dobPicker setMaximumDate:[NSDate date]];
    self.txtDoB.inputView = dobPicker;
    
    [self.btnDocument.layer setBorderWidth:1.0];
    [self.btnDocument.layer setBorderColor:[UIColor whiteColor].CGColor];
    for (UITextField *text in self.mainSubview.subviews) {
        if ([text isKindOfClass:[UITextField class]]) {
            text.layer.borderColor = [UIColor whiteColor].CGColor;
            text.layer.borderWidth = 1.0;
            [text setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 15, 0)];
            text.leftView = paddingView;
            text.leftViewMode = UITextFieldViewModeAlways;
            [text setReturnKeyType:UIReturnKeyDone];
            [text addTarget:self action:@selector(textFieldShouldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
        }
    }


    
    [self.lblHeaderTitle setTitle:[Util localized:@"header_register_as_driver"]];
    
//    SWRevealViewController *revealViewController = self.revealViewController;
//    [self.navigationController.navigationBar setBarTintColor:[Util colorWithHexString:@"#333333" alpha:1]];
//    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
//    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Raleway-Medium" size:20], NSFontAttributeName, nil]];
//    
//    if ( revealViewController )
//    {
//        [self.revealBtn setTarget: self.revealViewController];
//        [self.revealBtn setAction: @selector(revealToggle:)];
//        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
//    }
    
//    [self.revealBtn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                            [UIFont fontAwesomeFontOfSize:24], NSFontAttributeName,
//                                            [UIColor whiteColor], NSForegroundColorAttributeName,
//                                            nil]
//                                  forState:UIControlStateNormal];
//    
//    [self.revealBtn setTitle:@"\uf0c9"];
//    
    
//    [self.btnSave setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                          [UIFont fontWithName:@"Pe-icon-7-stroke" size:26], NSFontAttributeName,
//                                          [UIColor whiteColor], NSForegroundColorAttributeName,
//                                          nil]
//                                forState:UIControlStateNormal];
//    
//    [self.btnSave setTitle:@"\ue65f"];
    [self setUpAvata];
    
}
//-(void)customsetting
//{
//    SWRevealViewController *revealViewController = self.revealViewController;
//    
//    if ( revealViewController )
//    {
//        
//        //        [self.revealButtonItem setTarget: self.revealViewController];
//        //        [self.revealButtonItem setAction: @selector(revealToggle:)];
//        //[self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
//        
//        //    SWRevealViewController *revealViewController = self.revealViewController;
//        if ( revealViewController )
//        {
//            [reveal_toggle addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
//            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        }
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//        
//        
//        
//    }
//}
- (void)setUpAvata{
    self.imgAvatar.layer.cornerRadius=self.imgAvatar.frame.size.height /2;
    self.imgAvatar.layer.borderWidth=2;
    self.imgAvatar.layer.masksToBounds = YES;
    self.imgAvatar.layer.borderColor = [UIColor whiteColor].CGColor;
    _lblTapToUpdateAv.hidden = YES;
    

}

- (IBAction) incidentDateValueChanged:(id)sender{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy"];
    _txtYear.text = [dateFormatter stringFromDate:[yearPicker date]];
    
    NSString *strTimeIn=@"21/07/2016";
    NSString *strTimeOut=@"20/08/2016";
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    NSDate *dtOne=[format dateFromString:strTimeIn];
    NSDate *dtTwo=[format dateFromString:strTimeOut];
    
    NSComparisonResult result;
    result = [dtOne compare:dtTwo];
    
}
- (IBAction) incidentDoBValueChanged:(id)sender{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    _txtDoB.text = [dateFormatter stringFromDate:[dobPicker date]];

}

-(void)getData{
    if (gUser.descriptions.length==0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [ModelManager getUserProfileWithSuccess:^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [self setUserData];
            } failure:^(NSString *err) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [self.view makeToast:[Util localized:@"msg_network_error"] duration:2.0 position:CSToastPositionCenter];
                sleep(2.0);
                //[self.revealViewController revealToggleAnimated:YES];
            }];
        });
    }
    else{
        [self setUserData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
    
}

-(void)setUserData{
    [self.imgAvatar setImageWithURL:[NSURL URLWithString:gUser.thumb]];
    self.imgAvatar.layer.cornerRadius=self.imgAvatar.frame.size.height /2;
    self.imgAvatar.layer.borderWidth=2;
    self.imgAvatar.layer.masksToBounds = YES;
    self.imgAvatar.layer.borderColor = [UIColor whiteColor].CGColor;
    self.txtName.text = gUser.name;
    self.txtCarPlate.text  = gUser.car.carPlate;
    self.txtBrandOfCar.text = gUser.car.brand;
    self.txtModelOfCar.text = gUser.car.model;
    self.txtYear.text = gUser.manufacture_year;
    self.txtEmail.text = gUser.email;
    self.txtStatus.text = gUser.car_status;
    self.txtAccount.text = gUser.cart_value;
    self.txtDoB.text = gUser.date_of_birth;
    
    if (gUser.document.length==0) {
//        CGRect rect = self.imgProfile1.frame;
//        rect.size.height = rect.size.height/2;
//        
//        [_btnAddMainCar setTitle:[Util localized:@"lbl_add_main_car"] forState:UIControlStateNormal];
//        [_btnAddMainCar addTarget:self action:@selector(onAddCar:) forControlEvents:UIControlEventTouchUpInside];
//        _btnAddMainCar.titleLabel.font = self.lblEmail.font;
//        _btnAddMainCar.layer.borderColor = [UIColor whiteColor].CGColor;
//        _btnAddMainCar.layer.borderWidth = 1.0;
//        _btnAddMainCar.hidden = NO;
    }
    else
    {
        //_btnAddMainCar.hidden = YES;
        [self.TLc_image setImageWithURL:[NSURL URLWithString:gUser.document]];
        [self.imgProfile1 setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddCar:)];
        [self.imgProfile1 addGestureRecognizer:tap];
        
    }
    if (gUser.License_document.length==0) {
        //        CGRect rect = self.imgProfile1.frame;
        //        rect.size.height = rect.size.height/2;
        //
        //        [_btnAddMainCar setTitle:[Util localized:@"lbl_add_main_car"] forState:UIControlStateNormal];
        //        [_btnAddMainCar addTarget:self action:@selector(onAddCar:) forControlEvents:UIControlEventTouchUpInside];
        //        _btnAddMainCar.titleLabel.font = self.lblEmail.font;
        //        _btnAddMainCar.layer.borderColor = [UIColor whiteColor].CGColor;
        //        _btnAddMainCar.layer.borderWidth = 1.0;
        //        _btnAddMainCar.hidden = NO;
    }
    else{
        //_btnAddMainCar.hidden = YES;
        [self.driver_licenseimage setImageWithURL:[NSURL URLWithString:gUser.License_document]];
        [self.driver_licenseimage setUserInteractionEnabled:YES];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddCar:)];
//        [self.imgProfile1 addGestureRecognizer:tap];
        
    }
    
    if (gUser.Insurance_document.length==0) {
        //        CGRect rect = self.imgProfile1.frame;
        //        rect.size.height = rect.size.height/2;
        //
        //        [_btnAddMainCar setTitle:[Util localized:@"lbl_add_main_car"] forState:UIControlStateNormal];
        //        [_btnAddMainCar addTarget:self action:@selector(onAddCar:) forControlEvents:UIControlEventTouchUpInside];
        //        _btnAddMainCar.titleLabel.font = self.lblEmail.font;
        //        _btnAddMainCar.layer.borderColor = [UIColor whiteColor].CGColor;
        //        _btnAddMainCar.layer.borderWidth = 1.0;
        //        _btnAddMainCar.hidden = NO;
    }
    else{
        //_btnAddMainCar.hidden = YES;
        [self.insurance_cert setImageWithURL:[NSURL URLWithString:gUser.License_document]];
        [self.insurance_cert setUserInteractionEnabled:YES];
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddCar:)];
        //        [self.imgProfile1 addGestureRecognizer:tap];
        
    }
    
    if (gUser.Refistration_document.length==0) {
        //        CGRect rect = self.imgProfile1.frame;
        //        rect.size.height = rect.size.height/2;
        //
        //        [_btnAddMainCar setTitle:[Util localized:@"lbl_add_main_car"] forState:UIControlStateNormal];
        //        [_btnAddMainCar addTarget:self action:@selector(onAddCar:) forControlEvents:UIControlEventTouchUpInside];
        //        _btnAddMainCar.titleLabel.font = self.lblEmail.font;
        //        _btnAddMainCar.layer.borderColor = [UIColor whiteColor].CGColor;
        //        _btnAddMainCar.layer.borderWidth = 1.0;
        //        _btnAddMainCar.hidden = NO;
    }
    else{
        //_btnAddMainCar.hidden = YES;
        [self.refistration_image setImageWithURL:[NSURL URLWithString:gUser.License_document]];
        [self.refistration_image setUserInteractionEnabled:YES];
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddCar:)];
        //        [self.imgProfile1 addGestureRecognizer:tap];
        
    }
    if (gUser.Permit_document.length==0) {
        //        CGRect rect = self.imgProfile1.frame;
        //        rect.size.height = rect.size.height/2;
        //
        //        [_btnAddMainCar setTitle:[Util localized:@"lbl_add_main_car"] forState:UIControlStateNormal];
        //        [_btnAddMainCar addTarget:self action:@selector(onAddCar:) forControlEvents:UIControlEventTouchUpInside];
        //        _btnAddMainCar.titleLabel.font = self.lblEmail.font;
        //        _btnAddMainCar.layer.borderColor = [UIColor whiteColor].CGColor;
        //        _btnAddMainCar.layer.borderWidth = 1.0;
        //        _btnAddMainCar.hidden = NO;
    }
    else{
        //_btnAddMainCar.hidden = YES;
        [self.permit_ setImageWithURL:[NSURL URLWithString:gUser.License_document]];
        [self.permit_ setUserInteractionEnabled:YES];
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddCar:)];
        //        [self.imgProfile1 addGestureRecognizer:tap];
        
    }
    if (gUser.main_car.length==0) {
        CGRect rect = self.imgProfile1.frame;
        rect.size.height = rect.size.height/2;

        [_btnAddMainCar setTitle:[Util localized:@"lbl_add_main_car"] forState:UIControlStateNormal];
        [_btnAddMainCar addTarget:self action:@selector(onAddCar:) forControlEvents:UIControlEventTouchUpInside];
        _btnAddMainCar.titleLabel.font = self.lblEmail.font;
        _btnAddMainCar.layer.borderColor = [UIColor whiteColor].CGColor;
        _btnAddMainCar.layer.borderWidth = 1.0;
        _btnAddMainCar.hidden = NO;
        
    }
    else{
        _btnAddMainCar.hidden = YES;
        [self.imgProfile1 setImageWithURL:[NSURL URLWithString:gUser.main_car]];
        [self.imgProfile1 setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddCar:)];
        [self.imgProfile1 addGestureRecognizer:tap];
        
    }
    if (gUser.sub_car.length==0) {
        CGRect rect = self.imgProfile2.frame;
        rect.size.height = rect.size.height/2;

        [_btnAddSubCar setTitle:[Util localized:@"lbl_add_sub_car"] forState:UIControlStateNormal];
        [_btnAddSubCar addTarget:self action:@selector(onAddCar2:) forControlEvents:UIControlEventTouchUpInside];
        _btnAddSubCar.titleLabel.font = self.lblEmail.font;
        _btnAddSubCar.layer.borderColor = [UIColor whiteColor].CGColor;
        _btnAddSubCar.layer.borderWidth = 1.0;
        _btnAddSubCar.hidden = NO;
    }
    else{
        [self.imgProfile2 setImageWithURL:[NSURL URLWithString:gUser.sub_car]];
        [self.imgProfile2 setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddCar2:)];
        [self.imgProfile2 addGestureRecognizer:tap];
        _btnAddSubCar.hidden = YES;
    }
    
    
}
-(void)onBack{
  

 // [self.navigationController popViewControllerAnimated:YES];
}
-(void)onSave{
    
    [self NormalLogin];
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}




-(IBAction)onAddCar2:(id)sender{
    selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                             delegate:self
                                    cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                    otherButtonTitles: [Util localized:@"lbl_take_photo"],
                   [Util localized:@"lbl_select_from_galery"], nil];
    isImgProfile1Selecting = NO;
    [selectPhoto showInView:self.view];
}

-(IBAction)onAddCar:(id)sender{
    selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                             delegate:self
                                    cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                    otherButtonTitles: [Util localized:@"lbl_take_photo"],
                   [Util localized:@"lbl_select_from_galery"], nil];
    isImgProfile1Selecting = YES;
    [selectPhoto showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex<2) {
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self showImagePickerWithSelection:buttonIndex];
        }];
    }
    
}
-(void)showImagePickerWithSelection:(NSInteger)selection{
    pickerCar = [[UIImagePickerController alloc]init];
    pickerCar.delegate = self;
    pickerCar.allowsEditing = YES;
    switch (selection) {
        case 1:
            pickerCar.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            break;
        case 0:
            pickerCar.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            break;
        default:
            break;
    }
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:pickerCar animated:YES completion:nil];
    });
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    if (isDocumentSelecting) {
        if (TLCLicense == true)
        {
            isDocumentSelecting = NO;
            [_TLc_image setImage:chosenImage];

            documentPath = chosenImage;
            document_img = chosenImage;
             gUser.document_img = chosenImage;
            NSLog (@"%@",gUser.document);
            document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
             gUser.document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
            return;
        }
        
        if (drivingLicense == true)
        {
            isDocumentSelecting = NO;
            [_driver_licenseimage setImage:chosenImage];
            Licence_Document =  [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
            Licence_img = chosenImage;
            License_documentPath = chosenImage;
            gUser.License_document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
            gUser.License_img = chosenImage;
            return;
        }
        if (insuranceselecting == true)
        {
    isDocumentSelecting = NO;
            [_insurance_cert setImage:chosenImage];
            License_documentPath = chosenImage;
            insurance_Document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
            insurance_img = chosenImage;
            gUser.Insurance_document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
            gUser.Insurance_img = chosenImage;
            return;
        }
        
        if (refistrationselecting == true)
        {
           isDocumentSelecting = NO;
            
            [_refistration_image setImage:chosenImage];
            License_documentPath = chosenImage;
            Refistration_Document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];

            Refistration_img = chosenImage;
            gUser.Refistration_document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
            gUser.Refistration_img = chosenImage;
            return;
        }
        
        if (Permitselecting == true)
        {
            isDocumentSelecting = NO;
            [_permit_ setImage:chosenImage];
            Licence_Document =  [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
            Licence_img = chosenImage;
            License_documentPath = chosenImage;
            gUser.Permit_document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
            gUser.Permit_img = chosenImage;
            return;
        }
        
        
            }
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
    if (isImgProfile1Selecting) {
        mainImage = chosenImage;


        [self.imgProfile1 setImage:mainImage];
        _btnAddMainCar.hidden = YES;
        maincar_Document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
        maincar_img = chosenImage;
        gUser.main_car = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
        gUser.main_car_image = chosenImage;
    }
    else{
        subImage = chosenImage;
        [self.imgProfile2 setImage:subImage];
        _btnAddSubCar.hidden = YES;
        subcar_Document = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
        subcar_img = chosenImage;
        gUser.sub_car = [NSString stringWithFormat:@"%@",(NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL]];
        gUser.sub_car_image = chosenImage;
    }
    
    
}
    
- (IBAction)TLCLicense:(id)sender
    
    {
        drivingLicense = false;
        insuranceselecting = false;
         refistrationselecting = false;
        Permitselecting = false;
        TLCLicense = true;
        isDocumentSelecting = YES;
        
        selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                                 delegate:self
                                        cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                        otherButtonTitles: [Util localized:@"lbl_take_photo"],
                       [Util localized:@"lbl_select_from_galery"], nil];
       
        [selectPhoto showInView:self.view];
        
        
        
        
        
        
       // [self showImagePickerWithSelection:1];
    }
 
    
- (IBAction)DriverLicense:(id)sender
    {
        drivingLicense = true;
        isDocumentSelecting = YES;
        insuranceselecting = false;
        refistrationselecting = false;
        Permitselecting = false;
        TLCLicense = false;
        selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                                 delegate:self
                                        cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                        otherButtonTitles: [Util localized:@"lbl_take_photo"],
                       [Util localized:@"lbl_select_from_galery"], nil];
        
        [selectPhoto showInView:self.view];
        
}

    
    
- (IBAction)insurance_Action:(id)sender
    {
        
      
        drivingLicense = false;
        isDocumentSelecting = YES;
        
        insuranceselecting = true;
        refistrationselecting = false;
        Permitselecting = false;
        TLCLicense = false;
        
        selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                                 delegate:self
                                        cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                        otherButtonTitles: [Util localized:@"lbl_take_photo"],
                       [Util localized:@"lbl_select_from_galery"], nil];
        
        [selectPhoto showInView:self.view];
        
        
}
    
    
- (IBAction)refistration_Action:(id)sender
        {
            drivingLicense = false;
            isDocumentSelecting = YES;
            
            insuranceselecting = false;
            refistrationselecting = true;
            Permitselecting = false;
            TLCLicense = false;
            selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                                     delegate:self
                                            cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                            otherButtonTitles: [Util localized:@"lbl_take_photo"],
                           [Util localized:@"lbl_select_from_galery"], nil];
            
            [selectPhoto showInView:self.view];
            
            
            
            
            
            
}
    
    
- (IBAction)Permit_Action:(id)sender
    {
        drivingLicense = false;
        isDocumentSelecting = YES;
        
        insuranceselecting = false;
        refistrationselecting = false;
        Permitselecting = true;
        TLCLicense = false;
        
        selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                                 delegate:self
                                        cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                        otherButtonTitles: [Util localized:@"lbl_take_photo"],
                       [Util localized:@"lbl_select_from_galery"], nil];
        
        [selectPhoto showInView:self.view];
        
}
    
    
    
    
- (IBAction)openDocument:(id)sender {
    isDocumentSelecting = YES;
    
   // [self showImagePickerWithSelection:1];
    
    selectPhoto = [[UIActionSheet alloc]initWithTitle:[Util localized:@"app_name"]
                                             delegate:self
                                    cancelButtonTitle:[Util localized:@"title_cancel"] destructiveButtonTitle:nil
                                    otherButtonTitles: [Util localized:@"lbl_take_photo"],
                   [Util localized:@"lbl_select_from_galery"], nil];
    
    [selectPhoto showInView:self.view];
    
}

#pragma mark- Uipickerview Datasource (state, city)
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView == carBrandPicker){
        return letterValues.allKeys.count;
    }
    else if (pickerView == carModalPicker){
        NSDictionary *dic = [letterValues valueForKey:self.txtBrandOfCar.text];
        return dic.count;
    }
    else if(pickerView == carTypePicker){
        return 1;
    }else if (pickerView == carAvailablePicker){
        return carAvailableArray.count;
    }
     return 0;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if(pickerView == carBrandPicker){
        
        return letterValues.allKeys[row];
    }
    else if (pickerView == carModalPicker){
        
        NSDictionary *dic = [letterValues valueForKey:self.txtBrandOfCar.text];
        return dic.allKeys[row];
    }
    else if(pickerView == carTypePicker){
        NSString *v = [letterValues valueForKeyPath:[NSString stringWithFormat:@"%@.%@",self.txtBrandOfCar.text,self.txtModelOfCar.text]];
        return v;
    }else if (pickerView == carAvailablePicker){
        return carAvailableArray[row];
    }
    return @"empty";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(pickerView == carBrandPicker){
        
        if(![self.txtBrandOfCar.text isEqualToString: letterValues.allKeys[row]]){
            self.txtBrandOfCar.text = letterValues.allKeys[row];
            self.txtModelOfCar.text = nil;
            self.tfCarType.text = nil;
            txtCarAvailable.text = nil;

        }
        
    }
    else if(pickerView == carModalPicker){
        NSDictionary *dic = [letterValues valueForKey:self.txtBrandOfCar.text];
        
        if( ![self.txtModelOfCar.text isEqualToString:dic.allKeys[row]] ){
            self.txtModelOfCar.text = dic.allKeys[row];
            self.tfCarType.text = nil;
            txtCarAvailable.text = nil;
        }
        
    }
    else if(pickerView == carTypePicker){
        
        self.tfCarType.text = [letterValues valueForKeyPath:[NSString stringWithFormat:@"%@.%@",self.txtBrandOfCar.text,self.txtModelOfCar.text]];
        txtCarAvailable.text = nil;

    }else if (pickerView == carAvailablePicker){
        
        txtCarAvailable.text = carAvailableArray[row];
        
    }
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    
//    
//    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    int length = [currentString length];
//    if (length > 4) {
//        return NO;
//    
//    }
//        else
//        {
//    return YES;
//    }
//    
//    
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        int length = [currentString length];
        if (length > 4) {
            return NO;
    
        }
            else
            {
        return NO;
        }
    
    
}// return NO to not change text
- (IBAction)Back_Action:(id)sender
{
    ViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:loginVC];
    [self.revealViewController setFrontViewController:naviRoot];
  //  [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)NormalLogin
{
    if (_txtEmail.text.length == 0) {
        [self.view makeToast:@"You must enter Email to continue!"];
        return;
    }
    if (![Validator validateEmail:_txtEmail.text]) {
        [self.view makeToast:@"Please insert your correct email!"];
        return;
    }
    if (_tfPassword.text.length == 0) {
        [self.view makeToast:@"You must enter Password to continue!"];
        return;
    }
    
    if (self.tfconfirmpasswrd.text.length == 0) {
        [self.view makeToast:@"confirm Password does not match with Pasword!"];
        return;
    }
    
    if ([_tfPassword isEqual: self.tfconfirmpasswrd])
    {
        [self.view makeToast:@"confirm Password does not match with Pasword"];
        return;
    }
    
    if (_txtName.text.length == 0) {
        [self.view makeToast:@"You must enter Name to continue!"];
        return;
    }
    if (_tfPhone.text.length == 0) {
        [self.view makeToast:@"You must enter  Phone number to continue!"];
        return;
    }
    if (_tfPhone.text.length < 10) {
        [self.view makeToast:@"You must enter correct Phone number!"];
        return;
    }
    
    //For Register as Driver
    
    ///////////
    //    if (_tfCountry.text.length == 0) {
    //        [self.view makeToast:@"You must select Country to continue!"];
    //        return;
    //    }
    //    if (_tfCity.text.length == 0) {
    //        [self.view makeToast:@"You must enter your City's name to continue!"];
    //        return;
    //    }
    //    if (_tfAddress.text.length == 0) {
    //        [self.view makeToast:@"You must enter Address to continue!"];
    //        return;
    //    }
    //    if (_tfPostCode.text.length == 0) {
    //        [self.view makeToast:@"You must enter Postcode to continue!"];
    //        return;
    //    }
    //    if (_txtAccount.text.length == 0) {
    //        [self.view makeToast:@"You mus enter Pauout Paypal to continue!"];
    //        return;
    //    }
    
    User *userTempObj = [[User alloc] init];
    userTempObj.name = _txtName.text;
    userTempObj.email = _txtEmail.text;
    userTempObj.password = _tfPassword.text;
    userTempObj.phone = [Validator getSafeString:_tfPhone.text];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    UIImage *img = [UIImage imageNamed:@"no_image.png"];
    [ModelManager registerNormalWithUser:userTempObj avatar:img withSuccess:^(NSString *successStr) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self loginfirstfortokenstring];
        
    } failure:^(NSString *err) {
        if ([err isEqualToString:@"Email is exist!"])
        {
            [self loginfirstfortokenstring];
           // [self getUserInfo];
        }
        else
        {
        
        [Util showMessage:err withTitle:@"Error"];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
    }];

}

-(void)loginfirstfortokenstring


{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        
        User *user = [[User alloc] init];
        user.email = _txtEmail.text;
        user.password = _tfPassword.text;
        user.token = deviceTokenString;
        user.lattitude = [NSString stringWithFormat:@"%f",[LocationManager sharedManager].myLocation.latitude];
        user.logitude = [NSString stringWithFormat:@"%f",[LocationManager sharedManager].myLocation.longitude];
        [ModelManager loginNormalWithUserObj:user withSuccess:^(User *userObj) {
            [self getUserInfo];

            
        } failure:^(NSString *err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"%@",err);
                if ([err isEqualToString:@"Email or password not correct!"]) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                    [Util showMessage:@"Email already exit" withTitle:[Util localized:@"app_name"]];
                    return ;
                }
                
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [Util showMessage:err withTitle:[Util localized:@"app_name"]];
                    
            });
        }];
    });
}


    - (void)getUserInfo{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager getUserProfileWithSuccess:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                NSLog(@"%@",gUser);
                
                documentPath = document_img;
                gUser.document = document;
                gUser.document_img = document_img;
                gUser.License_document = Licence_Document;
                gUser.License_img = Licence_img;
                gUser.Insurance_document = insurance_Document;
                gUser.Insurance_img = insurance_img;
                gUser.Refistration_document = Refistration_Document;
                gUser.Refistration_img = Refistration_img;
                gUser.Permit_img = Permit_img;
                gUser.Permit_document = Permit_Document;
                gUser.sub_car = subcar_Document;
                gUser.sub_car_image = subcar_img;
                gUser.main_car = maincar_Document;
                gUser.main_car_image = maincar_img;
                
                
                
                
                [self registerasdriver];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                            });
            
        } failure:^(NSString *err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            });
        }];
    }





-(void)registerasdriver
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy"];
    NSDate *dtOne = [dateFormatter dateFromString:_txtYear.text];
    
    NSString *two  = [NSDate date];
    
    NSDate *dttwo = [dateFormatter dateFromString:@"2017"];
    
    NSComparisonResult result;
    result = [dtOne compare:dttwo];
    
    
    for (UITextField *text in self.mainSubview.subviews) {
        if ([text isKindOfClass:[UITextField class]]&&text.text.length==0) {
            [self.view makeToast:[Util localized:@"msg_missing_data"]];
            return;
        }
    }
    if (!documentPath) {
        [self.view makeToast:[Util localized:@"msg_missing_data"]];
        return;
    }
    if (gUser.main_car.length==0||gUser.sub_car.length==0){
        [self.view makeToast:[Util localized:@"msg_missing_image"]];
        return;
    }
    
    if (txtCarAvailable.text.length == 0){
        [self.view makeToast:[Util localized:@"msg_missing_data"]];
        return;
    }
    
    gUser.car = [[DriverCar alloc]init];
    gUser.name = self.txtName.text;
    gUser.car.carPlate =self.txtCarPlate.text;
    gUser.car.brand = self.txtBrandOfCar.text;
    gUser.car.model =self.txtModelOfCar.text ;
//    gUser.car.carType = carTypeDic[_tfCarType.text];
    gUser.car.carType = carAvailableDic[txtCarAvailable.text];

    NSLog(@"Driver Type is %@",gUser.car.carType);
    gUser.manufacture_year = self.txtYear.text;
    gUser.email = self.txtEmail.text ;
    gUser.car_status = self.txtStatus.text;
    gUser.cart_value = self.txtAccount.text;
    gUser.date_of_birth = self.txtDoB.text;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager registerAsDriver:gUser documentPath:documentPath withSuccess:^(NSDictionary *dic) {
        
        [ModelManager getUserProfileWithSuccess:^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            NSString *status = [dic objectForKey:@"status"];
            if ([status.lowercaseString isEqualToString:@"success"]) {
                if ([gUser.driver.isActive isEqualToString:@"0"]) {
                    [Util showMessage:[Util localized:@"msg_register_driver_success"] withTitle:[Util localized:@"app_name"]];

                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [ModelManager logoutWithSuccess:^{
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        
                        ViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                        UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:loginVC];
                        [self.revealViewController setFrontViewController:naviRoot];
                        [Util setBool:NO forKey:IS_LOGGED_IN_KEY];
                        
                    } andFailure:^(NSString *err) {
                                            }];
                }
                else if ([gUser.driver.isActive isEqualToString:@"1"]){
                    
                    [Util showMessage:[Util localized:@"msg_register_driver_success"] withTitle:[Util localized:@"app_name"]];

                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [ModelManager logoutWithSuccess:^{
                        
                        
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        ViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                        UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:loginVC];
                        [self.revealViewController setFrontViewController:naviRoot];
                        [Util setBool:NO forKey:IS_LOGGED_IN_KEY];
                        
                        
                        
                    } andFailure:^(NSString *err) {
                    }];

                }
            
            }
            else{
                

                [self.view makeToast:[dic objectForKey:@"message"]];


            }
            
        } failure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            NSString *status = [dic objectForKey:@"status"];
            if ([status.lowercaseString isEqualToString:@"success"]) {

                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [ModelManager logoutWithSuccess:^{
                    
                    
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    ViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                    UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:loginVC];
                    [self.revealViewController setFrontViewController:naviRoot];
                    [Util setBool:NO forKey:IS_LOGGED_IN_KEY];
                    [self.view makeToast:[Util localized:@"msg_register_driver_success"]];

                    
                    
                } andFailure:^(NSString *err) {
                }];
                

            }
        }];
        
        
    } failure:^(NSString *arr) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager logoutWithSuccess:^{
            
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            
            //Show Error Message here 
//            ViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//            UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:loginVC];
//            [self.revealViewController setFrontViewController:naviRoot];
//            [Util setBool:NO forKey:IS_LOGGED_IN_KEY];
            
            
            
        } andFailure:^(NSString *err) {
        }];
        

        [self.view makeToast:arr];
    }];

}


@end
