//
//  ViewController.h
//  LinkRider
//
//  Created by hieu nguyen on 6/22/15.
//  Copyright (c) 2015 Fruity Solution. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Google/SignIn.h>
#import "UIKeyboardViewController.h"

@interface ViewController : UIViewController<UIKeyboardViewControllerDelegate, CLLocationManagerDelegate,GIDSignInUIDelegate>{
UIKeyboardViewController *keyboard;

}
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnFBLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnGoogleLogin;

@property (weak, nonatomic) IBOutlet UILabel *icFB;
@property (weak, nonatomic) IBOutlet UILabel *icGP;

- (IBAction)onExplain:(id)sender;

- (IBAction)onFacebook:(id)sender;
- (IBAction)onGooglePlus:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPass;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
//@property (weak, nonatomic) IBOutlet UITextField *tfUserName;
//@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (strong, nonatomic) IBOutlet UITextField *tfUserName;
@property (strong, nonatomic) IBOutlet UITextField *tfPassword;
@property (strong, nonatomic) IBOutlet UIButton *Tick_rider;
@property (strong, nonatomic) IBOutlet UIView *back_view;
@property (strong, nonatomic) IBOutlet UIButton *Tick_chauffer;

@property (strong, nonatomic) IBOutlet UIView *chauffer_backview;


@end

