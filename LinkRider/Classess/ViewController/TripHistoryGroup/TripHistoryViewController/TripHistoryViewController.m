//
//  TripHistoryViewController.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "TripHistoryViewController.h"
#import "MBProgressHUD.h"
#import "ModelManager.h"
#import "UIView+Toast.h"
#import "TripHistoryDetailViewController.h"
#import "FutureTableViewCell.h"
@interface TripHistoryViewController ()

@end

@implementation TripHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    curPage = 1;
    tripArr = [[NSMutableArray alloc]init];
    [_tblView setDragDelegate:self refreshDatePermanentKey:@"FriendList"];
    _tblView.showLoadMoreView = YES;
    _tblView.frame = self.view.bounds;
    self.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self getData];
    [self customSetup];
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    // [self.navigationController.navigationBar setBarTintColor:[Util colorWithHexString:@"#333333" alpha:1]];
    //[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    //[self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Raleway-Medium" size:20], NSFontAttributeName, nil]];
    // self.navigationController.navigationBar.translucent = YES;
    
    if ( revealViewController )
    {
        
        //        [self.revealButtonItem setTarget: self.revealViewController];
        //        [self.revealButtonItem setAction: @selector(revealToggle:)];
        //[self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
        
        //    SWRevealViewController *revealViewController = self.revealViewController;
        if ( revealViewController )
        {
            [reveal_toggle addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        }
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
    }
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{

    
    [self.navigationController setNavigationBarHidden:YES];
    //[self setText];
    
}

- (void)dragTableDidTriggerRefresh:(UITableView *)tableView{
    curPage = 1;
    tripArr = [[NSMutableArray alloc]init];
    if (futuretrips == false)
    {
        [self getData];
 
    }
    else
    {
        tripArr = [[NSMutableArray alloc]init];

        [self getData1];
  
    }
//    self.tblView.frame = CGRectMake(0, 55, self.tblView.frame.size.width, self.view.frame.size.height-55);
}
- (void)dragTableRefreshCanceled:(UITableView *)tableView{
    
}
- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView{
    curPage++;
    if (futuretrips == false)
    {
        [self getData];
        
    }
    else
    {
        tripArr = [[NSMutableArray alloc]init];

        [self getData1];
        
    }
//    self.tblView.frame = CGRectMake(0, 55, self.tblView.frame.size.width, self.view.frame.size.height-55);
    
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView{
    
}

-(void)setText{
    [self.lblHeaderTitle setTitle:[Util localized:@"header_history"]];
    
    SWRevealViewController *revealViewController = self.revealViewController;
   // [self.navigationController.navigationBar setBarTintColor:[Util colorWithHexString:@"#333333" alpha:1]];
    //[self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
   // [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Raleway-Medium" size:20], NSFontAttributeName, nil]];
    [self.revealBtn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontAwesomeFontOfSize:24], NSFontAttributeName,
                                          [UIColor whiteColor], NSForegroundColorAttributeName,
                                          nil]
                                forState:UIControlStateNormal];
    
    [self.revealBtn setTitle:@"\uf0c9"];
    if ( revealViewController )
    {
        [self.revealBtn setTarget: self.revealViewController];
        [self.revealBtn setAction: @selector(revealToggle:)];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }

}

-(void)getData1
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager showMyfutureTripsWithPage:curPage Success:^(NSMutableArray *arr) {
            tripArr = [NSMutableArray arrayWithArray:[tripArr arrayByAddingObjectsFromArray:arr]];
            [self.tblView reloadData];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            if (arr.count==0) {
                [self.view makeToast:[Util localized:@"msg_no_trip_history"]];
            }
            [self.tblView stopLoadMore];
            [self.tblView stopRefresh];
        } andFailure:^(NSString *err) {
            [self.tblView stopLoadMore];
            [self.tblView stopRefresh];
            
        }];
    });
//        }:curPage Success:^(NSMutableArray *arr) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//               
//                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            
//              
//            });
//            
//            
//        } andFailure:^(NSString *err) {
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//           
//        }];
   // });
    
}
-(void)getData{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager showMyTripsWithPage:curPage Success:^(NSMutableArray *arr) {
            dispatch_async(dispatch_get_main_queue(), ^{
                tripArr = [NSMutableArray arrayWithArray:[tripArr arrayByAddingObjectsFromArray:arr]];
                [self.tblView reloadData];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                if (arr.count==0) {
                    [self.view makeToast:[Util localized:@"msg_no_trip_history"]];
                }
                [self.tblView stopLoadMore];
                [self.tblView stopRefresh];
            });
          
            
        } andFailure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.tblView stopLoadMore];
            [self.tblView stopRefresh];
        }];
    });
    
}


-(void)getdata2
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager showMydriverTripsWithPage:curPage Success:^(NSMutableArray *arr) {
            dispatch_async(dispatch_get_main_queue(), ^{
                tripArr = [NSMutableArray arrayWithArray:[tripArr arrayByAddingObjectsFromArray:arr]];
                [self.tblView reloadData];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                if (arr.count==0) {
                    [self.view makeToast:[Util localized:@"msg_no_trip_history"]];
                }
                [self.tblView stopLoadMore];
                [self.tblView stopRefresh];
            });
            
            
        } andFailure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.tblView stopLoadMore];
            [self.tblView stopRefresh];
        }];
    });
    
}
#pragma mark tableview datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (futuretrips == false)
    {
        return 288;
    }else{
        return 217;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tripArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (futuretrips == false)
    {
    TripHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TripHistoryCell"];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"TripHistoryCell" owner:self options:nil];
        cell = (TripHistoryCell *)[nib objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
    if (indexPath.row<tripArr.count) {
        Trips *trip = [tripArr objectAtIndex:indexPath.row];
        cell.lblTransactionIdValue.text = trip.tripId;
        cell.lblLinkValue.text = trip.link;
        NSLog(@"%@",trip.driver.driver_unique_id);
        cell.Driveridd.text = trip.driver.driver_unique_id;
        cell.lblLinkValue.text = [DriverCar typeNameByTypeId:trip.link];
        cell.lblEndTimeValue.text = trip.endTime;
        cell.LblDepartureValue.text = trip.startLocation.address;
        cell.lblDestinationValue.text = trip.endLocation.address;
        int timeInterval = [trip.totalTime intValue];
        NSString *time = [NSString stringWithFormat:@"%02d:%02d",timeInterval/60,timeInterval%60];
        if (timeInterval>60) {
            time = [NSString stringWithFormat:@"%02d %@ %02d %@",timeInterval/60,[Util localized:@"lbl_hour"],timeInterval%60,[Util localized:@"lbl_minutes"]];
        }
        else{
            time = [NSString stringWithFormat:@"%02d %@", timeInterval,[Util localized:@"lbl_minutes"]];
        }
        
        cell.lblDurationValue.text = time;
        //NSDate* created = [Util dateFromString:trip.dateCreated format:@"yyyy-MM-dd hh:mm:ss a"];
        NSDate* created = [Util getDateFromDateString:trip.dateCreated :@"yyyy-MM-dd HH:mm:ss"];
        
        cell.lblDate.text = [Util stringFromDate:created format:@"MMMM dd, yyyy"];
        cell.lblTime.text = [Util stringFromDate:created format:@"hh:mm a"];
        
        
        cell.lblDistanceValue.text = [NSString stringWithFormat:@"%@ miles",trip.distance];
        cell.lblPriceValue.text = trip.actualFare;
        if ([trip.driverId isEqualToString:gUser.driver_id]) {
            cell.bgImg.image = [UIImage imageNamed:@"bg_history_add"];
            cell.lblPriceValue.text = [NSString stringWithFormat:@"+%@",trip.actualEarn];
        }
        else{
            cell.bgImg.image = [UIImage imageNamed:@"bg_history_minus"];
            cell.lblPriceValue.text = [NSString stringWithFormat:@"-%@",trip.actualFare];
        }
        cell.bgImg.layer.cornerRadius = 3;
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;
    }
    else
    {
        
        if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"0"] || [gUser.login_type  isEqual: @"Rider"])
        {
        FutureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FutureTableViewCell"];
        if (!cell) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"FutureTableViewCell" owner:self options:nil];
            cell = (FutureTableViewCell *)[nib objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        if (indexPath.row<tripArr.count)
        {
            Trips *trip = [tripArr objectAtIndex:indexPath.row];
            cell.passenger_id.text = trip.passengerId;
            NSLog(@"%@",cell.passenger_id.text);
            cell.lblLinkValue.text = trip.link;
            cell.lblLinkValue.text = [DriverCar typeNameByTypeId:trip.link];
            //cell.lble.text = trip.endTime;
            cell.LblDepartureValue.text = trip.startLocation.address;
            cell.lblDestinationValue.text = trip.endLocation.address;
            //prabhjot
            cell.constraintHeightOfDriverId.constant = 29;
            if (trip.driver_unique_id != nil) {
                cell.lblDriverId.text = trip.driver_unique_id;

            }else{
                cell.lblDriverId.text = @"";
            }
            
            
            NSLog(@"%@",cell.starting_time.text);
            
            NSDate * created = [NSDate dateWithTimeIntervalSince1970:[trip.RequestTime doubleValue]];
            
            //NSDate* created = [Util dateFromString:trip.RequestTime format:@"yyyy-MM-dd HH:mm:ss"];
            cell.lblDate.text = [Util stringFromDate:created format:@"MMMM dd, yyyy"];
            cell.lblTime.text = [Util stringFromDate:created format:@"hh:mm a"];
            
            NSDate * created1 = [NSDate dateWithTimeIntervalSince1970:[trip.startTime doubleValue]];
            // NSDate* created1 = [Util dateFromString:trip.startTime format:@"yyyy-MM-dd HH:mm:ss"];
            
            cell.starting_time.text = [Util stringFromDate:created1 format:@"yyyy-MM-dd hh:mm a"];
            
           //prabhjot comment
            
//            NSDateFormatter* formatter1 = [[NSDateFormatter alloc]init];
//            [formatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//            [formatter1 setTimeZone:[NSTimeZone systemTimeZone]];
//            
            
            
            
//            NSDate* date1 = [formatter1 dateFromString:trip.startTime];
//            NSLog(@"%@",date1);
//            
//            cell.starting_time.text = [Util stringFromDate:date1 format:@"yyyy-MM-dd hh:mm a"];
//            NSLog(@"%@",cell.starting_time.text);
//            //cell.lblTime.text = [Util stringFromDate:date1 format:@"HH:mm"];
//            
//            
//            
//            //NSDate* created = [Util dateFromString:trip.RequestTime format:@"yyyy-MM-dd hh:mm:ss a"];
//            NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
//            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//            [formatter setTimeZone:[NSTimeZone systemTimeZone]];
//
//            NSDate* date = [formatter dateFromString:trip.RequestTime];
//            cell.lblDate.text = [Util stringFromDate:date format:@"MMMM dd, yyyy"];
//            cell.lblTime.text = [Util stringFromDate:date format:@"hh:mm a"];
//
            
        //end
            
            
//            NSDate* created = [Util dateFromString:trip.RequestTime format:@"yyyy-MM-dd HH:mm:ss"];
//           cell.lblDate.text = [Util stringFromDate:created format:@"MMMM dd, yyyy"];
//            cell.lblTime.text = [Util stringFromDate:created format:@"HH:mm"];
//            
//            
//            
//            int timeInterval1 = [trip.startTime intValue];
//            NSString *time1 = [NSString stringWithFormat:@"%02d:%02d",timeInterval/60,timeInterval%60];
//            if (timeInterval1>60) {
//                time1 = [NSString stringWithFormat:@"%02d %@ %02d %@",timeInterval/60,[Util localized:@"lbl_hour"],timeInterval%60,[Util localized:@"lbl_minutes"]];
//            }
//            else{
//                time1 = [NSString stringWithFormat:@"%02d %@", timeInterval,[Util localized:@"lbl_minutes"]];
//            }
//            NSDate* created1 = [Util dateFromString:trip.dateCreated format:@"yyyy-MM-dd HH:mm:ss"];
//            
            
            
//            NSDateFormatter *serverFormatter = [[NSDateFormatter alloc] init];
//            [serverFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//            NSDate *theDate = [serverFormatter dateFromString:trip.RequestTime];
//            NSDateFormatter *userFormatter = [[NSDateFormatter alloc] init];
//            [userFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//            [userFormatter setTimeZone:[NSTimeZone localTimeZone]];
//            NSString *dateConverted = [userFormatter stringFromDate:theDate];
//            cell.lblDate.text = dateConverted;
//            
//           NSDate* created = [Util dateFromString:trip.RequestTime format:@"yyyy-MM-dd HH:mm:ss"];
//           cell.lblDate.text = [Util stringFromDate:created format:@"MMMM dd, yyyy"];
//           cell.lblTime.text = [Util stringFromDate:created format:@"HH:mm"];
//            
//            
//            
//            int timeInterval1 = [trip.startTime intValue];
//            NSString *time1 = [NSString stringWithFormat:@"%02d:%02d",timeInterval/60,timeInterval%60];
//            if (timeInterval1>60) {
//                time1 = [NSString stringWithFormat:@"%02d %@ %02d %@",timeInterval/60,[Util localized:@"lbl_hour"],timeInterval%60,[Util localized:@"lbl_minutes"]];
//            }
//            else{
//                time1 = [NSString stringWithFormat:@"%02d %@", timeInterval,[Util localized:@"lbl_minutes"]];
//            }
        //NSDate* created1 = [Util dateFromString:trip.dateCreated format:@"yyyy-MM-dd HH:mm:ss"];
           // cell.starting_time.text = [ Util stringFromDate:created1 format:@"yyyy-MM-dd HH:mm:ss"];
            
            
            
            //cell.lblDistanceValue.text = [NSString stringWithFormat:@"%@ km",trip.distance];
            //cell.lblriceValue.text = trip.actualFare;
//            if ([trip.driverId isEqualToString:gUser.driver_id]) {
//                cell.bgimg.image = [UIImage imageNamed:@"bg_history_add"];
//                //cell.lblriceValue.text = [NSString stringWithFormat:@"+%@",trip.actualEarn];
//            }
//            else
//            {
            
                cell.bgimg.image = [UIImage imageNamed:@"bg_history_minus"];
//                //cell.lblriceValue.text = [NSString stringWithFormat:@"-%@",trip.actualFare];
      //      }
          cell.bgimg.layer.cornerRadius = 3;
        }
        [cell setBackgroundColor:[UIColor clearColor]];
        return cell;
            
        }
        else
        {
            FutureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FutureTableViewCell"];
            if (!cell) {
                NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"FutureTableViewCell" owner:self options:nil];
                cell = (FutureTableViewCell *)[nib objectAtIndex:0];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
            if (indexPath.row<tripArr.count) {
                cell.constraintHeightOfDriverId.constant = 0;

                Trips *trip = [tripArr objectAtIndex:indexPath.row];
                cell.lblTransactionIdValue.text = trip.tripId;
                cell.lblLinkValue.text = trip.link;
                cell.passenger_id.text = trip.passengerId;

                NSLog(@"%@",trip.driver.driver_unique_id);
                cell.lblLinkValue.text = [DriverCar typeNameByTypeId:trip.link];
                cell.LblDepartureValue.text = trip.startLocation.address;
                cell.lblDestinationValue.text = trip.endLocation.address;
                int timeInterval = [trip.startlog intValue];
                NSString *time = [NSString stringWithFormat:@"%02d:%02d",timeInterval/60,timeInterval%60];
                if (timeInterval>60) {
                    time = [NSString stringWithFormat:@"%02d %@ %02d %@",timeInterval/60,[Util localized:@"lbl_hour"],timeInterval%60,[Util localized:@"lbl_minutes"]];
                }
                else{
                    time = [NSString stringWithFormat:@"%02d %@", timeInterval,[Util localized:@"lbl_minutes"]];
                }
                cell.lblDurationValue.text = time;
                NSDate * created = [NSDate dateWithTimeIntervalSince1970:[trip.RequestTime doubleValue]];

                //NSDate* created = [Util dateFromString:trip.RequestTime format:@"yyyy-MM-dd HH:mm:ss"];
                cell.lblDate.text = [Util stringFromDate:created format:@"MMMM dd, yyyy"];
                cell.lblTime.text = [Util stringFromDate:created format:@"hh:mm a"];
                
                NSDate * created1 = [NSDate dateWithTimeIntervalSince1970:[trip.startTime doubleValue]];
               // NSDate* created1 = [Util dateFromString:trip.startTime format:@"yyyy-MM-dd HH:mm:ss"];
                
                cell.starting_time.text = [Util stringFromDate:created1 format:@"yyyy-MM-dd hh:mm a"];
                cell.bgimg.image = [UIImage imageNamed:@"bg_history_minus"];
                cell.bgimg.layer.cornerRadius = 3;
            }
            [cell setBackgroundColor:[UIColor clearColor]];
            
            return cell;
        }
    }
}

#pragma mark :- Button Actions
- (IBAction)check_history:(id)sender
{
    tripArr = [[NSMutableArray alloc]init];

    if ([sender tag]== 0)
    {
        futuretrips = false;
        [self getData];
        
    }
    else
    {
        futuretrips = true;

        if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"0"] || [gUser.login_type  isEqual: @"Rider"])

        {
           [self getData1];
        }
        else
        {
            [self getdata2];
        }
        
    
    }
    
    }

- (IBAction)future_Acion:(id)sender
{
    
    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"0"] || [gUser.login_type  isEqual: @"Rider"])
        
    {
        [self getData1];
    }
    else
    {
        [self getdata2];
    }
    
    
  
}





@end
