//
//  RattingPassengerViewController.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "RattingPassengerViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "Trips.h"
#import "UIFont+FontAwesome.h"
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>

@interface RattingPassengerViewController ()

@end

@implementation RattingPassengerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    [self.btnCallAdmin setTitle:[NSString fontAwesomeIconStringForEnum:FAPhone] forState:UIControlStateNormal];
    _viewHelp.layer.borderColor = [UIColor whiteColor].CGColor;
    _viewHelp.layer.borderWidth = 1;
    
    [self getGeneralSetting];

    [self showTripDetail];

}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [self setText];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationItem setHidesBackButton:NO animated:YES];
    
}
-(void)getGeneralSetting{
    
}
-(void)setText{
    [self.lblHeaderTitle setTitle:[Util localized:@"header_your_trip"]];

    self.lblDistanceValue.text = [Util localized:@"lbl_trip_finished"];
    
    _lblRateValue.backgroundColor  = [UIColor clearColor];
    _lblRateValue.starImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _lblRateValue.starHighlightedImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [_lblRateValue setMaxRating:5.0];
    [_lblRateValue setDelegate:self];
    [_lblRateValue setHorizontalMargin:1.0];
    [_lblRateValue setEditable:YES];
    [_lblRateValue setDisplayMode:EDStarRatingDisplayHalf];
    [_lblRateValue setRating:5];
    [_lblRateValue setNeedsDisplay];
    
  
    
    [self.profileView setProfileWithAvatarLink:gTrip.passenger.thumb phoneNumber:gTrip.passenger.phone  starValue:gTrip.passenger_UP down_value:gTrip.passenger_down name:gTrip.passenger.name type:2];
    
    
    
    
    self.lblPointValue.text = gTrip.actualEarn;
    
    
    
    NSDictionary *fontDict = @{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:10],NSForegroundColorAttributeName:[Util colorWithHexString:@"#e8c03a"]};
    
    NSMutableAttributedString *att =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict];
    
    [att appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict]];
    
    [att appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict]];
    
    [att appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict]];
    
    [att appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict]];
    
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: STEP_TITLE_FONT forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[Util localized:@"finished_title"]] attributes: arialDict];
    
    [att appendAttributedString:aAttrString];
    
    
    self.lblTitle.attributedText =att;
    
    
    self.lblFromName.font = self.lblSeatNum.font;
    self.lblToName.font = self.lblSeatNum.font;
    self.lblFromName.textColor = [UIColor blackColor];
    self.lblToName.textColor = [UIColor blackColor];
    
    
    self.lblSeats.text = [Util localized:@"lbl_seats"];
    self.lblFromA.text = [Util localized:@"lbl_from_a"];
    self.lblToB.text = [Util localized:@"lbl_to_b"];

    self.lblSeatNum.text = [DriverCar typeNameByTypeId:gTrip.link];
    self.lblPointValue.text = [NSString stringWithFormat:@"%@%@", [Util localized:@"currentcy"], gTrip.actualEarn ];
    self.lblFromName.text = gTrip.startLocation.address;
    self.lblToName.text = gTrip.endLocation.address;
    self.lblDistanceValue.text = [Util localized:@"lbl_trip_finished"];
    
    self.lblPoint.text = [Util localized:@"lbl_fare"];
    self.lblRate.text = [Util localized:@"lbl_rate"];
    [self.btnRate setTitle:[Util localized:@"lbl_rate"].uppercaseString forState:UIControlStateNormal];

    
}
-(void)showTripDetail{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager showTripDetailWithTripId:gTrip.tripId withSuccess:^(Trips *trip) {
            gTrip = trip;
            [Util setObject:gTrip.tripId forKey:CURRENT_TRIP_ID_KEY];
            [self setText];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            notificationsDic = nil;
        } andFailure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    });
}

- (IBAction)onSend:(id)sender {
//    if (_lblRateValue.rating == 0) {
//        [self.view makeToast:@"Rate value has to exceed than 0"];
//        return;
//    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   [ModelManager ratePassengerWithRate:rateup andratedown:ratedown Success:^{
       
            gTrip = [[Trips alloc]init];
            [Util setObject:@"" forKey:DRIVER_CURRENT_TRIP_ID_KEY];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        } andFailure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
}

- (IBAction)onCallAdmin:(id)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager sendHelpRequestWithSuccess:^(NSString *strSuccessReturn) {
            
        } failure:^(NSString *err) {
            
        }];
    });
    [self ShareWithEmail];
   // [Util callPhoneNumber:[AppSettings share].adminPhoneNumber];

}
-(void)ShareWithEmail
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        [mailController setMailComposeDelegate:self];
        [mailController setSubject:@"Drivus Help"];
        [mailController setToRecipients:[NSArray arrayWithObjects:@"Help@Drivus.com", @"email2", nil]];
        NSString *temp =[NSString stringWithFormat:@"Please Wite Message here wat kind of help you need"];
        [mailController setMessageBody:temp isHTML:NO];
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please setup mail in your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
    
    
}
- (IBAction)Rate_Action:(id)sender
{
    if ([sender tag]== 0)
    {
        [_Rate_2_btn setSelected: false];
        ratedown = 0;
    if (![sender isSelected])
    {
        [sender setSelected:true];
        rateup = 1;
        [_Rate_1_btn setImage:[UIImage imageNamed:@"thm"] forState: UIControlStateSelected];
        [_Rate_2_btn setImage:[UIImage imageNamed:@"thm2-1"] forState: UIControlStateNormal];

    }
        else
        {
            [sender setSelected:false];
            rateup = 0;
            [_Rate_1_btn setImage:[UIImage imageNamed:@"Th_un"] forState: UIControlStateNormal];
            [_Rate_2_btn setImage:[UIImage imageNamed:@"thm2-1"] forState: UIControlStateNormal];
            


        }
        
    }
    else if ([sender tag]==1)
    {
        [self.Rate_1_btn setSelected:false];
         rateup = 0;
        if (![sender isSelected])
        {
            [sender setSelected:true];
            ratedown = 1;
            [_Rate_2_btn setImage:[UIImage imageNamed:@"thm-1"] forState: UIControlStateSelected];
            [_Rate_1_btn setImage:[UIImage imageNamed:@"Th_un"] forState: UIControlStateNormal];

        }
        else
        {
            [sender setSelected:false];
            ratedown = 0;
            [_Rate_2_btn setImage:[UIImage imageNamed:@"thm2-1"] forState: UIControlStateNormal];
            [_Rate_1_btn setImage:[UIImage imageNamed:@"Th_un"] forState: UIControlStateNormal];

        }
    }
    
    
}



@end
