//
//  RattingPassengerViewController.h
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
#import "ProfileView.h"
#import <MessageUI/MessageUI.h>

@interface RattingPassengerViewController : UIViewController<MFMessageComposeViewControllerDelegate,
EDStarRatingProtocol>{
    NSString *driver_earn;
    int rateup;
    int ratedown;
}

@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@property (weak, nonatomic) IBOutlet UIButton *btnRate;

@property (weak, nonatomic) IBOutlet ProfileView *profileView;


@property (weak, nonatomic) IBOutlet UILabel *lblSeats;
@property (weak, nonatomic) IBOutlet UILabel *lblFromA;
@property (weak, nonatomic) IBOutlet UILabel *lblToB;
@property (weak, nonatomic) IBOutlet UILabel *lblPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;



@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *lblSeatNum;

@property (weak, nonatomic) IBOutlet EDStarRating *lblRateValue;

@property (weak, nonatomic) IBOutlet UILabel *lblPointValue;

@property (weak, nonatomic) IBOutlet UILabel *lblDistanceValue;

@property (weak, nonatomic) IBOutlet UILabel *lblFromName;
@property (weak, nonatomic) IBOutlet UILabel *lblToName;

- (IBAction)onSend:(id)sender;
@property (nonatomic) float rating_value;

@property (weak, nonatomic) IBOutlet UIView *viewHelp;
@property (weak, nonatomic) IBOutlet UIButton *btnCallAdmin;

@property (strong, nonatomic) IBOutlet UIButton *Rate_1_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_2_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_3_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_4_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_5_btn;


@end
