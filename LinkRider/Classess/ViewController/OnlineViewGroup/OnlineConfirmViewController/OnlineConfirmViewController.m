//
//  OnlineConfirmViewController.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "OnlineConfirmViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "Trips.h"
#import "UIImageView+WebCache.h"
#import "UIFont+FontAwesome.h"
#import "MyPinAnnotationView.h"
#import "OnlineViewController.h"
//#import <GoogleMapsM4B/GoogleMaps.h>
#import "OpenInGoogleMapsController.h"


@interface OnlineConfirmViewController ()<GMSMapViewDelegate, LocationManagerDelegate>

@end

@implementation OnlineConfirmViewController{
    GMSMarker *fromAMarker, *toBMaker , *toBMaker1 , *toBMaker2, *toBMaker3, *toBMaker4;
    GMSPolyline *singleLine;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.btnCancelTrip setTitle:[NSString fontAwesomeIconStringForEnum:FALongArrowLeft] forState:UIControlStateNormal];

    [self setNeedsStatusBarAppearanceUpdate];
    [self configGoogleMap];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCancelTrip) name:PASSENGER_CANCEL_REQUEST object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCancelTrip) name:DRIVER_CANCEL_TRIP_KEY object:nil];

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)configGoogleMap
{
    _gMap.myLocationEnabled = true;
    _gMap.camera = [GMSCameraPosition cameraWithLatitude:gShareLocation.myLocation.latitude longitude:gShareLocation.myLocation.longitude zoom:16];
    _gMap.delegate = self;
}

-(void)onCancelTrip

{

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Drivus"
                                                    message:@"Trip Cancelled by Passenger"
                                                   delegate:self
                                          cancelButtonTitle:@"ok"
                                          otherButtonTitles:nil, nil];
    
    [alert show];
    
    //    OnlineViewController *myNewVC = (OnlineViewController *)[self.storyboard       instantiateViewControllerWithIdentifier:@"OnlineViewController"];
    //    [self.navigationController pushViewController:myNewVC animated:YES];
    ////    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    gShareLocation.delegate = self;
    
   // [self getTripDetail];
    [self setTextLocationNameLbl];
    [self setInitLayout];
}

- (void)setTextLocationNameLbl{
    
    NSString *prefix = [Util localized:@"lbl_to_a"];
    NSString *content = gTrip.startLocation.address.length? gTrip.startLocation.address : [Util localized:@"lbl_loading"];
    
    NSString *infoString = [NSString stringWithFormat:@"%@%@", prefix, content];
    
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:infoString];
    
  //  UIFont *font_regular=[UIFont fontWithName:@"Raleway-Medium" size:14.0f];
    UIFont *font_bold=[UIFont fontWithName:@"Raleway-Bold" size:14.0f];
    
    [attString addAttribute:NSFontAttributeName value:font_bold range:NSMakeRange(0, prefix.length)];
    [self.lblLocationName setAttributedText:attString];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    gShareLocation.delegate = nil;
    [super viewWillDisappear:animated];
}


- (void)setInitLayout{

    
    NSLog(@"%@",gTrip.passenger_UP);
    NSLog(@"%@",gTrip.passenger_down);
    
    
    
    [self.profileView setProfileWithAvatarLink:gTrip.passenger.thumb phoneNumber:gTrip.passenger.phone starValue:gTrip.passenger_UP downvalue:gTrip.passenger_down name:gTrip.passenger.name];
    

    [self.btnArrived setTitle:[Util localized:@"arrived_title"] forState:UIControlStateNormal];
  
    [self setTextLocationNameLbl];
  
//    if (APP_IN_TEST_MODE) {
//        driverAnno = [[MKPointAnnotation alloc] init];
//        driverAnno.coordinate = [LocationManager sharedManager].myLocation;
//        [_mapView addAnnotation:driverAnno];
//    }
    [_gMap clear];
    fromAMarker = [GMSMarker markerWithPosition:gTrip.startLocation.position];
    fromAMarker.icon = [UIImage imageNamed:@"icon_fromA"];
//    toBMaker = [GMSMarker markerWithPosition:gTrip.endLocation.position];
//    toBMaker.icon = [UIImage imageNamed:@"icon_toB"];
    fromAMarker.map = _gMap;
   
    toBMaker = nil;
    _gMap.myLocationEnabled = true;
    _gMap.camera = [GMSCameraPosition cameraWithLatitude:gShareLocation.myLocation.latitude longitude:gShareLocation.myLocation.longitude zoom:16];
    _gMap.delegate = self;
    toBMaker.map = nil;
    toBMaker = [GMSMarker markerWithPosition:gTrip.endLocation.position];
    toBMaker.icon = [UIImage imageNamed:@"icon_toB"];
    toBMaker.map = _gMap;
    
//    toBMaker1.map = nil;
//    toBMaker1 = [GMSMarker markerWithPosition:gTrip.endLocation1.position];
//    toBMaker1.icon = [UIImage imageNamed:@"icon_fromc"];
//    toBMaker1.map = _gMap;
//    toBMaker2.map = nil;
//    toBMaker2 = [GMSMarker markerWithPosition:gTrip.endLocation2.position];
//    toBMaker2.icon = [UIImage imageNamed:@"d1"];
//    toBMaker2.map = _gMap;
//    toBMaker3.map = nil;
//    toBMaker3 = [GMSMarker markerWithPosition:gTrip.endLocation3.position];
//    toBMaker3.icon = [UIImage imageNamed:@"icon_frome"];
//    toBMaker3.map = self.gMap;
//    
//    toBMaker4.map = nil;
//    toBMaker4 = [GMSMarker markerWithPosition:gTrip.endLocation4.position];
//    toBMaker4.icon = [UIImage imageNamed:@"icon_fromf"];
//    toBMaker4.map = self.gMap;


     [self drawDirectionFrom:[LocationManager sharedManager].myLocation to:gTrip.startLocation.position];
 
}
#pragma mark - Butons actions

- (IBAction)btnNavigationAction:(id)sender {
    
    NSString *currentMapSelected;
    currentMapSelected = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentMapSelected"];
    if ([currentMapSelected isEqualToString: @"google"]) {
        [self openDirectionsInGoogleMaps];
        
    }else if ([currentMapSelected isEqualToString: @"waze"]){
        [self openWazeMaps];
        
    }else{
        [self openAppleMaps];
    }
    
    
}




- (IBAction)onArrived:(id)sender {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [ModelManager changeStatusOfTripToArrivedWithSuccess:^(NSString *strSuccess) {
            
        } failure:^(NSString *err) {
            
        }];

    });
    
       [self performSegueWithIdentifier:@"onArrived" sender:self];
}

- (IBAction)onCancelTrip:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self cancelTrip];
    
}

-(void)cancelTrip{
    [ModelManager cancelTrip:gTrip withSuccess:^{
        
        [self.view makeToast:[Util localized:@"msg_cancel_success"]];
        gTrip = [[Trips alloc]init];
        [Util setObject:@"" forKey:DRIVER_CURRENT_TRIP_ID_KEY];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
    } andFailure:^(NSString *err) {
        [self.view makeToast:[Util localized:@"msg_cancel_failed"]];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
}

- (IBAction)onCurrentLocation:(id)sender {
    [_gMap animateToLocation:gShareLocation.myLocation];
    if (APP_IN_TEST_MODE) {
        self.gMap.myLocationEnabled = NO;
    }
}



-(void)openAppleMaps{
    
    
    NSString *lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlatitude"] ;
    NSString *log = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlogitude"];
    
    NSString *lat1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlatitude"] ;
    NSString *log1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlogitude"];
    
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",gShareLocation.myLocation.latitude, gShareLocation.myLocation.longitude, [lat1 doubleValue], [log1 doubleValue]];
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:^(BOOL success) {}];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL]];
    }
}


-(void)openWazeMaps{
    
    NSString *lat1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlatitude"] ;
    NSString *log1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlogitude"];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"waze://"]]) {
        //Waze is installed. Launch Waze and start navigation
        NSString *urlStr = [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes", [lat1 doubleValue], [log1 doubleValue]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    } else {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"You don't have Waze application, Please install it"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        
        //Waze is not installed. Launch AppStore to install Waze app
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
    
}
- (void)openDirectionsInGoogleMaps {
    
    NSString *lat1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlatitude"] ;
    NSString *log1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlogitude"];
    
    if (![[OpenInGoogleMapsController sharedInstance] isGoogleMapsInstalled]) {
        NSLog(@"Google Maps not installed, but using our fallback strategy");
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"You don't have GoogleMaps application, Please install it"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        return;
    }
    
    
    GoogleDirectionsDefinition *directionsDefinition = [[GoogleDirectionsDefinition alloc] init];
    directionsDefinition.startingPoint = nil;
    
    GoogleDirectionsWaypoint *startingPoint = [[GoogleDirectionsWaypoint alloc] init];
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(gShareLocation.myLocation.latitude, gShareLocation.myLocation.longitude);
    startingPoint.queryString = nil;
    startingPoint.location = location;
    directionsDefinition.startingPoint = startingPoint;
    
    
    CLLocationCoordinate2D locations = CLLocationCoordinate2DMake([lat1 doubleValue], [log1 doubleValue]);
    
    GoogleDirectionsWaypoint *destination = [[GoogleDirectionsWaypoint alloc] init];
    destination.queryString = nil;
    destination.location = locations;
    directionsDefinition.destinationPoint = destination;
    
    directionsDefinition.travelMode = 1;
    [[OpenInGoogleMapsController sharedInstance] openDirections:directionsDefinition];
}


#pragma mark =>>LocationManager delegate
- (void)didUpdateLocationFrom:(CLLocationCoordinate2D)fromLocation toLocation:(CLLocationCoordinate2D)toLocation{
    
}

- (void)drawDirectionFrom:(CLLocationCoordinate2D)fromLocation to:(CLLocationCoordinate2D)toLocation{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
    
    [ModelManager getRoutingByGoogleWithStart:fromLocation endLocation:toLocation handlerSuccess:^(NSDictionary *response) {
        GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
        singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 2;
        singleLine.strokeColor = [UIColor blueColor];
        dispatch_async(dispatch_get_main_queue(), ^{
             singleLine.map = self.gMap;
        });
       
    } handlerFailed:^(NSString *err) {
    
    }];
  });

}


@end
