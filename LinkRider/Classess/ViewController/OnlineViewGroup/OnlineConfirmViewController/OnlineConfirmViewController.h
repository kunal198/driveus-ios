//
//  OnlineConfirmViewController.h
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBAutoScrollLabel.h"
#import <GoogleMaps/GoogleMaps.h>
@interface OnlineConfirmViewController : UIViewController<UIAlertViewDelegate,UIAlertViewDelegate>{

}
@property (weak, nonatomic) IBOutlet GMSMapView *gMap;

@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnArrived;

@property (weak, nonatomic) IBOutlet UILabel *lblLocationName;

- (IBAction)onArrived:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelTrip;
@property (weak, nonatomic) IBOutlet UILabel *lblHelp;
    @property (weak, nonatomic) IBOutlet ProfileHasActionView *profileView;

@end
