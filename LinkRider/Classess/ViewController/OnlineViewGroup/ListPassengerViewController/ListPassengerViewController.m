//
//  ListPassengerViewController.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "ListPassengerViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "Trips.h"
#import "UIFont+FontAwesome.h"
#import "NSString+FontAwesome.h"
#import "UIImageView+WebCache.h"
@interface ListPassengerViewController ()

@end

@implementation ListPassengerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    self.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCancelTripdriveer) name:PASSENGER_CANCEL_REQUEST object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCancelTripdriveer) name:DRIVER_CANCEL_TRIP_KEY object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBecomForceGround) name:DRIVER_RECEIVE_REQUEST_KEY object:nil];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBecomForceGround) name:WILL_ENTER_FORCE_GROUND object:nil];
}
- (void)appWillBecomForceGround{
    
    if ([[Util topViewController] isMemberOfClass:[ListPassengerViewController class]]) {
        [self showMyTrip];
    }
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    notificationsDic = nil;
   
    [self.navigationItem setHidesBackButton:NO animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [self setText];
    [self showMyTrip];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PASSENGER_CANCEL_REQUEST object:nil];
}
-(void)setText{
    self.lblDesc.text = [Util localized:@"lbl_receive_link_by_passenger"];
    [self.btnBack setTitle:[NSString fontAwesomeIconStringForEnum:FALongArrowLeft] forState:UIControlStateNormal];
    NSDictionary *fontDict = @{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:10],NSForegroundColorAttributeName:[Util colorWithHexString:@"#e8c03a"]};
    
    NSMutableAttributedString *att =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict];
    
    
    
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: STEP_TITLE_FONT forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[Util localized:@"request_link_title"]] attributes: arialDict];
    
    [att appendAttributedString:aAttrString];
    
    for(int i=0;i<4;i++){
        
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircleO]] attributes: fontDict];
        [att appendAttributedString:aAttrString];
    }
    self.lblRequest.attributedText =att;
    
    
}

-(void)onCancelTripdriveer{
    if ([[Util topViewController] isMemberOfClass:[ListPassengerViewController class]]) {
        [self showMyTrip];
    }
    
}

-(void)showMyTrip{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            self.tripArr = [NSMutableArray arrayWithArray:arr];
            //self.tripArr = [arr copy];
        
            [self.tblView reloadData];
            
            if (self.tripArr.count==0) {
//                [Util showMessage:[Util localized:@"msg_trip_cancel_by_passenger"] withTitle:[Util localized:@"app_name"]];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        } andFailure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
//    });
}
-(IBAction)onBack:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark tableview datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 205;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return self.tripArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ListPassengerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListPassengerCell"];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ListPassengerCell" owner:self options:nil];
        cell = (ListPassengerCell *)[nib objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    Trips *t = [self.tripArr objectAtIndex:indexPath.row];
    
    [cell.profileView setProfileWithAvatarLink:t.passenger.thumb phoneNumber:t.passenger.phone starValue:t.passenger.passenger_rateup down_value:t.passenger.passenger_ratedown  name:t.passenger.name type:0];
    
    cell.Acceptbtn.tag = indexPath.row;
    cell.Declinebtn.tag = indexPath.row;
    [cell.Acceptbtn addTarget:self action:@selector(AcceptAction:) forControlEvents:UIControlEventTouchUpInside];
     [cell.Declinebtn addTarget:self action:@selector(DeclineAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.lblStartValue.text = t.startLocation.address;
    cell.lblEndValue.text = t.endLocation.address;
    cell.lblEstFareValue.text =  t.estimateFare;
    cell.lblSeatValue.text = [DriverCar typeNameByTypeId:t.link];
    
    
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}


-(void)AcceptAction:(UIButton*)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        
        Trips *t = [self.tripArr objectAtIndex:sender.tag];
        
        [ModelManager driverConfirmWithTriId:t.tripId acceptStatus:@"1" withSuccess:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [Util setObject:@"" forKey:@"tripIsEnd"];
                [Util setObject:gTrip.tripId forKey:DRIVER_CURRENT_TRIP_ID_KEY];
                [self performSegueWithIdentifier:@"selectTrip" sender:self];
                
            });
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            
            
        } failure:^(NSString *err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [self.view makeToast:err];
                [self showMyTrip];
            });
            
            
        }];
    });
}


-(void)DeclineAction:(UIButton*)sender
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        
        Trips *t = [self.tripArr objectAtIndex:sender.tag];
        
        [ModelManager driverConfirmWithTriId:t.tripId acceptStatus:@"0" withSuccess:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tripArr removeObject:t];
                
                if (self.tripArr.count ==0)
                {
                    [self.tblView reloadData];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    
                }else
                {
                    
                    [self.tblView reloadData];
                }

                
            });
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            
            
        } failure:^(NSString *err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [self.view makeToast:err];
                [self showMyTrip];
            });
            
            
        }];
    });
    
    
    
    
    
        
        
//        [ModelManager driverConfirmWithTriId:t.tripId withSuccess:^{
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [Util setObject:@"" forKey:@"tripIsEnd"];
//                [Util setObject:gTrip.tripId forKey:DRIVER_CURRENT_TRIP_ID_KEY];
//                [self performSegueWithIdentifier:@"selectTrip" sender:self];
//                
//            });
//            
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            
//            
//            
//        } failure:^(NSString *err) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                [self.view makeToast:err];
//                [self showMyTrip];
//            });
//            
//            
//        }];
 
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


@end
