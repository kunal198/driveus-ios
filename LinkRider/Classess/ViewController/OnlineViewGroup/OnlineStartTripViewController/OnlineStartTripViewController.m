//
//  OnlineStartTripViewController.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "OnlineStartTripViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "Trips.h"
#import "UIImageView+WebCache.h"
#import "ShowDistanceViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "UIFont+FontAwesome.h"
#import "MyPinAnnotationView.h"
#import "OnlineViewController.h"
#import <GoogleMapsM4B/GoogleMaps.h>
#import "OpenInGoogleMapsController.h"

@interface OnlineStartTripViewController ()<ShowDistanceViewControllerDelegate, GMSMapViewDelegate>
{
    IBOutlet UIButton *btnNavigationOutlet;
    
    UIView *blackView;
    NSTimer *timer;
    NSString *currentMapSelected;
}

@end
static NSString * const kOpenInMapsSampleURLScheme = @"OpenInGoogleMapsSample://";

@implementation OnlineStartTripViewController{
    GMSMarker *fromMarker, *toMaker ,*toMaker1, *toMaker2 , *toMaker3 , *toMaker4 ;
    GMSMarker  *carMaker;
    GMSPolyline *singleLine;
    GMSPolyline *singleLine1;
    GMSPolyline *singleLine2;
    GMSPolyline *singleLine3;
    GMSPolyline *singleLine4;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    location = @"A";
    [OpenInGoogleMapsController sharedInstance].callbackURL =
    [NSURL URLWithString:kOpenInMapsSampleURLScheme];
    
    // If the user doesn't have Google Maps installed, let's try Chrome. And if they don't
    // have Chrome installed, let's use Apple Maps. This gives us the best chance of having an
    // x-callback-url that points back to our application.
    [OpenInGoogleMapsController sharedInstance].fallbackStrategy =
    kGoogleMapsFallbackChromeThenAppleMaps;
    
    
    [self setNeedsStatusBarAppearanceUpdate];
    [self.btnCallAdmin setTitle:[NSString fontAwesomeIconStringForEnum:FAPhone] forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCancelTrips) name:PASSENGER_CANCEL_REQUEST object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCancelTrips) name:DRIVER_CANCEL_TRIP_KEY object:nil];
    [self setupGoogleMap];
    currentMapSelected = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentMapSelected"];
    if (gTrip.status == inprogress) {
        btnNavigationOutlet.hidden = false;
    }
    else
    {
        btnNavigationOutlet.hidden = true;
        blackView = [[UIView alloc]initWithFrame:CGRectMake(40, 100 , CGRectGetWidth(self.view.frame) - 80 , 160)];
        blackView.backgroundColor = [UIColor blackColor];
        blackView.alpha = 0.8;
        
        blackView.layer.cornerRadius = 5;
        [blackView.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [blackView.layer setShadowOffset:CGSizeMake(0, 0)];
        [blackView.layer setShadowRadius:5.0];
        [blackView.layer setShadowOpacity:1];
        
        UILabel *remainingTime = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, blackView.frame.size.width - 40, blackView.frame.size.height - 50)];
        remainingTime.text = [NSString stringWithFormat:@"%.2f",canceltimer.fireDate.timeIntervalSinceNow];
        remainingTime.textColor = [UIColor whiteColor];
        [remainingTime setTextAlignment:NSTextAlignmentCenter];
        [remainingTime setFont:[UIFont systemFontOfSize:48 weight:10]];
        [remainingTime setTag:1];
        
        UIButton *cross = [[UIButton alloc]initWithFrame:CGRectMake(10, remainingTime.frame.origin.y+remainingTime.frame.size.height, blackView.frame.size.width - 40, 35)];
        [cross setTitle:@"Cancel Trip" forState:UIControlStateNormal];
        [cross addTarget:self action:@selector(crossBlackView) forControlEvents:UIControlEventTouchUpInside];
        
        [blackView addSubview:remainingTime];
        [blackView addSubview:cross];
        
        float fireDate = [[NSUserDefaults standardUserDefaults] floatForKey:@"fireDate"];
        if (fireDate != 0) {
        NSDate *date    = [NSDate dateWithTimeIntervalSince1970:fireDate];
        float nowTime   = date.timeIntervalSinceNow;
        if (nowTime > 0) {
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hideBlackView:) userInfo:nil repeats:YES];
          canceltimer = [NSTimer scheduledTimerWithTimeInterval:nowTime target:self selector:@selector(cancelwait) userInfo:nil repeats:NO];
            
        }else{
            
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hideBlackView:) userInfo:nil repeats:YES];
            canceltimer = [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(cancelwait) userInfo:nil repeats:NO];
        }
            
        }else{
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hideBlackView:) userInfo:nil repeats:YES];
        
        canceltimer = [NSTimer scheduledTimerWithTimeInterval:180 target:self selector:@selector(cancelwait) userInfo:nil repeats:NO];
        }
        [self.view addSubview:blackView];
        [self.view bringSubviewToFront:blackView];
        

    }
    updatetimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateDistance) userInfo:nil repeats:YES];

    
}

- (IBAction)btnNavigateAction:(id)sender {
    
    if ([currentMapSelected isEqualToString: @"google"]) {
        [self openDirectionsInGoogleMaps];
        
    }else if ([currentMapSelected isEqualToString: @"waze"]){
        [self openWazeMaps];
        
    }else{
        [self openAppleMaps];
    }
 
}


-(void)crossBlackView{
    [blackView removeFromSuperview];
    [timer invalidate];
    [canceltimer invalidate];
    timer = nil;
    canceltimer = nil;
    [self cancelTrip];
    
    
   // [blackView removeFromSuperview];
    
}

-(void)hideBlackView:(NSTimer *)timer{
    
    UILabel *l = [blackView viewWithTag:1];
    
    float x = canceltimer.fireDate.timeIntervalSinceNow;
    
    int m  = floor(x/60);
    int s = round(x - m * 60);
    if (s < 10 ){
        [l setText:[NSString stringWithFormat:@"%d:0%d",m,s]];
    }
    else{
       [l setText:[NSString stringWithFormat:@"%d:%d",m,s]];
    }
}



-(void)cancelwait
{
    
    [blackView removeFromSuperview];
    [timer invalidate];
    [canceltimer invalidate];
    timer = nil;
    canceltimer = nil;
    
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"Do You Want to cancel Trip! Cancelled Fee will be charged From Passenger!"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        [self cancelTrip];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:okAction];
    [alertVC addAction:cancelAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

-(void)cancelTrip{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager cancelTrip:gTrip withSuccess:^{
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.view makeToast:[Util localized:@"msg_cancel_success"]];
        gTrip = [[Trips alloc]init];
        [Util setObject:@"" forKey:DRIVER_CURRENT_TRIP_ID_KEY];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
    } andFailure:^(NSString *err) {
        [self.view makeToast:[Util localized:@"msg_cancel_failed"]];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
}



-(void)updateDistance{
    NSLog(@"will update distance");
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        
        [ModelManager getCurrentLocationWithDriverId:gTrip.driverId withSuccess:^(float latitude, float longtitude) {
            CLLocation *driverLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longtitude];
            
            if (driverLocation)
            {
                NSArray *items = @[@"A", @"B", @"C" ,@"D",@"E"];
                int item = [items indexOfObject:location];
                switch (item) {
                    case 0:
                    {
                        
                        self.ENDLOCation = [[CustomPosition alloc]initWithPositon:(gTrip.endLocation.position) addressName:nil];
                        
                        CLLocation *loc1 = [[CLLocation alloc]initWithLatitude:gTrip.endLocation.position.latitude longitude:gTrip.endLocation.position.longitude];
                        CLLocationDistance distance = [driverLocation distanceFromLocation:loc1];
                        
                        if (distance <= 100)
                        {
                            location = @"B";
                            fromMarker = nil;
                            
                            self.ENDLOCation = [[CustomPosition alloc]initWithPositon:(gTrip.endLocation1.position) addressName:nil];
                            [self drawDirectionFrom:gTrip.endLocation.position to:gTrip.endLocation1.position];
                            
                        }
                        else
                        {
                            [self makeDirectionWithCurrentLocation:driverLocation.coordinate];
                        }
                        
                    }
                        break;
                    case 1:
                    {
                        
                        self.ENDLOCation = [[CustomPosition alloc]initWithPositon:(gTrip.endLocation1.position) addressName:nil];
                        
                        CLLocation *loc1 = [[CLLocation alloc]initWithLatitude:gTrip.endLocation1.position.latitude longitude:gTrip.endLocation1.position.longitude];
                        CLLocationDistance distance = [driverLocation distanceFromLocation:loc1];
                        
                        if (distance <= 100)
                        {
                            
                            location = @"C";
                            fromMarker = nil;
                            toMaker = nil;
                            
                            self.ENDLOCation = [[CustomPosition alloc]initWithPositon:(gTrip.endLocation2.position) addressName:nil];
                            [self drawDirectionFrom:gTrip.endLocation.position to:gTrip.endLocation2.position];
                            
                            
                        }
                        else
                        {
                            [self makeDirectionWithCurrentLocation:driverLocation.coordinate];
                        }
                        
                    }
                        
                        
                        break;
                    case 2:
                    {
                        
                        self.ENDLOCation = [[CustomPosition alloc]initWithPositon:(gTrip.endLocation2.position) addressName:nil];
                        
                        CLLocation *loc1 = [[CLLocation alloc]initWithLatitude:gTrip.endLocation2.position.latitude longitude:gTrip.endLocation2.position.longitude];
                        CLLocationDistance distance = [driverLocation distanceFromLocation:loc1];
                        
                        if (distance == 100)
                        {
                            
                            location = @"D";
                            fromMarker = nil;
                            
                            self.ENDLOCation = [[CustomPosition alloc]initWithPositon:(gTrip.endLocation3.position) addressName:nil];
                            [self drawDirectionFrom:gTrip.endLocation.position to:gTrip.endLocation3.position];
                            
                            
                            
                        }
                        else
                        {
                            [self makeDirectionWithCurrentLocation:driverLocation.coordinate];
                        }
                        
                    }
                        
                        
                        break;
                    case 3:
                    {
                        
                        self.ENDLOCation = [[CustomPosition alloc]initWithPositon:(gTrip.endLocation3.position) addressName:nil];
                        
                        CLLocation *loc1 = [[CLLocation alloc]initWithLatitude:gTrip.endLocation3.position.latitude longitude:gTrip.endLocation2.position.longitude];
                        CLLocationDistance distance = [driverLocation distanceFromLocation:loc1];
                        
                        if (distance == 100)
                        {
                            
                            location = @"E";
                            fromMarker = nil;
                            
                            self.ENDLOCation = [[CustomPosition alloc]initWithPositon:(gTrip.endLocation4.position) addressName:nil];
                            [self drawDirectionFrom:gTrip.endLocation.position to:gTrip.endLocation4.position];
                            
                            
                            
                        }
                        else
                        {
                            [self makeDirectionWithCurrentLocation:driverLocation.coordinate];
                        }
                        
                    }
                        
                        
                    default:
                        break;
                }
                
            }
        }
         
         
                                             failure:^(NSString *err) {
                                                 
                                             }];
    });
    
    
}



- (void)setupGoogleMap{
    _gMap.myLocationEnabled = true;
    _gMap.camera = [GMSCameraPosition cameraWithLatitude:gShareLocation.myLocation.latitude longitude:gShareLocation.myLocation.longitude zoom:16];
    _gMap.delegate = self;
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)onCancelTrips
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Drivus"
                                                    message:@"Trip Cancelled by Passenger"
                                                   delegate:self
                                          cancelButtonTitle:@"ok"
                                          otherButtonTitles:nil, nil];
    
    [alert show];
    
    //    OnlineViewController *myNewVC = (OnlineViewController *)[self.storyboard       instantiateViewControllerWithIdentifier:@"OnlineViewController"];
    //    [self.navigationController pushViewController:myNewVC animated:YES];
    ////    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}




-(void)viewWillAppear:(BOOL)animated{
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    gShareLocation.delegate = self;
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    //  [self.locationManager startUpdatingLocation];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setData];
    
    
}

-(void)setData{
    //    [self.profileView setProfileWithAvatarLink:gTrip.passenger.thumb phoneNumber:gTrip.passenger.phone starValue:gTrip.passenger.passenger_rating name:gTrip.passenger.name];
    
    
    [self.profileView setProfileWithAvatarLink:gTrip.passenger.thumb phoneNumber:gTrip.passenger.phone starValue:gTrip.passenger_UP downvalue:gTrip.passenger_down name:gTrip.passenger.name];
    
    
    
    
    [self setTextLocationNameLbl];
    [self.btnStart setTitle:[Util localized:@"lbl_begin_trip"] forState:UIControlStateNormal];
    
    if (gTrip.status == inprogress) {
        [self.btnStart setTitle:[Util localized:@"lbl_end_trip"] forState:UIControlStateNormal];
    }
    
    
    
    [_gMap clear];
    fromMarker = [GMSMarker markerWithPosition:gTrip.startLocation.position];
    fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
    fromMarker.map = _gMap;
    
    toMaker = [GMSMarker markerWithPosition:gTrip.endLocation.position];
    toMaker.icon = [UIImage imageNamed:@"icon_toB"];
    toMaker.map = _gMap;
    
    //prabhjot
//    toMaker1.map = nil;
//    toMaker1 = [GMSMarker markerWithPosition:gTrip.endLocation1.position];
//    toMaker1.icon = [UIImage imageNamed:@"icon_fromc"];
//    toMaker1.map = _gMap;
//    toMaker2.map = nil;
//    toMaker2 = [GMSMarker markerWithPosition:gTrip.endLocation2.position];
//    toMaker2.icon = [UIImage imageNamed:@"d1"];
//    toMaker2.map = _gMap;
//    toMaker3.map = nil;
//    toMaker3 = [GMSMarker markerWithPosition:gTrip.endLocation3.position];
//    toMaker3.icon = [UIImage imageNamed:@"icon_frome"];
//    toMaker3.map = self.gMap;
//    
//    toMaker4.map = nil;
//    toMaker4 = [GMSMarker markerWithPosition:gTrip.endLocation4.position];
//    toMaker4.icon = [UIImage imageNamed:@"icon_fromf"];
//    toMaker4.map = self.gMap;
    
    [self drawDirectionFrom:gTrip.startLocation.position to:gTrip.endLocation.position];
}

- (void)setTextLocationNameLbl{
    
    NSString *prefix = [Util localized:@"lbl_to_b"];
    NSString *content = gTrip.startLocation.address.length? gTrip.endLocation.address : [Util localized:@"lbl_loading"];
    
    NSString *infoString = [NSString stringWithFormat:@"%@%@", prefix, content];
    
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:infoString];
    
    //  UIFont *font_regular=[UIFont fontWithName:@"Raleway-Medium" size:14.0f];
    UIFont *font_bold=[UIFont fontWithName:@"Raleway-Bold" size:14.0f];
    
    [attString addAttribute:NSFontAttributeName value:font_bold range:NSMakeRange(0, prefix.length)];
    
    [self.lblToName setAttributedText:attString];
}


-(void)viewWillDisappear:(BOOL)animated{
    
    if (canceltimer != nil) {

        
        // Get
//        NSTimeInterval myDateTimeInterval = [[NSUserDefaults standardUserDefaults] floatForKey:@"myDateKey"];
//        NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:myDateTimeInterval];

        [[NSUserDefaults standardUserDefaults] setFloat:canceltimer.fireDate.timeIntervalSince1970 forKey:@"fireDate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        
        [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"fireDate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    [self.navigationItem setHidesBackButton:NO animated:YES];
    gShareLocation.delegate = nil;
    [[UIApplication sharedApplication] setIdleTimerDisabled: NO];
    [super viewWillDisappear:animated];
    
}

- (IBAction)onCurrentLocation:(id)sender {
    [_gMap animateToLocation:gShareLocation.myLocation];
}

- (IBAction)onCallToAdmin:(id)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager sendHelpRequestWithSuccess:^(NSString *strSuccessReturn) {
            
        } failure:^(NSString *err) {
            
        }];
    });
    [self ShareWithEmail];
    //[Util callPhoneNumber:[AppSettings share].adminPhoneNumber];
}


-(void)ShareWithEmail
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        [mailController setMailComposeDelegate:self];
        [mailController setSubject:@"Drivus Help"];
        [mailController setToRecipients:[NSArray arrayWithObjects:@"Help@Drivus.com", @"email2", nil]];
        NSString *temp =[NSString stringWithFormat:@"Please Wite Message here wat kind of help you need"];
        [mailController setMessageBody:temp isHTML:NO];
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please setup mail in your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
 
}
- (IBAction)onBeginTrip:(id)sender {
    
    
    if (gTrip.status != inprogress) {
        
        [blackView removeFromSuperview];
        [timer invalidate];
        [canceltimer invalidate];
        timer = nil;
        canceltimer = nil;

        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            [ModelManager driverStartTripWithTriId:gTrip.tripId withSuccess:^{
                gTrip.status = inprogress;
                [gShareLocation startCalculateDistanceTravel];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    
                    if (![[OpenInGoogleMapsController sharedInstance] isGoogleMapsInstalled]) {
                        NSLog(@"Google Maps not installed, but using our fallback strategy");
                    }
                    if ([currentMapSelected isEqualToString: @"google"]) {
                        [self openDirectionsInGoogleMaps];

                    }else if ([currentMapSelected isEqualToString: @"waze"]){
                        [self openWazeMaps];

                    }else{
                        [self openAppleMaps];
                    }
                    
                    //[self openDirectionsInGoogleMaps];
                    btnNavigationOutlet.hidden = false;
                    
                    
                    [self.btnStart setTitle:[Util localized:@"lbl_end_trip"] forState:UIControlStateNormal];
                    [self.btnStart setBackgroundColor:[UIColor redColor]];
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                });
            } failure:^(NSString *err) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                });
                
            }];
        });
        
    }
    else
    {
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"Do you really want to End trip?"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self onAddDistance:sender];
            
            
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:okAction];
        [alertVC addAction:cancelAction];
        
        [self presentViewController:alertVC animated:YES completion:nil];
 
    }
}

-(void)openAppleMaps{
    
    
    NSString *lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlatitude"] ;
    NSString *log = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlogitude"];
    
    NSString *lat1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"endlatitude"] ;
    NSString *log1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"endlogitude"];
    
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",[lat doubleValue], [log doubleValue], [lat1 doubleValue], [log1 doubleValue]];
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:^(BOOL success) {}];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL]];
    }
}


-(void)openWazeMaps{
    
    NSString *lat1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"endlatitude"] ;
    NSString *log1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"endlogitude"];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"waze://"]]) {
        //Waze is installed. Launch Waze and start navigation
        NSString *urlStr = [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes", [lat1 doubleValue], [log1 doubleValue]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    } else {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"You don't have Waze application, Please install it"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        
        //Waze is not installed. Launch AppStore to install Waze app
       // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
    
}
- (void)openDirectionsInGoogleMaps {
    
    NSString *lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlatitude"] ;
    NSString *log = [[NSUserDefaults standardUserDefaults] objectForKey:@"startlogitude"];
    
    NSString *lat1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"endlatitude"] ;
    NSString *log1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"endlogitude"];
    
    if (![[OpenInGoogleMapsController sharedInstance] isGoogleMapsInstalled]) {
        NSLog(@"Google Maps not installed, but using our fallback strategy");
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"You don't have GoogleMaps application, Please install it"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        return;
    }
    
    
    GoogleDirectionsDefinition *directionsDefinition = [[GoogleDirectionsDefinition alloc] init];
    directionsDefinition.startingPoint = nil;
    
    GoogleDirectionsWaypoint *startingPoint = [[GoogleDirectionsWaypoint alloc] init];
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([lat doubleValue], [log doubleValue]);
    startingPoint.queryString = nil;
    startingPoint.location = location;
    directionsDefinition.startingPoint = startingPoint;
    
    
    CLLocationCoordinate2D locations = CLLocationCoordinate2DMake([lat1 doubleValue], [log1 doubleValue]);
    
    GoogleDirectionsWaypoint *destination = [[GoogleDirectionsWaypoint alloc] init];
    destination.queryString = nil;
    destination.location = locations;
    directionsDefinition.destinationPoint = destination;
    
    directionsDefinition.travelMode = 1;
    [[OpenInGoogleMapsController sharedInstance] openDirections:directionsDefinition];
}



- (IBAction)onAddDistance:(id)sender {
    [gShareLocation stopCalculateDistanceTravel];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    imgView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    [self.view addSubview:imgView];
    ShowDistanceViewController *dVC = [[ShowDistanceViewController alloc]initWithNibName:@"ShowDistanceViewController" bundle:nil];
    dVC.delegate = self;
    dVC.view.center = self.view.center;
    [self.view addSubview:dVC.view];
    [self addChildViewController:dVC];
}

-(void)showDistanceSuccessDelegate{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    float distance = [[[NSUserDefaults standardUserDefaults] stringForKey:kSaveDistance] floatValue];
    [ModelManager driverEndTripWithTriId:gTrip.tripId andDistance:[NSString stringWithFormat:@"%.2f",((distance/1000)/0.621371192)] withSuccess:^{
        [Util setObject:gTrip.tripId forKey:@"tripIsEnd"];
        [Util removeObjectForKey:kSaveDistance];
        [Util setObject:@"0" forKey:kSaveDistance];
        [self performSegueWithIdentifier:@"endTrip" sender:self];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } failure:^(NSString *err) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.view makeToast:err];
    }];
    
    
    }

#pragma mark =>>LocationManager delegate
- (void)didUpdateLocationFrom:(CLLocationCoordinate2D)fromLocation toLocation:(CLLocationCoordinate2D)toLocation{
}


- (void)makeDirectionWithCurrentLocation:(CLLocationCoordinate2D)currentLocation1{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        MKPlacemark *placemarkTo;
        if (gTrip.status != inprogress) {
            placemarkTo = [[MKPlacemark alloc] initWithCoordinate:gTrip.startLocation.position addressDictionary:nil] ;
            
        }else{
            placemarkTo = [[MKPlacemark alloc] initWithCoordinate:gTrip.endLocation.position addressDictionary:nil] ;
        }
        [self drawDirectionFrom:currentLocation1 to:self.ENDLOCation.position];
    });
    
}


- (void)drawDirectionFrom:(CLLocationCoordinate2D)fromLocation to:(CLLocationCoordinate2D)toLocation{
    [ModelManager getRoutingByGoogleWithStart:fromLocation endLocation:toLocation handlerSuccess:^(NSDictionary *response) {
        carMaker.map = nil;
        carMaker = [GMSMarker markerWithPosition:fromLocation];
        
        carMaker.icon = [UIImage imageNamed:@"img_car"];
        carMaker.map = _gMap;
        
        GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
        singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 2;
        singleLine.strokeColor = [UIColor blueColor];
        singleLine.map = self.gMap;
        
        
    } handlerFailed:^(NSString *err) {
        
    }];
    
}
@end
