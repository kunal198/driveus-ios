//
//  OnlineStartTripViewController.h
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+FontAwesome.h"
#import "EDStarRating.h"
#import "CBAutoScrollLabel.h"
#import "UIKeyboardViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <MessageUI/MessageUI.h>

@interface OnlineStartTripViewController : UIViewController<LocationManagerDelegate,MFMessageComposeViewControllerDelegate>{
    UIKeyboardViewController *keyboard;
    NSTimer *updatetimer;
     NSTimer *canceltimer;
    NSString *location;

}

@property (strong, nonatomic) CustomPosition *ENDLOCation;

    @property (weak, nonatomic) IBOutlet ProfileHasActionView *profileView;

@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnStart;

@property (weak, nonatomic) IBOutlet UILabel *lblToName;

@property (weak, nonatomic) IBOutlet GMSMapView *gMap;


@property (weak, nonatomic) IBOutlet UIButton *btnCallAdmin;

- (IBAction)onBeginTrip:(id)sender;



@end
