//
//  OnlineViewController.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "OnlineViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "OnlineStartTripViewController.h"
#import "OnlineConfirmViewController.h"
#import "UIFont+FontAwesome.h"
#import "NSString+FontAwesome.h"
#import "RattingPassengerViewController.h"
@interface OnlineViewController ()

@end

@implementation OnlineViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    [self setNeedsStatusBarAppearanceUpdate];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBecomForceGround) name:WILL_ENTER_FORCE_GROUND object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveRequest) name:DRIVER_RECEIVE_REQUEST_KEY object:nil];
    [self showMyTrip];
    
    //[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(setupDataAfterViewDidLoad) userInfo:nil repeats:NO];
    mainTimer = [[NSTimer alloc]init];

   ///  mainTimer = [NSTimer scheduledTimerWithTimeInterval:15.0 target:self selector:@selector(showMyTrip) userInfo:nil repeats:YES];
    
}

-(void)zoomlocation
{
    [_Map_view animateToLocation:marker.position];
    [_Map_view animateToZoom:MAP_ZOOM_DEFAULT];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    if (location == false)
    {
        location= true;
    CLLocation *currentLocationss = newLocation;
            if (currentLocationss != nil)
            {
                
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
   marker = [GMSMarker markerWithPosition:position];
    marker.title = gUser.name;
    marker.icon = [UIImage imageNamed:@"img_carIII"];
    marker.map = self.Map_view;
     [self zoomlocation];
                
            }
        
            }
    
}



- (void)appWillBecomForceGround{
      
    [self showMyTrip];
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)zoomtocurrentLocaion:(id)sender {
    
    [self zoomlocation];
}






-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self setText];
    self.navigationController.navigationBarHidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveRequest) name:DRIVER_RECEIVE_REQUEST_KEY object:nil];
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


-(void)showMyTrip{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [ModelManager showMyTripsWithSuccess:^(NSMutableArray *arr) {
            
            if (gTrip.status == approadching) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    NSString *vcStoryboardId = [AppSettings share].arrSceneName[[AppSettings share].currentScene - 1];
                     NSString *OnlineStartTripView = [AppSettings share].arrSceneName[[AppSettings share].currentScene ];
                    
                    if ([[Util topViewController] isKindOfClass:[vcStoryboardId class]] || [[Util topViewController] isKindOfClass:[OnlineStartTripViewController class]])
                    {
                        
                        
                        
                    }
                    
                    else
                    {
                        float fireDate = [[NSUserDefaults standardUserDefaults] floatForKey:@"fireDate"];
                        if (fireDate != 0) {
                            id classInstance = [self.storyboard instantiateViewControllerWithIdentifier:OnlineStartTripView];
                            [self.navigationController pushViewController:classInstance animated:NO];
                        
                        }else{
                            id classInstance = [self.storyboard instantiateViewControllerWithIdentifier:vcStoryboardId];
                            [self.navigationController pushViewController:classInstance animated:NO];
                        }
                  
                    }
                    return;
                });
            }else if (gTrip.status == inprogress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    NSString *vcStoryboardId = [AppSettings share].arrSceneName[[AppSettings share].currentScene - 1];
                    if ([[Util topViewController] isKindOfClass:[vcStoryboardId class]]) {
                        return;
                    }
                    else
                    {
                        id classInstance = [self.storyboard instantiateViewControllerWithIdentifier:vcStoryboardId];
                        [self.navigationController pushViewController:classInstance animated:NO];
                        return;
                    }
                    
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                });
                [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        if (arr.count>0) {
                            gTrip = [arr objectAtIndex:0];
                            [self onReceiveRequest];
                            return;
                        }
                        
                    });
                } andFailure:^(NSString *err) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    });
                    
                }];
            }
            
        } andFailure:^(NSString *err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            });
            
        }];
    });
    //    }
}

-(void)setText{
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    [self.lblHeaderTitle setTitle:[Util localized:@"header_online"]];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    
    
    if ([gUser.is_online boolValue]) {
        
        [gShareLocation startMonitoringLocation];
        gShareLocation.updateOn = YES;
        self.StatusLabel.text = [Util localized:@"msg_online_desc"];
        [self.btnOnline setTitle:[Util localized:@"status_offline"] forState:UIControlStateNormal];
        
        self.top_view1.hidden = true;
        self.top_view2.hidden = true;
    }
    else{
        gShareLocation.updateOn = NO;
        self.StatusLabel.text = @"";
        [self.btnOnline setTitle:[Util localized:@"status_online"] forState:UIControlStateNormal];
        
        self.top_view1.hidden = false;
        self.top_view2.hidden = false;
        
    }
    
    self.btnMenu.hidden = [[gUser is_online] boolValue];
    
    
    [self.btnOnline.layer setBorderColor: [UIColor whiteColor].CGColor];
    [self.btnOnline.layer setBorderWidth:1.0f];
    [self setupAttributeTitleForOfflineMode];
    
}

-(void)setupAttributeTitleForOnlineMode{
    
    NSDictionary *fontDict = @{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:8],NSForegroundColorAttributeName:[Util colorWithHexString:@"#e8c03a"]};
    
    NSMutableAttributedString *att =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject:STEP_TITLE_FONT forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[Util localized:@"request_link_title"]] attributes: arialDict];
    
    [att appendAttributedString:aAttrString];
    
    for(int i=0;i<4;i++){
        
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircleO]] attributes: fontDict];
        [att appendAttributedString:aAttrString];
    }
    self.lblRequest.attributedText =att;
}

-(void)setupAttributeTitleForOfflineMode{
    
    NSDictionary *fontDict = @{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:8],NSForegroundColorAttributeName:[Util colorWithHexString:@"#e8c03a"]};
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject:STEP_TITLE_FONT forKey:NSFontAttributeName];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[Util localized:@"request_link_title"]] attributes: arialDict];
    
    for(int i=0;i<5;i++){
        
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircleO]] attributes: fontDict];
        [att appendAttributedString:aAttrString];
    }
    self.lblRequest.attributedText =att;
}

- (void)onReceiveRequest {
    if (self) {
        UIViewController* actualVC = [self.navigationController.viewControllers lastObject];
        if (![actualVC isKindOfClass:[ListPassengerViewController class]]) {
             [mainTimer invalidate]; 
            [self performSegueWithIdentifier:@"onSelect" sender:self];
            
        }
    }
}
- (IBAction)onSelectOnlineOrOffline:(id)sender {
    
    
    
    if ([gUser.is_online boolValue]) {
        [Util showMessage:[Util localized:@"msg_confirm_offline"] withTitle:[Util localized:@"app_name"] cancelButtonTitle:[Util localized:@"title_cancel"] otherButtonTitles:[Util localized:@"title_ok"] delegate:self andTag:1];
    }
    else{
        [self onChangeStatus];
    }
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        [self onChangeStatus];
    }
}

- (void)updateStatus{
    [ModelManager setDriverOnline:[gUser.is_online boolValue] withSuccess:^{
        
    } andFailure:^(NSString *err) {
        
    }];
}
-(void)onChangeStatus{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0) , ^{
        [ModelManager setDriverOnline:![gUser.is_online boolValue] withSuccess:^{
            [ModelManager getUserProfileWithSuccess:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    self.btnMenu.hidden = [gUser.is_online boolValue];
                    if ([gUser.is_online boolValue]) {
                        self.StatusLabel.text = [Util localized:@"msg_online_desc"];
                        [self.btnOnline setTitle:[Util localized:@"status_offline"] forState:UIControlStateNormal];
                        
                        self.btnOnline.titleLabel.textColor = (__bridge UIColor * _Nullable)([[UIColor colorWithRed:232.0/255.0
                                                                                                              green:192.0/255.0
                                                                                                               blue:58.0/255.0
                                                                                                              alpha:1.0] CGColor]);

                        
                        _top_view1.hidden = true;
                          _top_view2.hidden = true;
                        [self setupAttributeTitleForOnlineMode];
                        gShareLocation.updateOn = YES;
                        [gShareLocation restartMonitoringLocation];
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    }
                    else{
                        self.StatusLabel.text = @"";
                        [self.btnOnline setTitle:[Util localized:@"status_online"] forState:UIControlStateNormal];
                        
                        self.btnOnline.titleLabel.textColor = (__bridge UIColor * _Nullable)([[UIColor colorWithRed:25.0/255.0
                                                                                                              green:55.0/255.0
                                                                                                               blue:147.0/255.0
                                                                                                              alpha:1.0] CGColor]);
                        _top_view1.hidden = false;
                        _top_view2.hidden = false;
                        [self setupAttributeTitleForOfflineMode];
                        gShareLocation.updateOn = NO;
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    }
                });
            } failure:^(NSString *err) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                });
            }];
            
        } andFailure:^(NSString *err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (err.length>0) {
                    [Util showMessage:err withTitle:[Util localized:@"app_name"]];
                }
                else{
                    [Util showMessage:[Util localized:@"msg_technical_error"] withTitle:[Util localized:@"app_name"]];
                }
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
            });
        }];
    });
}





@end
