//
//  OnlineViewController.h
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ListPassengerViewController.h"
@interface OnlineViewController : UIViewController<UIAlertViewDelegate,CLLocationManagerDelegate>{

    CLLocation *currentLocation;
    BOOL  waiting ;
      NSTimer *mainTimer;
    CLLocationManager *locationManager;
    GMSMarker *marker;
    BOOL location;
}
@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRequest;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnOnline;

- (IBAction)onSelectOnlineOrOffline:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *top_view1;
@property (strong, nonatomic) IBOutlet UIView *top_view2;

 @property (strong, nonatomic) IBOutlet GMSMapView *Map_view;


@property (strong, nonatomic) IBOutlet UILabel *StatusLabel;



@end
