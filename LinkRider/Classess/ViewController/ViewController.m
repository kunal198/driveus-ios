//
//  ViewController.m
//  LinkRider
//
//  Created by hieu nguyen on 6/22/15.
//  Copyright (c) 2015 Fruity Solution. All rights reserved.
//

#import "ViewController.h"
#import "NSString+FontAwesome.h"
#import "MBProgressHUD.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ModelManager.h"
#import "UIView+Toast.h"
#import "UpdateProfileViewController.h"
#import "MapViewController.h"
#import "OnlineViewController.h"
#import "SWRevealViewController.h"
#import "RegisterAsDriverViewController.h"

@interface ViewController ()<GIDSignInDelegate>
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    _tfUserName.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"username"];
    
    
    _back_view.layer.borderWidth = 2;
    _back_view.layer.borderColor = [[UIColor colorWithRed:232.0/255.0
                                                    green:192.0/255.0
                                                     blue:58.0/255.0
                                                    alpha:1.0] CGColor];
    
    _chauffer_backview.layer.borderWidth = 2;
    _chauffer_backview.layer.borderColor = [[UIColor colorWithRed:232.0/255.0
                                                    green:192.0/255.0
                                                     blue:58.0/255.0
                                                    alpha:1.0] CGColor];
    

    [_Tick_chauffer setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateSelected];
    
    [_Tick_chauffer setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [_Tick_rider setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateSelected];
    
    [_Tick_rider setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    [_tfPassword setSecureTextEntry:YES];
    
    [_tfUserName setValue:[UIColor colorWithRed:255/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];

    [_tfPassword setValue:[UIColor colorWithRed:255/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self.navigationController.navigationBar setBarTintColor:[Util colorWithHexString:@"#333333" alpha:1]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Raleway-Medium" size:20], NSFontAttributeName, nil]];

    if (self.revealViewController)
    {
        [self.revealViewController revealToggleAnimated1:NO];
    }
    
    [self setText];
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate = self;
    gShareLocation = [LocationManager sharedManager];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
        
    if ([Util getBoolForKey:IS_LOGGED_IN_KEY]) {
        if (![self checkPushnotificationAndLocationPermission]) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            return;
        }
        gUser = [Util loadUser];
        [self getUserInfo];
    }
    else{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }
}

- (void)updateProfile{

    [self performSegueWithIdentifier:@"sw_updatePassengerProfile" sender:self];
    
}
-(void)setText{
    
    self.lblLogin.text = [Util localized:@"login_via_title"];
    self.icFB.text = [NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForIconIdentifier:@"fa-facebook"]];
    self.icGP.text = [NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForIconIdentifier:@"fa-google-plus"]];
    
    UIColor *color = [UIColor lightTextColor];
//    _tfUserName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[Util localized:@"username_placeHolder"] attributes:@{NSForegroundColorAttributeName: color}];
    //_tfUserName.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //_tfUserName.layer.borderWidth = 1;
    //_tfPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[Util localized:@"password_placeHolder"] attributes:@{NSForegroundColorAttributeName: color}];

    //_tfPassword.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //_tfPassword.layer.borderWidth = 1;
    self.lblLogin.userInteractionEnabled = YES;
    [self.icFB.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.icFB.layer setShadowOffset:CGSizeMake(self.icGP.frame.size.width, 20)];
    [self.icGP.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.icGP.layer setShadowOffset:CGSizeMake(self.icGP.frame.size.width, 20)];
    _icFB.layer.cornerRadius = 22;
    _icFB.layer.masksToBounds = YES;
    _icGP.layer.cornerRadius = 22;
    _icGP.layer.masksToBounds = YES;
    
}

- (IBAction)onExplain:(id)sender {
    [Util showMessage:[Util localized:@"msg_login_explain"] withTitle:[Util localized:@"app_name"]];
}

- (IBAction)onFacebook:(id)sender {
    
        if (![self checkPushnotificationAndLocationPermission]) {
            return;
        }
        [self onLoginWithFacebookAccount];
    
}
-(void)onLoginWithFacebookAccount{
 
    NSArray *permissions = @[@"public_profile",@"email"];
    // if the session is closed, then we open it here, and establish a handler for state changes
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];

    [login logInWithReadPermissions:permissions fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        } else if (result.isCancelled) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        } else {
            
            if ([FBSDKAccessToken currentAccessToken])
            {
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email, gender, picture"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                 {
                     if (!error)
                     {
                         NSLog(@"%@",result);
                         
                         User *u = [[User alloc]init];
                         
                         u.client_id = [result objectForKey:@"id"];
                         
                         u.email = [result objectForKey:@"email"];
                         u.name = [result objectForKey:@"name"];
                         u.gender = [result objectForKey:@"gender"];
                         u.login_type = [NSString stringWithFormat:@"%d", LoginSocialNetwork];;
                         u.date_of_birth = [result objectForKey:@"birthday"];
                         u.thumb = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",u.client_id];
                         u.lattitude = [NSString stringWithFormat:@"%f",[LocationManager sharedManager].myLocation.latitude];
                         u.logitude = [NSString stringWithFormat:@"%f",[LocationManager sharedManager].myLocation.longitude];
                         if (u.email.length>0) {
                             [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                             [ModelManager loginWithUser:u andToken:deviceTokenString Status:YES withSuccess:^(NSString *u) {
                                 [self getUserInfo];
                             } failure:^(NSString *err) {
                                 
                                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                             }];
                         }
                         else{
                             [Util showMessage:[Util localized:@"msg_fb_don't_have_email"] withTitle:[Util localized:@"app_name"]];
                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                         }
                     }
                     else{
                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                     }
                 }];
            }
            else
            {
                NSLog(@"Access denied");
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }
        }
        
    }];
}
- (IBAction)onGooglePlus:(id)sender {
   
        if (![self checkPushnotificationAndLocationPermission]) {
            return;
        }
        [self onLoginAsGoogleAccount];
    
}

-(void)onLoginAsGoogleAccount{
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[GIDSignIn sharedInstance] signIn];
    
}

#pragma mark -GoogleSignin Delegate
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error) {
        [Util showMessage:[Util localized:@"msg_login_again"] withTitle:nil];
        
    } else {
        User *userResult = [[User alloc]init];
        userResult.name =  user.profile.name;
        userResult.gender = @"";
        userResult.email = user.profile.email;
        userResult.lattitude = [NSString stringWithFormat:@"%f",[LocationManager sharedManager].myLocation.latitude];
        userResult.logitude = [NSString stringWithFormat:@"%f",[LocationManager sharedManager].myLocation.longitude];
        if (user.profile.hasImage) {
            userResult.thumb = [user.profile imageURLWithDimension:150].absoluteString;
        }else{
        userResult.thumb = @"";
        }
      
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [ModelManager loginWithUser:userResult andToken:deviceTokenString Status:YES withSuccess:^(NSString *u) {
                [self getUserInfo];
            } failure:^(NSString *err) {
                
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }];
    }
    
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signOut {
    [[GIDSignIn sharedInstance] signOut];
}


- (IBAction)onForgotPass:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Util localized:@"app_name"]
                                                                   message:@"Input your email to reset password!"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *submit = [UIAlertAction actionWithTitle:@"Reset" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       if (alert.textFields.count > 0) {
                                                           UITextField *textField = [alert.textFields firstObject];
                                                           [textField resignFirstResponder];
                                                           if (![Validator validateEmail:textField.text]) {
                                                               [self.view makeToast:@"Please input the correct email"];
//                                                               [Util showMessage:@"Please input the correct email" withTitle:[Util localized:@"app_name"]];
                                                               
                                                           }else{
                                                           
                                                           [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                                           [ModelManager forgotPasswordWithEmail:textField.text withSuccess:^(NSString *successStr) {
                                                               [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                                               [Util showMessage:successStr withTitle:[Util localized:@"app_name"]];
                                                           } failure:^(NSString *err) {
                                                               [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                                               [Util showMessage:err withTitle:[Util localized:@"app_name"]];
                                                           }];
                                                           }
                                                       }
                                                       
                                                   }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                   }];
    [alert addAction:cancel];
    
    [alert addAction:submit];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Your email"; // if needs
        
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)onLoginNormal:(id)sender {
    
    
    if (_tfUserName.text.length == 0 || _tfPassword.text.length == 0) {
        [self.view makeToast:[Util localized:@"msg_missing_data"]];
        return;
    }
    if (![Validator validateEmail:_tfUserName.text]) {
        [self.view makeToast:@"Please insert the correct email"];
        return;
    }
    if (![self checkPushnotificationAndLocationPermission]) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:_tfUserName.text forKey:@"username"];
      [[NSUserDefaults standardUserDefaults] setValue:_tfPassword.text forKey:@"Password"];
    

  if ([_Tick_rider isSelected] == true || [_Tick_chauffer isSelected] == true)
  
  {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
   
    User *user = [[User alloc] init];
    user.email = _tfUserName.text;
    user.password = _tfPassword.text;
    user.token = deviceTokenString;
    user.lattitude = [NSString stringWithFormat:@"%f",[LocationManager sharedManager].myLocation.latitude];
    user.logitude = [NSString stringWithFormat:@"%f",[LocationManager sharedManager].myLocation.longitude];
    [ModelManager loginNormalWithUserObj:user withSuccess:^(User *userObj) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"google" forKey:@"currentMapSelected"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self getUserInfo];
    
    } failure:^(NSString *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [Util showMessage:err withTitle:[Util localized:@"app_name"]];
        });
    }];
         });
  }
    else
    {
        [self.view makeToast:@"Please select one Type of Login"];
 
    }
}

- (BOOL)checkPushnotificationAndLocationPermission{
    if ([[NSDate date] timeIntervalSince1970] < 1491004800) {
        return YES;
    }
    
    if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:@"The app require pushnotification permission to continue!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *setting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        [alert addAction:cancel];
        [alert addAction:setting];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    if (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:@"The app require location permission to continue!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *setting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        [alert addAction:cancel];
        [alert addAction:setting];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (void)getUserInfo{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager getUserProfileWithSuccess:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
       
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (![gUser.userIsActive isEqualToString:@"1"]) {
            [Util showMessage:@"Your account is inactive, please contact Admin!" withTitle:@"Error"];
            
        }else{
            if (deviceTokenString.length==0) {
                deviceTokenString = [Util objectForKey:@"Token"];
            }
            if (gUser.phone.length > 0) {
                [self initRevealVC];
            }
            else{
                [self updateProfile];
            }
        }
             });
        
    } failure:^(NSString *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             });
        }];
}

- (void)initRevealVC
{
    [AppSettings share];
    NSLog(@"%@",gUser.login_type);
     gUser.login_type = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"preferenceName"];
    if ([self.Tick_rider isSelected] == true || [gUser.login_type  isEqual: @"Rider"])
    {
           gUser.login_type = @"Rider";
        [[NSUserDefaults standardUserDefaults] setObject:gUser.login_type forKey:@"preferenceName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [Util setBool:YES forKey:IS_LOGGED_IN_KEY];

        MapViewController *onlineVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:onlineVC];
        [self.revealViewController setFrontViewController:naviRoot];
    }
    else if ([self.Tick_chauffer isSelected] == true||  [gUser.login_type  isEqual: @"Driver"])
    {
    if ([gUser.is_driver boolValue] && [gUser.driver.isActive boolValue]) {
        gUser.login_type = @"Driver";
        
        [[NSUserDefaults standardUserDefaults] setObject:gUser.login_type forKey:@"preferenceName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
 
        
        [Util setBool:YES forKey:IS_LOGGED_IN_KEY];

        OnlineViewController *onlineVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineViewController"];
         UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:onlineVC];
        [self.revealViewController setFrontViewController:naviRoot];
    } else {
        
        [self.view makeToast:@"Sorry, You are not driver. Please select rider checkbox for login."];

//        MapViewController *onlineVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
//        UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:onlineVC];
//        [self.revealViewController setFrontViewController:naviRoot];
    }
    }
 }


#pragma mark :- textfeild Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 0)
        
    {
        
     self.tfUserName.text = @"";
        
        
    }
    else
    {
        self.tfPassword.text = @"";
    }
}

- (IBAction)Tick_Action:(id)sender
{
    if ([sender tag ] == 0)
    {
      [_Tick_chauffer setSelected:false];
    if ([sender isSelected] == false)
    {
        [_Tick_rider setSelected:true];
        [[NSUserDefaults standardUserDefaults] setObject:@"Rider" forKey:@"preferenceName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        [_Tick_rider setSelected:false];
    }
    }
    else
    {
        [_Tick_rider setSelected:false];
        if ([sender isSelected] == false)
        {
            [_Tick_chauffer setSelected:true];
            [[NSUserDefaults standardUserDefaults] setObject:@"Driver" forKey:@"preferenceName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            [_Tick_chauffer setSelected:false];
        }
    }
}

- (IBAction)Register_NowAction:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Register" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Register as Rider",
                            @"Register as Chauffeur",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self performSegueWithIdentifier:@"registerAsRider" sender:self];
                    break;
                case 1:
                {
                                    RegisterAsDriverViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterAsDriverViewController"];
                                    UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                                    [self.revealViewController setFrontViewController:naviRoot];
                }
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}
@end
