//
//  ProfileViewController.h
//  LinkRider
//
//  Created by Hicom on 6/23/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+FontAwesome.h"
#import "EDStarRating.h"
#import "UpdateProfileViewController.h"
@interface ProfileViewController : UIViewController<EDStarRatingProtocol>

{
    
    IBOutlet UIButton *reveal_btn;
}

@property (nonatomic) IBOutlet UIButton* revealButtonItem;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet EDStarRating *lblRateValue;

@property (strong, nonatomic) IBOutlet UILabel *UPcount;

@property (strong, nonatomic) IBOutlet UILabel *downcount;

#pragma mark driver data

@property (weak, nonatomic) IBOutlet UILabel *icPlate;
@property (weak, nonatomic) IBOutlet UILabel *icPhone;
//
@property (weak, nonatomic) IBOutlet UILabel *icBrandOfCar;
@property (weak, nonatomic) IBOutlet UILabel *icModelOfCar;
@property (weak, nonatomic) IBOutlet UILabel *icYearManufacturer;
@property (weak, nonatomic) IBOutlet UILabel *icEmail;
@property (weak, nonatomic) IBOutlet UILabel *icAddress;
@property (weak, nonatomic) IBOutlet UILabel *icAccount;

//@property (weak, nonatomic) IBOutlet UILabel *icAccount;
//@property (weak, nonatomic) IBOutlet UILabel *icStatus;
@property (weak, nonatomic) IBOutlet UILabel *icDescription;
@property (weak, nonatomic) IBOutlet UILabel *icCarType;





//@property (weak, nonatomic) IBOutlet UILabel *lblIdDriver;
@property (weak, nonatomic) IBOutlet UILabel *lblCarPlate;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblAccount;

@property (weak, nonatomic) IBOutlet UILabel *lblBrandOfCar;
@property (weak, nonatomic) IBOutlet UILabel *lblModelOfCar;
@property (weak, nonatomic) IBOutlet UILabel *lblYearManufacturer;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblState;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblCarType;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblAccount;
//@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

//@property (weak, nonatomic) IBOutlet UILabel *lblIdDriverValue;
@property (weak, nonatomic) IBOutlet UILabel *lblCarePlateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneValue;

@property (weak, nonatomic) IBOutlet UIButton *btnUpdateProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblBrandValue;
@property (weak, nonatomic) IBOutlet UILabel *lblModelValue;
@property (weak, nonatomic) IBOutlet UILabel *lblYearValue;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailValue;
@property (weak, nonatomic) IBOutlet UILabel *lblStateValue;
@property (weak, nonatomic) IBOutlet UILabel *lblCityValue;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionValue;
@property (weak, nonatomic) IBOutlet UILabel *lblCarTypeValue;
@property (weak, nonatomic) IBOutlet UILabel *lblAccountValue;
//lbl
//@property (weak, nonatomic) IBOutlet UILabel *lblAccountValue;
//@property (weak, nonatomic) IBOutlet UILabel *lblStatusValue;

@property (weak, nonatomic) IBOutlet UIStackView *stackPlate;
@property (weak, nonatomic) IBOutlet UIStackView *stackBrandOfCar;
@property (weak, nonatomic) IBOutlet UIStackView *stackModelOfCar;
@property (weak, nonatomic) IBOutlet UIStackView *stackCarType;
@property (weak, nonatomic) IBOutlet UIStackView *stackYearManufac;


#pragma mark Passenger data



#pragma mark Action

- (IBAction)onUpdate:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *Rate_1_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_2_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_3_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_4_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_5_btn;

@end
