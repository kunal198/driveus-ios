//
//  ProfileViewController.m
//  LinkRider
//
//  Created by Hicom on 6/23/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "ProfileViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIView+Toast.h"
@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [self customSetup];
    SWRevealViewController *revealViewController = self.revealViewController;
    
    
   
//    [self.navigationController.navigationBar setBarTintColor:[Util colorWithHexString:@"#333333" alpha:1]];
//    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
//    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Raleway-Medium" size:20], NSFontAttributeName, nil]];
    //[self.revealButtonItem setTitle:@"\uf0c9" forState:UIControlStateNormal];
   // [self.revealButtonItem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    if ( revealViewController )
//    {
//        [self.revealButtonItem addTarget:revealViewController action:@selector( revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
//    }
    
    //star-highlighted-template
    _lblRateValue.starImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    _lblRateValue.starHighlightedImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    [_lblRateValue setMaxRating:5.0];
    [_lblRateValue setDelegate:self];
    [_lblRateValue setHorizontalMargin:1.0];
    [_lblRateValue setEditable:NO];
    [_lblRateValue setDisplayMode:EDStarRatingDisplayHalf];
    
    [_lblRateValue setNeedsDisplay];
   _lblRateValue.tintColor =  [UIColor darkTextColor];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setText];
    [self getData];
    [self setupScrollView];
    [self.imgUser setImageWithURL:[NSURL URLWithString:gUser.thumb]];
    // _imgUser.image = [UIImage imageNamed:@"180.png"];
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.height /2;
    self.imgUser.layer.borderWidth=1;
    self.imgUser.layer.masksToBounds = YES;
    self.imgUser.layer.borderColor = [UIColor whiteColor].CGColor;
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)getData
{

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ModelManager getUserProfileWithSuccess:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupScrollView];
            });
            
        } failure:^(NSString *err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupScrollView];
            });
        }];
    });

    
}


- (void)setText
{
    [self.imgUser setImageWithURL:[NSURL URLWithString:gUser.thumb]];
//    _imgUser.image = [UIImage imageNamed:@"180.png"];
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.height /2;
    self.imgUser.layer.borderWidth=1;
    self.imgUser.layer.masksToBounds = YES;
    self.imgUser.layer.borderColor = [UIColor whiteColor].CGColor;
    //prabhjot
   // if (![gUser.driver.isActive isEqualToString:@"1"]) {
        _stackPlate.hidden = YES;
        _stackCarType.hidden = YES;
        _stackBrandOfCar.hidden = YES;
        _stackModelOfCar.hidden = YES;
        _stackYearManufac.hidden = YES;

    //}
    self.icPhone.text =  @"\ue670";
    self.icEmail.text =  @"\ue639";
    self.icAddress.text =  @"\ue638";
    self.icDescription.text =  @"\ue647";
    self.icPlate.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-mobile"];
    self.icBrandOfCar.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-glass"];
    self.icModelOfCar.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-trophy"];
    self.icYearManufacturer.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-calendar"];
    self.icAccount.text =  [NSString fontAwesomeIconStringForIconIdentifier:@"fa-money"];
    self.icCarType.text = [NSString fontAwesomeIconStringForIconIdentifier:@"fa-car"];
    self.lblCarPlate.text = [Util localized:@"lbl_plate"];
    self.lblPhone.text = [Util localized:@"lbl_phone"];
    self.lblBrandOfCar.text = [Util localized:@"lbl_brand_of_car"];
    self.lblModelOfCar.text = [Util localized:@"lbl_model_of_car"];
    self.lblYearManufacturer.text = [Util localized:@"lbl_year_manufacturer"];
    self.lblEmail.text = [Util localized:@"lbl_email"];
    self.lblState.text = [Util localized:@"lbl_state"];
    self.lblCity.text = [Util localized:@"lbl_city"];
    self.lblAddress.text = [Util localized:@"lbl_address"];
    self.lblDescription.text = [Util localized:@"lbl_description"];
    self.lblCarType.text = [Util localized:@"lbl_carType"];
    self.lblAccount.text = [Util localized:@"lbl_account"];
    [self.btnUpdateProfile setTitle:[Util localized:@"lbl_update_profile"].uppercaseString forState:UIControlStateNormal];
    self.lblName.text = gUser.name;
    self.UPcount.text = gUser.passenger_rateup;
    self.downcount.text = gUser.passenger_ratedown;
    
    if (![gUser.driver.isActive boolValue])
    {
        self.UPcount.text = gUser.passenger_rateup;
        self.downcount.text = gUser.passenger_ratedown;
    }
    else
    {
        self.UPcount.text = gUser.driverRateup;
        self.downcount.text = gUser.driverRatedown;
    }
    
    NSLog(@"%f",[gUser.driver.driverRate floatValue]/2);
    

}


-(void)setupScrollView
{
    self.lblEmailValue.text = gUser.email;
    self.lblCarePlateValue.text = gUser.car.carPlate;
    self.lblBrandValue.text = gUser.car.brand;
    self.lblModelValue.text = gUser.car.model;
    self.lblYearValue.text = gUser.car.year;
    self.lblPhoneValue.text = gUser.phone;
    self.lblAddressValue.text = gUser.address;
    self.lblCityValue.text = gUser.city;
    self.lblStateValue.text = gUser.state;
    self.lblDescriptionValue.text = gUser.descriptions;
    self.lblAccountValue.text = gUser.cart_value;
    for (int i = 0; i < carTypeDic.allKeys.count; i++) {
        if ([gUser.carType isEqualToString:carTypeDic.allValues[i]]) {
            self.lblCarTypeValue.text = carTypeDic.allKeys[i];
        }
    }

}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    
    
    if ( revealViewController )
    {

        if ( revealViewController )
        {
            [reveal_btn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        }
        
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}


- (IBAction)onUpdate:(id)sender
{

    if([[NSString stringWithFormat:@"%@",gUser.is_driver] isEqualToString:@"1"]&&[[NSString stringWithFormat:@"%@",gUser.driver.isActive] isEqualToString:@"0"]){
        [self.view makeToast:[Util localized:@"msg_driver_approving"]];
        return;
    }
    if ([gUser.driver.update_pending isEqualToString:@"1"]) {
        [self.view makeToast:[Util localized:@"msg_pending_update"]];
        return;
    }
   // if ([[NSString stringWithFormat:@"%@",gUser.is_driver] isEqualToString:@"1"]) {
       // [self performSegueWithIdentifier:@"onUpdateProfile" sender:self];
    //}
//    else{
        [self performSegueWithIdentifier:@"onUpdatePassengerProfile" sender:self];
//    }
}
@end
