//
//  UpdateProfileViewController.h
//  LinkRider
//
//  Created by Hicom on 6/29/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"
typedef enum {
    SelectingAvatar = 0,
    SelectingDocument,
    SelectingLicenseDocument,
    SelectinginsuranceDocument,
    selectingrefristration,
    SelectingvehiclepermitDocument,
    SelectingImg1,
    SelectingImg2
} SelectionImage;

@interface UpdateProfileViewController : UIViewController<UIKeyboardViewControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    UIKeyboardViewController *keyboard;
    UIDatePicker *yearPicker;
    UIImage *documentPath;
    
    IBOutlet UIButton *reveal_toggle;
    int selectingImage;

    UIActionSheet *selectPhoto;
    UIImagePickerController *pickerCar;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnBack;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSave;

@property (strong, nonatomic) IBOutlet UIButton *update_save;
@property (strong, nonatomic) IBOutlet UIButton *updatesave;

@property (weak, nonatomic) IBOutlet UIButton *btnAddMainCar;
@property (weak, nonatomic) IBOutlet UIButton *btnAddSubCar;
@property (weak, nonatomic) IBOutlet UIView *mainSubView;
@property (weak, nonatomic) IBOutlet UILabel *lblTapToUpdateAv;

@property (weak, nonatomic) IBOutlet UILabel *icAvatar;
@property (weak, nonatomic) IBOutlet UILabel *icName;
@property (weak, nonatomic) IBOutlet UILabel *icCarPlate;
@property (weak, nonatomic) IBOutlet UILabel *icBrandOfCar;
@property (weak, nonatomic) IBOutlet UILabel *icModelOfCar;
@property (weak, nonatomic) IBOutlet UILabel *icYear;
@property (weak, nonatomic) IBOutlet UILabel *icPhone;
@property (weak, nonatomic) IBOutlet UILabel *icEmail;
@property (weak, nonatomic) IBOutlet UILabel *icAddress;
@property (weak, nonatomic) IBOutlet UILabel *icCarType;


@property (weak, nonatomic) IBOutlet UILabel *icDesc;
@property (weak, nonatomic) IBOutlet UILabel *icDocument;


@property (weak, nonatomic) IBOutlet UILabel *lblAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCarPlate;
@property (weak, nonatomic) IBOutlet UILabel *lblBrandOfCar;
@property (weak, nonatomic) IBOutlet UILabel *lblModelOfCar;
@property (weak, nonatomic) IBOutlet UILabel *lblYear;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblState;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblDocument;
@property (weak, nonatomic) IBOutlet UILabel *lblCarType;



@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtCarPlate;
@property (weak, nonatomic) IBOutlet UITextField *txtBrandOfCar;
@property (weak, nonatomic) IBOutlet UITextField *txtModelOfCar;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfCarType;

@property (weak, nonatomic) IBOutlet UITextField *txtDescValue;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDocument;


@property (weak, nonatomic) IBOutlet UIImageView *imgProfile1;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile2;


    @property (strong, nonatomic) IBOutlet UIImageView *TLC_image;
 
    @property (strong, nonatomic) IBOutlet UIImageView *Driver_licenseimage;
    @property (strong, nonatomic) IBOutlet UIImageView *Insurance_image;
    
    @property (strong, nonatomic) IBOutlet UIImageView *refistration_image;
    
    @property (strong, nonatomic) IBOutlet UIImageView *v_Permit_image;
    
    

@property (weak, nonatomic) IBOutlet UILabel *icStatus;
@property (weak, nonatomic) IBOutlet UILabel *icAccount;

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblAccount;

@property (weak, nonatomic) IBOutlet UITextField *txtStatus;
@property (weak, nonatomic) IBOutlet UITextField *txtAccount;




    @property (strong, nonatomic) IBOutlet UIButton *insurance_btn;

- (IBAction)onDocument:(id)sender;

@end
