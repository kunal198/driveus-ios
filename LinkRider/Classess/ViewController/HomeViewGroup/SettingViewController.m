//
//  SettingViewController.m
//  LinkRider
//
//  Created by Brst981 on 18/04/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import "SettingViewController.h"
@import GooglePlaces;
@interface SettingViewController ()<GMSMapViewDelegate>

@end

@implementation SettingViewController
{
    NSMutableArray<GMSAutocompletePrediction *> *data;
    GMSPlacesClient *_placesClient;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self customsetting];
    _placesClient = [GMSPlacesClient sharedClient];

    [[self navigationController] setNavigationBarHidden:YES animated:YES];
   // Office_Location.text
    
    NSString *str = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"officeAddress"];
    if (str != nil)
    {
        Office_Location.text = str;
    }
    
    NSString *str1=  [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"HomeAddress"];
    if (str1 != nil)
    {
        Home_location.text = str1;
    }
    
  
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)customsetting
{
    SWRevealViewController *revealViewController = self.revealViewController;

    if ( revealViewController )
    {
  
        if ( revealViewController )
        {
            [reveal_toggle addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        }
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
            }
}

- (IBAction)pickofficeLocation:(id)sender
{
  
    ishomeselect = FALSE;
    if ([Office_Location.text  isEqual: @"No Location"])
    {
        textvalue.text = @"";
         textvalue.placeholder = @"No Location";
    }
    else
    {
         textvalue.text = Office_Location.text;
    }
   
    popup_view.hidden=NO;
    popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  popup_view.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];

}



- (IBAction)pickhomelocation:(id)sender
{
    
    ishomeselect = TRUE;
    popup_view.hidden=NO;
    if ([Office_Location.text  isEqual: @"No Location"])
    {
        textvalue.text = @"";
        textvalue.placeholder = @"No Location";
    }
    else
    {
    textvalue.text = Home_location.text;
    }
    popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  popup_view.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];
}

- (IBAction)close_Action:(id)sender {
    
    
    popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    popup_view.hidden=YES;
 
    
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }                     completion:^(BOOL finished) {
                             
                             [UIView animateWithDuration:0.3/2 animations:^{
                                 popup_view.transform = CGAffineTransformIdentity;
                                 
                             }];
                         }];
                     }];
    
}



#pragma mark TEXTFIELD DELEGATE
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    // self.gMap.hidden = true;
    NSLog(@"%@",string);
    [self generateDataWithKey:textField.text];
    return YES;
    
}

#pragma mark MPGTextField Delegate Methods

- (NSArray *)dataForPopoverInTextField:(MPGTextField *)textField
{
    return data;
}


- (BOOL)textFieldShouldSelect:(MPGTextField *)textField
{
    return YES;
}

- (void)generateDataWithKey:(NSString *)key
{
    
    data = [[NSMutableArray alloc]init];
    [_placesClient autocompleteQuery:key bounds:nil filter:nil callback:^(NSArray<GMSAutocompletePrediction *> * _Nullable results, NSError * _Nullable error) {
        
        if (!error) {
            [data addObjectsFromArray:results];
            [textvalue provideSuggestions];
            
        }
        else
        {
            NSLog (@"error placing searching %@",error);
        }
        
    }];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    return YES;
}

- (void)textField:(MPGTextField *)textField didEndEditingWithSelection:(GMSAutocompletePrediction *)result
{
    
    if (!result) {
        return;
    }
    [_placesClient lookUpPlaceID:result.placeID callback:^(GMSPlace * _Nullable result, NSError * _Nullable error) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (ishomeselect == YES )
                {
                    
                    if ((result.coordinate.latitude!=0)||(result.coordinate.longitude!=0))
                    {
                    Home_location.text = result.formattedAddress;
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:result.formattedAddress forKey:@"HomeAddress"];
                    
                   // [defaults setDouble:result.coordinate.latitude forKey:@"homelatitude"];
                   // [defaults setDouble:result.coordinate.longitude forKey:@"homelogitude"];
                        
                        NSString *latitude = [NSString stringWithFormat:@"%f", result.coordinate.latitude];
                        NSString *longitude = [NSString stringWithFormat:@"%f", result.coordinate.longitude];
                        
                       // NSNumber *latitude = [NSNumber numberWithDouble:result.coordinate.latitude];
                       // NSNumber *longitude = [NSNumber numberWithDouble:result.coordinate.longitude ];
                        [defaults setObject:latitude forKey:@"homelatitude"];
                        [defaults setObject:longitude forKey:@"homelogitude"];
                        
                           [defaults synchronize];
                    
                    
                           // fromMarker = [GMSMarker markerWithPosition:result.coordinate];
                    
                    }
                    
                    
                }
                else{
                    
                    if (((ishomeselect == NO && result.coordinate.latitude!=0)||result.coordinate.longitude!=0))
                    {
                        
                        
                        Office_Location.text = result.formattedAddress;

                         [[NSUserDefaults standardUserDefaults] setObject:result.formattedAddress forKey:@"officeAddress"];
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        NSString *latitude = [NSString stringWithFormat:@"%f", result.coordinate.latitude];
                        NSString *longitude = [NSString stringWithFormat:@"%f", result.coordinate.longitude];
                        
                        [defaults setObject:latitude forKey:@"officelatitude"];
                        [defaults setObject:longitude forKey:@"officelogitude"];
                     
                        [defaults synchronize];
                        //                        [self.txtToName setText:result.formattedAddress];
                        //                        toMaker.map = nil;
                        //                        toMaker = [GMSMarker markerWithPosition:result.coordinate];
                        //                        toMaker.icon = [UIImage imageNamed:@"icon_toB"];
                        //                        toMaker.map = _gMap;
                        //                        [self zoomEndPlace];
                        //                        gTrip.endLocation = [[CustomPosition alloc] initWithPositon:result.coordinate addressName:result.formattedAddress];
                        //                        //                        [UIView transitionWithView:_office_view
                        //                        //                                          duration:0.4
                        //                        //                                           options:UIViewAnimationOptionTransitionCrossDissolve
                        //                        //                                        animations:^{
                        //                        //                                            _office_view.hidden = TRUE;
                        //                        //                                            _Home_view.hidden = TRUE;
                        //                        //                                            _searching_vieew.hidden = TRUE;
                        //                        //                                            _Gmap_opcontraint.constant = 84;
                        //                        //                                            //_Findingride_view.hidden =  false;
                        //                        //                                            _selectcar_view.hidden = false;
                        //                        //                                        }
                        //                        //                                        completion:NULL];
                        //                        //
                    }
                }
                //  self.gMap.userInteractionEnabled = TRUE;
                // [self showDirections];
            });
        }
    }];
    
    
}

- (IBAction)Navigate_Action:(id)sender
{
    
    
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isNavigate"];
    } else {
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNavigate"];
    }
   

    
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?center=%f,%f&q=%f,%f",mosqueLocation.latitude,mosqueLocation.longitude, mosqueLocation.latitude,mosqueLocation.longitude]];
//        [[UIApplication sharedApplication] openURL:url];
//    } else {
//        NSLog(@"Can't use comgooglemaps://");
//    }
    
    
}






@end
