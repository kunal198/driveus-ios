//
//  OrderConfirmViewController.h
//  LinkRider
//
//  Created by Hicom on 6/26/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+FontAwesome.h"
#import <MapKit/MapKit.h>
#import "CBAutoScrollLabel.h"
#import "LocationManager.h"
#import "ProfileHasActionView.h"
#import <GoogleMaps/GoogleMaps.h>
@interface OrderConfirmViewController : UIViewController <UIAlertViewDelegate,UIAlertViewDelegate>{
    NSTimer *timer;

}

@property (weak, nonatomic) IBOutlet ProfileHasActionView *profileView;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet GMSMapView *gMap;

@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblCarePlateValue;

@property (weak, nonatomic) IBOutlet UIButton *btnCancelTrip;

@property (weak, nonatomic) IBOutlet UILabel *lblDistanceValue;

@property (weak, nonatomic) IBOutlet UIImageView *carImg;
@property (weak, nonatomic) IBOutlet UILabel *lblHelp;

- (IBAction)onCancelTrip:(id)sender;





@end
