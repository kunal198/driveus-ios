//
//  OrderConfirmViewController.m
//  LinkRider
//
//  Created by Hicom on 6/26/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "OrderConfirmViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "UIFont+FontAwesome.h"
#import "NSString+FontAwesome.h"
#import "MyPinAnnotationView.h"
#import <AudioToolbox/AudioServices.h>
@interface OrderConfirmViewController ()<GMSMapViewDelegate>

@end

@implementation OrderConfirmViewController{
    GMSMarker *fromMarker, *toMaker, *carMaker;
    GMSPolyline *singleLine;
    BOOL isloading, didDirectToA;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [self configMapView];
    isloading = false;
    didDirectToA = false;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(driverCancelTrip) name:CANCEL_TRIP_KEY object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEndTrip) name:END_TRIP_KEY object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(driverArrived) name:DRIVER_ARRIVED_KEY object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBecomForceGround) name:WILL_ENTER_FORCE_GROUND object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onStartTrip) name:START_TRIP_KEY object:nil];
    [self.navigationController setNavigationBarHidden:YES];
    [self getTripDetail];
    
}

- (void)configMapView{
    
    _gMap.myLocationEnabled = true;
    _gMap.camera = [GMSCameraPosition cameraWithLatitude:gShareLocation.myLocation.latitude longitude:gShareLocation.myLocation.longitude zoom:16];
    _gMap.delegate = self;
    toMaker = [GMSMarker markerWithPosition:gTrip.endLocation.position];
    toMaker.icon = [UIImage imageNamed:@"icon_toB"];
    fromMarker = [GMSMarker markerWithPosition:gTrip.startLocation.position];
    fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
    toMaker.map = _gMap;
    fromMarker.map = _gMap;

}

- (void)appWillBecomForceGround{
    if ([[Util topViewController] isMemberOfClass:[OrderConfirmViewController class]]) {
        [ModelManager showMyTripsWithSuccess:^(NSMutableArray *arr) {
            [self  setupHelpBtnWhenStatusChange];
            [self updateDistance];
            timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateDistance) userInfo:nil repeats:YES];
        } andFailure:^(NSString *err) {
            
        }];
    }
}

- (IBAction)onCurrentLocation:(id)sender {
    [_gMap animateToLocation:gShareLocation.myLocation];
    
}

//- (void)driverArrived{
//    
//    if (gTrip.status == approadching) {
//      
//        _lblStatus.text = @"DRIVER ARRIVED";
////        _lblHelp.text = @"HELP";
//        self.btnCancelTrip.hidden = true;
//       // [self.btnCancelTrip setTitle:[NSString fontAwesomeIconStringForEnum:FAPhone] forState:UIControlStateNormal];
//        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
//        [self.view makeToast:@"Your driver has been arrived!"];
//        
//    }
//    
//}

-(void)onEndTrip{
    gTrip.status = pendingPayment;
    [self performSegueWithIdentifier:@"endTrip" sender:self];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    [self.navigationController setNavigationBarHidden:YES];
    gShareLocation.updateOn = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [[UIApplication sharedApplication] setIdleTimerDisabled: NO];
    // gShareLocation.updateOn = NO;
    
    [timer invalidate];
    [super viewWillDisappear:YES];
    
}

-(void)getTripDetail{
    
    [ModelManager showMyTripsWithSuccess:^(NSMutableArray *arr) {
        [self updateInfo];
        if (gTrip.status == approadching) {
            
            return;
        }
        else if(gTrip.status == inprogress){
            [self onStartTrip];
            return;
        }
        else  if (gTrip.status == pendingPayment) {
            [self onEndTrip];
            return;
        }
    } andFailure:^(NSString *err) {
        
    }];
    
}

- (void)updateInfo{
    if (gTrip.tripId.length>0) {
        [ModelManager showTripDetailWithTripId:gTrip.tripId withSuccess:^(Trips *trip) {
            gTrip = trip;
            [Util setObject:gTrip.tripId forKey:CURRENT_TRIP_ID_KEY];
            if (self) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setText123];
                });
            }
        } andFailure:^(NSString *err) {
        }];
        
    }
}

-(void)updateDistance{
    NSLog(@"will update distance");
    if (gTrip.tripId.length > 0 && !isloading) {
        isloading = true;
        
          
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
          
            
            [ModelManager getCurrentLocationWithDriverId:gTrip.driverId withSuccess:^(float latitude, float longtitude) {
                CLLocation *driverLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longtitude];
                
                if (driverLocation)
                {
                isloading = false;
                if (gTrip.status ==approadching && !didDirectToA) {
                    didDirectToA = true;
                    [self drawDirectionFrom:driverLocation.coordinate to:gTrip.startLocation.position];
                    NSLog(@"Did update distance:%.2f %.2f", driverLocation.coordinate.latitude, driverLocation.coordinate.longitude);
                }
                    [self makeDirectionWithCurrentLocation:driverLocation.coordinate];
                }
            }
            
             
             failure:^(NSString *err) {
                isloading = false;
            }];
        });
        
    }
}
                       
-(void)driverCancelTrip{
    [Util showMessage:[Util localized:@"msg_trip_cancel_by_driver"] withTitle:[Util localized:@"app_name"]];
    
    gTrip = [[Trips alloc] init];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
}

-(void)onStartTrip{
    gTrip.status = inprogress;
    [self setupHelpBtnWhenStatusChange];
    //    [timer invalidate];
    
}

- (void)setText123{
    
    
//    [self.profileView setProfileWithAvatarLink:gTrip.driver.imageDriver phoneNumber:gTrip.driver.phone starValue:gTrip.driver.driverRate name:gTrip.driver.driverName];
    
    
    [self.profileView setProfileWithAvatarLink:gTrip.driver.imageDriver phoneNumber:gTrip.driver.phone starValue:gTrip.driver.rateup downvalue:gTrip.driver.ratedown name:gTrip.driver.driverName];
    
    
    
    
    [self setCarPlatetextWithAttribute];
    [self setDistancetext:[Util localized:@"lbl_calculating"]];
    [self.carImg setImageWithURL:[NSURL URLWithString:gTrip.driver.carImage]];
    _lblStatus.text = @"DRIVER IS COMING";
    [self setupHelpBtnWhenStatusChange];
    [self updateDistance];
    timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateDistance) userInfo:nil repeats:YES];
}

- (void)setCarPlatetextWithAttribute{
    NSString *prefixPlate = [Util localized:@"lbl_car_plate"];
    NSString *contentPlate = gTrip.driver.carPlate.length? gTrip.driver.carPlate : [Util localized:@"lbl_loading"];
    NSString *infoString = [NSString stringWithFormat:@"%@%@", prefixPlate, contentPlate];
    
    NSMutableAttributedString *attStringPlate=[[NSMutableAttributedString alloc] initWithString:infoString];
    
    //  UIFont *font_regular=[UIFont fontWithName:@"Raleway-Medium" size:14.0f];
    UIFont *font_bold=[UIFont fontWithName:@"Raleway-Bold" size:14.0f];
    
    [attStringPlate addAttribute:NSFontAttributeName value:font_bold range:NSMakeRange(0, prefixPlate.length)];
    [self.lblCarePlateValue setAttributedText:attStringPlate];
}

- (void)setDistancetext:(NSString*)contentText{
    NSString *prefixPlate = [Util localized:@"lbl_distance"];
    
    NSString *infoString = [NSString stringWithFormat:@"%@%@", prefixPlate, contentText];
    
    NSMutableAttributedString *attStringPlate=[[NSMutableAttributedString alloc] initWithString:infoString];
    
    //  UIFont *font_regular=[UIFont fontWithName:@"Raleway-Medium" size:14.0f];
    UIFont *font_bold=[UIFont fontWithName:@"Raleway-Bold" size:14.0f];
    
    [attStringPlate addAttribute:NSFontAttributeName value:font_bold range:NSMakeRange(0, prefixPlate.length)];
    [self.lblDistanceValue setAttributedText:attStringPlate];
    
}

-(void)onCall{
    [Util callPhoneNumber:gUser.phone];
}
- (IBAction)onCancelTrip:(id)sender {
    if (gTrip.status != inprogress && ![_lblStatus.text isEqualToString:@"DRIVER ARRIVED"]) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ModelManager cancelTrip:gTrip withSuccess:^{
            
            
            [Util showMessage:[Util localized:@"msg_cancel_success"] withTitle:[Util localized:@"app_name"]];
            
            gTrip = [[Trips alloc]init];
            [Util setObject:@"" forKey:CURRENT_TRIP_ID_KEY];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
        } andFailure:^(NSString *err) {
            if (err.length==0) {
                [Util showMessage:[Util localized:@"msg_cancel_failed"] withTitle:[Util localized:@"app_name"]];
            }
            else{
                [Util showMessage:err withTitle:[Util localized:@"app_name"]];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
        //    });
        
    }else{
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [ModelManager sendHelpRequestWithSuccess:^(NSString *strSuccessReturn) {
                
            } failure:^(NSString *err) {
                
            }];
        });
        [Util callPhoneNumber:[AppSettings share].adminPhoneNumber];
        
    }
}

- (void)makeDirectionWithCurrentLocation:(CLLocationCoordinate2D)currentLocation1{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        MKPlacemark *placemarkTo;
        if (gTrip.status != inprogress) {
            placemarkTo = [[MKPlacemark alloc] initWithCoordinate:gTrip.startLocation.position addressDictionary:nil] ;
            
        }else{
            placemarkTo = [[MKPlacemark alloc] initWithCoordinate:gTrip.endLocation.position addressDictionary:nil] ;
        }
        
        [self updateDistanceWithGoogleDirec:currentLocation1 toPoint:placemarkTo.coordinate];
    });
    
}

- (void)updateDistanceWithGoogleDirec:(CLLocationCoordinate2D)fromPoint toPoint:(CLLocationCoordinate2D)toPoint{
    carMaker.map = nil;
    carMaker = [GMSMarker markerWithPosition:fromPoint];
    carMaker.icon = [UIImage imageNamed:@"img_car"];
    carMaker.map = _gMap;
    
    [ModelManager getRoutingByGoogleWithStart:fromPoint endLocation:toPoint handlerSuccess:^(NSDictionary *dicLegsReturn) {
        float distance = [Validator getSafeFloat:dicLegsReturn[@"routes"][0][@"legs"][0][@"distance"][@"value"]];
        float expectedTravelTime = [Validator getSafeFloat:dicLegsReturn[@"routes"][0][@"legs"][0][@"duration"][@"value"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (gTrip.status != inprogress) {
                if (distance < 1000) {
                    [self setDistancetext:[NSString stringWithFormat:@"Driver %.1f m to A (%.0f min)", distance, expectedTravelTime/60]];
                }else{
                    [self setDistancetext:[NSString stringWithFormat:@"Driver %.1f km to A (%.0f min)", distance/1000,expectedTravelTime/60]];
                }
                
            }else{
                if (distance < 1000) {
                    [self setDistancetext:[NSString stringWithFormat:@"Trip %.1f m to B (%.0f min)", distance, expectedTravelTime/60]];
                    
                }else{
                    [self setDistancetext:[NSString stringWithFormat:@"Trip %.1f km to B (%.0f min)", distance/1000, expectedTravelTime/60]];
                    
                }
            }
        });
        
    } handlerFailed:^(NSString *err) {
        
    }];
    
}

- (void)setupHelpBtnWhenStatusChange{
    
    
    if (gTrip.status != inprogress) {
        _lblStatus.text = @"DRIVER IS COMING";
        [self.btnCancelTrip setTitle:[NSString fontAwesomeIconStringForEnum:FALongArrowLeft] forState:UIControlStateNormal];
        _lblHelp.text = @"CANCEL";

    }else{
        _lblHelp.text = @"HELP";
        _lblStatus.text = [Util localized:@"lbl_in_process"].uppercaseString;
        [self drawDirectionFrom:gTrip.startLocation.position to:gTrip.endLocation.position];
        [self.btnCancelTrip setTitle:[NSString fontAwesomeIconStringForEnum:FAPhone] forState:UIControlStateNormal];
        
    }
}

- (void)drawDirectionFrom:(CLLocationCoordinate2D)fromLocation to:(CLLocationCoordinate2D)toLocation{
    
    [ModelManager getRoutingByGoogleWithStart:fromLocation endLocation:toLocation handlerSuccess:^(NSDictionary *response) {
        singleLine.map = nil;
        GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
        singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 2;
        singleLine.strokeColor = [UIColor blueColor];
        dispatch_async(dispatch_get_main_queue(), ^{
             singleLine.map = self.gMap;
        });
       
    } handlerFailed:^(NSString *err) {
        
    }];
}




@end
