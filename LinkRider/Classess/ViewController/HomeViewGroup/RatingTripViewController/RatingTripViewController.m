//
//  RatingTripViewController.m
//  LinkRider
//
//  Created by Hicom on 6/26/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "RatingTripViewController.h"
#import "NSString+FontAwesome.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "NSString+FontAwesome.h"
#import "UIFont+FontAwesome.h"
#import <MessageUI/MessageUI.h>
#import "UIKeyboardViewController.h"
@interface RatingTripViewController ()<STPAddCardViewControllerDelegate,MFMailComposeViewControllerDelegate,UIKeyboardViewControllerDelegate,UITextFieldDelegate>
{
    
    IBOutlet UIButton *btnTip10Outlet;
    
    IBOutlet UITextField *txtCustomTip;
    IBOutlet UIButton *btnTip20Outlet;
    IBOutlet UIButton *btnTip15Outlet;
    
    int tip;
    UIKeyboardViewController *keyboard;
    
}
@end

@implementation RatingTripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [self.btnCallAdmin setTitle:[NSString fontAwesomeIconStringForEnum:FAPhone] forState:UIControlStateNormal];
    _viewHelp.layer.borderColor = [UIColor whiteColor].CGColor;
    _viewHelp.layer.borderWidth = 1;
    // Do any additional setup after loading the view.
    isRate = NO;
     [self showTripDetail];
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
    txtCustomTip.delegate = self;
    tip = 0;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationItem setHidesBackButton:NO animated:YES];
    
}

-(void)setupPaypal{
    
    _payPalConfig = [[PayPalConfiguration alloc] init];
//#if HAS_CARDIO
//    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
//    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
//    // for more details.
//    _payPalConfig.acceptCreditCards = YES;
//#else
//    _payPalConfig.acceptCreditCards = NO;
//#endif
    _payPalConfig.acceptCreditCards = NO;
    _payPalConfig.merchantName = [NSString stringWithFormat:@"%@ %@",[Util localized:@"app_name"],[Util localized:@"lbl_payment"]];
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    self.environment = kPayPalEnvironment;
    
    
}
- (BOOL)acceptCreditCards {
    return self.payPalConfig.acceptCreditCards;
}

- (void)setAcceptCreditCards:(BOOL)acceptCreditCards {
    self.payPalConfig.acceptCreditCards = acceptCreditCards;
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}
#pragma mark - MailView Delegate Methods
-(void)ShareWithEmail
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        [mailController setMailComposeDelegate:self];
        [mailController setSubject:@"Drivus Help"];
        [mailController setToRecipients:[NSArray arrayWithObjects:@"Help@Drivus.com", @"email2", nil]];
        NSString *temp =[NSString stringWithFormat:@"Please Wite Message here wat kind of help you need"];
        [mailController setMessageBody:temp isHTML:NO];
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please setup mail in your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];    }
}

-(void)setText{
    [self.lblHeaderTitle setTitle:[Util localized:@"header_your_trip"]];
    self.lblTripNeed.text = [NSString stringWithFormat:@"%@%@", [Util localized:@"currentcy"], gTrip.actualFare];
    
    _ratingStar.backgroundColor  = [UIColor lightTextColor];
    _ratingStar.starImage = [[UIImage imageNamed:@"ic_star_NotSelect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingStar.starHighlightedImage = [[UIImage imageNamed:@"ic_star"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [_ratingStar setMaxRating:5.0];
    [_ratingStar setDelegate:self];
    [_ratingStar setHorizontalMargin:1.0];
    [_ratingStar setEditable:YES];
    [_ratingStar setDisplayMode:EDStarRatingDisplayHalf];
    [_ratingStar setRating:5];
    [_ratingStar setNeedsDisplay];
    
    

    
    
    [self.profileView setProfileWithAvatarLink:gTrip.driver.imageDriver phoneNumber:gTrip.driver.phone starValue:gTrip.driver.rateup down_value:gTrip.driver.ratedown name:gTrip.driver.driverName type:1];
    
    NSDictionary *fontDict = @{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:10],NSForegroundColorAttributeName:[Util colorWithHexString:@"#e8c03a"]};
    NSMutableAttributedString *att =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict];
    
    [att appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict]];
    
    [att appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict]];
    
    [att appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict]];
    
    [att appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict]];
    
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: STEP_TITLE_FONT forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[Util localized:@"finished_title"]] attributes: arialDict];
    
    [att appendAttributedString:aAttrString];
    
    
    self.lblTitle.attributedText =att;
    
    
    self.lblFromName.font = self.lblSeatNum.font;
    self.lblToName.font = self.lblSeatNum.font;
    self.lblFromName.textColor = [UIColor blackColor];
    self.lblToName.textColor = [UIColor blackColor];
    
    
    self.lblSeats.text = [Util localized:@"lbl_seats"];
    self.lblFromA.text = [Util localized:@"lbl_from_a"];
    self.lblToB.text = [Util localized:@"lbl_to_b"];
       
    self.lblSeatNum.text = [DriverCar typeNameByTypeId:gTrip.link];
    
    self.lblFromName.text = gTrip.startLocation.address;
    self.lblToName.text = gTrip.endLocation.address;
    self.lblCar.text = [Util localized:@"lbl_car"];
    
    self.lblDistanceValue.text = [Util localized:@"lbl_trip_finished"];
    
    self.lblPoint.text = [Util localized:@"lbl_fare"];
    self.lblRate.text = [Util localized:@"lbl_rate"];
    [self.btnRate setTitle:[Util localized:@"lbl_rate"].uppercaseString forState:UIControlStateNormal];
    [self.btnSend setTitle:[Util localized:@"lbl_payment"].uppercaseString forState:UIControlStateNormal];
    self.lblCarePlateValue.text = gTrip.driver.carPlate;
    [self.carImg setImageWithURL:[NSURL URLWithString:gTrip.driver.carImage]];
   
}


-(void)showTripDetail{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager showTripDetailWithTripId:gTrip.tripId withSuccess:^(Trips *trip) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                gTrip = trip;
                [Util setObject:gTrip.tripId forKey:CURRENT_TRIP_ID_KEY];
                [self setText];
                
                
            });
            
            notificationsDic = nil;
        } andFailure:^(NSString *err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                //                [self showTripDetail];
            });
            
        }];
    });
}

- (IBAction)onSend:(id)sender {
    if (isRate) {
        [self onPayment];
    }
    else{
        [self.view makeToast:[Util localized:@"msg_rate_missing"]];
    }
}

- (IBAction)onRate:(id)sender

{
//    if (_ratingStar.rating == 0) {
//        [self.view makeToast:@"Rate value has to exceed than 0"];
//        return;
//    }
    
    if (![txtCustomTip.text isEqualToString: @""]) {
        tip = [txtCustomTip.text intValue];
    }
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
   // [ModelManager rateDriverWithRate:self.ratingStar.rating*2 Success:^{
          
          [ModelManager rateDriverWithRate:rateup andratedown:ratedown andTip:tip Success:^{
     
        dispatch_async(dispatch_get_main_queue(), ^{
       
        [self.view makeToast:[Util localized:@"msg_rate_trip_success"]];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        isRate = YES;
            [self onPayment];
        });
    } andFailure:^(NSString *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.view makeToast:[Util localized:@"msg_error"]];
        });
    }];
    });
}

-(void)onPayment

{
    float price = [gUser.point floatValue] - [gTrip.actualFare floatValue];
    if (price > 0) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        [ModelManager tripPaymentWithSuccess:^{
            [Util removeObjectForKey:CURRENT_TRIP_ID_KEY];
            [self.view makeToast:[Util localized:@"msg_end_trip"]];
            gTrip = [[Trips alloc]init];
            [ModelManager getUserProfileWithSuccess:^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [self.navigationController popToRootViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
                
            } failure:^(NSString *err) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }];
        } andFailure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
           
            //        [Util showMessage:err withTitle:[Util localized:@"app_name"] andDelegate:self];
        }];

    }else{
     [self showAlertSelectPaymentMethod];
    }
       //    });
}

- (void)showAlertSelectPaymentMethod{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Payment"
                                 message:@"Please select the payment method!"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Add Buttons
    
    UIAlertAction* paypal = [UIAlertAction
                                actionWithTitle:@"Payment via Paypal"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self paymentWithPaypal];
                                }];
    
    UIAlertAction* creditCard = [UIAlertAction
                               actionWithTitle:@"Payment by Cards"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [self paymentWithCreditCard];
                                   //Handle no, thanks button
                               }];
    
    UIAlertAction* noButton = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action) {
                                     //Handle no, thanks button
                                 }];
    
    //Add your buttons to alert controller
    
    [alert addAction:paypal];
    [alert addAction:creditCard];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)paymentWithPaypal{
    
    [self setupPaypal];
    [self setPayPalEnvironment:_environment];
    
    float price = [gTrip.actualFare floatValue] - [gUser.point floatValue] + 0.5;
    PayPalItem *item1 = [PayPalItem itemWithName:[NSString stringWithFormat:@"%@ %@",[Util localized:@"app_name"],[Util localized:@"lbl_payment"]]
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",price]]
                                    withCurrency:@"USD"
                                         withSku:@"Hip-00037"];
    
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.shortDescription = [NSString stringWithFormat:@"%@ %@",[Util localized:@"app_name"],[Util localized:@"lbl_payment"]];
    payment.items = items;
    payment.paymentDetails = paymentDetails;
    
    if (!payment.processable) {
        
    }
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
    
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    [self sendCompletedPaymentToServer:completedPayment];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Authorize Profile Sharing

- (IBAction)getUserAuthorizationForProfileSharing:(id)sender {
    
    NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
    
    PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:self.payPalConfig delegate:self];
    [self presentViewController:profileSharingPaymentViewController animated:YES completion:nil];
}


#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    
    float price = [gTrip.actualFare floatValue] - [gUser.point floatValue] + 0.5;
    NSDictionary *payment = [completedPayment.confirmation objectForKey:@"response"];
    if ([payment isKindOfClass:[NSDictionary class]]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager pointExchangewithAmount:[NSString stringWithFormat:@"%.2f",price]transactionId:[payment objectForKey:@"id"] andPaymentMethod:@"1" withSuccess:^{
            [self.view makeToast:[Util localized:@"msg_payment_success"]];
            gUser.point = [NSString stringWithFormat:@"%.2f", [gUser.point floatValue] + price];
            [self onPayment];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        } andFailure:^(NSString *err) {
            [self.view makeToast:[Util localized:@"msg_error"]];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
        //        });
    }
}

#pragma mark -> Payment with CreditCard
- (void)paymentWithCreditCard{
    STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
    addCardViewController.delegate = self;
    // STPAddCardViewController must be shown inside a UINavigationController.
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark STPAddCardViewControllerDelegate

- (void)addCardViewControllerDidCancel:(STPAddCardViewController *)addCardViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"CARD CANCELED");
}

- (void)addCardViewController:(STPAddCardViewController *)addCardViewController
               didCreateToken:(STPToken *)token
                   completion:(STPErrorBlock)completion {
    NSLog(@"%@",token.tokenId);
    float price = [gTrip.actualFare floatValue] - [gUser.point floatValue] + 0.5;
    [ModelManager addToAccountAmount:[NSString stringWithFormat:@"%.2f",price] withStripToken:token.tokenId withSuccess:^(NSString *strSuccess) {
        [ModelManager pointExchangewithAmount:[NSString stringWithFormat:@"%.2f",price] transactionId:token.tokenId andPaymentMethod:@"1" withSuccess:^{
            [self dismissViewControllerAnimated:YES completion:^{
                gUser.point = [NSString stringWithFormat:@"%.2f", [gUser.point floatValue] + price];
                [self onPayment];
            }];
        
        } andFailure:^(NSString *err) {
            [self dismissViewControllerAnimated:YES completion:^{
                [self.view makeToast:[Util localized:@"msg_error"]];
            }];
        }];
        
    } failure:^(NSString *err) {
        completion(nil);
    }];

}

- (IBAction)onCallAdmin:(id)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager sendHelpRequestWithSuccess:^(NSString *strSuccessReturn) {
            
        } failure:^(NSString *err) {
            
        }];
    });
    
    [self ShareWithEmail];
    //[Util callPhoneNumber:[AppSettings share].adminPhoneNumber];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (IBAction)Rate_Action:(id)sender
{
    if ([sender tag]== 0)
    {
        [self.Rate_2_btn setSelected:false];
        ratedown = 0;
        if (![sender isSelected])
        {
            [sender setSelected:true];
            rateup = 1;
            
            [_Rate_1_btn setImage:[UIImage imageNamed:@"thm"] forState: UIControlStateSelected];
            [_Rate_2_btn setImage:[UIImage imageNamed:@"thm2-1"] forState: UIControlStateNormal];
        }
        else
        {
            [sender setSelected:false];
             rateup = 0;
            [_Rate_1_btn setImage:[UIImage imageNamed:@"Th_un"] forState: UIControlStateNormal];
            [_Rate_2_btn setImage:[UIImage imageNamed:@"thm2-1"] forState: UIControlStateNormal];
        }
        
    }
    else if ([sender tag]==1)
    {
        [self.Rate_1_btn setSelected:false];

         rateup = 0;
        if (![sender isSelected])
        {
            [sender setSelected:true];
            ratedown = 1;
            
            [_Rate_2_btn setImage:[UIImage imageNamed:@"thm-1"] forState: UIControlStateSelected];
             [_Rate_1_btn setImage:[UIImage imageNamed:@"Th_un"] forState: UIControlStateNormal];
        }
        else
        {
            [sender setSelected:false];
            ratedown = 0;
            [_Rate_2_btn setImage:[UIImage imageNamed:@"thm2-1"] forState: UIControlStateNormal];
            [_Rate_1_btn setImage:[UIImage imageNamed:@"Th_un"] forState: UIControlStateNormal];
        }
    }
    
}
#pragma mark
#pragma mark - tipButtons Action

- (IBAction)btnTip1oAction:(id)sender {
    [btnTip10Outlet setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnTip15Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btnTip20Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];   txtCustomTip.text = @"";
    tip = 10;
    
}
- (IBAction)btnTip15Action:(id)sender {
    
    [btnTip15Outlet setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnTip10Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btnTip20Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
      txtCustomTip.text = @"";
    tip = 15;

}
- (IBAction)btnTip20Action:(id)sender {
    
    [btnTip20Outlet setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnTip10Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btnTip15Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    txtCustomTip.text = @"";
    tip = 20;

    
}

#pragma mark TEXTFIELD DELEGATE
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    if (textField == txtCustomTip) {
        
        [btnTip10Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [btnTip15Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [btnTip20Outlet setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 2;
        
    }
    return true;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
   
        return true;
}
    

@end
