//
//  RatingTripViewController.h
//  LinkRider
//
//  Created by Hicom on 6/26/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "CBAutoScrollLabel.h"
#import "EDStarRating.h"
#import "ProfileView.h"
@interface RatingTripViewController : UIViewController<EDStarRatingProtocol, UIAlertViewDelegate,PayPalPaymentDelegate, PayPalProfileSharingDelegate>{
    BOOL isRate;
    
    int rateup;
    int ratedown;
}

@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet EDStarRating *ratingStar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTripNeed;

@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *btnRate;

@property (weak, nonatomic) IBOutlet ProfileView *profileView;

@property (nonatomic) float rating_value;
@property (weak, nonatomic) IBOutlet UILabel *lblSeats;
@property (weak, nonatomic) IBOutlet UILabel *lblFromA;
@property (weak, nonatomic) IBOutlet UILabel *lblToB;
@property (weak, nonatomic) IBOutlet UILabel *lblPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;


@property (weak, nonatomic) IBOutlet UILabel *lblCarPlate;
@property (weak, nonatomic) IBOutlet UILabel *lblCar;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *lblSeatNum;

@property (weak, nonatomic) IBOutlet UILabel *lblCarePlateValue;


@property (weak, nonatomic) IBOutlet UILabel *lblDistanceValue;

@property (weak, nonatomic) IBOutlet UIImageView *carImg;


@property (weak, nonatomic) IBOutlet UILabel *lblFromName;
@property (weak, nonatomic) IBOutlet UILabel *lblToName;

@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

- (IBAction)onSend:(id)sender;
- (IBAction)onRate:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnCallAdmin;
@property (weak, nonatomic) IBOutlet UIView *viewHelp;

@property (strong, nonatomic) IBOutlet UIButton *Rate_1_btn;
@property (strong, nonatomic) IBOutlet UIButton *Rate_2_btn;

@end
