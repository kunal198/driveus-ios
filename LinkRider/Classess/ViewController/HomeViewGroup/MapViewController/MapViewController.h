//
//  MapViewController.h
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MPGTextField.h"
#import "WaitingViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ProfileHasActionView.h"
#import "UIKeyboardViewController.h"
#import <MessageUI/MessageUI.h>
@interface MapViewController : UIViewController<MFMessageComposeViewControllerDelegate,UIKeyboardViewControllerDelegate,UITextFieldDelegate,GMSMapViewDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate,MPGTextFieldDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate>
{
    
    
    UIKeyboardViewController *keyboard;
//    MKPointAnnotation *fromPoint;
//    MKPointAnnotation *toPoint;
    
    
    MKUserLocation *currentLocation;
    BOOL isEdittingTfFrom, isEdittingTfTo;
    float estimateFare;
    BOOL reloadsearch;
    BOOL comefromwaiting;
    int numberDriversSent;
        NSTimer *timer;
    NSTimer *updatetimer;
    BOOL iscalled;
    BOOL isiconselect;
    BOOL issearchingdriver ;
    BOOL firsttime;
    BOOL arrive;
    
    BOOL fromlocation;
    BOOL tolocation;
    BOOL getdestLocation;
    
    BOOL moveAlocation;
    BOOL moveBlocation;
    BOOL routestrue;
    int routeno;
    BOOL ontrip;
    IBOutlet UIView *table_backview;
    
    IBOutlet NSLayoutConstraint *Hight_constraint;
    
    CLLocation *cordines;
    
    IBOutlet UIButton *cancel_trip;
    
    IBOutlet NSLayoutConstraint *Heightconstraint;
    
    CLLocation *tocordlocation;
    CLLocation *fromcordlocation;
     CLLocation *nearcordlocatio ;
    
    NSMutableArray *locationhistory;
    NSMutableArray *locationcordhistory;
    
    
    
    IBOutlet UIButton *Addroutebtn;
    
    
    
    IBOutlet UIImageView *firstroutedash;
    IBOutlet UIImageView *firstroutedestimg;
    
    IBOutlet UIView *secrouteview;
    IBOutlet UILabel *secrouteLabel;
    IBOutlet UIImageView *secroutedash;
    IBOutlet UIImageView *secroutedestimg;
    
    IBOutlet UIView *thirdrouteview;
    IBOutlet UILabel *thirdrouteLabel;
    IBOutlet UIImageView *thirdroutedash;
    IBOutlet UIImageView *thirdroutedestimg;
    
    
    IBOutlet UIView *loc_searchview;
    
    IBOutlet UIView *fourthrouteview;
    IBOutlet UILabel *fourthrouteLabel;
    IBOutlet UIImageView *fourthroutedash;
    IBOutlet UIImageView *fourthroutedestimg;
    
    IBOutlet UIButton *firstCrossBtn;
    IBOutlet UIButton *secCrossBtn;
    IBOutlet UIButton *thirdCrossBtn;
    IBOutlet UIButton *fourthCrossBtn;
    
}
@property (weak, nonatomic) IBOutlet UINavigationItem *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet GMSMapView *gMap;

@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet MPGTextField *txtFromName;
@property (strong, nonatomic) IBOutlet UIButton *btnLocationFrom;
    @property (weak, nonatomic) IBOutlet UIButton *btnBookTrip;

@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet MPGTextField *txtToName;
@property (strong, nonatomic) IBOutlet UIButton *btnLocationTo;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnRefresh;

@property (weak, nonatomic) IBOutlet UIButton *btn1seat;
@property (weak, nonatomic) IBOutlet UIButton *btn2seat;
@property (weak, nonatomic) IBOutlet UIButton *btn3seat;
@property (weak, nonatomic) IBOutlet UILabel *bgFrom;
@property (weak, nonatomic) IBOutlet UILabel *bgTo;

@property (strong, nonatomic) IBOutlet UIImageView *menu_img;
@property (strong, nonatomic) IBOutlet UIButton *menu;
@property (strong, nonatomic) IBOutlet UIButton *revealButtonItem;

@property (strong, nonatomic) IBOutlet UILabel *mapview_header;

@property (strong, nonatomic) IBOutlet UIButton *Backbtn;
@property (strong, nonatomic) IBOutlet UILabel *Addroute_Label;


- (IBAction)onFromLocation:(id)sender;
- (IBAction)onToLocation:(id)sender;
- (IBAction)onSelectSeat:(id)sender;
- (IBAction)onRefresh:(id)sender;
- (IBAction)Addmultipleroute:(id)sender;




@property (strong, nonatomic) IBOutlet UIView *Maprout_view;


//  Ist step
@property (strong, nonatomic) IBOutlet UIView *office_view;
@property (strong, nonatomic) IBOutlet UIView *Home_view;
@property (strong, nonatomic) IBOutlet UIView *searching_vieew;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *Gmap_opcontraint;

//Finding Ride
@property (strong, nonatomic) IBOutlet UIView *Findingride_view;



@property (strong, nonatomic) IBOutlet MPGTextField *route_textfeild;


//secondTask
@property (strong, nonatomic) IBOutlet UIView *selectcar_view;
@property (strong, nonatomic) IBOutlet UILabel *estimated_fare;
@property (strong, nonatomic) IBOutlet UIButton *First_car;
@property (strong, nonatomic) IBOutlet UIButton *second_car;
@property (strong, nonatomic) IBOutlet UIButton *Third_car;

// when select start or End Point 
@property (strong, nonatomic) IBOutlet UIView *meetlocation_view;
@property (strong, nonatomic) IBOutlet UIView *connectingdrivers;
@property (strong, nonatomic) IBOutlet UILabel *connectingdriver_Label;
@property (strong, nonatomic) IBOutlet UIImageView *icon_img;

@property (strong, nonatomic) IBOutlet UIView *Request_view;
@property (strong, nonatomic) IBOutlet UILabel *request_label;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottom_layout;

@property (strong, nonatomic) IBOutlet UIView *Main_requestview;
@property (strong, nonatomic) IBOutlet UIView *driver_view;
@property (strong, nonatomic) IBOutlet UIView *driver_mainview;
@property (strong, nonatomic) IBOutlet UIView *DatePicker_view;
@property (strong, nonatomic) IBOutlet UILabel *Pickup_Location;
@property (strong, nonatomic) IBOutlet UIDatePicker *date_picker;
@property (weak, nonatomic) IBOutlet MPGTextField *route_Location;
@property (weak, nonatomic) IBOutlet ProfileHasActionView *profileView;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblCarePlateValue;

@property (weak, nonatomic) IBOutlet UIButton *btnCancelTrip;

@property (weak, nonatomic) IBOutlet UILabel *lblDistanceValue;

@property (weak, nonatomic) IBOutlet UIImageView *carImg;
@property (weak, nonatomic) IBOutlet UILabel *lblHelp;

- (IBAction)getcurrentlocation:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *Locations_option;
@property (strong, nonatomic) IBOutlet UIView *locItemsView;
@property (strong, nonatomic) IBOutlet UITableView *recent_Locations;

@property (strong, nonatomic) IBOutlet UIButton *Donoe_btn;
    
    @property (strong, nonatomic) IBOutlet NSLayoutConstraint *Height_constraint;
    

@property (strong, nonatomic) IBOutlet UIButton *Booknow_btn;
@property (strong, nonatomic) IBOutlet UIButton *BookLatr_btn;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loc_searchViewPos;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchingViewPos;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelOne_Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelTwo_Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelThree_Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelFour_Height;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *optionTableViewPos;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recentLocationPos;

@end
