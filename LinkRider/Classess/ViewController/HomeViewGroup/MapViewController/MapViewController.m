//
//  MapViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#import "MapViewController.h"
#import "MyPinAnnotationView.h"
#import "NSString+FontAwesome.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "OrderConfirmViewController.h"
#import "StartTripViewController.h"
#import "RatingTripViewController.h"
#import "UIImage+FontAwesome.h"
#import "UIFont+FontAwesome.h"
#import "OnlineViewController.h"
#import "UIView+Toast.h"
#import "AddPaymentViewController.h"
#import "ProfileHasActionView.h"
#import "UIImageView+WebCache.h"
#import "recent_locationTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioServices.h>


@import GooglePlaces;
@interface MapViewController ()<GMSMapViewDelegate>
@end

@implementation MapViewController{
    NSMutableArray *arrCarAnnotation;
    GMSMarker *currentMarker,*fromMarker, *toMaker ,*tomaker1,*tomaker2,*tomaker3,*tomaker4;
    GMSPolyline *singleLine;
    GMSPolyline *singleLine1;
    GMSPolyline *singleLine2;
    GMSPolyline *singleLine3;
    GMSPolyline *singleLine4;
    GMSPlacesClient *_placesClient;
    NSMutableArray<GMSAutocompletePrediction *> *data;
    BOOL isTapping;
    CLLocationManager *locationManager;
    CLLocation *HomeLocation;
    CLLocation *OfficeLocation;
    NSTimer *mainTimer, *backgrTimer;
    NSInteger timesRepeat;
    UIBackgroundTaskIdentifier bgTask;
    Boolean backFromBackground;
    BOOL isloading, didDirectToA;
    GMSMarker  *carMaker;
    
    int setpos;
    NSMutableArray *lablePosArray;

 
}
@synthesize btnLocationFrom,btnLocationTo;
- (void)viewDidLoad
{[super viewDidLoad];
    setpos = 0;
    lablePosArray = [[NSMutableArray alloc]init];
   // CGFloat label = (_Addroute_Label.frame.origin.y);
    
    
    firstCrossBtn.hidden = YES;
    secCrossBtn.hidden = YES;
    thirdCrossBtn.hidden = YES;
    fourthCrossBtn.hidden = YES;
    
    _Addroute_Label.tag = 10;
    secrouteLabel.tag = 11;
    thirdrouteLabel.tag = 12;
    fourthrouteLabel.tag = 13;
    
    _route_Location.delegate = self;
    routeno = 1;
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
    
        
    locationhistory = [[NSMutableArray alloc]init];
    locationcordhistory = [[NSMutableArray alloc]init];
    
    issearchingdriver = false;
    firsttime = false;
     Addroutebtn.hidden = true;
    
    isiconselect = false;
    moveAlocation = false;
    moveBlocation = false;
    

    
    _recent_Locations.dataSource = self;
    _recent_Locations.delegate = self;
    NSString *lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"homelatitude"] ;
    NSString *log = [[NSUserDefaults standardUserDefaults] objectForKey:@"homelogitude"];
    NSString *lat1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"officelatitude"] ;
    NSString *log1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"officelogitude"];
    
    HomeLocation = [[CLLocation alloc] initWithLatitude:[lat doubleValue]  longitude:[log doubleValue]];
    OfficeLocation = [[CLLocation alloc] initWithLatitude:[lat1 doubleValue]  longitude:[log1 doubleValue]];

    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
   
    _Locations_option.layer.cornerRadius = 10;
    [_Locations_option.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_Locations_option.layer setShadowOffset:CGSizeMake(0, 0)];
    [_Locations_option.layer setShadowRadius:10.0];
    [_Locations_option.layer setShadowOpacity:1];
    
    
    CALayer *layer = self.Home_view.layer;
    CALayer *layer1 = self.office_view.layer;
    //changed to zero for the new fancy shadow
    layer.shadowOffset = CGSizeZero;
    
    layer.shadowColor = [[UIColor blackColor] CGColor];
    
    //changed for the fancy shadow
    layer.shadowRadius = 2.0f;
    layer.shadowOpacity = 0.80f;
    
    //call our new fancy shadow method
    layer.shadowPath = [self fancyShadowForRect:layer.bounds];

    layer1.shadowOffset = CGSizeZero;
    
    layer1.shadowColor = [[UIColor blackColor] CGColor];
    
    //changed for the fancy shadow
    layer1.shadowRadius = 2.0f;
    layer1.shadowOpacity = 0.80f;
    
    //call our new fancy shadow method
    layer1.shadowPath = [self fancyShadowForRect:layer1.bounds];
    
    self.meetlocation_view.layer.cornerRadius = 5;
    [_meetlocation_view.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_meetlocation_view.layer setShadowOffset:CGSizeMake(0, 0)];
    [_meetlocation_view.layer setShadowRadius:5.0];
    [_meetlocation_view.layer setShadowOpacity:1];
    
    //_mapview_header.text = @"";
    isEdittingTfFrom = NO;
    isEdittingTfTo = NO;
    arrCarAnnotation = [[NSMutableArray alloc] init];
   
    gTrip = [[Trips alloc]init];
    [self customSetup];
    [self setText];
    [self setupGoogleMap];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self setupDataAfterViewDidLoad];
    
    
#pragma MArk :- Notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCancelTrippassenger) name:DRIVER_CANCEL_TRIP_KEY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(driverCancelTrippasenger) name:CANCEL_TRIP_KEY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEndTrip) name:END_TRIP_KEY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(driverArrived) name:DRIVER_ARRIVED_KEY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(driverconfirm) name:DRIVER_CONFIRM_KEY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBecomForceGround) name:WILL_ENTER_FORCE_GROUND object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onStartTripconfirm) name:START_TRIP_KEY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillGoToBackGround123) name:@"AppDidEnterBackground" object:nil];

    
}


#pragma mark - CurrentLocation Button
- (IBAction)btnCurrentLocation:(id)sender {
    
    [self zoomCurrentLocation];
    
}

-(void)zoomCurrentLocation{
    
    [_gMap animateToLocation:currentMarker.position];
    [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
}


-(void)reloadroutesview
{
    routeno = 1;
    firstroutedash.hidden = true;
    firstroutedestimg.hidden = true;
    secrouteLabel.text = @"";
    secrouteview.hidden = true;
    secroutedestimg.hidden = true;
    secroutedestimg.hidden = true;
    thirdrouteLabel.text = @"";
    thirdrouteview.hidden = true;
    thirdroutedestimg.hidden = true;
    thirdroutedash.hidden = true;
    fourthrouteview.hidden = true;
fourthrouteLabel.text = @"";
    fourthroutedestimg.hidden = true;
    fourthroutedash.hidden = true;
    
    Heightconstraint.constant = 126;
    
    
}
#pragma MArk Forground/Background Action
- (void)appWillBecomForceGround{
    [self loadData];
    
}
- (void)setupDataAfterViewDidLoad{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onResetPoint) name:@"resetAllPoint" object:nil];
    [self onResetPoint];
    [self loadData];
    
}
-(void)loadData
{
    
    if ([gUser.is_driver boolValue]&&[gUser.driver.isOnline boolValue]) {
        
        OnlineViewController *onlineVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineViewController"];
        UINavigationController *rootNavi = [[UINavigationController alloc] initWithRootViewController:onlineVC];
        [self.revealViewController setFrontViewController:rootNavi];
        
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager showMyTripsWithSuccess:^(NSMutableArray *arr) {
        if (gTrip.status == pendingPayment) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            if ([[Util topViewController] isMemberOfClass:[RatingTripViewController class]]) {
                return;
            }
            else{
                [updatetimer invalidate];
                updatetimer = nil;
                RatingTripViewController *orderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingTripViewController"];
                [self.navigationController pushViewController:orderVC animated:NO];
                return;
            }
        }
        
        
        if (gTrip.status == approadching || gTrip.status == inprogress        ) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            if ([[Util topViewController] isMemberOfClass:[OrderConfirmViewController class]]) {
                return;
            }
            
            _bottom_layout.constant = -120;
            [UIView animateWithDuration:1.5
                             animations:^{
                                 [self.view layoutIfNeeded];
                                 self.Findingride_view.hidden = TRUE;// Called on parent view
                             }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.100 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.3 animations:^{
                    
                } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
                    _driver_mainview.hidden = false;
                    _mapview_header.text = @"Drivus";
                    _office_view.hidden = TRUE;
                    _Home_view.hidden = TRUE;
                    _searching_vieew.hidden = TRUE;
                    self.Findingride_view.hidden = TRUE;
                    _Gmap_opcontraint.constant = 84;
                    _Backbtn.hidden = true;
                    
                    self.selectcar_view.hidden = TRUE;
                    self.meetlocation_view.hidden = TRUE;
                    self.Main_requestview.hidden = TRUE;
                    // _bottom_layout.constant = -50;
                    
                }];
            });
            [ mainTimer invalidate];
            _gMap.delegate = self;
           // [self reloadCarAnnotation];
            [self setNeedsStatusBarAppearanceUpdate];
           
            isloading = false;
            didDirectToA = false;
            
            
            [self.navigationController setNavigationBarHidden:YES];
            [self getTripDetail];
            [self updateDistance];
            
            [ mainTimer invalidate];
            [updatetimer invalidate];
            updatetimer=nil;
            ontrip = true;
             [self configMapView];
            updatetimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateDistance) userInfo:nil repeats:YES];
       
            return;
        }
        
        
        [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            if (arr.count>0) {
                gTrip = [arr objectAtIndex:0];
                
                
                if (![[Util topViewController] isKindOfClass:[WaitingViewController class]])
                {
                    
                    _Main_requestview.hidden = false;
                    [self.view layoutIfNeeded];
                    _bottom_layout.constant = 0;
                    [UIView animateWithDuration:1.5
                                     animations:^{
                                         [self.view layoutIfNeeded]; // Called on parent view
                                     }];
                    
                    [UIView transitionWithView:_office_view
                                      duration:0.4
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        _meetlocation_view .hidden = false;
                                        _office_view.hidden = TRUE;
                                        _Home_view.hidden = TRUE;
                                        _searching_vieew.hidden = TRUE;
                                        _Gmap_opcontraint.constant = 84;
                                        _mapview_header.text = @"Connecting";
                                        _selectcar_view.hidden = TRUE;
                                        _connectingdrivers.hidden = false;
                                        _meetlocation_view.hidden = false;
                                        
                                    }
                                    completion:NULL];
                    
                    numberDriversSent = 0;
                    backFromBackground = false;
                    if ((gTrip.tripId>0)) {
                        if (gTrip.startTime.length>0) {
                            [self onStartTrip];
                        }
                    }
                    else{
                        
                        timesRepeat = [AppSettings share].maxTimeSendRequest;
                        [self sendRequestInForceGround123];

                    }
                    
                    
                    
                }
            }
            
        } andFailure:^(NSString *err) {
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
        
    } andFailure:^(NSString *err) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
    
}
#pragma MARK Cancel Trips
-(void)onCancelTrippassenger
{
    ontrip = false;
    [Util showMessage:[Util localized:@"msg_trip_cancel_by_passenger"] withTitle:[Util localized:@"app_name"]];
    
    [UIView transitionWithView:_office_view
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self  reloadroutesview];
                         Addroutebtn.hidden = true;
                        issearchingdriver = false;
                        _driver_mainview.hidden = TRUE;
                        _mapview_header.text = @"Drivus";
                        _office_view.hidden = false;
                        _Home_view.hidden = false;
                        _searching_vieew.hidden = false;
                        self.Findingride_view.hidden = TRUE;
                        _Gmap_opcontraint.constant = 84;
                        _Backbtn.hidden = true;
                        self.selectcar_view.hidden = TRUE;
                        self.meetlocation_view.hidden = TRUE;
                        self.Main_requestview.hidden = TRUE;
                        
                    }
                    completion:NULL];
    [self reloadCarAnnotation];
}
-(void)driverCancelTrippasenger{
    ontrip = false;
    [Util showMessage:[Util localized:@"msg_trip_cancel_by_driver"] withTitle:[Util localized:@"app_name"]];
    [UIView transitionWithView:_office_view
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                          [self  reloadroutesview];
                         Addroutebtn.hidden = true;
                        issearchingdriver = false;
                        _driver_mainview.hidden = TRUE;
                        _mapview_header.text = @"Drivus";
                        _office_view.hidden = false;
                        _Home_view.hidden = false;
                        _searching_vieew.hidden = false;
                        self.Findingride_view.hidden = TRUE;
                        _Gmap_opcontraint.constant = 84;
                        _Backbtn.hidden = true;
                        
                        self.selectcar_view.hidden = TRUE;
                        self.meetlocation_view.hidden = TRUE;
                        self.Main_requestview.hidden = TRUE;
                        
                    }
                    completion:NULL];
    [self reloadCarAnnotation];
    gTrip = [[Trips alloc] init];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
}
-(void)cancelTrip
{
    iscalled = false;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (gTrip.tripId.length > 0) {
        [ModelManager cancelRequest:gTrip withSuccess:^{
            ontrip = false;
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.view makeToast:[Util localized:@"msg_cancel_success"]];
            [UIView transitionWithView:_office_view
                              duration:0.4
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                  [self  reloadroutesview];
                                issearchingdriver = false;
                                _Addroute_Label.text = @"";
                                _driver_mainview.hidden = TRUE;
                                Addroutebtn.hidden = true;
                                _mapview_header.text = @"Drivus";
                                _office_view.hidden = false;
                                _Home_view.hidden = false;
                                _searching_vieew.hidden = false;
                                self.Findingride_view.hidden = TRUE;
                                _Gmap_opcontraint.constant = 84;
                                _Backbtn.hidden = true;
                                
                                self.selectcar_view.hidden = TRUE;
                                self.meetlocation_view.hidden = TRUE;
                                self.Main_requestview.hidden = TRUE;
                                
                            }
                            completion:NULL];
            [self reloadCarAnnotation];
            [mainTimer invalidate];
            [self setupGoogleMap];
            [UIView animateWithDuration:0.5 animations:^{
                
            } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
                _bottom_layout.constant = -120;
                
            }];
            gTrip = [[Trips alloc]init];
            [self reloadCarAnnotation];
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
        } andFailure:^(NSString *err) {
            if (err.length==0) {
                [self.view makeToast:[Util localized:@"msg_cancel_failed"]];
            }
            [self.view makeToast:err];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    }else{
        [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
            if (arr.count>0) {
                gTrip = [arr objectAtIndex:0];
                [self cancelTrip];
            }else{
                gTrip = [[Trips alloc]init];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
                //Here i need to hide all view and set new selection ...
                
                
                [UIView transitionWithView:_office_view
                                  duration:0.4
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    
                                    
                                    issearchingdriver = false;
                                    _driver_mainview.hidden = TRUE;
                                    _mapview_header.text = @"Drivus";
                                    Addroutebtn.hidden = true;
                                    _office_view.hidden = false;
                                    _Home_view.hidden = false;
                                    _searching_vieew.hidden = false;
                                    self.Findingride_view.hidden = TRUE;
                                    _Gmap_opcontraint.constant = 84;
                                    _Backbtn.hidden = true;
                                    
                                    self.selectcar_view.hidden = TRUE;
                                    self.meetlocation_view.hidden = TRUE;
                                    self.Main_requestview.hidden = TRUE;
                                    
                                }
                                completion:NULL];
                [self reloadCarAnnotation];
                [mainTimer invalidate];
                //[self.navigationController popViewControllerAnimated:YES];
                
                
                
                //////////////////////////
                
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
                return;
            }
            
        } andFailure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [Util showMessage:err withTitle:nil];
            
        }];
    }
    
}


- (CGPathRef)fancyShadowForRect:(CGRect)rect
{
    CGSize size = rect.size;
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    //right
    [path moveToPoint:CGPointZero];
    [path addLineToPoint:CGPointMake(size.width, 0.0f)];
    [path addLineToPoint:CGPointMake(size.width, size.height + 15.0f)];
    
    //curved bottom
    [path addCurveToPoint:CGPointMake(0.0, size.height + 15.0f)
            controlPoint1:CGPointMake(size.width - 15.0f, size.height)
            controlPoint2:CGPointMake(15.0f, size.height)];
    
    [path closePath];
    
    return path.CGPath;
}



#pragma Mark Location Update
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    
       CLLocation *currentLocationss = newLocation;
    
    currentMarker.map = nil;
    currentMarker = [GMSMarker markerWithPosition:currentLocationss.coordinate];
    currentMarker.icon = nil;
    // Addroutebtn.hidden = false;
    //currentMarker.map = _gMap;

    
    if ( firsttime == false)
    {
        firsttime = true;
        if (getdestLocation == false)
        {
        
        firsttime = true;
  
    if (currentLocationss != nil)
    {
    
         fromcordlocation =  newLocation;
        fromMarker.map = nil;
        fromMarker = [GMSMarker markerWithPosition:currentLocationss.coordinate];
        fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
       
        fromMarker.map = _gMap;
        [self zoomStartPlace];
     
    }

        }
        else
        {
            tocordlocation =  newLocation;
            toMaker.map = nil;
            toMaker = [GMSMarker markerWithPosition:currentLocationss.coordinate];
            toMaker.icon = [UIImage imageNamed:@"icon_toB"];
           // Addroutebtn.hidden = false;
            toMaker.map = _gMap;
            [self zoomEndPlace];
        }
        
        [self getLocationNameWithPosition:currentLocationss.coordinate];
    }
    
}
- (void)setupGoogleMap
{
    _gMap.myLocationEnabled = true;
    _gMap.camera = [GMSCameraPosition cameraWithLatitude:gShareLocation.myLocation.latitude longitude:gShareLocation.myLocation.longitude zoom:MAP_ZOOM_DEFAULT];
    _gMap.delegate = self;
    
}



-(void)onResetPoint
{
    
    [_gMap clear];
    [self onSelectSeat:_btn1seat];
    [self removeAllPolyLine];
    self.txtFromName.text = @"";
    self.txtToName.text = @"";
    fromMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(0,0)];
    fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
    toMaker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(0,0)];
    toMaker.icon = [UIImage imageNamed:@"icon_toB"];
    
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;

    
    if ( revealViewController )
    {
        

                if ( revealViewController )
                {
                    [_revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
                }
                [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
            
        
        
    }
    

    
    [self.btnRefresh setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                             [UIFont fontWithName:@"Pe-icon-7-stroke" size:26], NSFontAttributeName,
                                             [UIColor whiteColor], NSForegroundColorAttributeName,
                                             nil]
                                   forState:UIControlStateNormal];
    
    [self.btnRefresh setTitle:@"\ue6c2"];
    
    self.txtToName.delegate=self;
    self.txtFromName.delegate = self;
    self.route_textfeild.delegate = self;
    _placesClient = [GMSPlacesClient sharedClient];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
     firsttime = false;
    [super viewWillAppear:animated];
    //Add Label to array
    [lablePosArray addObject:_Addroute_Label];
    [lablePosArray addObject:secrouteLabel];
    [lablePosArray addObject:thirdrouteLabel];
    [lablePosArray addObject:fourthrouteLabel];
    
    
    [self  setupGoogleMap];
    [self reloadroutesview];
    iscalled = false;
    isEdittingTfFrom = NO;
    isEdittingTfTo = NO;
    arrCarAnnotation = [[NSMutableArray alloc] init];
    [self setupGoogleMap];
   
   // [self.navigationController setNavigationBarHidden:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    //[self createDestinationLabels];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    
//    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        CGRect f = self.view.frame;
//        f.origin.y = -keyboardSize.height;
//        self.view.frame = f;
//    }];
   // printf("%lu", (unsigned long)locationhistory.count);
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel isEqualToString:@"iPhone"])
    {
        if(self.view.frame.size.height < 600)
        {
            _recent_Locations.hidden = true;
            _Locations_option.hidden = true;
            if(setpos<2)
            {
                _loc_searchViewPos.constant = -50;
            }
            else{
                _loc_searchViewPos.constant = -30;
            }
        }
        else if(self.view.frame.size.height > 650)
        {
            
            if(setpos>0)
            {
            _loc_searchViewPos.constant = 40;
            _recent_Locations.hidden = true;
            _Locations_option.hidden = true;
            }
            
        }
        
        else{
            if(setpos>1)
            {
                _loc_searchViewPos.constant = -10;
                _recent_Locations.hidden = true;
                _Locations_option.hidden = true;
            }
        }
        
    }
    
    
}

-(void)keyboardWillHide:(NSNotification *)notification
{
     loc_searchview.backgroundColor = [UIColor clearColor];
       loc_searchview .hidden = true;
  // _loc_searchViewPos.constant = 55;
    //_searchingViewPos.constant = 123;
   // loc_searchview.hidden = YES;
//    [UIView animateWithDuration:0.3 animations:^{
//        CGRect f = self.view.frame;
//        f.origin.y = 0.0f;
//        self.view.frame = f;
//    }];
}

-(void)setText
{
    
    [self.lblHeaderTitle setTitle:[Util localized:@"header_request_car"]];
    
    self.bgFrom.layer.cornerRadius = 5;
    self.bgTo.layer.cornerRadius = 5;
    self.bgFrom.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.bgFrom.layer.shadowOffset = CGSizeMake(10, 5);
    self.bgFrom.layer.shadowOpacity = 0.3;
    self.bgFrom.layer.shadowRadius = 5;
    self.bgFrom.clipsToBounds = NO;
    
    self.bgTo.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.bgTo.layer.shadowOffset = CGSizeMake(10, 5);
    self.bgTo.layer.shadowOpacity = 0.3;
    self.bgTo.layer.shadowRadius = 5;
    self.bgTo.clipsToBounds = NO;
}


#pragma mark MAPVIEW DELEGATE

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
   
    if (ontrip == false)
    {
        [self reloadCarAnnotation];

    }
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{

//      isiconselect = false;
//    if (issearchingdriver == false)
//    {
//  
//    [self removeAllPolyLine];
//    if (isEdittingTfFrom == YES) {
//        fromMarker.map = nil;
//        fromMarker = [GMSMarker markerWithPosition:coordinate];
//        fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
//        fromMarker.map = _gMap;
//        [self zoomStartPlace];
//        
//    }
//    else if (isEdittingTfTo == YES){
//        
//        toMaker.map = nil;
//        toMaker = [GMSMarker markerWithPosition:coordinate];
//        toMaker.icon = [UIImage imageNamed:@"icon_toB"];
//        toMaker.map = _gMap;
//        [self zoomEndPlace];
//
//        
//        
//    }
//    else if (!_txtFromName.text.length) {
//        fromMarker.map = nil;
//        fromMarker = [GMSMarker markerWithPosition:coordinate];
//        fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
//        fromMarker.map = _gMap;
//        [self zoomStartPlace];
//    }
//    else{
//        toMaker.map = nil;
//        toMaker = [GMSMarker markerWithPosition:coordinate];
//        toMaker.icon = [UIImage imageNamed:@"icon_toB"];
//        toMaker.map = _gMap;
//        [self zoomEndPlace];
//    }
//  
//    }
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled: NO];
    // gShareLocation.updateOn = NO;
    [updatetimer invalidate];
    [super viewWillDisappear:YES];
}

-(void)getLocationNameWithPosition:(CLLocationCoordinate2D)position
{
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [ModelManager getAddressNameByPosition:position handlerSuccess:^(NSString *addressName) {
            dispatch_async(dispatch_get_main_queue(), ^{
                CustomPosition *addressObj = [[CustomPosition alloc] initWithPositon:position addressName:addressName];
                
                if (position.latitude == toMaker.position.latitude && position.longitude == toMaker.position.longitude) {
                    if (      isiconselect == false)
                    {
                       self.txtToName.text = addressName;
                    }

                    
                    gTrip.endLocation = addressObj;
                    
                }
                if (position.longitude == fromMarker.position.longitude && position.latitude == fromMarker.position.latitude) {
                    
                    if (      isiconselect == false)
                    {
                        self.txtFromName.text = addressName;
                    }
                    gTrip.startLocation = addressObj;
                    
                }
                
               //[self reloadCarAnnotation];
                
                iscalled = false;
                //  _Donoe_btn.hidden = false;
               // [self showDirections];
            });
        } handlerFailed:^(NSString *err) {
            
        }];
    });
   
}


#pragma mark TEXTFIELD DELEGATE
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self searchViewheightFunc];
   if (textField == _txtFromName)
   {
       loc_searchview .hidden = true;
       _Locations_option.hidden =true;
       
   }
    else if (textField == _txtToName)
    {
        loc_searchview .hidden = true;
        _Locations_option.hidden =true;
        loc_searchview .hidden = true;
    }
    
    
      // _selectcar_view.userInteractionEnabled = false;
        [self generateDataWithKey:textField.text];
        return YES;
    
    
}
-(void)searchViewheightFunc
{
    _searchViewHeight.constant = 195;
    _locItemsView.frame = CGRectMake(_locItemsView.frame.origin.x, _locItemsView.frame.origin.y, _locItemsView.frame.size.width, 350);
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self searchViewheightFunc];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    isiconselect = false;
    _recent_Locations.hidden = true;
     _Locations_option.hidden = true;
    [self searchViewheightFunc];
  //  self.gMap.userInteractionEnabled = FALSE;
    
   /// _locItemsView.backgroundColor = [UIColor yellowColor];
    if (textField == _txtFromName) {
        isEdittingTfFrom = YES;
        isEdittingTfTo = NO;
    }else if (textField == _txtToName){
        isEdittingTfFrom = NO;
        isEdittingTfTo = YES;
    }
    else
    {
        isEdittingTfFrom = NO;
        isEdittingTfTo = NO;
        
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    table_backview.hidden = true;
    if (routeno <=1)
    {
        Heightconstraint.constant = 121;

    }
    
   // self.gMap.userInteractionEnabled = TRUE;
    if (textField == _txtFromName) {
        isEdittingTfFrom = NO;
    }
    if (textField == _txtToName) {
        isEdittingTfTo = NO;
    }
    
    if (_txtFromName.text.length == 0) {
        fromMarker.map = nil;
        fromMarker.position= CLLocationCoordinate2DMake(0,0);
        [self removeAllPolyLine];
        
    }
    if (_txtToName.text.length == 0) {
        toMaker.map = nil;
        toMaker.position= CLLocationCoordinate2DMake(0,0) ;
        [self removeAllPolyLine];
    }
  
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (routeno <=1)
    {
        Heightconstraint.constant = 121;
        
    }


    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    loc_searchview.backgroundColor = [UIColor whiteColor];
    return YES;
}


#pragma mark BUTTON ACTION
- (IBAction)onFromLocation:(id)sender {
    [self.view endEditing:YES];
    [_gMap animateToLocation:gShareLocation.myLocation];
    fromMarker.map = nil;
         tocordlocation =   [[CLLocation alloc] initWithLatitude:gShareLocation.myLocation.latitude  longitude:gShareLocation.myLocation.longitude];
    fromMarker = [GMSMarker markerWithPosition:gShareLocation.myLocation];
    fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
    fromMarker.map = _gMap;
    [self zoomStartPlace];
    [self getLocationNameWithPosition:fromMarker.position];
}

- (IBAction)onToLocation:(id)sender {
    [self.view endEditing:YES];
    [_gMap animateToLocation:gShareLocation.myLocation];
    toMaker.map = nil;
    toMaker = [GMSMarker markerWithPosition:gShareLocation.myLocation];
    tocordlocation =   [[CLLocation alloc] initWithLatitude:gShareLocation.myLocation.latitude  longitude:gShareLocation.myLocation.longitude];
    toMaker.icon = [UIImage imageNamed:@"icon_toB"];
   // Addroutebtn.hidden = false;
    toMaker.map = _gMap;
    [self zoomEndPlace];
    [self getLocationNameWithPosition:toMaker.position];
}

- (IBAction)onSelectSeat:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    UIFont *fontSelected = [UIFont fontWithName:@"Raleway-Bold" size:17];
    UIFont *fontNormal = [UIFont fontWithName:@"Raleway-Regular" size:14];
    
    [self.btn1seat.titleLabel setFont:fontNormal];
    self.btn1seat.selected = NO;
    [self.btn2seat.titleLabel setFont:fontNormal];
    self.btn2seat.selected = NO;
    [self.btn3seat.titleLabel setFont:fontNormal];
    self.btn3seat.selected = NO;
    
    gTrip.link = @"";
    
    for (int i=0; i<button.tag; i++) {
        gTrip.link = [NSString stringWithFormat:@"%@I",gTrip.link];
    }
    [button.titleLabel setFont:fontSelected];
    button.selected = YES;
    
    switch (button.tag) {
        case 1:
            [ self.First_car   setBackgroundImage:[UIImage imageNamed:@"yellow-grren-car"] forState:UIControlStateNormal];
            [self.second_car setBackgroundImage:[UIImage imageNamed:@"second-car"] forState:UIControlStateNormal];
            [self.Third_car setBackgroundImage:[UIImage imageNamed:@"second-car"] forState:UIControlStateNormal];
            estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
            self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];

            break;
        case 2:
            [self.second_car setBackgroundImage:[UIImage imageNamed:@"third-car"] forState:UIControlStateNormal];

            [self.Third_car setBackgroundImage:[UIImage imageNamed:@"second-car"] forState:UIControlStateNormal];
            [ self.First_car   setBackgroundImage:[UIImage imageNamed:@"first-car"] forState:UIControlStateNormal];
            estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
            self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];
            break;
        case 3:
            [ self.First_car   setBackgroundImage:[UIImage imageNamed:@"first-car"] forState:UIControlStateNormal];
            [self.second_car setBackgroundImage:[UIImage imageNamed:@"second-car"] forState:UIControlStateNormal];

            [self.Third_car setBackgroundImage:[UIImage imageNamed:@"third-car"] forState:UIControlStateNormal];
            estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
            self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];
            break;
        default:
            break;
    }
    estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
    self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];
    [self reloadCarAnnotation];
}

- (IBAction)onRefresh:(id)sender {
  
    [self reloadCarAnnotation];
}
- (IBAction)deleteMutiRoute:(id)sender
{

    setpos--;
     loc_searchview .hidden = true;
    _recent_Locations.hidden = true;
    _Locations_option.hidden = true;
    
    if ([sender tag] == 20) {
        
        if (routeno == 2) {
            routeno = 1;
            _Addroute_Label.text = @"";
            firstroutedash.hidden = true;
            firstCrossBtn.hidden = true;
            firstroutedestimg.hidden = true;
            tomaker1.map = nil;
            tomaker1 = nil;

            gTrip.endLocation1 = nil;
            Heightconstraint.constant = 121;

        }else if (routeno == 3){
            routeno = 2;
           
            _Addroute_Label.text = secrouteLabel.text;
            tomaker1.map = nil;
            tomaker1 = [GMSMarker markerWithPosition:tomaker2.position];
            tomaker1.icon = [UIImage imageNamed:@"icon_fromc"];
            tomaker1.map = _gMap;
            Heightconstraint.constant = 180;
            
            
            [_gMap animateToLocation:tomaker1.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation1 = gTrip.endLocation2;

            
            secCrossBtn.hidden = true;
            secrouteLabel.text = @"";
            secroutedash.hidden = true;
     
            tomaker2.map = nil;
            tomaker2 = nil;
            secroutedestimg.hidden = true;
            secrouteview.hidden = true;
            secrouteLabel.hidden = true;
            gTrip.endLocation2 = nil;
            Heightconstraint.constant = 180;
            
            
            

            
        }else if (routeno == 4){
            routeno = 3;
            //1
            _Addroute_Label.text = secrouteLabel.text;
            tomaker1.map = nil;
            tomaker1 = [GMSMarker markerWithPosition:tomaker2.position];;
            tomaker1.icon = [UIImage imageNamed:@"icon_fromc"];
            tomaker1.map = _gMap;
            
            [_gMap animateToLocation:tomaker1.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation1 = gTrip.endLocation2;

            //2
            secrouteLabel.text = thirdrouteLabel.text;
            tomaker2.map = nil;
            tomaker2 = tomaker3;
            tomaker2.icon = [UIImage imageNamed:@"d1"];

            tomaker2.map = _gMap;
            [_gMap animateToLocation:tomaker2.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation2 = gTrip.endLocation3;
            
           //3
           
            
            thirdrouteLabel.text = @"";
            thirdroutedash.hidden = true;
            thirdroutedestimg.hidden = true;
            thirdCrossBtn.hidden = true;

            tomaker3.map = nil;
            tomaker3 = nil;

            thirdrouteview.hidden = true;
            thirdrouteLabel.hidden = true;
            Heightconstraint.constant = 200;
            gTrip.endLocation3 = nil;
            
            
            
        }else if (routeno == 5){
            routeno = 4;
            //1
            _Addroute_Label.text = secrouteLabel.text;
            tomaker1.map = nil;
            tomaker1 = [GMSMarker markerWithPosition:tomaker2.position];;
            tomaker1.icon = [UIImage imageNamed:@"icon_fromc"];
            tomaker1.map = _gMap;
            
            [_gMap animateToLocation:tomaker1.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation1 = gTrip.endLocation2;
            //2
            secrouteLabel.text = thirdrouteLabel.text;
            
            tomaker2.map = nil;
            tomaker2 = tomaker3;
            tomaker2.icon = [UIImage imageNamed:@"d1"];
            tomaker2.map = _gMap;

            [_gMap animateToLocation:tomaker2.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation2 = gTrip.endLocation3;
            
            //3
            
            
            thirdrouteLabel.text = fourthrouteLabel.text;
            
            tomaker3.map = nil;
            tomaker3 = [GMSMarker markerWithPosition:tomaker4.position];;
            tomaker3.icon = [UIImage imageNamed:@"icon_frome"];
            tomaker3.map = _gMap;

            [_gMap animateToLocation:tomaker3.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation3 = gTrip.endLocation4;

           //4
            
            
            fourthCrossBtn.hidden = true;
            fourthrouteLabel.text = @"";
            fourthroutedash.hidden = true;
            fourthroutedestimg.hidden = true;
            
            tomaker4.map = nil;
            tomaker4 = nil;

            fourthrouteview.hidden = true;
            fourthrouteLabel.hidden = true;
            Heightconstraint.constant = 230;
            gTrip.endLocation4 = nil;
            
            
        }

        
        
        
    }else if ([sender tag] == 21){
        
        if (routeno == 3){
            routeno = 2;
            
            
            
            
            secCrossBtn.hidden = true;
            secrouteLabel.text = @"";
            secroutedash.hidden = true;
            
            tomaker2.map = nil;
            tomaker2 = nil;

            secroutedestimg.hidden = true;
            secrouteview.hidden = true;
            secrouteLabel.hidden = true;
            Heightconstraint.constant = 200;
            gTrip.endLocation2 = nil;
            Heightconstraint.constant = 180;
            
            
        }else if (routeno == 4){
            routeno = 3;
            //1
        
            
            //2
            secrouteLabel.text = thirdrouteLabel.text;
            tomaker2.map = nil;
            tomaker2 = [GMSMarker markerWithPosition:tomaker3.position];;
            tomaker2.icon = [UIImage imageNamed:@"d1"];

            tomaker2.map = _gMap;
            [_gMap animateToLocation:tomaker2.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation2 = gTrip.endLocation3;
            
            //3
            
            
            thirdrouteLabel.text = @"";
            thirdroutedash.hidden = true;
            thirdroutedestimg.hidden = true;
            thirdCrossBtn.hidden = true;
            
            tomaker3.map = nil;
            tomaker3 = nil;

            thirdrouteview.hidden = true;
            thirdrouteLabel.hidden = true;
            Heightconstraint.constant = 200;
            gTrip.endLocation3 = nil;
            
            
            
        }else if (routeno == 5){
            routeno = 4;
            //1
    
            
            //2
            secrouteLabel.text = thirdrouteLabel.text;
            tomaker2.map = nil;
            tomaker2 = [GMSMarker markerWithPosition:tomaker3.position];;
            tomaker2.icon = [UIImage imageNamed:@"d1"];

            tomaker2.map = _gMap;
            [_gMap animateToLocation:tomaker2.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation2 = gTrip.endLocation3;
            
            //3
            
            
            thirdrouteLabel.text = fourthrouteLabel.text;
            
            tomaker3.map = nil;
            tomaker3 = tomaker4;
            tomaker3.icon = [UIImage imageNamed:@"icon_frome"];
            tomaker3.map = _gMap;
            
            [_gMap animateToLocation:tomaker3.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation3 = gTrip.endLocation4;
            
            //4
            
            
            fourthCrossBtn.hidden = true;
            fourthrouteLabel.text = @"";
            fourthroutedash.hidden = true;
            fourthroutedestimg.hidden = true;
            
            tomaker4.map = nil;
            tomaker4 = nil;

            fourthrouteview.hidden = true;
            fourthrouteLabel.hidden = true;
            Heightconstraint.constant = 230;
            gTrip.endLocation4 = nil;
            
            
        }
        
        
        
        
        
    }else if ([sender tag] == 22){
        
         if (routeno == 4){
            routeno = 3;
            //1
       
            
            //2
        
            
            //3
            
            
            thirdrouteLabel.text = @"";
            thirdroutedash.hidden = true;
            thirdroutedestimg.hidden = true;
            thirdCrossBtn.hidden = true;
            
            tomaker3.map = nil;
            tomaker3 = nil;

            thirdrouteview.hidden = true;
            thirdrouteLabel.hidden = true;
            Heightconstraint.constant = 200;
            gTrip.endLocation3 = nil;
            
            
            
        }else if (routeno == 5){
            routeno = 4;
            //1
      
            
            //2
       
            
            //3
            
            
            thirdrouteLabel.text = fourthrouteLabel.text;
            
            tomaker3.map = nil;
            tomaker3 = [GMSMarker markerWithPosition:tomaker4.position];;
            tomaker3.icon = [UIImage imageNamed:@"icon_frome"];
            tomaker3.map = _gMap;
            
            [_gMap animateToLocation:tomaker3.position];
            [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
            gTrip.endLocation3 = gTrip.endLocation4;
            
            //4
            
            
            fourthCrossBtn.hidden = true;
            fourthrouteLabel.text = @"";
            fourthroutedash.hidden = true;
            fourthroutedestimg.hidden = true;
            
            tomaker4.map = nil;
            tomaker4 = nil;

            fourthrouteview.hidden = true;
            fourthrouteLabel.hidden = true;
            Heightconstraint.constant = 230;
            gTrip.endLocation4 = nil;
            
            
        }
        
        
        
        
        
    }else{
        
         if (routeno == 5){
            routeno = 4;
            //1
       
            
            //2
       
            
            //3
            
            
          
            
            //4
            
            
            fourthCrossBtn.hidden = true;
            fourthrouteLabel.text = @"";
            fourthroutedash.hidden = true;
            fourthroutedestimg.hidden = true;
            
            tomaker4.map = nil;
             tomaker4 = nil;

            fourthrouteview.hidden = true;
            fourthrouteLabel.hidden = true;
            Heightconstraint.constant = 230;
            gTrip.endLocation4 = nil;
            
            
        }
        
        
        
        
        
    }
    
    
    
    
//    
//    UIButton *button = [self.view viewWithTag:([sender tag])];
//    button.hidden = YES;
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    long var = [sender tag]-10;
//    UILabel *label = [self.view viewWithTag:(var)];
//    label.hidden = YES;
////    CGRect btFrame = label.frame;
////    btFrame.size.height = 0;
////    label.frame = btFrame;
//    
//    long varView = [sender tag]+10;
//    UILabel *vieww = [self.view viewWithTag:(varView)];
//    vieww.hidden = YES;
//
//    
    
    


}
- (IBAction)Addmultipleroute:(id)sender
{
    _locItemsView.hidden = NO;
    loc_searchview.hidden = NO;
    
    _recent_Locations.hidden = true;
    _Locations_option.hidden = true;
    
//    _searchViewHeight.constant = 300;
//    _locItemsView.frame = CGRectMake(_locItemsView.frame.origin.x, _locItemsView.frame.origin.y, _locItemsView.frame.size.width, 300);
    
    if (routeno == 5)
    {
        _locItemsView.hidden = YES;
        loc_searchview.hidden = YES;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Maximum Route already Added." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (setpos < 0)
    {
        setpos = 0;
    }
    _loc_searchViewPos.constant = 22;
    
    NSLog(@"%fd",_loc_searchViewPos.constant);
    
    if (_Addroute_Label.text.length>0)
    {
    if (setpos==0)
       {
        _loc_searchViewPos.constant = 23;
        
    }
    else if (setpos==1)
    {
        _loc_searchViewPos.constant = 77;
    }
    else if (setpos==2)
    {
        _loc_searchViewPos.constant = 101;
        
    }
    else if (setpos==3)
    {
        _loc_searchViewPos.constant = 131;
    }
    }
    //[lablePosArray addObject:_loc_searchViewPos];
    NSLog(@"The array is %@", lablePosArray);
    //    if (lablePosArray.count !=0)
    //    {
    //    if(![lablePosArray containsObject:_loc_searchViewPos])
    //    {
    //
    //    }
    //    else{
    //
    //    }
    //    }
    _route_Location.text = @"";
    _route_textfeild.text = @"";
    loc_searchview.hidden = false;
    routestrue = true;
    locationhistory = [[NSMutableArray alloc]init];
    locationcordhistory = [[NSMutableArray alloc]init];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    locationhistory = [userDefaults objectForKey:@"locationhistory"];
    locationcordhistory = [userDefaults objectForKey:@"locationcordhistory"];
    
    _Height_constraint.constant = 40;
    if ([locationhistory count ] > 0)
    {
        if ([locationhistory count] >4)
        {
            
            _Height_constraint.constant = 350;
        }
        else
        {
            _Height_constraint.constant = 250 ;
        }
    }
    
    
    if ([locationhistory count] >0)
    {
        _recent_Locations.hidden = false;
    }
    [_recent_Locations reloadData];
    
    [UIView animateWithDuration:0.25 animations:^{
        fromlocation = false;
        tolocation = false;
        moveBlocation = false;
        moveAlocation = false;
        routestrue = true;
        NSLog(@"here is route number %d",routeno);
        
    } completion:^(BOOL finished) {
    }];
    
}

- (void)reloadCarAnnotation{
    
    if (reloadsearch == false)
    {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        GMSVisibleRegion vr = [_gMap.projection visibleRegion];
        
        CLLocationDistance distance = GMSGeometryDistance(vr.nearLeft, vr.farRight)/2;
        
         reloadsearch = TRUE;
        
        
        [ModelManager getNumberOfCarsWithLatitude:_gMap.camera.target.latitude Longitude:_gMap.camera.target.longitude Distance:distance withSuccess:^(int carNumberReturn, NSArray *arrCarPointReturn) {
            if (self) {
               //prabhjot
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (GMSMarker *makerThumb in arrCarAnnotation) {
                        makerThumb.map = nil;
                    }
                    [arrCarAnnotation removeAllObjects];
                    [arrCarAnnotation addObjectsFromArray:arrCarPointReturn];
                    reloadsearch = false;
                    for (GMSMarker *makerThumb in arrCarAnnotation) {
                        makerThumb.map = _gMap;
                    }
                });
               
            }
        } failure:^(NSString *err) {
            
        }];

    });
  
}
}
- (IBAction)hideroute_search:(id)sender
{
   
    loc_searchview .hidden = true;
    _recent_Locations.hidden = false;
    _Locations_option.hidden = true;
    
    
}

- (IBAction)backAction:(id)sender
{
    
    MapViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
    [self.revealViewController setFrontViewController:naviRoot];
}




- (void)generateDataWithKey:(NSString *)key
{
    
    data = [[NSMutableArray alloc]init];
    [_placesClient autocompleteQuery:key bounds:nil filter:nil callback:^(NSArray<GMSAutocompletePrediction *> * _Nullable results, NSError * _Nullable error) {
        
        if (!error) {
            [data addObjectsFromArray:results];
            if (_txtFromName.isEditing) {
                [_txtFromName provideSuggestions];
            }
            else if  (_txtToName.isEditing)
            {
                [_txtToName provideSuggestions];
            }
            else
            {
                [self.route_Location provideSuggestions];
            }
        }
        else
        {
            NSLog (@"error placing searching %@",error);
        }
        
    }];
    
}


#pragma mark MPGTextField Delegate Methods

- (NSArray *)dataForPopoverInTextField:(MPGTextField *)textField
{
    Heightconstraint.constant = 320;
      table_backview.hidden = false;
      return data;
}


- (BOOL)textFieldShouldSelect:(MPGTextField *)textField
{
    return YES;
}


- (void)textField:(MPGTextField *)textField didEndEditingWithSelection:(GMSAutocompletePrediction *)result{
    table_backview.hidden = true;
    if (!result) {
        return;
    }
    
    if (routeno <=1)
    {
        Heightconstraint.constant = 121;
    }
   
    if (setpos < 0)
    {
        setpos=0;
    }

    

    
     table_backview.hidden = true;
    NSLog(@"%@",result);
    [_placesClient lookUpPlaceID:result.placeID callback:^(GMSPlace * _Nullable result, NSError * _Nullable error) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([textField isEqual:self.txtFromName]&&(result.coordinate.latitude!=0||result.coordinate.longitude!=0)) {
                    
                    [self.txtFromName setText:result.formattedAddress];
                     fromcordlocation =   [[CLLocation alloc] initWithLatitude:result.coordinate.latitude  longitude:result.coordinate.longitude];
                    fromMarker.map = nil;
                    fromMarker = [GMSMarker markerWithPosition:result.coordinate];
                    fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
                    fromMarker.map = _gMap;
                    [self zoomStartPlace];
                    gTrip.startLocation = [[CustomPosition alloc] initWithPositon:result.coordinate addressName:result.formattedAddress];
                    
                    
                }
                else if ([textField isEqual:self.txtToName]&&(result.coordinate.latitude!=0||result.coordinate.longitude!=0))
                {
                    

                        tocordlocation =   [[CLLocation alloc] initWithLatitude:result.coordinate.latitude  longitude:result.coordinate.longitude];
                   // Addroutebtn.hidden = false;
                        [self.txtToName setText:result.formattedAddress];
                        toMaker.map = nil;
                        toMaker = [GMSMarker markerWithPosition:result.coordinate];
                        toMaker.icon = [UIImage imageNamed:@"icon_toB"];
                        toMaker.map = _gMap;
                        [self zoomEndPlace];
                        gTrip.endLocation = [[CustomPosition alloc] initWithPositon:result.coordinate addressName:result.formattedAddress];
             
                 
                }
                else
                {
                    
                    nearcordlocatio =   [[CLLocation alloc] initWithLatitude:result.coordinate.latitude  longitude:result.coordinate.longitude];
                    
                    _route_Location.text = result.formattedAddress;
                    _route_textfeild.text = result.formattedAddress;
                    loc_searchview .hidden = true;
                    
                    
                   
                    
                    
                    
                    
                   // [self zoomEndPlace];
                    
                    if (routeno == 1)
                    {
                         setpos++;
                        routeno = 2;
                        firstCrossBtn.hidden = NO;
                        _Addroute_Label.text = result.formattedAddress;
                        firstroutedash.hidden = false;
                        firstroutedestimg.hidden = false;
                       
                        tomaker1.map = nil;
                        tomaker1 = [GMSMarker markerWithPosition:result.coordinate];
                        tomaker1.icon = [UIImage imageNamed:@"icon_fromc"];
                        tomaker1.map = _gMap;
                        Heightconstraint.constant = 180;
                        gTrip.endLocation1 = [[CustomPosition alloc] initWithPositon:result.coordinate addressName:result.formattedAddress];
                        
                        [_gMap animateToLocation:tomaker1.position];
                        [_gMap animateToZoom:MAP_ZOOM_DEFAULT];

                    }
                    else if (routeno == 2)
                    {
                        
                        if ([result.formattedAddress isEqual:_Addroute_Label.text])
                        {
                           // setpos--;
                        }
                        else{
                        routeno = 3;
                        secCrossBtn.hidden = NO;
                        secrouteLabel.text = result.formattedAddress;
                         setpos++;
                        
                        secroutedash.hidden = false;
                        
                        
                        tomaker2.map = nil;
                        tomaker2 = [GMSMarker markerWithPosition:result.coordinate];
                        tomaker2.icon = [UIImage imageNamed:@"d1"];
                        tomaker2.map = _gMap;
                        
                        [_gMap animateToLocation:tomaker2.position];
                        [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
                        
                        secroutedestimg.hidden = false;
                        secrouteview.hidden = false;
                        secrouteLabel.hidden = false;
                        Heightconstraint.constant = 200;
                        gTrip.endLocation2 = [[CustomPosition alloc] initWithPositon:result.coordinate addressName:result.formattedAddress];
                    }
                    }
                    else if (routeno == 3)
                    {
                        if ([result.formattedAddress isEqual:secrouteLabel.text])
                        {
                            //setpos--;
                        }
                        else{
                        routeno = 4;
                        thirdrouteLabel.text = result.formattedAddress;
                         setpos++;
        
                        thirdroutedash.hidden = false;
                        thirdroutedestimg.hidden = false;
                        thirdCrossBtn.hidden = NO;
                        
                        tomaker3.map = nil;
                        tomaker3 = [GMSMarker markerWithPosition:result.coordinate];
                        tomaker3.icon = [UIImage imageNamed:@"icon_frome"];
                        tomaker3.map = _gMap;
                        
                        
                        [_gMap animateToLocation:tomaker3.position];
                        [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
                        
                        
                        
                        thirdrouteview.hidden = false;
                        thirdrouteLabel.hidden = false;
                        Heightconstraint.constant = 230;
                        gTrip.endLocation3 = [[CustomPosition alloc] initWithPositon:result.coordinate addressName:result.formattedAddress];

                        }
                    }
                    else if (routeno == 4)
                    {
                        if ([result.formattedAddress isEqual:thirdrouteLabel.text])
                        {
                           // setpos--;
                        }
                        else{
                        routeno = 5;
                        fourthCrossBtn.hidden = NO;
                        fourthrouteLabel.text = result.formattedAddress;
                         setpos++;
                        fourthroutedash.hidden = false;
                        fourthroutedestimg.hidden = false;
                        
                        tomaker4.map = nil;
                        tomaker4 = [GMSMarker markerWithPosition:result.coordinate];
                        tomaker4.icon = [UIImage imageNamed:@"icon_fromf"];
                        tomaker4.map = _gMap;
                        fourthrouteview.hidden = false;
                        fourthrouteLabel.hidden = false;
                        Heightconstraint.constant = 270;
                        [_gMap animateToLocation:tomaker4.position];
                        [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
                        
                        
                        gTrip.endLocation4 = [[CustomPosition alloc] initWithPositon:result.coordinate addressName:result.formattedAddress];
                        }
                        
                    }
                    
                    
                    
                }
              //  self.gMap.userInteractionEnabled = TRUE;
                [self removeAllPolyLine];
                iscalled = false;
                  _Donoe_btn.hidden = false;
//                [self performSelector:@selector(showDirections)
//                           withObject:nil
//                           afterDelay:0.5f];
               // [self showDirections];
            });
        }
    }];
    
    
}
-(void)showDirections

{
    if (iscalled == false)
    {

    if (self.txtFromName.text.length>0&&self.txtToName.text.length>0 && (fromMarker.position.latitude > 0 || fromMarker.position.longitude > 0) && (toMaker.position.latitude || toMaker.position.longitude)) {
        
        
        
      
    //.......... +++++++++++++++++++++++++++++++++++++++++++++..............................//
        
        CLLocation *driverLocation = [[CLLocation alloc] initWithLatitude:fromMarker.position.latitude longitude:fromMarker.position.longitude];
        //[self zoomToDirections];
        [ModelManager getRoutingByGoogleWithStart:fromMarker.position endLocation:toMaker.position handlerSuccess:^(NSDictionary *response) {
            

            
            gTrip.estimateDistance = [NSString stringWithFormat:@"%.3f", [Validator getSafeFloat:response[@"routes"][0][@"legs"][0][@"distance"][@"value"]]/1000];
            singleLine.map = nil;
            GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
            singleLine = [GMSPolyline polylineWithPath:path];
            singleLine.strokeWidth = 2;
            singleLine.strokeColor = [UIColor blueColor];
            singleLine.map = self.gMap;
            estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
            self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];
            if (tomaker1.position.latitude)
            {
            
            [ModelManager getRoutingByGoogleWithStart:toMaker.position endLocation:tomaker1.position handlerSuccess:^(NSDictionary *response) {
                
            
                gTrip.estimateDistance = [NSString stringWithFormat:@"%.3f", [Validator getSafeFloat:response[@"routes"][0][@"legs"][0][@"distance"][@"value"]]/1000];
                singleLine1.map = nil;
                GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
                singleLine1 = [GMSPolyline polylineWithPath:path];
                singleLine1.strokeWidth = 2;
                singleLine1.strokeColor = [UIColor blueColor];
                singleLine1.map = self.gMap;
                estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
                self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];
                
                
                if (tomaker2.position.latitude)
                {
                    [ModelManager getRoutingByGoogleWithStart:tomaker1.position endLocation:tomaker2.position handlerSuccess:^(NSDictionary *response) {
                        
                        
                        gTrip.estimateDistance = [NSString stringWithFormat:@"%.3f", [Validator getSafeFloat:response[@"routes"][0][@"legs"][0][@"distance"][@"value"]]/1000];
                        singleLine1.map = nil;
                        GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
                        singleLine2 = [GMSPolyline polylineWithPath:path];
                        singleLine2.strokeWidth = 2;
                        singleLine2.strokeColor = [UIColor blueColor];
                        singleLine2.map = self.gMap;
                        estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
                        self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];
                        
                        
                        
                        if (tomaker3.position.latitude)
                        {
                            [ModelManager getRoutingByGoogleWithStart:tomaker2.position endLocation:tomaker3.position handlerSuccess:^(NSDictionary *response) {
                                
                                
                                gTrip.estimateDistance = [NSString stringWithFormat:@"%.3f", [Validator getSafeFloat:response[@"routes"][0][@"legs"][0][@"distance"][@"value"]]/1000];
                                singleLine1.map = nil;
                                GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
                                singleLine3 = [GMSPolyline polylineWithPath:path];
                                singleLine3.strokeWidth = 2;
                                singleLine3.strokeColor = [UIColor blueColor];
                                singleLine3.map = self.gMap;
                                estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
                                self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];
                                
                                
                                
                                if (tomaker4.position.latitude)
                                {
                                    [ModelManager getRoutingByGoogleWithStart:tomaker3.position endLocation:tomaker4.position handlerSuccess:^(NSDictionary *response)
                                    {
                                        
                                    gTrip.estimateDistance = [NSString stringWithFormat:@"%.3f", [Validator getSafeFloat:response[@"routes"][0][@"legs"][0][@"distance"][@"value"]]/1000];
                                        singleLine1.map = nil;
                                        GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
                                        singleLine4 = [GMSPolyline polylineWithPath:path];
                                        singleLine4.strokeWidth = 2;
                                        singleLine4.strokeColor = [UIColor blueColor];
                                        singleLine4.map = self.gMap;
                                        estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
                                        self.estimated_fare.text = [NSString stringWithFormat:@"%@%.2f",[Util localized:@"currentcy"],estimateFare];
                                        
                                    }
                                     
                                                                handlerFailed:^(NSString *err) {
                                                                    
                                                                    
                                                                    gTrip.estimateDistance = 0;
                                                                    
                                                                    
                                                                }];
                                    
                                }
 
                                
                                
                                
                                
                                
                            }
                             
                                                        handlerFailed:^(NSString *err) {
                                                            
                                                            
                                                            gTrip.estimateDistance = 0;
                                                            
                                                            
                                                        }];
                            
                        }

                        
                        
                    }
                     
                                                handlerFailed:^(NSString *err) {
                                                    
                                                    
                                                    gTrip.estimateDistance = 0;
                                                    
                                                    
                                                }];
                    
                }

                
                                
                
                
            }
             
                                        handlerFailed:^(NSString *err) {
                                            
                                            
                                            gTrip.estimateDistance = 0;
                                            
                                            
                                        }];
            
            }
             
            [UIView transitionWithView:_office_view
                              duration:0.20
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                _mapview_header.text = @"Select";
                                _office_view.hidden = TRUE;
                                _Home_view.hidden = TRUE;
                                _searching_vieew.hidden = TRUE;
                                self.Findingride_view.hidden = TRUE;
                                self.Main_requestview.hidden = TRUE;
                                _Gmap_opcontraint.constant = 84;
                                _Backbtn.hidden = false;

                                self.selectcar_view.hidden = false;
                                iscalled = true;
                                
                            }
                            completion:NULL];
            
            
        }
              handlerFailed:^(NSString *err) {
            
            gTrip.estimateDistance = 0;
 
        }];
    
    
        if (!(gTrip.estimateDistance > 0)) {

            return;
        }
     
        
    }
    
    else{
        _Backbtn.hidden = true;

        self.selectcar_view.hidden = true;


    }
    
    }
}


-(void)zoomToDirections{
    
//    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:fromMarker.position coordinate:toMaker.position];
//    [_gMap animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:80.0f]];
}
-(void)zoomStartPlace{
    
    [_gMap animateToLocation:fromMarker.position];
    [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
}
-(void)zoomEndPlace{
    [_gMap animateToLocation:toMaker.position];
    [_gMap animateToZoom:MAP_ZOOM_DEFAULT];
}
-(void)removeAllPolyLine{
    singleLine.map = nil;
}
-(IBAction)onDone:(id)sender{
    if (_txtFromName.text.length==0||_txtToName.text.length==0) {
        [Util showMessage:[Util localized:[Util localized:@"missing_from_point_or_endpoint_title"]] withTitle:[Util localized:@"app_name"]];
        return;
    }
    else if (gUser.phone.length==0) {
        [Util showMessage:[Util localized:@"msg_update_phone_number"] withTitle:[Util localized:@"app_name"]];
        return;
    }
    else if (gTrip.link.length==0) {
        [Util showMessage:[Util localized:@"msg_update_trip_link"] withTitle:[Util localized:@"app_name"]];
        return;
    }
    
    if (!(gTrip.estimateDistance > 0)) {
        
        [self.view makeToast:@"The direction from A to B which you picked is not verified by the map yet. You should choose either suggestion address or pick a point on a map." duration:2.0 position:CSToastPositionBottom];
        return;
    }
    else
    {
        float estimateFare = [Util calculateEstimateFareWithDistance:gTrip.estimateDistance.floatValue numberOfSeats:(int)gTrip.link.length];
        
        
        if (estimateFare >= gUser.point.floatValue) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"Your balance (%@) is not enough to book this trip (about %.2f).", gUser.point, estimateFare] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                AddPaymentViewController *addPaymentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPaymentViewController"];
                [self.navigationController pushViewController:addPaymentVC animated:YES];
                
            }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:okAction];
            [alertVC addAction:cancelAction];
            
            [self presentViewController:alertVC animated:YES completion:nil];
            
            return;
        }
        else
        {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"Estimated fare: %@%.2f. Do you want to continue?", [Util localized:@"currentcy"], estimateFare] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Book Now" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                WaitingViewController *waitingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WaitingViewController"];
                [self.navigationController pushViewController:waitingVC animated:YES];
                
            }];
            
//            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
//            [alertVC addAction:okAction];
//            [alertVC addAction:cancelAction];
//            
//            [self presentViewController:alertVC animated:YES completion:nil];
        }
 
    }
    
    
}
- (IBAction)BookNow_Action:(id)sender
{
    _Backbtn.hidden = true;

    locationcordhistory = [[NSMutableArray alloc]init];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    NSMutableArray *arraymain = [[NSMutableArray alloc]init];
    NSMutableArray * location = [[NSMutableArray alloc]init];
       NSArray *arr =  [[NSUserDefaults standardUserDefaults] objectForKey:@"locationhistory"];
    arraymain = [[NSUserDefaults standardUserDefaults] objectForKey:@"locationcordhistory"];
    
    for (int i=0;i< [arr count];i++)
    {
        [ locationcordhistory addObject:[[arraymain valueForKey:@"lat"]objectAtIndex:i]
         ];
        [ array addObject:[[arraymain valueForKey:@"long"]objectAtIndex:i]
         ];
        
        
    }
    for (int i=0;i< [arr count];i++)
    {
        [ location addObject:[arr objectAtIndex:i]
         ];
        
    }
    NSNumber *lat = [NSNumber numberWithDouble:fromcordlocation.coordinate.latitude];
    NSNumber *lon = [NSNumber numberWithDouble:fromcordlocation.coordinate.longitude];
    NSNumber *lat1 = [NSNumber numberWithDouble:tocordlocation.coordinate.latitude];
    NSNumber *lon2 = [NSNumber numberWithDouble:tocordlocation.coordinate.longitude];
    
    NSString *str =  _txtFromName.text;
    
    NSString *str1 =  _txtToName.text;

    if (![location containsObject:str] && !(lat == nil) && !(lon == nil) &&  !(lat == 0) && !(lon == 0) )
    {
        [location addObject: str];
       
         [locationcordhistory addObject: lat];
         [array addObject:lon];
    }
    
    if (![location containsObject:str1] && !(lat1 == nil) && !(lon2 == nil) &&  !(lat1 == 0) && !(lon2 == 0))
    {
        [location addObject:str1];
   
        
        [locationcordhistory addObject:lat1];
        
        [array addObject:lon2];
    }
    
     NSDictionary *userLocation=@{@"lat":locationcordhistory,@"long":array 
                                 };
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:location forKey:@"locationhistory"];
    [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:@"locationcordhistory"];
    [userDefaults synchronize];
    
    
    issearchingdriver = true;
    arrive = false;
    _selectcar_view.hidden = TRUE;
    if (estimateFare >= gUser.point.floatValue) {
        
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"Your balance (%@) is not enough to book this trip (about %.2f).", gUser.point, estimateFare] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            AddPaymentViewController *addPaymentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPaymentViewController"];
            [self.navigationController pushViewController:addPaymentVC animated:YES];
            
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:okAction];
        [alertVC addAction:cancelAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        
    }
    else
    {
        
//        WaitingViewController *waitingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WaitingViewController"];
        
        
  //For WAiting Task :-
        numberDriversSent = 0;
        backFromBackground = false;
        if ((gTrip.tripId>0)) {
            if (gTrip.startTime.length>0) {
                [self onStartTrip];
            }
        }
        else{
            
            timesRepeat = [AppSettings share].maxTimeSendRequest;
            [self sendRequestInForceGround123];
//            mainTimer = [NSTimer scheduledTimerWithTimeInterval:[AppSettings share].timeToSendRequestAgain target:self selector:@selector(sendRequestInForceGround123) userInfo:nil repeats:YES];
            
        }
        
        
                                [UIView transitionWithView:_office_view
                                                  duration:0.4
                                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                                animations:^{
                                                    _meetlocation_view .hidden = false;
                                                    _office_view.hidden = TRUE;
                                                    _Home_view.hidden = TRUE;
                                                    _searching_vieew.hidden = TRUE;
                                                    _Gmap_opcontraint.constant = 84;
                                                    _mapview_header.text = @"Connecting";
                                                    //_Findingride_view.hidden =  false;
                                                    _selectcar_view.hidden = TRUE;
                                                    _connectingdrivers.hidden = false;
                                                    _Main_requestview.hidden = false;
                                                    _meetlocation_view.hidden = false;
                                     
                                                }
                                                completion:NULL];
    }
    
}

#pragma mark Waiting Task :-

- (void)sendRequestInForceGround123{
    
    timesRepeat -= 1;
    _selectcar_view.hidden = TRUE;
    if (timesRepeat == 0) {
        [mainTimer invalidate];
        [self cancelTrip];
    }else{
       [self sendRequest];
    }
    
}

- (void)appWillBecomForceGround123{
    
    if ([[Util topViewController] isKindOfClass:[WaitingViewController class]]) {
        
        if (backFromBackground) {
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
            return;
        }
        if (backgrTimer) {
            [backgrTimer invalidate];
            if (bgTask != UIBackgroundTaskInvalid)
            {
                [[UIApplication sharedApplication] endBackgroundTask:bgTask];
            }
            
        }
        
        if (self) {
            if (gTrip.status > 0) {
                return;
            }
            //  [self reloadStatus];
        }
    }
}

- (void)appWillGoToBackGround123{
    if ([[Util topViewController] isKindOfClass:[WaitingViewController class]]) {
        [mainTimer invalidate];
        bgTask = UIBackgroundTaskInvalid;
        bgTask = [[UIApplication sharedApplication]
                  beginBackgroundTaskWithExpirationHandler:^{
                      [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                  }];
        
       // backgrTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(sendRequestInBackground123) userInfo:nil repeats:YES];
    }
    
}

//- (void)sendRequestInBackground123{
//    
//    timesRepeat -= 1;
//    
//    if (timesRepeat == 0) {
//        [backgrTimer invalidate];
//        [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
//            if (arr.count>0) {
//                gTrip = [arr objectAtIndex:0];
//                [ModelManager cancelRequest:gTrip withSuccess:^{
//                    NSLog(@"Cancel request in background Success");
//                    backFromBackground = true;
//                    if (bgTask != UIBackgroundTaskInvalid)
//                    {
//                        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
//                    }
//                } andFailure:^(NSString *err) {
//                    if (bgTask != UIBackgroundTaskInvalid)
//                    {
//                        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
//                    }
//                }];
//            }else{
//                gTrip = [[Trips alloc]init];
//                if (bgTask != UIBackgroundTaskInvalid)
//                {
//                    [[UIApplication sharedApplication] endBackgroundTask:bgTask];
//                }
//            }
//            
//        } andFailure:^(NSString *err) {
//            if (bgTask != UIBackgroundTaskInvalid)
//            {
//                [[UIApplication sharedApplication] endBackgroundTask:bgTask];
//            }
//        }];
//        
//    }else{
//        [ModelManager sendRequestWithSuccess:^(NSDictionary *dict) {
//            NSLog(@"send request in background success");
//            numberDriversSent += [Validator getSafeInt:dict[@"count"]];
//        } andFailure:^(NSString *err) {
//            
//        }];
//        
//    }
//    
//}



-(void)sendRequest
{
    _selectcar_view.hidden = TRUE;

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager sendRequestWithSuccess:^(NSDictionary *dic) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        numberDriversSent += [Validator getSafeInt:dic[@"count"]];
        if (numberDriversSent == 0) {
            _selectcar_view.hidden = true;
            _bottom_layout.constant = 0;
            [UIView animateWithDuration:1.2
                             animations:^{
                                 [self.view layoutIfNeeded]; // Called on parent view
                             }];
            self.connectingdriver_Label.text = [NSString stringWithFormat:@"0 driver has received your request. Your request will auto refresh every %d seconds!", (int)[AppSettings share].timeToSendRequestAgain];
        }else{
            self.connectingdriver_Label.text = [NSString stringWithFormat:@"%d driver has received your request. Your request will auto refresh every %d seconds!", numberDriversSent, (int)[AppSettings share].timeToSendRequestAgain];
            
                [self.view layoutIfNeeded];
                
                _bottom_layout.constant = 0;
                [UIView animateWithDuration:1.2
                                 animations:^{
                                     [self.view layoutIfNeeded]; // Called on parent view
                                 }];
            
            }
        
        
        if ([[dic objectForKey:@"status"] isEqualToString:@"ERROR"]) {
            //            [Util showMessage:[dic objectForKey:@"message"] withTitle:[Util localized:@"app_name"]];
            //            [self.navigationController popViewControllerAnimated:YES];
        }else{
//            estim.text = [NSString stringWithFormat:@"*Estimated fare: %@", [Validator getSafeString:dic[@"estimate_fare"]]];
            [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
                //if (arr.count>0) {
                    //gTrip = [arr objectAtIndex:0];
                    [ModelManager showMyTripsWithSuccess:^(NSMutableArray *arr) {
                        if (arr.count>0) {
                            gTrip = [arr objectAtIndex:0];
                            
                            if (gTrip.status == approadching || gTrip.status == inprogress        ) {
                                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                
                                if ([[Util topViewController] isMemberOfClass:[OrderConfirmViewController class]]) {
                                    return;
                                }
                                
                             
                             _bottom_layout.constant = -120;
                                
                                [UIView animateWithDuration:1.5
                                                 animations:^{
                                                     [self.view layoutIfNeeded];
                                                      self.Findingride_view.hidden = TRUE;// Called on parent view
                                                 }];

                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.100 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    [UIView animateWithDuration:0.3 animations:^{
                                        
                                    } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
                                        [arrCarAnnotation removeAllObjects];
                                        _driver_mainview.hidden = false;
                                        _mapview_header.text = @"Drivus";
                                        _office_view.hidden = TRUE;
                                        _Home_view.hidden = TRUE;
                                        _searching_vieew.hidden = TRUE;
                                        self.Findingride_view.hidden = TRUE;
                                        _Gmap_opcontraint.constant = 84;
                                        _Backbtn.hidden = true;
                                        self.selectcar_view.hidden = TRUE;
                                        self.meetlocation_view.hidden = TRUE;
                                        self.Main_requestview.hidden = TRUE;
                                        // _bottom_layout.constant = -50;
                                        
                                    }];
                                });
                                 [self reloadCarAnnotation];
                                [ mainTimer invalidate];
                                _gMap.delegate = self;

                                [self configMapView];
                                isloading = false;
                                didDirectToA = false;
                                
                            
                              
                                [self.navigationController setNavigationBarHidden:YES];
                                [self getTripDetail];
                                [self updateDistance];

                                    [updatetimer invalidate];
                                    updatetimer=nil;
                              
                                
                                
                                return;
                            }
                            
                            
                            
                        }
                        
                    } andFailure:^(NSString *err) {
                        
                    }];
                        }
                
             andFailure:^(NSString *err) {
                
            }];
        }
        
    } andFailure:^(NSString *err) {
        [Util showMessage:err withTitle:[Util localized:@"app_name"]];
        //        [self.navigationController popViewControllerAnimated:YES];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}


-(void)onStartTrip
{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.100 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3 animations:^{
            
        }
                         completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
                             [arrCarAnnotation removeAllObjects];
            _driver_mainview.hidden = false;
            _mapview_header.text = @"Drivus";
            _office_view.hidden = TRUE;
            _Home_view.hidden = TRUE;
            _searching_vieew.hidden = TRUE;
            self.Findingride_view.hidden = TRUE;
            _Gmap_opcontraint.constant = 84;
                             _Backbtn.hidden = true;

            self.selectcar_view.hidden = TRUE;
            self.meetlocation_view.hidden = TRUE;
            self.Main_requestview.hidden = TRUE;
            _selectcar_view.hidden = true;

            // _bottom_layout.constant = -50;
            
        }];
    });
    
    [UIView animateWithDuration:0.1 animations:^{
        
    } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
        _bottom_layout.constant = -120;
        
    }];
     [self reloadCarAnnotation];
    [ mainTimer invalidate];
    _gMap.delegate = self;
      _selectcar_view.hidden = true;
    [self setNeedsStatusBarAppearanceUpdate];
    [self configMapView];
    isloading = false;
    didDirectToA = false;
    
    
    [self.navigationController setNavigationBarHidden:YES];
    [self getTripDetail];
   
//    if ( [updatetimer isValid]){
//        [updatetimer invalidate];
//        updatetimer=nil;
//    }
//    else
//    {
//        updatetimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateDistance) userInfo:nil repeats:YES];
//        
//    }
    
//    if ([[Util topViewController] isMemberOfClass:[WaitingViewController class]]) {
//        [self performSegueWithIdentifier:@"orderConfirm" sender:self];
//    }
    
    
}



- (IBAction)cancel_action:(id)sender
{
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"Do you really want to cancel trip?"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self reloadroutesview];
        [self cancelTrip];
        
        
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:okAction];
    [alertVC addAction:cancelAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
    
}


#pragma mark :- order confirm

- (void)configMapView{
    [_gMap clear];
    fromMarker = nil;
    toMaker = nil;
    _gMap.myLocationEnabled = true;
    _gMap.camera = [GMSCameraPosition cameraWithLatitude:gShareLocation.myLocation.latitude longitude:gShareLocation.myLocation.longitude zoom:16];
    _gMap.delegate = self;
    toMaker.map = nil;
    toMaker = [GMSMarker markerWithPosition:gTrip.endLocation.position];
    toMaker.icon = [UIImage imageNamed:@"icon_toB"];
    toMaker.map = _gMap;
    
//     tomaker1.map = nil;
//    tomaker1 = [GMSMarker markerWithPosition:gTrip.endLocation1.position];
//    tomaker1.icon = [UIImage imageNamed:@"icon_fromc"];
//    tomaker1.map = _gMap;
//     tomaker2.map = nil;
//    tomaker2 = [GMSMarker markerWithPosition:gTrip.endLocation2.position];
//    tomaker2.icon = [UIImage imageNamed:@"d1"];
//    tomaker2.map = _gMap;
//     tomaker3.map = nil;
//    tomaker3 = [GMSMarker markerWithPosition:gTrip.endLocation3.position];
//    tomaker3.icon = [UIImage imageNamed:@"icon_frome"];
//    tomaker3.map = self.gMap;
//    
//    tomaker4.map = nil;
//

//    tomaker4 = [GMSMarker markerWithPosition:gTrip.endLocation4.position];
//    tomaker4.icon = [UIImage imageNamed:@"icon_fromf"];
//    tomaker4.map = self.gMap;
    
    fromMarker.map = nil;
    fromMarker = [GMSMarker markerWithPosition:gTrip.startLocation.position];
    fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
    fromMarker.map = _gMap;
    
}


- (void)driverArrived{
    if (gTrip.status == approadching) {
      //  arrive = true;
        ontrip =  true;
        _lblStatus.text = @"DRIVER ARRIVED";
        _lblHelp.text = @"Cancel ";
       // [self.btnCancelTrip setTitle:[NSString fontAwesomeIconStringForEnum:FAPhone] forState:UIControlStateNormal];
       AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        [self.view makeToast:@"The driver has arrived!"];
        
    }
    
}

-(void)onStartTripsmain{
    gTrip.status = inprogress;
    [self setupHelpBtnWhenStatusChange];
    //    [timer invalidate];
    
}
-(void)onEndTrip{
    ontrip = false;
    iscalled = false;
     [timer invalidate];
    [updatetimer invalidate];
    updatetimer = nil;
    gTrip.status = pendingPayment;
    //[self cancelTrip]; // prabhjot
    [UIView transitionWithView:_office_view
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        _driver_mainview.hidden = TRUE;
                        _mapview_header.text = @"Drivus";
                        Addroutebtn.hidden = true;
                        _office_view.hidden = false;
                        _Home_view.hidden = false;
                        _searching_vieew.hidden = false;
                        self.Findingride_view.hidden = TRUE;
                        _Gmap_opcontraint.constant = 84;
                        _Backbtn.hidden = true;

                        self.selectcar_view.hidden = TRUE;
                        self.meetlocation_view.hidden = TRUE;
                        self.Main_requestview.hidden = TRUE;
                        
                    }
                    completion:NULL];
    [self reloadCarAnnotation];
    
    [UIView animateWithDuration:0.1 animations:^{
        
    } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
        _bottom_layout.constant = -120;
        
    }];
    if ([[Util topViewController] isMemberOfClass:[RatingTripViewController class]]) {
        return;
    }
    else
    {
    RatingTripViewController *orderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingTripViewController"];
    [self.navigationController pushViewController:orderVC animated:NO];
    }
}

-(void)getTripDetail{
    
    [ModelManager showMyTripsWithSuccess:^(NSMutableArray *arr) {
        [self updateInfo];
        if (gTrip.status == approadching) {
            ontrip = true;
            [self configMapView];
            return;
        }
        else if(gTrip.status == inprogress){
            [self onStartTripsmain];
            [self configMapView];
            return;
        }
        else  if (gTrip.status == pendingPayment) {
            
            [updatetimer invalidate];
            updatetimer = nil;
            [self onEndTrip];
            return;
        }
    } andFailure:^(NSString *err) {
        
    }];
    
}

- (void)updateInfo
{
    if (gTrip.tripId.length>0) {
        [ModelManager showTripDetailWithTripId:gTrip.tripId withSuccess:^(Trips *trip) {
            gTrip = trip;
            [Util setObject:gTrip.tripId forKey:CURRENT_TRIP_ID_KEY];
            if (self) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setText123];
                });
            }
        } andFailure:^(NSString *err) {
        }];
        
    }
}


-(void)driverconfirm
{
    ontrip = true;
    _bottom_layout.constant = -120;
    [UIView animateWithDuration:1.5
                     animations:^{
                         [self.view layoutIfNeeded];
                         self.Findingride_view.hidden = TRUE;// Called on parent view
                     }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.00 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3 animations:^{
            
        } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
            [arrCarAnnotation removeAllObjects];
            _driver_mainview.hidden = false;
            _mapview_header.text = @"Drivus";
            _office_view.hidden = TRUE;
            _Home_view.hidden = TRUE;
            _searching_vieew.hidden = TRUE;
            self.Findingride_view.hidden = TRUE;
            _Gmap_opcontraint.constant = 84;
            _Backbtn.hidden = true;

            self.selectcar_view.hidden = TRUE;
            self.meetlocation_view.hidden = TRUE;
            self.Main_requestview.hidden = TRUE;
            // _bottom_layout.constant = -50;
            
        }];
    });
     [self reloadCarAnnotation];
    [ mainTimer invalidate];
    _gMap.delegate = self;
    
    [self configMapView];
    isloading = false;
    didDirectToA = false;
    

    [self.navigationController setNavigationBarHidden:YES];
    [self getTripDetail];
    [self updateDistance];
    
    [updatetimer invalidate];
    updatetimer=nil;
    updatetimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateDistance) userInfo:nil repeats:YES];
    

}

-(void)updateDistance{
    NSLog(@"will update distance");
    if (gTrip.tripId.length > 0 && !isloading) {
        isloading = true;
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            
            [ModelManager getCurrentLocationWithDriverId:gTrip.driverId withSuccess:^(float latitude, float longtitude) {
                CLLocation *driverLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longtitude];
                
                if (driverLocation)
                {
                    isloading = false;
                    if (gTrip.status ==approadching && !didDirectToA) {
                        didDirectToA = true;
                        [self drawDirectionFrom:driverLocation.coordinate to:gTrip.startLocation.position];
                        NSLog(@"Did update distance:%.2f %.2f", driverLocation.coordinate.latitude, driverLocation.coordinate.longitude);
                       
                    }

                    [self makeDirectionWithCurrentLocation:driverLocation.coordinate];
                }
            }
             
             
                                                 failure:^(NSString *err) {
                                                     isloading = false;
                                                 }];
        });
        
    }
}

-(void)driverCancelTrip{
    [Util showMessage:[Util localized:@"msg_trip_cancel_by_driver"] withTitle:[Util localized:@"app_name"]];
      [self  reloadroutesview];
    gTrip = [[Trips alloc] init];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
}

-(void)onStartTripconfirm{
    gTrip.status = inprogress;
    [self setupHelpBtnWhenStatusChange];
    //    [timer invalidate];
    
}

- (void)setCarPlatetextWithAttribute{
    NSString *prefixPlate = [Util localized:@"lbl_car_plate"];
    NSString *contentPlate = gTrip.driver.carPlate.length? gTrip.driver.carPlate : [Util localized:@"lbl_loading"];
    NSString *infoString = [NSString stringWithFormat:@"%@%@", prefixPlate, contentPlate];
    
    NSMutableAttributedString *attStringPlate=[[NSMutableAttributedString alloc] initWithString:infoString];
    
      UIFont *font_regular=[UIFont fontWithName:@"Raleway-Medium" size:14.0f];
    UIFont *font_bold=[UIFont fontWithName:@"Raleway-Bold" size:14.0f];
    
    [attStringPlate addAttribute:NSFontAttributeName value:font_bold range:NSMakeRange(0, prefixPlate.length)];
    [self.lblCarePlateValue setAttributedText:attStringPlate];
}

- (void)setDistancetext:(NSString*)contentText{
    NSString *prefixPlate = [Util localized:@"lbl_distance"];
    
    NSString *infoString = [NSString stringWithFormat:@"%@%@", prefixPlate, contentText];
    
    NSMutableAttributedString *attStringPlate=[[NSMutableAttributedString alloc] initWithString:infoString];
    
      UIFont *font_regular=[UIFont fontWithName:@"Raleway-Medium" size:14.0f];
    UIFont *font_bold=[UIFont fontWithName:@"Raleway-Bold" size:14.0f];
    
    [attStringPlate addAttribute:NSFontAttributeName value:font_bold range:NSMakeRange(0, prefixPlate.length)];
    [self.lblDistanceValue setAttributedText:attStringPlate];
    
}


-(void)onCall{
    [self ShareWithEmail];
}

-(void)ShareWithEmail
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        [mailController setMailComposeDelegate:self];
        [mailController setSubject:@"Drivus Help"];
        [mailController setToRecipients:[NSArray arrayWithObjects:@"Help@Drivus.com", @"email2", nil]];
        NSString *temp =[NSString stringWithFormat:@"Please Wite Message here wat kind of help you need"];
        [mailController setMessageBody:temp isHTML:NO];
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please setup mail in your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
 
}
- (IBAction)onCancelTrip:(id)sender {
    if (gTrip.status != inprogress && ![_lblStatus.text isEqualToString:@"ON THE TRIP"]) {
    
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"app_name", nil) message:[NSString stringWithFormat:@"Do you really want to cancel trip?"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self reloadroutesview];
            [self cancelTrip];
            
            
            
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:okAction];
        [alertVC addAction:cancelAction];
        
        [self presentViewController:alertVC animated:YES completion:nil];
        

    }else{
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [ModelManager sendHelpRequestWithSuccess:^(NSString *strSuccessReturn) {
                
            } failure:^(NSString *err) {
                
            }];
        });
        [self ShareWithEmail];

        //[Util callPhoneNumber:[AppSettings share].adminPhoneNumber];
        
    }
}

- (void)makeDirectionWithCurrentLocation:(CLLocationCoordinate2D)currentLocation1{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        MKPlacemark *placemarkTo;
        if (gTrip.status != inprogress) {
            placemarkTo = [[MKPlacemark alloc] initWithCoordinate:gTrip.startLocation.position addressDictionary:nil] ;
            
        }else{
            placemarkTo = [[MKPlacemark alloc] initWithCoordinate:gTrip.endLocation.position addressDictionary:nil] ;
        }
        
        [self updateDistanceWithGoogleDirec:currentLocation1 toPoint:placemarkTo.coordinate];
    });
    
}

- (void)updateDistanceWithGoogleDirec:(CLLocationCoordinate2D)fromPoint toPoint:(CLLocationCoordinate2D)toPoint{
    carMaker.map = nil;
    carMaker = [GMSMarker markerWithPosition:fromPoint];
  
  carMaker.icon = [UIImage imageNamed:@"img_car"];
    carMaker.map = _gMap;
    
    [ModelManager getRoutingByGoogleWithStart:fromPoint endLocation:toPoint handlerSuccess:^(NSDictionary *dicLegsReturn) {
        float distance = [Validator getSafeFloat:dicLegsReturn[@"routes"][0][@"legs"][0][@"distance"][@"value"]];
        float expectedTravelTime = [Validator getSafeFloat:dicLegsReturn[@"routes"][0][@"legs"][0][@"duration"][@"value"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (gTrip.status != inprogress) {
                if (distance < 1000) {
                    
                    [self setDistancetext:[NSString stringWithFormat:@"Driver %.1f miles to A (%.0f min)", ((distance/1000)/0.621371192), expectedTravelTime/60]];
                }else{
                    [self setDistancetext:[NSString stringWithFormat:@"Driver %.1f miles to A (%.0f min)", ((distance/1000)/0.621371192),expectedTravelTime/60]];
                }
                
            }else{
                if (distance < 1000) {
                    [self setDistancetext:[NSString stringWithFormat:@"Trip %.1f miles to B (%.0f min)", ((distance/1000)/0.621371192), expectedTravelTime/60]];
                    
                }else{
                    [self setDistancetext:[NSString stringWithFormat:@"Trip %.1f miles to B (%.0f min)", ((distance/1000)/0.621371192), expectedTravelTime/60]];
                    
                }
            }
        });
        
    } handlerFailed:^(NSString *err) {
        
    }];
    
}
- (void)setText123{
    
    
    [self.profileView setProfileWithAvatarLink:gTrip.driver.imageDriver phoneNumber:gTrip.driver.phone starValue:gTrip.driver.rateup downvalue:gTrip.driver.ratedown name:gTrip.driver.driverName ];
    
  
    
    [self setCarPlatetextWithAttribute];
    [self setDistancetext:[Util localized:@"lbl_calculating"]];
    [self.carImg setImageWithURL:[NSURL URLWithString:gTrip.driver.carImage]];
    //_lblStatus.text = @"DRIVER IS COMING";
    [self setupHelpBtnWhenStatusChange];
   
    

    
    
}

- (void)setupHelpBtnWhenStatusChange

{
    if (gTrip.status != inprogress) {
        if (arrive == false)
        {
        _lblStatus.text = @"DRIVER IS COMING";
//    [self.btnCancelTrip setTitle:[NSString fontAwesomeIconStringForEnum:FALongArrowLeft] forState:UIControlStateNormal];
        _lblHelp.text = @"CANCEL";
        }
    }
    else
    {
        
        cancel_trip.hidden=true;
        _lblHelp.text = @"HELP";
        _lblStatus.text = [Util localized:@"lbl_in_process"].uppercaseString;
        [self drawDirectionFrom:gTrip.startLocation.position to:gTrip.endLocation.position];
//        [self.btnCancelTrip setTitle:[NSString fontAwesomeIconStringForEnum:FAPhone] forState:UIControlStateNormal];
        return;
    }
}



- (void)drawDirectionFrom:(CLLocationCoordinate2D)fromLocation to:(CLLocationCoordinate2D)toLocation{
    
    [ModelManager getRoutingByGoogleWithStart:fromLocation endLocation:toLocation handlerSuccess:^(NSDictionary *response) {
        singleLine.map = nil;
        GMSPath *path =[GMSPath pathFromEncodedPath:response[@"routes"][0][@"overview_polyline"][@"points"]];
        singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 2;
        singleLine.strokeColor = [UIColor blueColor];
        dispatch_async(dispatch_get_main_queue(), ^{
            singleLine.map = self.gMap;
        });
        
    } handlerFailed:^(NSString *err) {
        
    }];
}


- (IBAction)Office_Action:(id)sender
{
    if ((OfficeLocation.coordinate.latitude !=0) || (OfficeLocation.coordinate.longitude !=0 ))
    {
        isiconselect = true;
        self.txtToName.text  = [[NSUserDefaults standardUserDefaults] valueForKey:@"officeAddress"];

    toMaker.map = nil;
    toMaker = [GMSMarker markerWithPosition:OfficeLocation.coordinate];
    toMaker.icon = [UIImage imageNamed:@"icon_toB"];
      //  Addroutebtn.hidden = false;
    toMaker.map = _gMap;
    [self zoomEndPlace];
    [self getLocationNameWithPosition:OfficeLocation.coordinate];
    }

}

- (IBAction)Home_Action:(id)sender {
    if ((HomeLocation.coordinate.latitude !=0) || (HomeLocation.coordinate.longitude !=0 ))
    {
        isiconselect = true;
        self.txtToName.text  = [[NSUserDefaults standardUserDefaults] valueForKey:@"HomeAddress"];
    NSLog(@"%@",self.txtToName);
    toMaker.map = nil;
    toMaker = [GMSMarker markerWithPosition:HomeLocation.coordinate];
    toMaker.icon = [UIImage imageNamed:@"icon_toB"];
      // Addroutebtn.hidden = false;
    toMaker.map = _gMap;
    [self zoomEndPlace];
    [self getLocationNameWithPosition:HomeLocation.coordinate];
}
}

//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    [self.view endEditing:YES];// this will do the trick
//}

- (IBAction)getcurrentlocation:(id)sender
{
     getdestLocation = false;
    firsttime = false;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

- (IBAction)getdestlocation:(id)sender
{
    getdestLocation = true;
    firsttime = false;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
}
-(void)darkenImage {
       
    [loc_searchview.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [loc_searchview.layer setShadowOffset:CGSizeMake(0, 0)];
    [loc_searchview.layer setShadowRadius:5.0];
    [loc_searchview.layer setShadowOpacity:0.9];
    

}

- (IBAction)Show_addlocationoptions:(id)sender
{
    [self darkenImage];
   //  _locItemsView.hidden = YES;
    //.frame.size.height = 100;
     loc_searchview .hidden = true;
    _searchViewHeight.constant = 70;
    [self.view endEditing:YES];
    //Change height
    _searchViewHeight.constant = 70;
    _locItemsView.frame = CGRectMake(_locItemsView.frame.origin.x, _locItemsView.frame.origin.y, _locItemsView.frame.size.width, 70);
//    CGRect newFrame = CGRectMake(loc_searchview.frame.origin.x,123, loc_searchview.frame.size.width, 80);
//      loc_searchview.frame = newFrame;
//    CGRect newFrame1 = CGRectMake(_locItemsView.frame.origin.x,_locItemsView.frame.origin.y, loc_searchview.frame.size.width, 80);
//    _locItemsView.frame = newFrame1;
    
//    CGRect newFrame = loc_searchview.frame;
//    newFrame.size.height = 80;
//    [loc_searchview setFrame:newFrame];
//    
//    CGRect newFrame1 = loc_searchview.frame;
//    newFrame1.size.height = 80;
//    [loc_searchview setFrame:newFrame1];
    
    locationhistory = [[NSMutableArray alloc]init];
    locationcordhistory = [[NSMutableArray alloc]init];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    locationhistory = [userDefaults objectForKey:@"locationhistory"];
    locationcordhistory = [userDefaults objectForKey:@"locationcordhistory"];
    
    _Height_constraint.constant = 50;
    if ([locationhistory count ] > 0)
    {
        if ([locationhistory count] >4)
        {
            
            _Height_constraint.constant = 350;
        }
        else
        {
            _Height_constraint.constant = 250 ;
        }
    }
    
    if ([sender tag] == 1)
    {
        
        if(![sender isSelected])
        {
            if ([locationhistory count] >0)
            {
                _recent_Locations.hidden = false;
            }
            [_recent_Locations reloadData];
            [sender setSelected:true];
            
        [UIView animateWithDuration:0.25 animations:^{
            fromlocation = true;
            tolocation = false;
            routestrue = false;
            _Locations_option.hidden = false;
        } completion:^(BOOL finished) {
        }];
            
            
        }
        else
        {
            [sender setSelected:false];
            
            [UIView animateWithDuration:0.25 animations:^{
                fromlocation = false;
                tolocation = false;
                _Locations_option.hidden = true;
            } completion:^(BOOL finished) {
            }];
            
        }
        
    }
    else
    {
      //  if(![sender isSelected])
       // {
            
            if ([locationhistory count] >0)
            {
                _recent_Locations.hidden = false;
            }
            [_recent_Locations reloadData];
            [sender setSelected:true];
            [UIView animateWithDuration:0.25 animations:^{
            fromlocation = false;
            tolocation = true;
          routestrue = false;
            _Locations_option.hidden = false;
        } completion:^(BOOL finished) {
        }];
        //}
       // else
       /// {
            //[sender setSelected:false];
            //[UIView animateWithDuration:0.25 animations:^{
                //fromlocation = false;
               // tolocation = false;
               // _Locations_option.hidden = true;
           // } completion:^(BOOL finished) {
            //}];
        //}
        }

}


- (IBAction)AddPin:(id)sender
{
  
    _Locations_option.hidden = true;
    if (fromlocation == true)
    {
     
        [UIView animateWithDuration:0.25 animations:^{
            moveAlocation = true;
            moveBlocation = false;
        } completion:^(BOOL finished) {
        }];
        
    }
    else if (tolocation == true)
    {
        
        [UIView animateWithDuration:0.25 animations:^{
            moveAlocation = false;
            moveBlocation = true;
        } completion:^(BOOL finished) {
        }];
        
     
    }
    else
    {
        [UIView animateWithDuration:0.25 animations:^{
            moveAlocation = false;
            moveBlocation = false;
            //routestrue = true;
        } completion:^(BOOL finished) {
        }];
    }
    
    
}

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{

    // if self.mapGesture == true
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
    CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake(latitude,longitude);
    [self updateLocationoordinates:addressCoordinates];
    
}



 -(void)updateLocationoordinates:(CLLocationCoordinate2D) coordinates
{
    if (moveAlocation == true)
    {
     
       fromcordlocation =   [[CLLocation alloc] initWithLatitude:coordinates.latitude  longitude:coordinates.longitude];
        //cordines = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.latitude);
            fromMarker.map = nil;
            fromMarker = [GMSMarker markerWithPosition:coordinates];
            fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
            fromMarker.map = _gMap;
            //[self zoomStartPlace];
            
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [ModelManager getAddressNameByPosition:coordinates handlerSuccess:^(NSString *addressName) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    CustomPosition *addressObj = [[CustomPosition alloc] initWithPositon:coordinates addressName:addressName];
                    
                    if (coordinates.latitude == toMaker.position.latitude && coordinates.longitude == toMaker.position.longitude) {
                        if (      isiconselect == false)
                        {
                            self.txtToName.text = addressName;
                            _Donoe_btn.hidden = false;
                        }
                        
                        gTrip.endLocation = addressObj;
                        
                    }
                    if (coordinates.longitude == fromMarker.position.longitude && coordinates.latitude == fromMarker.position.latitude)
                    
                    {
                        
                        if (      isiconselect == false)
                        {
                            self.txtFromName.text = addressName;
                            
                        }
                        gTrip.startLocation = addressObj;
                        
                    }
                    
                    //[self reloadCarAnnotation];
                    
                    
                   
                });
            } handlerFailed:^(NSString *err) {
                
            }];
        });
        //[self getLocationNameWithPosition:coordinates];
    }
    
    else if (moveBlocation == true)
    {
        tocordlocation =   [[CLLocation alloc] initWithLatitude:coordinates.latitude  longitude:coordinates.longitude];
        toMaker.map = nil;
        toMaker = [GMSMarker markerWithPosition:coordinates];
        toMaker.icon = [UIImage imageNamed:@"icon_toB"];
       // Addroutebtn.hidden = false;
        toMaker.map = _gMap;
        //[self zoomEndPlace];
        //[self getLocationNameWithPosition:coordinates];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [ModelManager getAddressNameByPosition:coordinates handlerSuccess:^(NSString *addressName) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    CustomPosition *addressObj = [[CustomPosition alloc] initWithPositon:coordinates addressName:addressName];
                    
                    if (coordinates.latitude == toMaker.position.latitude && coordinates.longitude == toMaker.position.longitude) {
                        if (      isiconselect == false)
                        {
                            self.txtToName.text = addressName;
                            _Donoe_btn.hidden = false;
                        }
                        
                        gTrip.endLocation = addressObj;
                        
                    }
                    if (coordinates.longitude == fromMarker.position.longitude && coordinates.latitude == fromMarker.position.latitude) {
                        
                        if (isiconselect == false)
                        {
                            self.txtFromName.text = addressName;
                            _Donoe_btn.hidden = false;
                        }
                        
                        gTrip.startLocation = addressObj;
                        
                    }

                    //[self reloadCarAnnotation];
                });
                
            } handlerFailed:^(NSString *err) {
                
            }];
        });
    }
    
//    else if (routestrue == true)
//    {
//        tocordlocation =   [[CLLocation alloc] initWithLatitude:coordinates.latitude  longitude:coordinates.longitude];
//        
//        toMaker.map = nil;
//        toMaker = [GMSMarker markerWithPosition:coordinates];
//        toMaker.icon = [UIImage imageNamed:@"icon_toB"];
//        toMaker.map = _gMap;
//
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            [ModelManager getAddressNameByPosition:coordinates handlerSuccess:^(NSString *addressName) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    CustomPosition *addressObj = [[CustomPosition alloc] initWithPositon:coordinates addressName:addressName];
//                    
//                    if (coordinates.latitude == toMaker.position.latitude && coordinates.longitude == toMaker.position.longitude) {
//                        if (      isiconselect == false)
//                        {
//                            
//                            
//                                                      //self.txtToName.text = addressName;
//                            //_Donoe_btn.hidden = false;
//                            
//                        }
//                        if (routeno == 1)
//                        {
//                             routeno++;
//                            _Addroute_Label.text = addressName;
//                            firstroutedash.hidden = false;
//                            firstroutedestimg.hidden = false;
//                            Heightconstraint.constant = 180;
//                            gTrip.endLocation1 = addressObj;
//                        }
//                        else if (routeno == 2)
//                        {
//                             routeno++;
//                            secrouteLabel.text = addressName;
//                            secroutedash.hidden = false;
//                            secroutedestimg.hidden = false;
//                            secrouteview.hidden = false;
//                            secrouteLabel.hidden = false;
//                            Heightconstraint.constant = 200;
//                            gTrip.endLocation2 = addressObj;
//                        }
//                        else if (routeno == 3)
//                        {
//                             routeno++;
//                            thirdrouteLabel.text = addressName;
//                            thirdroutedash.hidden = false;
//                            thirdroutedestimg.hidden = false;
//                            thirdrouteview.hidden = false;
//                            thirdrouteLabel.hidden = false;
//                            Heightconstraint.constant = 230;
//                            gTrip.endLocation3 = addressObj;
//                            
//                        }
//                        else if (routeno == 4)
//                        {
//                             routeno++;
//                            fourthrouteLabel.text = addressName;
//                           fourthroutedash.hidden = false;
//                            fourthroutedestimg.hidden = false;
//                            fourthrouteview.hidden = false;
//                            fourthrouteLabel.hidden = false;
//                            Heightconstraint.constant = 270;
//                            gTrip.endLocation4 = addressObj;
//                            
//                        }
//                        
//                    }
//                    if (coordinates.longitude == fromMarker.position.longitude && coordinates.latitude == fromMarker.position.latitude) {
//                        
//                        if (routeno == 1)
//                        {
//                             routeno++;
//                            _Addroute_Label.text = addressName;
//                            firstroutedash.hidden = false;
//                            firstroutedestimg.hidden = false;
//                            Heightconstraint.constant = 180;
//                            gTrip.endLocation1 = addressObj;
//                        }
//                        else if (routeno == 2)
//                        {
//                             routeno++;
//                            secrouteLabel.text = addressName;
//                            secroutedash.hidden = false;
//                            secroutedestimg.hidden = false;
//                            secrouteview.hidden = false;
//                            secrouteLabel.hidden = false;
//                            Heightconstraint.constant = 200;
//                            gTrip.endLocation2 = addressObj;
//                        }
//                        else if (routeno == 3)
//                        {
//                             routeno++;
//                            thirdrouteLabel.text = addressName;
//                            thirdroutedash.hidden = false;
//                            thirdroutedestimg.hidden = false;
//                            thirdrouteview.hidden = false;
//                            thirdrouteLabel.hidden = false;
//                            Heightconstraint.constant = 230;
//                            gTrip.endLocation3 = addressObj;
//                            
//                        }
//                        else if (routeno == 4)
//                        {
//                             routeno++;
//                            fourthrouteLabel.text = addressName;
//                            fourthroutedash.hidden = false;
//                            fourthroutedestimg.hidden = false;
//                            fourthrouteview.hidden = false;
//                            fourthrouteLabel.hidden = false;
//                            Heightconstraint.constant = 270;
//                            gTrip.endLocation4 = addressObj;
//                            
//                        }
//                        
//
//                        //gTrip.startLocation = addressObj;
//                        
//                    }
//                    
                    //[self reloadCarAnnotation];
               // });
                
          //  } handlerFailed:^(NSString *err) {
          //
          //  }];
        //});

  //  }
}

- (IBAction)Done_Action:(id)sender
{
    _locItemsView.hidden = YES;
    loc_searchview.hidden = YES;
    _recent_Locations.hidden = true;
    _Locations_option.hidden = true;
    _Donoe_btn.hidden = true;
    moveAlocation = false;
    moveBlocation = false;
    routestrue = false;
    iscalled = false;
    [self showDirections];
}


// tableview datasource and delegate :-

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [locationhistory count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    recent_locationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.Location_label.text = [locationhistory objectAtIndex:indexPath.row];
    // Configure the cell...
    //cell.textLabel.text = @"Hello there!";
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    _Locations_option.hidden = true;
    
   
    loc_searchview.hidden = true;
    NSNumber *lat = [[locationcordhistory valueForKey:@"lat"]objectAtIndex:indexPath.row];
    NSNumber *log = [[locationcordhistory valueForKey:@"long"]objectAtIndex:indexPath.row];
    isiconselect = false;
    
    CLLocation *cord = [[CLLocation alloc] initWithLatitude:[lat doubleValue]  longitude:[log doubleValue]];
    CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake([lat doubleValue] ,[log doubleValue]);

    CustomPosition *addressObj = [[CustomPosition alloc] initWithPositon:addressCoordinates addressName:[locationhistory objectAtIndex:indexPath.row]];

    if (fromlocation == true) //&& (_txtFromName.text.length == 0))
    {
        self.txtFromName.text =[locationhistory objectAtIndex:indexPath.row];

        
        gTrip.startLocation = addressObj;
                fromMarker.map = nil;
                fromMarker = [GMSMarker markerWithPosition:cord.coordinate];
                fromMarker.icon = [UIImage imageNamed:@"icon_fromA"];
                fromMarker.map = _gMap;
                [self zoomStartPlace];
        [self getLocationNameWithPosition:cord.coordinate];
        
        [self.Donoe_btn setHidden:false];
      //  [self showDirections];
    }
    else  if (tolocation == true) //&& (_txtToName.text.length == 0))
    {
        self.txtToName.text = [locationhistory objectAtIndex:indexPath.row];

                toMaker.map = nil;
                toMaker = [GMSMarker markerWithPosition:cord.coordinate];
                toMaker.icon = [UIImage imageNamed:@"icon_toB"];
        //Addroutebtn.hidden = false;
                toMaker.map = _gMap;
        gTrip.endLocation = addressObj;
                [self zoomEndPlace];
        [self getLocationNameWithPosition:cord.coordinate];
          _Donoe_btn.hidden = false;
      //  [self showDirections];
    }
   /* else if (_txtToName.text.length != 0)
    {
//      if (routeno == 1)
//      {
//                 }
//        else
//        {
//            NSString *str = _Addroute_Label.text;
//            _Addroute_Label.text = [NSString stringWithFormat:@"%@,%@",str,[locationhistory objectAtIndex:indexPath.row]];
//        }
        
       _route_Location.text = [locationhistory objectAtIndex:indexPath.row];
        _route_textfeild.text = [locationhistory objectAtIndex:indexPath.row];
        toMaker.map = nil;
        toMaker = [GMSMarker markerWithPosition:cord.coordinate];
        toMaker.icon = [UIImage imageNamed:@"icon_toB"];
        toMaker.map = _gMap;
        
        if (routeno == 1)
        {
             setpos++;
             routeno++;
            firstCrossBtn.hidden = false;
            firstroutedash.hidden = false;
            firstroutedestimg.hidden = false;
    _Addroute_Label.text = [locationhistory objectAtIndex:indexPath.row];

            Heightconstraint.constant = 180;
            gTrip.endLocation1 = addressObj;
        }
        else if (routeno == 2)
        {
             setpos++;
             routeno++;
            secCrossBtn.hidden = false;
            secroutedash.hidden = false;
            secroutedestimg.hidden = false;
            secrouteview.hidden = false;
            secrouteLabel.hidden = false;
    secrouteLabel.text = [locationhistory objectAtIndex:indexPath.row];
            Heightconstraint.constant = 200;
            gTrip.endLocation2 = addressObj;
        }
        else if (routeno == 3)
        {
             setpos++;
             routeno++;
            thirdCrossBtn.hidden = false;
            thirdroutedash.hidden = false;
            thirdroutedestimg.hidden = false;
            thirdrouteview.hidden = false;
            thirdrouteLabel.hidden = false;
            thirdrouteLabel.text = [locationhistory objectAtIndex:indexPath.row];
            Heightconstraint.constant = 230;
            gTrip.endLocation3 = addressObj;
            
        }
        else if (routeno == 4)
        {
             setpos++;
             routeno++;
            fourthCrossBtn.hidden = false;
            fourthroutedash.hidden = false;
            fourthroutedestimg.hidden = false;
            fourthrouteview.hidden = false;
            fourthrouteLabel.hidden = false;
            fourthrouteLabel.text = [locationhistory objectAtIndex:indexPath.row];
            Heightconstraint.constant = 270;
            gTrip.endLocation4 = addressObj;
            
        }

        
        
        gTrip.endLocation = addressObj;
        [self zoomEndPlace];
        
    }
    */
    

}

- (IBAction)Book_Later:(id)sender
{
    _Backbtn.hidden = true;
    _DatePicker_view.hidden = false;
    
    _Booknow_btn.enabled = false;
    _BookLatr_btn.enabled = false;
    
    self.selectcar_view.userInteractionEnabled = false;
    
   // NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
//    NSLocale *usLocale = [[NSLocale alloc]
//                          initWithLocaleIdentifier:@"IST"];
//    /_date_picker.timeZone = [NSTimeZone localTimeZone];
    
   // [_date_picker setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];

    
    
    NSDate *pickerDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    comps.day = 7;
    NSDate *sevenDays = [calendar dateByAddingComponents:comps toDate:pickerDate options:0];
    
    self.date_picker.minimumDate = pickerDate;
    self.date_picker.maximumDate = sevenDays;
    
//    NSString *selectionString = [[NSString alloc] initWithFormat:@"%@",
//                                 [pickerDate descriptionWithLocale:usLocale]];
//    _Pickup_Location.text = selectionString;

    NSTimeInterval ti = [pickerDate timeIntervalSince1970];
    gTrip.futuretime = [NSString stringWithFormat:@"%ld",(long)ti];
    NSLog(@"Timezone are %@",gTrip.futuretime);

}

- (IBAction)getselection:(id)sender
{
   // NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
   //[_date_picker setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    //temp date picker
//    UIDatePicker *tempDate = [[UIDatePicker alloc] init];
//    tempDate.date = [self.date_picker date];
//    [tempDate setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    NSDate *pickerDate = [self.date_picker date];
    
    NSTimeInterval ti = [pickerDate timeIntervalSince1970];
    gTrip.futuretime = [NSString stringWithFormat:@"%ld",(long)ti];
    NSLog(@"Timezone are %@",gTrip.futuretime);

   // tempDate = nil;
    

    
//    int day;
//    
//    
//    NSDateFormatter *Format = [[NSDateFormatter alloc] init];
//    [Format setDateFormat:@"dd/MM/yyyy"];
//    
//    NSDate *LocalDateStrinf=[NSDate date];
//    
//    if ([pickerDate compare:LocalDateStrinf] == NSOrderedDescending) {
//       
//        day =  [MapViewController daysBetweenDate:LocalDateStrinf andDate:pickerDate];
//
//    } else if ([pickerDate compare:LocalDateStrinf] == NSOrderedAscending) {
//        day = -1;
//        NSLog(@"date1 is later than date2");
//    }
//    else
//    {
//        day =  [MapViewController daysBetweenDate:LocalDateStrinf andDate:pickerDate];
//        
//    }
//    
//    
//    if (day >=0)
//    {
//        if (day > 7)
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
//                                                            message:@"You can book cab with coming 7 Days only!"
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//        }
//        else
//        {
//            
//            NSTimeInterval ti = [pickerDate timeIntervalSince1970];
//            gTrip.futuretime = [NSString stringWithFormat:@"%ld",(long)ti];
//            NSLog(@"Timezone are %@",gTrip.futuretime);
//
//          
//        }
//        
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
//                                                        message:@"You can book cab with coming 7 Days only!"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
 
    
    //NSTimeInterval ti = [pickerDate timeIntervalSince1970];
    

  }

-(void)getSelection:(id)sender
{
    
//    NSLocale *usLocale = [[NSLocale alloc]
//                          initWithLocaleIdentifier:@"en_US"];
    
    NSDate *pickerDate = [self.date_picker date];
    int day;
    NSDateFormatter *Format = [[NSDateFormatter alloc] init];
    [Format setDateFormat:@"dd/MM/yyyy"];
    
    NSDate *LocalDateStrinf=[NSDate date];
    
    if ([pickerDate compare:LocalDateStrinf] == NSOrderedDescending) {
        day = -1;
        NSLog(@"date1 is later than date2");
        
    } else if ([pickerDate compare:LocalDateStrinf] == NSOrderedAscending) {
        
        day =  [MapViewController daysBetweenDate:LocalDateStrinf andDate:pickerDate];

    }
    else
    {
        day =  [MapViewController daysBetweenDate:LocalDateStrinf andDate:pickerDate];
    }

    if (day >=0)
    {
    if (day > 47)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"You can book cab with coming 47 Days only!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
    
    NSTimeInterval ti = [pickerDate timeIntervalSince1970];

    NSString *selectionString = [[NSString alloc]
                                 initWithFormat:@"%@",
                                 pickerDate];
    _Pickup_Location.text = selectionString;
    }
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                        message:@"You can book cab with coming 47 Days only!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}
+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

-(void)setfutureride
{
    
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager sendFutureRequestWithSuccess:^(NSDictionary *dic)
         {
             
             
             [self customSetup];
             [self setText];
             [self setupGoogleMap];
             [self onResetPoint];
             gTrip = [[Trips alloc]init];
             issearchingdriver = false;
             firsttime = false;
             isiconselect = false;
             moveAlocation = false;
             moveBlocation = false;
             self.selectcar_view.userInteractionEnabled = true;
             
             _Booknow_btn.enabled = TRUE;
             _BookLatr_btn.enabled = TRUE;
             
             [UIView transitionWithView:_office_view
                               duration:0.4
                                options:UIViewAnimationOptionTransitionCrossDissolve
                             animations:^{
                                 
                                 issearchingdriver = false;
                                 _driver_mainview.hidden = TRUE;
                                 _mapview_header.text = @"Drivus";
                                 _office_view.hidden = false;
                                 _Home_view.hidden = false;
                                 _searching_vieew.hidden = false;
                                 self.Findingride_view.hidden = TRUE;
                                 _Gmap_opcontraint.constant = 84;
                                 _Backbtn.hidden = true;
                                 
                                 self.selectcar_view.hidden = TRUE;
                                 self.meetlocation_view.hidden = TRUE;
                                 self.Main_requestview.hidden = TRUE;
                                 
                             }
                             completion:NULL];
             
             [self.view makeToast:@"Request sent"];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             
             
         } andFailure:^(NSString *err)
    {
             
             [Util showMessage:err withTitle:[Util localized:@"app_name"]];
             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             
         }];

    
}

- (IBAction)setpickupTime:(id)sender
{
    
    _DatePicker_view.hidden = true;

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Future Rides!"
                                                                   message:@"Do you wanna Use any Particular Driver Unique Id?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        textField.keyboardType = UIKeyboardTypeAlphabet;
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         UITextField *textField = [alert.textFields firstObject];
                                                         gTrip.driver_unique_id = textField.text;
                                                          [self setfutureride];
                                                     }];
    [alert addAction:okAction];
    
    UIAlertAction *normal = [UIAlertAction actionWithTitle:@"cancel"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         gTrip.driver_unique_id = @"";
                                                         [self setfutureride];
                                                     }];
    [alert addAction:normal];

    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


- (IBAction)Close_Pickuptime:(id)sender
{
    self.selectcar_view.userInteractionEnabled = true;
    _Booknow_btn.enabled = TRUE;
    _BookLatr_btn.enabled = TRUE;
    _DatePicker_view.hidden = true;
    gTrip = [[Trips alloc]init];
    [self customSetup];
    [self setText];
    [self setupGoogleMap];
    [self onResetPoint];
    
    issearchingdriver = false;
    firsttime = false;
    [super viewDidLoad];
   
    isiconselect = false;
    moveAlocation = false;
    moveBlocation = false;
    [UIView transitionWithView:_office_view
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        issearchingdriver = false;
                        _driver_mainview.hidden = TRUE;
                        _mapview_header.text = @"Drivus";
                        _office_view.hidden = false;
                        _Home_view.hidden = false;
                        _searching_vieew.hidden = false;
                        self.Findingride_view.hidden = TRUE;
                        _Gmap_opcontraint.constant = 84;
                        _Backbtn.hidden = true;
                        self.selectcar_view.hidden = TRUE;
                        self.meetlocation_view.hidden = TRUE;
                        self.Main_requestview.hidden = TRUE;
                        
                    }
                    completion:NULL];
   
}
@end
