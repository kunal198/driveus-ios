//
//  settingdriverViewController.m
//  LinkRider
//
//  Created by Brst981 on 16/08/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import "settingdriverViewController.h"

@interface settingdriverViewController ()
{
    
    IBOutlet UIImageView *imgGoogleMaps;
    
    IBOutlet UIImageView *imgWaze;
    
    IBOutlet UIImageView *imgAppleMaps;
    int mapSelected ;
    
}
@end

@implementation settingdriverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self customsetting];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)customsetting
{
    NSString *currentMapSelected = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentMapSelected"];
    
    if ([currentMapSelected isEqualToString: @"google"]) {
        
        imgGoogleMaps.image = [UIImage imageNamed:@"checked"];
        imgWaze.image = [UIImage imageNamed:@"unchecked"];
        imgAppleMaps.image = [UIImage imageNamed:@"unchecked"];
        
    }else if ([currentMapSelected isEqualToString: @"waze"]){
        
        imgGoogleMaps.image = [UIImage imageNamed:@"unchecked"];
        imgWaze.image = [UIImage imageNamed:@"checked"];
        imgAppleMaps.image = [UIImage imageNamed:@"unchecked"];
        
    }else{
        
        imgGoogleMaps.image = [UIImage imageNamed:@"unchecked"];
        imgWaze.image = [UIImage imageNamed:@"unchecked"];
        imgAppleMaps.image = [UIImage imageNamed:@"checked"];
    }
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        
     
        if ( revealViewController )
        {
            [reveal_toggle addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        }
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
    }

}

#pragma mark switch actions
- (IBAction)btnGoogle:(id)sender {
    imgGoogleMaps.image = [UIImage imageNamed:@"checked"];
    imgWaze.image = [UIImage imageNamed:@"unchecked"];
    imgAppleMaps.image = [UIImage imageNamed:@"unchecked"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"google" forKey:@"currentMapSelected"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (IBAction)btnWaze:(id)sender {
    
    imgGoogleMaps.image = [UIImage imageNamed:@"unchecked"];
    imgWaze.image = [UIImage imageNamed:@"checked"];
    imgAppleMaps.image = [UIImage imageNamed:@"unchecked"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"waze" forKey:@"currentMapSelected"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
- (IBAction)btnApple:(id)sender {
    
    imgGoogleMaps.image = [UIImage imageNamed:@"unchecked"];
    imgWaze.image = [UIImage imageNamed:@"unchecked"];
    imgAppleMaps.image = [UIImage imageNamed:@"checked"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"apple" forKey:@"currentMapSelected"];
    [[NSUserDefaults standardUserDefaults] synchronize];


}




@end
