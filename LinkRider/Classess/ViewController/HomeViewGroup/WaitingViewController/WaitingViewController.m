//
//  WaitingViewController.m
//  LinkRider
//
//  Created by Hicom on 6/26/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "WaitingViewController.h"
#import "MapviewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "Appdelegate.h"
#import "NSString+FontAwesome.h"
#import "UIImageView+WebCache.h"
#import "UIFont+FontAwesome.h"
#import "ListPassengerCell.h"
#import "StartTripViewController.h"
#import "RatingTripViewController.h"
@interface WaitingViewController ()

@end

@implementation WaitingViewController{
    NSTimer *mainTimer, *backgrTimer;
    NSInteger timesRepeat;
    UIBackgroundTaskIdentifier bgTask;
    Boolean backFromBackground;
    int numberDriversSent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    _lblEstimatedFare.text = @"";
    numberDriversSent = 0;
    backFromBackground = false;
    if ((gTrip.tripId>0)) {
        if (gTrip.startTime.length>0) {
            [self onStartTrip];
        }
    }
    else{
        
        timesRepeat = [AppSettings share].maxTimeSendRequest;
        [self sendRequestInForceGround123];
        mainTimer = [NSTimer scheduledTimerWithTimeInterval:[AppSettings share].timeToSendRequestAgain target:self selector:@selector(sendRequestInForceGround123) userInfo:nil repeats:YES];
        
    }
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onStartTrip) name:DRIVER_CONFIRM_KEY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillBecomForceGround123) name:WILL_ENTER_FORCE_GROUND object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillGoToBackGround123) name:@"AppDidEnterBackground" object:nil];
}


- (void)sendRequestInForceGround123{
    
    timesRepeat -= 1;
    
    if (timesRepeat == 0) {
        [mainTimer invalidate];
        [self cancelTrip];
    }else{
        [self sendRequest];
    }
    
}

- (void)appWillBecomForceGround123{
  
    if ([[Util topViewController] isKindOfClass:[WaitingViewController class]]) {
    
    if (backFromBackground) {
        [self.navigationController popViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
        return;
    }
    if (backgrTimer) {
        [backgrTimer invalidate];
        if (bgTask != UIBackgroundTaskInvalid)
        {
            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
        }
        
    }
    
    if (self) {
        if (gTrip.status > 0) {
            return;
        }
      //  [self reloadStatus];
    }
    }
}

- (void)appWillGoToBackGround123{
    if ([[Util topViewController] isKindOfClass:[WaitingViewController class]]) {
        [mainTimer invalidate];
        bgTask = UIBackgroundTaskInvalid;
        bgTask = [[UIApplication sharedApplication]
                  beginBackgroundTaskWithExpirationHandler:^{
                      [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                  }];
        
        backgrTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(sendRequestInBackground123) userInfo:nil repeats:YES];
    }
    
}

- (void)sendRequestInBackground123{
    
    timesRepeat -= 1;
    
    if (timesRepeat == 0) {
        [backgrTimer invalidate];
        [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
            if (arr.count>0) {
                gTrip = [arr objectAtIndex:0];
                [ModelManager cancelRequest:gTrip withSuccess:^{
                    NSLog(@"Cancel request in background Success");
                    backFromBackground = true;
                    if (bgTask != UIBackgroundTaskInvalid)
                    {
                        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                    }
                } andFailure:^(NSString *err) {
                    if (bgTask != UIBackgroundTaskInvalid)
                    {
                        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                    }
                }];
            }else{
                gTrip = [[Trips alloc]init];
                if (bgTask != UIBackgroundTaskInvalid)
                {
                    [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                }
            }
            
        } andFailure:^(NSString *err) {
            if (bgTask != UIBackgroundTaskInvalid)
            {
                [[UIApplication sharedApplication] endBackgroundTask:bgTask];
            }
        }];
        
    }else{
        [ModelManager sendRequestWithSuccess:^(NSDictionary *dict) {
            NSLog(@"send request in background success");
            numberDriversSent += [Validator getSafeInt:dict[@"count"]];
        } andFailure:^(NSString *err) {
            
        }];
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [self setText];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [mainTimer invalidate];
    timesRepeat = 0;
    [super viewWillDisappear:animated];
}

- (void)reloadStatus{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager showMyTripsWithSuccess:^(NSMutableArray *arr) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (gTrip.status == approadching || gTrip.status == pendingPayment || gTrip.status == inprogress        ) {
            NSString *vcStoryboardId = [AppSettings share].arrSceneName[[AppSettings share].currentScene - 1];
            
            id classInstance = [self.storyboard instantiateViewControllerWithIdentifier:vcStoryboardId];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.navigationController popToRootViewControllerAnimated:NO];
            [self.navigationController pushViewController:classInstance animated:NO];
            return;
        }
        
    } andFailure:^(NSString *err) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

-(void)sendRequest{
    

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [ModelManager sendRequestWithSuccess:^(NSDictionary *dic) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        numberDriversSent += [Validator getSafeInt:dic[@"count"]];
        if (numberDriversSent == 0) {
            _lblRequestStatus.text = [NSString stringWithFormat:@"0 driver has received your request. Your request will auto refresh every %d seconds!", (int)[AppSettings share].timeToSendRequestAgain];
        }else{
            _lblRequestStatus.text = [NSString stringWithFormat:@"%d driver has received your request. Your request will auto refresh every %d seconds!", numberDriversSent, (int)[AppSettings share].timeToSendRequestAgain];
       
        }
   
        if ([[dic objectForKey:@"status"] isEqualToString:@"ERROR"]) {
            //            [Util showMessage:[dic objectForKey:@"message"] withTitle:[Util localized:@"app_name"]];
            //            [self.navigationController popViewControllerAnimated:YES];
        }else{
            _lblEstimatedFare.text = [NSString stringWithFormat:@"*Estimated fare: %@", [Validator getSafeString:dic[@"estimate_fare"]]];
            [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
                if (arr.count>0) {
                    gTrip = [arr objectAtIndex:0];
                }
                
            } andFailure:^(NSString *err) {
                
            }];
        }
        
    } andFailure:^(NSString *err) {
        [Util showMessage:err withTitle:[Util localized:@"app_name"]];
        //        [self.navigationController popViewControllerAnimated:YES];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

-(void)setText{
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    NSDictionary *fontDict = @{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:10],NSForegroundColorAttributeName:[Util colorWithHexString:@"#e8c03a"]};
    
    self.lblRequestLink.text = [Util localized:@"request_link_title"];
    
    NSMutableAttributedString *att =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircle]] attributes: fontDict];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: STEP_TITLE_FONT forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[Util localized:@"request_link_title"]] attributes: arialDict];
    
    [att appendAttributedString:aAttrString];
    
    for(int i=0;i<4;i++){
        
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ",[NSString fontAwesomeIconStringForEnum:FACircleO]] attributes: fontDict];
        [att appendAttributedString:aAttrString];
    }
    self.lblRequestLink.attributedText =att;
    
    self.lblawaiting.text = [Util localized:@"awaiting_title"];
    
    [self.btnCancel setTitle:[NSString fontAwesomeIconStringForEnum:FALongArrowLeft] forState:UIControlStateNormal];
    [self.btnCancelTrip setTitle:[Util localized:@"lbl_cancel_trip"].uppercaseString forState:UIControlStateNormal];
    //    [_ratingStar setTintColor:[UIColor whiteColor]];
    
}

-(void)onStartTrip{
    if ([[Util topViewController] isMemberOfClass:[WaitingViewController class]]) {
        [self performSegueWithIdentifier:@"orderConfirm" sender:self];
    }
}
- (IBAction)onCancelTrip:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self cancelTrip];
}

-(void)cancelTrip{
    if (gTrip.tripId.length > 0) {
        [ModelManager cancelRequest:gTrip withSuccess:^{
           
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.view makeToast:[Util localized:@"msg_cancel_success"]];
            gTrip = [[Trips alloc]init];
            
            [self.navigationController popViewControllerAnimated:YES];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
        } andFailure:^(NSString *err) {
            if (err.length==0) {
                [self.view makeToast:[Util localized:@"msg_cancel_failed"]];
            }
            [self.view makeToast:err];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    }else{
        [ModelManager showMyRequestWithSuccess:^(NSMutableArray *arr) {
            if (arr.count>0) {
                gTrip = [arr objectAtIndex:0];
                [self cancelTrip];
            }else{
                gTrip = [[Trips alloc]init];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resetAllPoint" object:nil userInfo:notificationsDic];
                return;
            }
            
        } andFailure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [Util showMessage:err withTitle:nil];
            
        }];
    }
    
}

#pragma mark tableview datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 186;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ListPassengerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListPassengerCell"];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ListPassengerCell" owner:self options:nil];
        cell = (ListPassengerCell *)[nib objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    [cell.profileView setProfileWithAvatarLink:gUser.thumb phoneNumber:gUser.phone starValue:gUser.passenger_rateup down_value:gUser.passenger_rateup name:gUser.name type:0];
    
    cell.lblStartValue.text = gTrip.startLocation.address;
    cell.lblEndValue.text = gTrip.endLocation.address;

    cell.lblSeatValue.text = [DriverCar typeNameByTypeId:gTrip.link];
    
    
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}
@end
