
//  MenuViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#import "MenuViewController.h"
#import "NSString+FontAwesome.h"
#import "FAImageView.h"
#import "UIImage+FontAwesome.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "ModelManager.h"
#import "StartTripViewController.h"
#import "RatingTripViewController.h"
#import "MapViewController.h"
#import "OnlineViewController.h"
#import "ProfileViewController.h"
#import "PaymentViewController.h"
#import "ShareViewController.h"
#import "HelpViewController.h"
#import "TripHistoryViewController.h"
#import "ViewController.h"
#import "SettingViewController.h"
#import "AboutViewController.h"
#import "settingdriverViewController.h"


@implementation MenuViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    _ratingStar.backgroundColor  = [UIColor clearColor];
    _ratingStar.starImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingStar.starHighlightedImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [_ratingStar setMaxRating:5.0];
    [_ratingStar setHorizontalMargin:1.0];
    [_ratingStar setEditable:NO];
    [_ratingStar setDisplayMode:EDStarRatingDisplayHalf];
    [_ratingStar setRating:5];
    [_ratingStar setNeedsDisplay];
    [_ratingStar setTintColor:[UIColor whiteColor]];
    [_tblView registerNib:[UINib nibWithNibName:@"MenuCell" bundle:nil] forCellReuseIdentifier:@"MenuCell"];
    _shareLocation = [LocationManager sharedManager];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setText];
    if (![gUser.userIsActive isEqualToString:@"1"]) {
        [self logOutWhileAccountInactive];
    }
    
        
}



- (void)viewDidAppear:(BOOL)animated{
    self.thumbImg.layer.cornerRadius=self.thumbImg.frame.size.height /2;
    self.thumbImg.layer.borderWidth=0;
    self.thumbImg.layer.masksToBounds = YES;
    self.thumbImg.layer.borderColor = [UIColor whiteColor].CGColor;
    self.thumbImg.layer.borderWidth = 2;
}

- (void) setText{
    

    
    nameArr = [NSMutableArray arrayWithArray:@[[Util localized:@"Home"],
                                               [Util localized:@"Promotions"],
                                               [Util localized:@"History"],
                                               [Util localized:@"Profile"],
                                               [Util localized:@"Help"],
                                               [Util localized:@"Payment"],
                                               [Util localized:@"Settings"]
                                               ]];

                                               
driverArr = [NSMutableArray arrayWithObjects:@"Home",@"Future Bookings",
             @"Earnings",
                @"Help",
               @"Setting",@"",nil];


   
    [self.tblView reloadData];
    self.lblName.text = gUser.name;
    
    if (![gUser.driver.isActive boolValue]) {
        [_ratingStar setRating:[gUser.passenger_rating floatValue]/2];
    }
    else{
        [_ratingStar setRating:[gUser.driver.driverRate floatValue]/2];
    }


    
    [self.thumbImg setImageWithURL:[NSURL URLWithString:gUser.thumb] placeholderImage:[UIImage imageNamed:@""]];

    if ([gUser.point isEqualToString:@"1"]) {
        self.lblPoint.text = [NSString stringWithFormat:@"%@: %@",[Util localized:@"lbl_point"],gUser.point];
    }
    else{
        self.lblPoint.text = [NSString stringWithFormat:@"%@: %@",[Util localized:@"lbl_points"],gUser.point];
    }
    
}



- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // configure the destination view controller:
    if ( [sender isKindOfClass:[UITableViewCell class]] )
    {
        
    }
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"0"]) {
      
        return nameArr.count;
    }
    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"1"]) {
        NSLog(@"%lu",(unsigned long)driverArr.count);
        return  driverArr.count;
        
    }
        
    return nameArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[Validator getSafeString:gUser.driver.isActive] isEqualToString:@"0"]&&[[Validator getSafeString:gUser.is_driver] isEqualToString:@"1"]) {
        if (indexPath.row == 6 || indexPath.row == 5) {
            return 0;
        }
        
    }
    
    
    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"0"]) {
        if (indexPath.row == 6) {
            return 0;
        }
        
    }
    
    
//    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"1"]) {
//        if (indexPath.row == 7) {
//            return 0;
//        }
//        
//        
//    }
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuCell";
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.lblTitle.text = @"";
    cell.lblIcon.text = @"";
    gUser.login_type = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"preferenceName"];
    if ([[Validator getSafeString:gUser.driver.isActive] isEqualToString:@"0"]&&[[Validator getSafeString:gUser.is_driver] isEqualToString:@"1"]) {
        cell.lblTitle.text = [driverArr objectAtIndex:indexPath.row];

        if (indexPath.row == 5||indexPath.row == 7) {
            return cell;
        }
        
    }
 
    NSLog(@"%@",gUser.login_type);
    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"1"] && [gUser.login_type  isEqual: @"Driver"]) {
        cell.lblTitle.text = [driverArr objectAtIndex:indexPath.row];

        if (indexPath.row == 5) {
            return cell;
        }
        
    }
    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"0"] || [gUser.login_type  isEqual: @"Rider"]) {
        cell.lblTitle.text = [nameArr objectAtIndex:indexPath.row];

        if (indexPath.row == 6) {
             cell.lblTitle.text = @"";
            return cell;
        }
    }
    
  
    //cell.lblTitle.text = [nameArr objectAtIndex:indexPath.row];
   // cell.lblIcon.text = [iconArr objectAtIndex:indexPath.row];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"0"] || [gUser.login_type  isEqual: @"Rider"]) {
    
    [self.tblView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            

            if (self.storyboard) {
                MapViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                [self.revealViewController setFrontViewController:naviRoot];
            }
            break;
        case 1:
            
                        if (self.storyboard) {
                            
                            
                            
                            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Promotions"
                                                                             message:@"coming Soon"
                                                                            delegate:self
                                                                   cancelButtonTitle:@"ok!"
                                                                   otherButtonTitles: nil];
                            
                            [alert show];
                            
                            
                            
                            
                            
                            
                         
                        }
            


                
            break;
        case 2:
            
          
            
                      if (self.storyboard) {
            
                          TripHistoryViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TripHistoryViewController"];
                          UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                          [self.revealViewController setFrontViewController:naviRoot];
                          
                          
                          
                          
                          
                          
//            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Favourites"
//                                                             message:@"coming Soon"
//                                                            delegate:self
//                                                   cancelButtonTitle:@"ok!"
//                                                   otherButtonTitles: nil];
//            
//            [alert show];
                      }
            
//                            TripHistoryViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TripHistoryViewController"];
//                            UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                            [self.revealViewController setFrontViewController:naviRoot];
//                        }

//            if (self.storyboard) {
//                PaymentViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
//                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                [self.revealViewController setFrontViewController:naviRoot];
//            }
            break;
        case 3:
            if (self.storyboard)
            {
//                ShareViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
//                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                [self.revealViewController setFrontViewController:naviRoot];
                
                
                
                                            ProfileViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                                            UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                                            [self.revealViewController setFrontViewController:naviRoot];
                
                
                
                
                
                
                
                
//                PaymentViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
//                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                [self.revealViewController setFrontViewController:naviRoot];
                
            }

           
            break;
        case 4:
            if (self.storyboard) {
                
                
                
                HelpViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                [self.revealViewController setFrontViewController:naviRoot];
//                HelpViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
//                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                [self.revealViewController setFrontViewController:naviRoot];
                
              
            }

            break;
        case 5:
            
            if (self.storyboard) {
                
                
                PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:vc];
                [self.revealViewController setFrontViewController:naviRoot];
                
            }
            
            break;

        case 6:
            
                        if (self.storyboard) {
                            
                            
                            SettingViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
                            UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:vc];
                            [self.revealViewController setFrontViewController:naviRoot];
                            
                            
                  
                            
//                            
//                            ProfileViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
//                            UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                            [self.revealViewController setFrontViewController:naviRoot];
                       }
           
            break;
        case 7:
            
            if (self.storyboard) {
                
                OnlineViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineViewController"];
                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                [self.revealViewController setFrontViewController:naviRoot];
                //                TripHistoryViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TripHistoryViewController"];
                //                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                //                [self.revealViewController setFrontViewController:naviRoot];
            }

          
            break;
        case 8:
            if (self.storyboard) {
//                RegisterAsDriverViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterAsDriverViewController"];
//                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                [self.revealViewController setFrontViewController:naviRoot];
            }
            break;
            
            
        default:
            break;
    }
    [self.revealViewController revealToggleAnimated:YES];
        
    }
    else if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"1"] && [gUser.login_type  isEqual: @"Driver"])
    {
        [self.tblView deselectRowAtIndexPath:indexPath animated:YES];
        switch (indexPath.row) {
            case 0:
            
            
            //            if (self.storyboard) {
            //                PaymentViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
            //                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
            //                [self.revealViewController setFrontViewController:naviRoot];
            //            }
            if (self.storyboard) {
                OnlineViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OnlineViewController"];
                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                [self.revealViewController setFrontViewController:naviRoot];
            }
            break;
            case 1:
            
            if (self.storyboard) {
                
                TripHistoryViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TripHistoryViewController"];
                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                [self.revealViewController setFrontViewController:naviRoot];
                
//                UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Future Bookings"
//                                                                 message:@"Need To Discuss"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"ok!"
//                                                       otherButtonTitles: nil];
//                
//                [alert show];
                
                
                
            }
            
            break;
            case 2:
            
            
            if (self.storyboard) {
//                ProfileViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
//                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                [self.revealViewController setFrontViewController:naviRoot];
                
                
              
                
                
                
                UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Earnings"
                                                                 message:@"Need To Discuss"
                                                                delegate:self
                                                       cancelButtonTitle:@"ok!"
                                                       otherButtonTitles: nil];
                
                [alert show];
                
                
                
                
            }
            
           
            break;
            case 3:
            if (self.storyboard) {
//                MapViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
//                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
//                [self.revealViewController setFrontViewController:naviRoot];
                
                HelpViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:mapVC];
                [self.revealViewController setFrontViewController:naviRoot];
                
                
                
                
                
                
            }
            
            
            break;
            case 4:
            if (self.storyboard)
            {
                
                settingdriverViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingdriverViewController"];
                UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:vc];
                [self.revealViewController setFrontViewController:naviRoot];
            }
            
            break;

            default:
            break;
    }
        [self.revealViewController revealToggleAnimated:YES];

    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager logoutWithSuccess:^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            ViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:loginVC];
            [self.revealViewController setFrontViewController:naviRoot];
            [Util setBool:NO forKey:IS_LOGGED_IN_KEY];
        } andFailure:^(NSString *err) {
            [Util showMessage:[Util localized:@"msg_error"] withTitle:[Util localized:@"app_name"]];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
    }
}
- (IBAction)onLogout:(id)sender {
    _shareLocation.updateOn = NO;
    [Util showMessage:[Util localized:@"msg_logout"] withTitle:[Util localized:@"app_name"] andDelegate:self];
}

- (void)logOutWhileAccountInactive{
    UIAlertController *alertLogout = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your account is inactive, please contact Admin!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertOk = [UIAlertAction actionWithTitle:@"Log Out" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager logoutWithSuccess:^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            ViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            UINavigationController *naviRoot = [[UINavigationController alloc] initWithRootViewController:loginVC];
            [self.revealViewController setFrontViewController:naviRoot];
            [Util setBool:NO forKey:IS_LOGGED_IN_KEY];
        } andFailure:^(NSString *err) {
            [Util showMessage:[Util localized:@"msg_error"] withTitle:[Util localized:@"app_name"]];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];

    }];
    [alertLogout addAction:alertOk];
    [self presentViewController:alertLogout animated:YES completion:nil];
    
}

- (IBAction)About_Driveus:(id)sender
{
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"About us"
                                                     message:@"Coming Soon"
                                                    delegate:self
                                           cancelButtonTitle:@"ok!"
                                           otherButtonTitles: nil];
   
    [alert show];
    
}


@end
