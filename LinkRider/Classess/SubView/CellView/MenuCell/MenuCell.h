//
//  MenuCell.h
//  LinkRider
//
//  Created by Hicom on 6/23/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
