//
//  MenuCell.m
//  LinkRider
//
//  Created by Hicom on 6/23/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
