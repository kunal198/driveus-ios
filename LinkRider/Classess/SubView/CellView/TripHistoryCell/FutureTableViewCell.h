//
//  FutureTableViewCell.h
//  LinkRider
//
//  Created by Brst981 on 05/06/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FutureTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *icCar;

@property (weak, nonatomic) IBOutlet UILabel *lblTransactionId;

@property (weak, nonatomic) IBOutlet UILabel *lblTransactionIdValue;

@property (weak, nonatomic) IBOutlet UILabel *lblLink;

@property (weak, nonatomic) IBOutlet UILabel *lblLinkValue;

@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet UILabel *LblDepartment;

@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *LblDepartureValue;


@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *lblDestinationValue;


@property (weak, nonatomic) IBOutlet UILabel *lblDurationValue;

@property (weak, nonatomic) IBOutlet UILabel *lblDuration;

@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *lblDriverId;


@property (weak, nonatomic) IBOutlet UILabel *lblriceValue;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightOfDriverId;

@property (weak, nonatomic) IBOutlet UIImageView *bgimg;

@property (strong, nonatomic) IBOutlet UILabel *passenger_id;

@property (strong, nonatomic) IBOutlet UILabel *starting_time;

@end
