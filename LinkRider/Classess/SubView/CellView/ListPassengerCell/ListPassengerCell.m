//
//  ListPassengerCell.m
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "ListPassengerCell.h"

@implementation ListPassengerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
    self.lblEndValue.textColor = self.lblSeatValue.textColor;
    self.lblEndValue.font = self.lblSeatValue.font;
    self.lblStartValue.textColor = self.lblSeatValue.textColor;
    self.lblStartValue.font = self.lblSeatValue.font;
    self.lblEstFareValue.textColor = self.lblSeatValue.textColor;
    self.lblEstFareValue.font = self.lblSeatValue.font;
    
    self.lblSeat.text = [Util localized:@"lbl_seats"];
    self.lblStart.text = [Util localized:@"lbl_from_a"];
    self.lblEnd.text = [Util localized:@"lbl_to_b"];
    
    self.lblStartValue.labelSpacing = 35;
    self.lblStartValue.pauseInterval = 3.7;
    self.lblStartValue.scrollSpeed = 30;
    self.lblStartValue.textAlignment = NSTextAlignmentLeft;
    self.lblStartValue.fadeLength = 12.f;
    
    self.lblEndValue.labelSpacing = 35;
    self.lblEndValue.pauseInterval = 3.7;
    self.lblEndValue.scrollSpeed = 30;
    self.lblEndValue.textAlignment = NSTextAlignmentLeft;
    self.lblEndValue.fadeLength = 12.f;
    
    self.lblEstFareValue.labelSpacing = 35;
    self.lblEstFareValue.pauseInterval = 3.7;
    self.lblEstFareValue.scrollSpeed = 30;
    self.lblEstFareValue.textAlignment = NSTextAlignmentLeft;
    self.lblEstFareValue.fadeLength = 12.f;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
