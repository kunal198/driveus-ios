//
//  ListPassengerCell.h
//  LinkRider
//
//  Created by Hicom on 7/2/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
#import "CBAutoScrollLabel.h"
#import "ProfileView.h"
@interface ListPassengerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblStart;
@property (weak, nonatomic) IBOutlet UILabel *lblEnd;

@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *lblStartValue;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *lblEndValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSeat;
@property (weak, nonatomic) IBOutlet UILabel *lblSeatValue;

@property (weak, nonatomic) IBOutlet UILabel *lblEstFare;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *lblEstFareValue;
    @property (weak, nonatomic) IBOutlet ProfileView *profileView;


@property (strong, nonatomic) IBOutlet UIButton *Acceptbtn;

@property (strong, nonatomic) IBOutlet UIButton *Declinebtn;



@end
