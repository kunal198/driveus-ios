//
//  AnnotationPointCar.h
//  LinkRider
//
//  Created by Pham Diep on 4/4/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface AnnotationPointCar : MKPointAnnotation

@end
