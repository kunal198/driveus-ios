//
//  AnnotationViewCar.m
//  LinkRider
//
//  Created by Pham Diep on 4/4/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import "AnnotationViewCar.h"

@implementation AnnotationViewCar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation
{
    // The re-use identifier is always nil because these custom pins may be visually different from one another
    self = [super initWithAnnotation:annotation
                     reuseIdentifier:nil];
    

    self.image = [UIImage imageNamed:@"img_car"];
    return self;
}


@end
