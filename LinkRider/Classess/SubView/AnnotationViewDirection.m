//
//  AnnotationViewDirection.m
//  LinkRider
//
//  Created by Pham Diep on 4/4/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import "AnnotationViewDirection.h"
#import "AnnotationPointDirection.h"
@implementation AnnotationViewDirection

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation{
    self = [super initWithAnnotation:annotation
                     reuseIdentifier:nil];
    AnnotationPointDirection *pointAnno = (AnnotationPointDirection*)annotation;
    self.title =  pointAnno.title;
    self.image = [UIImage imageNamed:@"img_pinDirection"];
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(2, -2 , 20, 40)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    label.text = self.title;
    label.font = [label.font fontWithSize:9];
    [self addSubview:label];

    return self;
}

@end
