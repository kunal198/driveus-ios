

#import "ModelManager.h"
#import "AFNetworking.h"
#import "Driver.h"
#import "DriverCar.h"
#import "StateObj.h"
#import "Trips.h"
@implementation ModelManager


+(void)sendData:(NSDictionary *)data
           path:(NSString *)path
    WithSuccess:(void (^)(NSDictionary *))success
        failure:(void (^)(NSString *))failure
{
    

    if (![Util isConnectNetwork]) {
        failure(@"No network found");
    }
    else{
   
        if (data != nil)
    {
        

        NSString*urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,path];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        if ([path  isEqual: @"driverConfirm"]  )
        {
            
           manager.requestSerializer = [AFJSONRequestSerializer serializer];

        }
       
        NSLog(@"url are %@",urlStr);
        NSLog(@"data are lat and long  %@",data);
        if (data != nil)
        {
            
        [manager POST:urlStr parameters:data progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
         {
            success(responseObject);
             NSLog (@"%@",responseObject);
            
                    }
              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"%@",error);
                    failure([Util localized:@"msg_technical_error"]);
        }];
        
    }
    }
    }
    
}


+(void)registerAsDriver:(User *)user documentPath:(UIImage *)document withSuccess:(void(^)(NSDictionary *))success failure:(void (^) (NSString *))failure
{
    
    NSString* token = [Util objectForKey:@"Token"];
    NSDictionary *dic = @{@"email":[Validator getSafeString:user.email],@"gcm_id":[Validator getSafeString:token],@"ime":[Validator getSafeString:token],@"type":@"2",@"lat":user.lattitude,@"long":user.logitude,@"gender":[Validator getSafeString:user.gender],@"name":[Validator getSafeString:user.name],@"image":[Validator getSafeString:user.thumb],@"dob":[Validator getSafeString:user.date_of_birth],@"carPlate":[Validator getSafeString:user.car.carPlate],@"brand":[Validator getSafeString:user.car.brand],@"model":[Validator getSafeString:user.car.model], @"link_type" : gUser.car.carType, @"year":[Validator getSafeString:user.manufacture_year],@"status":@"true",@"account":[Validator getSafeString:user.cart_value],@"token":[Validator getSafeString:gUser.token]};
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:60];
    [manager POST:[NSString stringWithFormat:@"%@driverRegister",BASE_URL] parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *main_car = UIImageJPEGRepresentation(user.main_car_image,0.1);
        [formData appendPartWithFileData:main_car name:@"image" fileName:[NSString stringWithFormat:@"image%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        
        if (user.sub_car) {
            NSData *sub_car = UIImageJPEGRepresentation(user.sub_car_image,0.1);
            [formData appendPartWithFileData:sub_car name:@"image2" fileName:[NSString stringWithFormat:@"image2%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        
        if (user.document)
        {
            NSData *document_car = UIImageJPEGRepresentation(user.document_img,0.1);
            [formData appendPartWithFileData:document_car name:@"document" fileName:[NSString stringWithFormat:@"document%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        if (user.License_document)
        {
            NSData *document_car1 = UIImageJPEGRepresentation(user.License_img,0.1);
            [formData appendPartWithFileData:document_car1 name:@"document1" fileName:[NSString stringWithFormat:@"document1%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        if (user.Insurance_document)
        {
            NSData *document_car2 = UIImageJPEGRepresentation(user.Insurance_img,0.1);
            [formData appendPartWithFileData:document_car2 name:@"document2" fileName:[NSString stringWithFormat:@"document2%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        if (user.Refistration_document)
        {
            NSData *document_car3 = UIImageJPEGRepresentation(user.Refistration_img,0.1);
            [formData appendPartWithFileData:document_car3 name:@"document3" fileName:[NSString stringWithFormat:@"document3%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        if (user.Permit_document)
        {
            NSData *document_car4 = UIImageJPEGRepresentation(user.Permit_img,0.1);
            [formData appendPartWithFileData:document_car4 name:@"document4" fileName:[NSString stringWithFormat:@"document4%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = responseObject;
        NSString *status = [dic objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success(responseObject);
        }
        else{
            failure([dic objectForKey:@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure([Util localized:@"msg_technical_error"]);
    }];
    
    
}
+(void)loginWithUser:(User *)user andToken:(NSString *)token Status:(BOOL)isEnable withSuccess:(void (^)(NSString *))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
        failure(nil);
    }
    if (user.lattitude.length==0)
    {
        user.lattitude = @"0";
        user.logitude = @"0";
    }
    if (token.length==0) {
        token = [Util objectForKey:@"Token"];
    }
    
    NSDictionary *di = @{@"email":[Validator getSafeString:user.email],
                         @"gcm_id":[Validator getSafeString:token],
                         @"ime":[Validator getSafeString:token],
                         @"type":@"2",
                         @"lat":user.lattitude,
                         @"long":user.logitude,
                         @"gender":[Validator getSafeString:user.gender],
                         @"name":[Validator getSafeString:user.name],
                         @"image":[Validator getSafeString:user.thumb]};
    
    if (user.date_of_birth.length>0)
    
    {
        di = @{@"email":[Validator getSafeString:user.email],
               @"gcm_id":[Validator getSafeString:token],
               @"ime":[Validator getSafeString:token],
               @"type":@"2",
               @"lat":user.lattitude,
               @"long":user.logitude,
               @"gender":[Validator getSafeString:user.gender],
               @"name":[Validator getSafeString:user.name],
               @"image":[Validator getSafeString:user.thumb],
               @"dob":[Validator getSafeString:user.date_of_birth]};
    }
    
    [self sendData:di path:API_LOGIN WithSuccess:^(NSDictionary *dic) {
        NSString *status =[dic objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            gUser = [[User alloc]init];
            NSDictionary *dict = [dic objectForKey:@"data"];
            if ([dict isKindOfClass:[NSDictionary class]]) {
                gUser = [[User alloc]init];
                gUser = user;
                gUser.client_id = [dict objectForKey:@"user_id"];
                gUser.point = [dict objectForKey:@"points"];
                gUser.token = [dict objectForKey:@"token"];
                gUser.is_driver = [dict objectForKey:@"isDriver"];
                gUser.driver = [[Driver alloc]init];
                gUser.driver.isActive = [Validator getSafeString:dict[@"driverActive"]];
                [Util saveUser:gUser];
                success([[dic objectForKey:@"data"] valueForKey:@"token"]);
            }
            else{
                failure([dic objectForKey:@"message"]);
            }
        }
        else{
            failure([dic objectForKey:@"message"]);
        }
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+ (void)loginNormalWithUserObj:(User*)user withSuccess:(void (^)(User *))returnSuccess failure:(void (^)(NSString *))returnFailure{
    if (user.lattitude.length==0) {
        user.lattitude = @"0";
        user.logitude = @"0";
    }
    NSString *token = deviceTokenString;
    if (token.length==0) {
        token = [Util objectForKey:@"Token"];
    }
    
    NSDictionary *di = @{
                         @"email":[Validator getSafeString:user.email],
                         @"gcm_id":[Validator getSafeString:token],
                         @"ime":[Validator getSafeString:token],
                         @"type":@"2",
                         @"lat":user.lattitude,
                         @"long":user.logitude,
                         @"password":[Validator getSafeString:user.password],

                         };
    
    [self sendData:di path:@"loginNormal" WithSuccess:^(NSDictionary *dic)
     {
        NSString *status = dic[@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            gUser = [[User alloc]init];
            NSDictionary *dict = [dic objectForKey:@"data"];
        
            if ([dict isKindOfClass:[NSDictionary class]])
            {
                gUser = user;
                gUser.client_id = [dict objectForKey:@"user_id"];
                gUser.point = [dict objectForKey:@"points"];
                gUser.token = [dict objectForKey:@"token"];
                gUser.is_driver = [dict objectForKey:@"isDriver"];
                gUser.driver = [[Driver alloc]init];
                gUser.driver.isActive = [Validator getSafeString:dict[@"driverActive"]];
                gUser.login_type = [NSString stringWithFormat:@"%d", LoginNormal];
                [Util saveUser:gUser];
                returnSuccess(gUser);
                
            }
            else
            {
                returnFailure([dic objectForKey:@"message"]);
            }
        }
        else{
            returnFailure([dic objectForKey:@"message"]);
        }
    } failure:^(NSString *err) {
        returnFailure(err);
    }];
    
    
}


+(void)getUserProfileWithAccessToken:(NSString *)accessToken
                         WithSuccess:(void (^)(User *))success
                             failure:(void (^)(NSString *))failure{
    NSString*urlStr = [NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@",accessToken];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSString *content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (!jsonDic) {

        if(failure)
            failure(nil);
    } else {
        User *u = [[User alloc]init];
        u.client_id = [Validator getSafeString:jsonDic[@"id"]];
        u.login_type = [NSString stringWithFormat:@"%d", LoginSocialNetwork];
        u.name = [Validator getSafeString:jsonDic[@"name"]];
        u.email = [Validator getSafeString:jsonDic[@"email"]];
        u.thumb = [Validator getSafeString:jsonDic[@"picture"]];
        u.gender = [Validator getSafeString:jsonDic[@"gender"]];
        
        [self getUserEmailWithAccessToken:accessToken WithSuccess:^(User *user) {
            u.email = user.email;
            
            success(u);
        } failure:^(NSString *err) {
            success(u);
        }];
    }
}

+(void)getUserEmailWithAccessToken:(NSString *)accessToken
                       WithSuccess:(void (^)(User *))success
                           failure:(void (^)(NSString *))failure{
    NSString*urlStr = [NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%@",accessToken];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSString *content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (!jsonDic) {
        
        if(failure)
            failure(nil);
    } else {
        User *u = [[User alloc]init];
        u.email = [Validator getSafeString:jsonDic[@"email"]];
        
        success(u);
    }
}

+(User *)parseUserFromdic:(NSDictionary *)dic{
    User *u = [[User alloc]init];
    u.name = [Validator getSafeString:dic[@"fullName"]];
    u.email = [Validator getSafeString:dic[@"email"]];
    u.thumb = [Validator getSafeString:dic[@"image"]];
    u.gender = [Validator getSafeString:dic[@"gender"]];
    u.phone = [Validator getSafeString:dic[@"phone"]];
    u.passenger_rating = [Validator getSafeString:dic[@"rate"]];
    NSLog(@"%@",u.passenger_ratedown);
    NSLog(@"%@",u.passenger_rateup);

    u.passenger_rateup = [Validator getSafeString:dic[@"rateup"]];
    u.passenger_ratedown = [Validator getSafeString:dic[@"ratedown"]];

    
    return u;
}



+(void)searchLocationWithKey:(NSString *)key withSuccess:(void (^)(NSMutableArray *))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
    }
    
    NSDictionary *dic = @{@"address":key,@"key":GOOGLE_API_KEY};
    NSString*urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:urlStr parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(nil);
    }];
    
    
}



+(void)updateCoordinatewithLat:(float)lat andLong:(float)lon withSuccess:(void (^)(void))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
    }
    if (APP_IN_TEST_MODE) {
        lat = kFakeLocationLat;
        lon = kFakeLocationLong;
    }
    NSDictionary *dic = @{@"token":gUser.token,@"lat":[NSString stringWithFormat:@"%f",lat],@"long":[NSString stringWithFormat:@"%f",lon]};
    //    NSDictionary *dic = @{@"token":gUser.token,@"lat":DEFAULT_LAT,@"long":DEFAULT_LONG};
    [self sendData:dic path:@"updateCoordinate" WithSuccess:^(NSDictionary *dict) {
        success();
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}


+(void)updateDistancewithSuccess:(void (^)(float))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
    }
    
    NSDictionary *dic = @{@"token":gUser.token,@"tripId":gTrip.tripId};
    [self sendData:dic path:@"showDistance" WithSuccess:^(NSDictionary *dict) {
        success([Validator getSafeFloat:dict[@"data"]]);
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}


+(void)getNumberOfCarsWithLatitude:(float)latitude Longitude:(float)longitude Distance:(float)distance withSuccess:(void (^)(int, NSArray *))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
    }
    
    NSDictionary *dic = @{@"startLat":[NSString stringWithFormat:@"%f",latitude],
                          @"startLong":[NSString stringWithFormat:@"%f",longitude],
                          @"distance" : [NSString stringWithFormat:@"%.2f", distance],
                          @"carType" : gTrip.link.length? gTrip.link : @"I"};
   
    [self sendData:dic path:@"searchDriver" WithSuccess:^(NSDictionary *dict) {
        int numberOfCar =[dict[@"count"] intValue];
        NSMutableArray *mulArr = [[NSMutableArray alloc] init];
        NSArray *arrPointDics = dict[@"data"];
        for (NSDictionary *carDict in arrPointDics) {
            GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake([Validator getSafeFloat:carDict[@"lat"]], [Validator getSafeFloat:carDict[@"long"]])];
            if ([gTrip.link isEqualToString:@"I"]) {
                marker.icon = [UIImage imageNamed:@"img_carI"];
            } else if ([gTrip.link isEqualToString:@"II"]) {
                marker.icon = [UIImage imageNamed:@"img_carII"];
            }else {
                marker.icon = [UIImage imageNamed:@"img_carIII"];
            }
            
            [mulArr addObject:marker];
        }
       
        success(numberOfCar, mulArr.copy);
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}



+(void)driverConfirmWithTriId:(NSString *)tripId acceptStatus:(NSString*)status withSuccess:(void (^)(void))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
    }
    
    NSDictionary *dic = @{@"token":gUser.token,@"requestId":tripId,@"accept_status":status};
    [self sendData:dic path:@"driverConfirm" WithSuccess:^(NSDictionary *dict) {
        if ([[Validator getSafeString:dict[@"status"]] isEqualToString:@"ERROR"]) {
            failure([Validator getSafeString:dict[@"message"]]);
        }else
        {
            if (![[dict valueForKey:@"message"] isEqualToString:@"CANCEL"]) {
                gTrip = [self parseTripFromDic:dict];
            }
            
            success();
        }
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}


+(void)driverStartTripWithTriId:(NSString *)tripId withSuccess:(void (^)(void))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
    }
    
    NSDictionary *dic = @{@"token":gUser.token,@"tripId":tripId};
    [self sendData:dic path:@"startTrip" WithSuccess:^(NSDictionary *dict) {
        success();
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}

+(void)driverEndTripWithTriId:(NSString *)tripId andDistance:(NSString *)distance withSuccess:(void (^)(void))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
    }
    
    NSDictionary *dic = @{@"token":gUser.token,@"tripId":tripId,@"distance":distance};
    [self sendData:dic path:@"endTrip" WithSuccess:^(NSDictionary *dict) {
        success();
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}
+(void)getUserProfileWithSuccess:(void (^)(void))success failure:(void(^)(NSString *))failure{
    if (![Util isConnectNetwork]) {
        failure(nil);
        failure(nil);
    }
    NSString *type = @"0";
    if (gUser.driver_id.length>0) {
        type = @"1";
    }
    
    NSDictionary *dic = @{@"token":gUser.token,@"driver":type};
    [self sendData:dic path:SHOW_USER_INFO WithSuccess:^(NSDictionary *dic) {
        
        if ([[dic objectForKey:@"status"] isEqualToString:@"SUCCESS"]) {
            NSDictionary *user = [dic objectForKey:@"data"];
            gUser.login_type = @"";
            gUser.point = [Validator getSafeString:user[@"balance"]];
           gUser.passenger_rateup = [Validator getSafeString:user[@"passengerRateup"]];
            gUser.passenger_ratedown = [Validator getSafeString:user[@"passengerRatedown"]];
            gUser.descriptions = [Validator getSafeString:user[@"description"]];
            gUser.cart_value = [Validator getSafeString:user[@"account"]];
            gUser.address = [Validator getSafeString:user[@"address"]];
            gUser.passenger_rating = [Validator getSafeString:user[@"passengerRate"]];
            gUser.passenger_rating_count = [Validator getSafeString:user[@"passengerRateCount"]];
            gUser.email = [Validator getSafeString:user[@"email"]];
            gUser.name = [Validator getSafeString:user[@"fullName"]];
            gUser.gender = [Validator getSafeString:user[@"gender"]];
            gUser.driver_id = [Validator getSafeString:user[@"id"]];
            gUser.phone = [Validator getSafeString:user[@"phone"]];
            gUser.thumb = [Validator getSafeString:user[@"image"]];
            gUser.is_online = [Validator getSafeString:user[@"isOnline"]];
            gUser.state = [Validator getSafeString:user[@"stateName"]];
            gUser.stateId = [Validator getSafeString:user[@"stateId"]];
            gUser.cityId = [Validator getSafeString:user[@"cityId"]];
            gUser.city = [Validator getSafeString:user[@"cityName"]];
            gUser.userIsActive = [Validator getSafeString:user[@"isActive"]];
            gUser.login_type = [Validator getSafeString:user[@"typeAccount"]];
            
            NSDictionary *car = [user objectForKey:@"car"];
            if ([car isKindOfClass:[NSDictionary class]]) {
                gUser.car = [[DriverCar alloc]init];
                gUser.car.brand = [Validator getSafeString:car[@"brand"]];
                gUser.car.carPlate = [Validator getSafeString:car[@"carPlate"]];
                gUser.car.dateCreated = [Validator getSafeString:car[@"dateCreated"]];
                gUser.car.document = [Validator getSafeString:car[@"document"]];
                gUser.car.car_id = [Validator getSafeString:car[@"car_id"]];
                gUser.car.carType = [Validator getSafeString:car[@"link_type"]];
               // NSLog(@"Car type: %@", gUser.car.carType);
                gUser.car.image1 = [Validator getSafeString:[[car objectForKey:@"images"] valueForKey:@"image1"]];
                gUser.car.image2 = [Validator getSafeString:[[car objectForKey:@"images"] valueForKey:@"image2"]];
                gUser.car.model = [Validator getSafeString:car[@"model"]];
                gUser.car.year = [Validator getSafeString:car[@"year"]];
                gUser.car.status = [Validator getSafeString:car[@"status"]];
            }
            
            
            NSDictionary *driver = [user objectForKey:@"driver"];
            if ([driver isKindOfClass:[NSDictionary class]]) {
                gUser.driver = [[Driver alloc]init];
                
                gUser.driver.document = [Validator getSafeString:driver[@"document"]];
                gUser.driver.driverRate = [Validator getSafeString:driver[@"driverRate"]];
                gUser.driver.driverRateCount = [Validator getSafeString:driver[@"driverRateCount"]];
                gUser.driver.isActive = [Validator getSafeString:driver[@"isActive"]];
                gUser.driver.isOnline = [Validator getSafeString:driver[@"isOnline"]];
                gUser.is_online = [Validator getSafeString:driver[@"isOnline"]];
                gUser.driver.status = [Validator getSafeString:driver[@"status"]];
                gUser.driverRateup = [Validator getSafeString:driver[@"driverRateup"]];
                gUser.driverRatedown = [Validator getSafeString:driver[@"driverRatedown"]];
                gUser.driver.update_pending = [Validator getSafeString:driver[@"updatePending"]];
                gUser.is_driver = @"1";
                 gUser.carType = [Validator getSafeString:driver[@"linkType"]];
                gUser.driver.document = [Validator getSafeString:driver[@"document"]];
                NSLog(@"%@",gUser.driver.document);
                gUser.driver.document1 = [Validator getSafeString:driver[@"document1"]];
                NSLog(@"%@",gUser.driver.document1);
                gUser.driver.document2 = [Validator getSafeString:driver[@"document2"]];
                NSLog(@"%@",gUser.driver.document2);
                gUser.driver.document3 = [Validator getSafeString:driver[@"document3"]];
                NSLog(@"%@",gUser.driver.document3);
                gUser.driver.document4 = [Validator getSafeString:driver[@"document4"]];

                
            }
            
            [Util saveUser:gUser];
            success();
        }
        else{
            failure([dic objectForKey:@"message"]);
        }
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}
+(void)sendRequestWithSuccess:(void(^)(NSDictionary *))success andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic;
    NSString *startLat = [NSString stringWithFormat:@"%f",gTrip.startLocation.position.latitude];
    NSString *startLong = [NSString stringWithFormat:@"%f",gTrip.startLocation.position.longitude];
    
    NSString *endLat = [NSString stringWithFormat:@"%f",gTrip.endLocation.position.latitude];
    NSString *endLong = [NSString stringWithFormat:@"%f",gTrip.endLocation.position.longitude];
    
    
    NSString *endLat1 = [NSString stringWithFormat:@"%f",gTrip.endLocation1.position.latitude];
    NSString *endLong1 = [NSString stringWithFormat:@"%f",gTrip.endLocation1.position.longitude];
    
    NSString *endLat2 = [NSString stringWithFormat:@"%f",gTrip.endLocation2.position.latitude];
    NSString *endLong2 = [NSString stringWithFormat:@"%f",gTrip.endLocation2.position.longitude];
    
    NSString *endLat3 = [NSString stringWithFormat:@"%f",gTrip.endLocation3.position.latitude];
    NSString *endLong3 = [NSString stringWithFormat:@"%f",gTrip.endLocation3.position.longitude];
    

    NSString *endLat4 = [NSString stringWithFormat:@"%f",gTrip.endLocation4.position.latitude];
    NSString *endLong4 = [NSString stringWithFormat:@"%f",gTrip.endLocation4.position.longitude];
    
    
    if ((startLat != 0)&& (startLong != 0)&& (endLat != 0)&&(endLong != 0)&&  (endLong != nil) &&(gTrip.startLocation.address != 0)&&(gTrip.endLocation.address != nil)&&(gTrip.estimateDistance != nil)&& endLat4 != 0 && endLong4 != 0  && endLat3 != 0 && endLong3 != 0 && endLat2 != 0 &&  endLong2 != 0 && endLat1 != 0 && endLong1 != 0 &&(gTrip.endLocation1.address != nil)&&(gTrip.endLocation2.address != nil)&&(gTrip.endLocation3.address != nil)&&(gTrip.endLocation4.address != nil))
    {
        dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"endLat1":endLat1,@"endLong1":endLong1,@"endLocation1":gTrip.endLocation1.address,@"endLat2":endLat2,@"endLong2":endLong2,@"endLocation2":gTrip.endLocation2.address,@"endLat3":endLat3,@"endLong3":endLong3,@"endLocation3":gTrip.endLocation3.address,@"endLat4":endLat4,@"endLong4":endLong4,@"endLocation4":gTrip.endLocation4.address,@"token":gUser.token, @"estimateDistance" : gTrip.estimateDistance};
    }
    
  else if ((startLat != 0) && (startLong != 0) && (endLat != 0) && (endLong != 0) &&  (endLong != nil)  && (gTrip.startLocation.address != 0) && (gTrip.endLocation.address != nil)&&(gTrip.estimateDistance != nil) && endLat3 != nil && endLong3 != nil && endLat2 != nil && endLong2 != nil && endLat1 != nil && endLong1 != nil && (gTrip.endLocation1.address != nil) && (gTrip.endLocation2.address != nil) && (gTrip.endLocation3.address != nil))
      
      
    {
        
        
        
        
               dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"endLat1":endLat1,@"endLong1":endLong1,@"endLocation1":gTrip.endLocation1.address,@"endLat2":endLat2,@"endLong2":endLong2,@"endLocation2":gTrip.endLocation2.address,@"endLat3":endLat3,@"endLong3":endLong3,@"endLocation3":gTrip.endLocation3.address,@"endLat4":@"",@"endLong4":@"",@"endLocation4":@"",@"token":gUser.token, @"estimateDistance" : gTrip.estimateDistance};
        
        
        
        
        
        
        
        
//        
//             dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"endLat1":endLat1,@"endLong1":endLong1,@"endLocation1":gTrip.endLocation1.address,@"endLat2":endLat2,@"endLong2":endLong2,@"endLocation2":gTrip.endLocation2.address,@"endLat3":endLat3,@"endLong3":endLong3,@"endLocation3":gTrip.endLocation3.address,@"token":gUser.token, @"estimateDistance" : gTrip.estimateDistance};
    }
  else if ((startLat != 0)&& (startLong != 0)&& (endLat != 0)&& (endLong != 0)&&  (endLong != nil) &&(gTrip.startLocation.address != 0)&&(gTrip.endLocation.address != nil)&&(gTrip.estimateDistance != nil)&&endLat2 != nil &&endLong2 != nil&&endLat1 != nil &&endLong1 != nil&&(gTrip.endLocation1.address != nil)&&(gTrip.endLocation2.address != nil))
    {
//    dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"endLat1":endLat1,@"endLong1":endLong1,@"endLocation1":gTrip.endLocation1.address,@"endLat2":endLat2,@"endLong2":endLong2,@"endLocation2":gTrip.endLocation2.address,@"token":gUser.token, @"estimateDistance" : gTrip.estimateDistance};
        
         dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"endLat1":endLat1,@"endLong1":endLong1,@"endLocation1":gTrip.endLocation1.address,@"endLat2":endLat2,@"endLong2":endLong2,@"endLocation2":gTrip.endLocation2.address,@"endLat3":@"",@"endLong3":@"",@"endLocation3":@"",@"endLat4":@"",@"endLong4":@"",@"endLocation4":@"",@"token":gUser.token, @"estimateDistance" : gTrip.estimateDistance};
        
          }
    
    
    else if  ((startLat != 0)&& (startLong != 0)&& (endLat != 0)&&(endLong != 0) &&  (endLong != nil) &&(gTrip.startLocation.address != 0)&&(gTrip.endLocation.address != nil)&&(gTrip.estimateDistance != nil) && endLat1 != nil && endLong1 != nil && (gTrip.endLocation1.address != nil))
    {
        
        
        dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"endLat1":endLat1,@"endLong1":endLong1,@"endLocation1":gTrip.endLocation1.address,@"endLat2":@"",@"endLong2":@"",@"endLocation2":@"",@"endLat3":@"",@"endLong3":@"",@"endLocation3":@"",@"endLat4":@"",@"endLong4":@"",@"endLocation4":@"",@"token":gUser.token, @"estimateDistance" : gTrip.estimateDistance};
        
        
        
        
        
        
        
        
//        dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"endLat1":endLat1,@"endLong1":endLong1,@"endLocation1":gTrip.endLocation1.address,@"token":gUser.token, @"estimateDistance" : gTrip.estimateDistance};
        
    }
    
    else
    {
        
        
//         dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"token":gUser.token, @"estimateDistance" :gTrip.estimateDistance};
        
        
       dic = @{@"link":gTrip.link,@"startLat":startLat,@"startLong":startLong,@"startLocation":gTrip.startLocation.address,@"endLat":endLat,@"endLong":endLong,@"endLocation":gTrip.endLocation.address,@"endLat1":@"",@"endLong1":@"",@"endLocation1":@"",@"endLat2":@"",@"endLong2":@"",@"endLocation2":@"",@"endLat3":@"",@"endLong3":@"",@"endLocation3":@"",@"endLat4":@"",@"endLong4":@"",@"endLocation4":@"",@"token":gUser.token, @"estimateDistance" : gTrip.estimateDistance};
        
        
    }
       [self sendData:dic path:CREATE_REQUEST WithSuccess:^(NSDictionary *dict) {
        success(dict);
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}

+(void)sendFutureRequestWithSuccess:(void(^)(NSDictionary *))success andFailure:(void (^)(NSString *))failure
{
    NSLog(@"start location%@",gTrip.startLocation);
    NSLog(@"End Location %@",gTrip.futuretime);
    NSString *startLat = [NSString stringWithFormat:@"%f",gTrip.startLocation.position.latitude];
    NSString *startLong = [NSString stringWithFormat:@"%f",gTrip.startLocation.position.longitude];
    
    NSString *endLat = [NSString stringWithFormat:@"%f",gTrip.endLocation.position.latitude];
    NSString *endLong = [NSString stringWithFormat:@"%f",gTrip.endLocation.position.longitude];
    
    if ((startLat == 0)|| (startLong == 0)|| (endLat == 0)|| (endLong == 0)||  (endLong == nil) ||(gTrip.startLocation.address == 0)||(gTrip.endLocation.address == nil)||(gTrip.estimateDistance == nil))
    {
        
  
    }
    else
    {
        NSDictionary *dic = @{@"link":gTrip.link,
                              @"startLat":startLat,
                              @"startLong":startLong,
                              @"startLocation":gTrip.startLocation.address,
                              @"endLat":endLat,
                              @"endLong":endLong,
                              @"endLocation":gTrip.endLocation.address,
                              @"token":gUser.token,
                              @"estimateDistance" : gTrip.estimateDistance,
                              @"startTime" :gTrip.futuretime,
                              @"driver_unique_id":gTrip.driver_unique_id };
        
        [self sendData:dic path:CREATE_FutureREQUEST WithSuccess:^(NSDictionary *dict) {
            success(dict);
        } failure:^(NSString *err) {
            failure(err);
        }];
    }
}



+(void)logoutWithSuccess:(void(^)(void ))success andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token};
    [self sendData:dic path:@"logout" WithSuccess:^(NSDictionary *dict) {
        success();
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(void)rateDriverWithRate:(int)rateup andratedown:(int)down andTip:(int)tip
                  Success:(void(^)(void ))success
               andFailure:(void (^)(NSString *))failure
{
    
    NSDictionary *dic = @{@"token":gUser.token,@"tripId":gTrip.tripId,@"rateup":[NSString stringWithFormat:@"%d",rateup],@"ratedown":[NSString stringWithFormat:@"%d",down],@"tip":[NSString stringWithFormat:@"%d",tip]};
    [self sendData:dic path:@"rateDriver" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [dict objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([dict objectForKey:@"message"]);
        }
    } failure:^(NSString *err) {
        failure(err);
    }];
    
}

+(void)shareWithType:(NSString *)social WithSuccess:(void(^)(void ))success andFailure:(void (^)(NSString *))failure{
    
    NSString *type = @"passenger";
    if ([[Validator getSafeString:gUser.is_driver] isEqualToString:@"1"]) {
        type = @"driver";
    }
    NSDictionary *dic = @{@"token":gUser.token,@"type":type,@"social":social};
    [self sendData:dic path:@"shareApp" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [dict objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([dict objectForKey:@"message"]);
        }
    } failure:^(NSString *err) {
        failure(err);
    }];
}
+(void)tripPaymentWithSuccess:(void(^)(void ))success andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token,@"tripId":gTrip.tripId};
    [self sendData:dic path:@"tripPayment" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [dict objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([dict objectForKey:@"message"]);
        }
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(void)ratePassengerWithRate:(int)rateup andratedown:(int)down
                     Success:(void(^)(void ))success
                  andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token,@"tripId":gTrip.tripId,@"rateup":[NSString stringWithFormat:@"%d",rateup],@"ratedown":[NSString stringWithFormat:@"%d",down]};
    [self sendData:dic path:@"ratePassenger" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [dict objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([dict objectForKey:@"message"]);
        }
    } failure:^(NSString *err) {
        failure(err);
    }];
}
+(void)showMyTripsWithSuccess:(void(^)(NSMutableArray *))success andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token};
    [self sendData:dic path:@"showMyTrip" WithSuccess:^(NSDictionary *dict) {
        NSArray *arrTrip = [self parseTripFromDicArr:dict];
        gTrip.status = 0;
        for (Trips *trip in arrTrip) {
            switch (trip.status) {
                case approadching:
                    if ([trip.driverId isEqualToString:gUser.driver_id]) {
                        [AppSettings share].currentScene = SceneOnlineConfirmViewController;
                    } else {
                        [AppSettings share].currentScene = SceneOrderConfirmViewController;
                    }
                    gTrip = trip;
                    break;
                case inprogress:
                    if ([trip.driverId isEqualToString:gUser.driver_id]) {
                        [AppSettings share].currentScene = SceneOnlineStartTripViewController;
                    } else {
                        [AppSettings share].currentScene = SceneOrderConfirmViewController;
                    }
                    gTrip = trip;
                    break;
                case pendingPayment:
                    if ([trip.driverId isEqualToString:gUser.driver_id]) {
                        [AppSettings share].currentScene = SceneOnlineViewController;
                    } else {
                        [AppSettings share].currentScene = SceneRatingTripViewController;
                    }
                    gTrip = trip;
                    break;
                    
                default:
                    
                    break;
            }
            
                      
        }

        success([self parseTripFromDicArr:dict]);
    } failure:^(NSString *err) {
        failure(err);
    }];
}
+(void)showMyTripsWithPage:(int)page Success:(void(^)(NSMutableArray *))success andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token,@"page":[NSString stringWithFormat:@"%d",page]};
    [self sendData:dic path:@"showMyTrip" WithSuccess:^(NSDictionary *dict) {
       
        success([self parseTripFromDicArr:dict]);
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(void)showMydriverTripsWithPage:(int)page Success:(void(^)(NSMutableArray *))success andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token};
    [self sendData:dic path:@"getdriverfuturerequests" WithSuccess:^(NSDictionary *dict) {
        
        success([self parseTripFromDicDriver:dict]);
    } failure:^(NSString *err) {
        failure(err);
    }];
}



+(void)showMyfutureTripsWithPage:(int)page Success:(void(^)(NSMutableArray *))success andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token};
    [self sendData:dic path:@"futureRequestListing" WithSuccess:^(NSDictionary *dict) {
        
        success([self parseTripFromDicArr1:dict]);
    } failure:^(NSString *err) {
        failure(err);
    }];
}


+(void)showTripDetailWithTripId:(NSString *)tripId withSuccess:(void(^)(Trips *))success andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token,@"tripId":tripId};
    [self sendData:dic path:@"showTripDetail" WithSuccess:^(NSDictionary *dict) {
        success([self parseTripFromDic:dict]);
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(void)showMyRequestWithSuccess:(void(^)(NSMutableArray *))success andFailure:(void (^)(NSString *))failure{
    NSString *isDriver = gUser.is_driver;
    if ([gUser.is_driver isEqualToString:@"1"]) {
        if(![gUser.driver.isOnline isEqualToString:@"1"]){
            isDriver = @"0";
        }
    }
    
    NSDictionary *dic = @{@"token":gUser.token,@"driver":isDriver};
    [self sendData:dic path:@"showMyRequest" WithSuccess:^(NSDictionary *dict) {
        
        
        success([self parseTripFromDicArr:dict]);
        
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}


+(void)cancelRequest:(Trips *)trip withSuccess:(void(^)(void))success andFailure:(void (^)(NSString *))failure{
    
    NSString *isDriver = gUser.is_driver;
    if ([gUser.is_driver isEqualToString:@"1"]) {
        if(![gUser.driver.isOnline isEqualToString:@"1"]){
            isDriver = @"0";
        }
    }
    NSDictionary *dic = @{@"token":gUser.token,@"driver":isDriver};
    if ([isDriver isEqualToString:@"1"]) {
        
        dic = @{@"token":gUser.token,@"driver":isDriver,@"requestId":trip.tripId};
    }
    
    [self sendData:dic path:@"cancelRequest" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(void)cancelTrip:(Trips *)trip withSuccess:(void(^)(void))success andFailure:(void (^)(NSString *))failure{
    NSDictionary *dic = @{@"token":gUser.token,@"driverId":trip.driverId,@"tripId":trip.tripId};
    
    
    [self sendData:dic path:@"cancelTrip" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure(nil);
    }];
}

+(void)setDriverOnline:(BOOL)isOnline withSuccess:(void(^)(void))success andFailure:(void (^)(NSString *))failure{
    NSString *status = @"0";
    if (isOnline) {
        status = @"1";
    }
    NSDictionary *dic = @{@"token":gUser.token,@"status":status};
    [self sendData:dic path:@"online" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}


+(void)updateProfileWithUser:(User *)user withSuccess:(void (^)(NSString *))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
        failure(nil);
    }
    if (user.lattitude.length==0) {
        user.lattitude = @"0";
        user.logitude = @"0";
    }
    NSString *token = gUser.token;
    
    NSDictionary *di = @{@"description":user.descriptions,@"address":user.address,@"cityId":user.city,@"cityName": user.city, @"stateId":user.stateId, @"stateName":user.state, @"phone":user.phone, @"account":[Validator getSafeString:user.cart_value], @"token":token, @"type_device" : @"2", @"full_name" : user.name};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:[NSString stringWithFormat:@"%@updateProfile",BASE_URL] parameters:di constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (user.avatarImage) {
            UIImage *img = user.avatarImage;
            NSData *main_car = UIImageJPEGRepresentation(user.avatarImage,0.5);

            
             [formData appendPartWithFileData:main_car name:@"image" fileName:[NSString stringWithFormat:@"image%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = responseObject;
        NSString *status =[dic objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
         
            NSDictionary *dict = [dic objectForKey:@"data"];
            if ([dict isKindOfClass:[NSDictionary class]]) {

                success([[dic objectForKey:@"data"] valueForKey:@"token"]);
            }
            else{
                failure([dic objectForKey:@"message"]);
            }
            
        }
        else{
            failure([dic objectForKey:@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure([Util localized:@"msg_technical_error"]);
    }];
    
}


+(void)updateProfileWithDriver:(User *)user andDocument:(UIImage *)document withSuccess:(void (^)(NSDictionary *))success failure:(void (^)(NSString *))failure
{
    
    if (![Util isConnectNetwork]) {
        failure(nil);
        failure(nil);
    }
    if (user.lattitude.length==0) {
        user.lattitude = @"0";
        user.logitude = @"0";
    }
    NSString *token = gUser.token;
    
    NSDictionary *dic = @{@"carPlate":user.car.carPlate,@"brand":user.car.brand,@"model":user.car.model, @"link_type" : user.car.carType, @"account":[Validator getSafeString:user.cart_value], @"year":user.manufacture_year,@"status":user.car.status,@"phone":user.phone,@"token":token, @"type_device" : @"2"};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    [manager POST:[NSString stringWithFormat:@"%@updateDriverData",BASE_URL] parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (user.avatarImage) {
            NSData *main_car = UIImageJPEGRepresentation(user.avatarImage,0.5);
            [formData appendPartWithFileData:main_car name:@"image_avatar" fileName:[NSString stringWithFormat:@"image%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }

        
        if (user.main_car_image) {
            NSData *main_car = UIImageJPEGRepresentation(user.main_car_image,0.5);
            [formData appendPartWithFileData:main_car name:@"image" fileName:[NSString stringWithFormat:@"image%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        
        
        if (user.sub_car_image) {
            NSData *sub_car = UIImageJPEGRepresentation(user.sub_car_image,0.5);
            [formData appendPartWithFileData:sub_car name:@"image2" fileName:[NSString stringWithFormat:@"image2%@.jpg",user.client_id] mimeType:@"image/jpeg"];
            
        }
        if (user.document)
        {
            NSData *document_car = UIImageJPEGRepresentation(user.document_img,0.1);
            [formData appendPartWithFileData:document_car name:@"document" fileName:[NSString stringWithFormat:@"document%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        if (user.License_document)
        {
            NSData *document_car1 = UIImageJPEGRepresentation(user.License_img,0.1);
            [formData appendPartWithFileData:document_car1 name:@"document1" fileName:[NSString stringWithFormat:@"document1%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        if (user.Insurance_document)
        {
            NSData *document_car2 = UIImageJPEGRepresentation(user.Insurance_img,0.1);
            [formData appendPartWithFileData:document_car2 name:@"document2" fileName:[NSString stringWithFormat:@"document2%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        if (user.Refistration_document)
        {
            NSData *document_car3 = UIImageJPEGRepresentation(user.Refistration_img,0.1);
            [formData appendPartWithFileData:document_car3 name:@"document3" fileName:[NSString stringWithFormat:@"document3%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        if (user.Permit_document)
        {
            NSData *document_car4 = UIImageJPEGRepresentation(user.Permit_img,0.1);
            [formData appendPartWithFileData:document_car4 name:@"document4" fileName:[NSString stringWithFormat:@"document4%@.jpg",user.client_id] mimeType:@"image/jpeg"];
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        NSDictionary *dic = responseObject;
        NSString *status = [dic objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success(dic);
        }
        else{
            failure([dic objectForKey:@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       
        failure([Util localized:@"msg_technical_error"]);
    }];
    
}


+(void)pointExchangewithAmount:(NSString *)amount
                 transactionId:(NSString *)tran
              andPaymentMethod:(NSString *)method
                   withSuccess:(void(^)(void))success
                    andFailure:(void (^)(NSString *))failure
{
    
    NSDictionary *dic = @{@"token":gUser.token,@"transactionId":tran,@"amount":amount,@"paymentMethod":method};
    [self sendData:dic path:@"pointExchange" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure([Util localized:@"msg_technical_error"]);
    }];
}

+(void)pointRedeemwithAmount:(NSString *)amount
                 withSuccess:(void(^)(void))success
                  andFailure:(void (^)(NSString *))failure
{
    
    NSDictionary *dic = @{@"token":gUser.token,@"amount":amount};
    [self sendData:dic path:@"pointRedeem" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(void)searchUserWithEmail:(NSString *)email
               withSuccess:(void(^)(User *))success
                andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token,@"email":email};
    [self sendData:dic path:@"searchUser" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            NSDictionary *user = [dict objectForKey:@"data"];
            if ([user isKindOfClass:[NSDictionary class]]) {
                success([self parseUserFromdic:user]);
            }
            else{
                failure([Validator getSafeString:dict[@"message"]]);
            }
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(void)transferWithAmount:(NSString *)amount
                    email:(NSString *)receiverEmail
                  andNote:(NSString *)note
              withSuccess:(void(^)(void))success
               andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token,@"amount":amount,@"receiverEmail":receiverEmail,@"note":note};
    [self sendData:dic path:@"pointTransfer" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success();
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}


+(void)transactionHistorywithPage:(int)page Success:(void(^)(NSMutableArray *))success
                       andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token,@"page":[NSString stringWithFormat:@"%d",page]};
    [self sendData:dic path:@"transactionHistory" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            NSArray *arr = [dict objectForKey:@"data"];
            success([self parseHistoryFromDic:arr]);
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(void)generalSettingWithSuccess:(void(^)(NSDictionary *))success
                      andFailure:(void (^)(NSString *))failure{
    
    NSDictionary *dic = @{@"token":gUser.token};
    [self sendData:dic path:@"generalSettings" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            success([dict objectForKey:@"data"]);
        }
        else{
            failure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        failure(err);
    }];
}

+(NSMutableArray *)parseHistoryFromDic:(NSArray*)dicArr{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (NSDictionary *dic in dicArr) {
        if ([dic isKindOfClass:[NSDictionary class]]) {
            History *his  = [[History alloc]init];
            his.amount = [Validator getSafeString:dic[@"amount"]];
            his.dateCreated =  [Validator getSafeString:dic[@"dateCreated"]];
            his.destination =  [Validator getSafeString:dic[@"destination"]];
            his.transactionId =  [Validator getSafeString:dic[@"id"]];
            his.userId =  [Validator getSafeString:dic[@"userId"]];
            his.tripId = [Validator getSafeString:dic[@"tripId"]];
            NSString *action =  [Validator getSafeString:dic[@"action"]];
            if ([action isEqualToString:CANCELLATION_ORDER_FEE]) {
                his.action = [Util localized:@"CANCELLATION_ORDER_FEE"];
            }
            else if ([action isEqualToString:EXCHANGE_POINT]) {
                his.action = [Util localized:@"EXCHANGE_POINT"];
            }
            else if ([action isEqualToString:REDEEM_POINT]) {
                his.action = [Util localized:@"REDEEM_POINT"];
            }
            else if ([action isEqualToString:TRANSFER_POINT]) {
                his.action = [Util localized:@"TRANSFER_POINT"];
            }
            else if ([action isEqualToString:TRIP_PAYMENT]) {
                his.action = [Util localized:@"TRIP_PAYMENT"];
            }
            else if ([action isEqualToString:PASSENGER_SHARE_BONUS]) {
                his.action = [Util localized:@"PASSENGER_SHARE_BONUS"];
            }
            else{
                his.action = [Util localized:@"DRIVER_SHARE_BONUS"];
            }
            his.type =  [Validator getSafeString:dic[@"type"]];
            [arr addObject:his];
        }
    }
    return arr;
}

//shikha check
+(Trips *)parseTripFromDic:(NSDictionary *)dic{
    Trips *trip = [[Trips alloc]init];
    NSDictionary *data = [dic objectForKey:@"data"];
    trip.tripId = [Validator getSafeString:data[@"id"]];
    trip.passengerId = [Validator getSafeString:data[@"passengerId"]];
    trip.startTime = [Validator getSafeString:data[@"startTime"]];
    trip.link = [Validator getSafeString:data[@"link"]];
    trip.startlat = [Validator getSafeString:data[@"startLat"]];
    trip.startlog = [Validator getSafeString:data[@"startLong"]];
    trip.endlat = [Validator getSafeString:data[@"endLat"]];
    trip.endlog = [Validator getSafeString:data[@"endLong"]];
    trip.driver_unique_id = [Validator getSafeString:data[@"driver_unique_id"]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // [defaults setDouble:result.coordinate.latitude forKey:@"homelatitude"];
    // [defaults setDouble:result.coordinate.longitude forKey:@"homelogitude"];
    

    
    // NSNumber *latitude = [NSNumber numberWithDouble:result.coordinate.latitude];
    // NSNumber *longitude = [NSNumber numberWithDouble:result.coordinate.longitude ];
    [defaults setObject:trip.startlat forKey:@"startlatitude"];
    [defaults setObject:trip.startlog forKey:@"startlogitude"];
    
    [defaults setObject: trip.endlat forKey:@"endlatitude"];
    [defaults setObject:trip.endlog forKey:@"endlogitude"];
    
    [defaults synchronize];
    
       
    
    float startLat = [Validator getSafeFloat:data[@"startLat"]];
    float startLong = [Validator getSafeFloat:data[@"startLong"]];
    NSString *startLocation = [Validator getSafeString:data[@"startLocation"]];
    trip.startLocation = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake(startLat, startLong) addressName:startLocation];
    
    float endLat = [Validator getSafeFloat:data[@"endLat"]];
    float endLong = [Validator getSafeFloat:data[@"endLong"]];
    NSString *endLocation = [Validator getSafeString:data[@"endLocation"]];
    trip.endLocation = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake(endLat, endLong) addressName:endLocation];
    
    trip.driverId = [Validator getSafeString:data[@"driverId"]];
    trip.status = [Validator getSafeInt:data[@"status"]];
    trip.distance = [Validator getSafeString:data[@"distance"]];
    trip.estimateFare = [Validator getSafeString:data[@"estimateFare"]];
    trip.actualFare = [Validator getSafeString:data[@"actualFare"]];
    trip.actualEarn = [NSString stringWithFormat:@"%.2f", [trip.actualFare floatValue]*[AppSettings share].driverEarn];
   
    trip.driverRate = [Validator getSafeString:data[@"driverRate"]];
    trip.passengerRate = [Validator getSafeString:data[@"passengerRate"]];
    
    NSDictionary *driver = [data objectForKey:@"driver"];
    trip.driver = [[Driver alloc]init];
    trip.driver.driverName = [Validator getSafeString:driver[@"driverName"]];
    trip.driver.driverRate = [Validator getSafeString:driver[@"rate"]];
    trip.driver.imageDriver = [Validator getSafeString:driver[@"imageDriver"]];
    trip.driver.carImage = [Validator getSafeString:driver[@"carImage"]];
    trip.driver.carPlate = [Validator getSafeString:driver[@"carPlate"]];
    trip.driver.rateup = [Validator getSafeString:driver[@"rateup"]];
    trip.driver.ratedown = [Validator getSafeString:driver[@"ratedown"]];

    trip.driver.phone = [Validator getSafeString:driver[@"phone"]];
    
    NSDictionary *passenger = [data objectForKey:@"passenger"];
    trip.passenger = [[User alloc]init];
    trip.passenger.name = [Validator getSafeString:passenger[@"passengerName"]];
    trip.passenger.passenger_rating = [Validator getSafeString:passenger[@"rate"]];
    trip.passenger.thumb = [Validator getSafeString:passenger[@"imagePassenger"]];
    trip.passenger.phone = [Validator getSafeString:passenger[@"phone"]];
    trip.passenger_UP = [Validator getSafeString:passenger[@"rateup"]];
    trip.passenger_down = [Validator getSafeString:passenger[@"ratedown"]];

    return trip;
}
+(NSMutableArray *)parseTripFromDicArr:(NSDictionary *)dic{
    NSArray *arr = [dic objectForKey:@"data"];
    NSMutableArray *tripArr = [[NSMutableArray alloc]init];
    if ([arr isKindOfClass:[NSArray class]]) {
        for (NSDictionary *dict in arr) {
            Trips *trip = [[Trips alloc]init];
            trip.tripId = [Validator getSafeString:dict[@"id"]];
            trip.passengerId = [Validator getSafeString:dict[@"passengerId"]];
            trip.link = [Validator getSafeString:dict[@"link"]];
            trip.startTime = [Validator getSafeString:dict[@"startTime"]];
            trip.endTime = [Validator getSafeString:dict[@"endTime"]];
            trip.distance = [Validator getSafeString:dict[@"distance"]];
            trip.status = [Validator getSafeInt:dict[@"status"]];
            trip.estimate_fare = [NSString stringWithFormat:@"%.2f", [Validator getSafeFloat:dict[@"estimate_fare"]]];
            trip.estimateFare = [Validator getSafeString:dict[@"estimate_fare"]];
            trip.actualFare = [Validator getSafeString:dict[@"actualFare"]];
            if (!trip.actualFare.length)
            {
                trip.actualFare = @"0.00";
            }
            trip.actualEarn = [Validator getSafeString:dict[@"actualReceive"]];
            if (!trip.actualEarn.length)
            {
                trip.actualEarn = @"0.00";
            }
            trip.driverRate = [Validator getSafeString:dict[@"driverRate"]];
            trip.passengerRate = [Validator getSafeString:dict[@"passengerRate"]];
            trip.dateCreated = [Validator getSafeString:dict[@"dateCreated"]];
            trip.totalTime = [Validator getSafeString:dict[@"totalTime"]];
            trip.driverId = [Validator getSafeString:dict[@"driverId"]];
            trip.estimateDistance = gTrip.estimateDistance;
            
            trip.passenger = [[User alloc]init];
            NSDictionary *passenger = dict[@"passenger"];
            if ([passenger isKindOfClass:[NSDictionary class]]){
                trip.passenger = [self parseUserFromdic:dict[@"passenger"]];
            }
            
            if (trip.status  ==  pendingPayment) {
                if ([trip.driverId isEqualToString:gUser.driver_id]) {
                    trip.status = finish;
                }
            }
            NSDictionary *driver = [dict objectForKey:@"driver"];
            if ([driver isKindOfClass:[NSDictionary class]]){
                trip.driver = [[Driver alloc]init];
                trip.driver.driverName = [Validator getSafeString:driver[@"fullName"]];
                trip.driver.ratedown = [Validator getSafeString:driver[@"ratedown"]];
                trip.driver.rateup = [Validator getSafeString:driver[@"rateup"]];

                
                trip.driver.imageDriver = [Validator getSafeString:driver[@"imageDriver"]];
                if (trip.driver.imageDriver.length==0) {
                    trip.driver.imageDriver = [Validator getSafeString:driver[@"image"]];
                }
                NSDictionary *images = driver[@"carImage"];
                if ([images isKindOfClass:[NSDictionary class]]) {
                    trip.driver.carImage = [Validator getSafeString:driver[@"image1"]];
                }
                
                trip.driver.carPlate = [Validator getSafeString:driver[@"carPlate"]];
                trip.driver.phone = [Validator getSafeString:driver[@"phone"]];
                NSLog(@"%@",[Validator getSafeString:driver[@"driver_unique_id"]]);

                trip.driver.driver_unique_id = [Validator getSafeString:driver[@"driver_unique_id"]];;

            }
            
            NSString *startLat = [Validator getSafeString:dict[@"startLat"]];
            NSString *startLong = [Validator getSafeString:dict[@"startLong"]];
            NSString *endLat = [Validator getSafeString:dict[@"endLat"]];
            NSString *endLong = [Validator getSafeString:dict[@"endLong"]];
            
            NSString *endLat1 = [Validator getSafeString:dict[@"endLat1"]];
            NSString *endLong1= [Validator getSafeString:dict[@"endLong1"]];
            
            NSString *endLat2 = [Validator getSafeString:dict[@"endLat2"]];
            NSString *endLong2 = [Validator getSafeString:dict[@"endLong2"]];
            
            NSString *endLat3 = [Validator getSafeString:dict[@"endLat3"]];
            NSString *endLong3 = [Validator getSafeString:dict[@"endLong3"]];
            
            
            NSString *endLat4 = [Validator getSafeString:dict[@"endLat4"]];
            NSString *endLong4 = [Validator getSafeString:dict[@"endLong4"]];
            
           
            NSString *startLocation = [Validator getSafeString:dict[@"startLocation"]];
            NSString *endLocation = [Validator getSafeString:dict[@"endLocation"]];
            
              NSString *endLocation1 = [Validator getSafeString:dict[@"endLocation1"]];
              NSString *endLocation2 = [Validator getSafeString:dict[@"endLocation2"]];
              NSString *endLocation3 = [Validator getSafeString:dict[@"endLocation3"]];
              NSString *endLocation4 = [Validator getSafeString:dict[@"endLocation4"]];
            
            
            
            
          trip.startLocation = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([startLat floatValue], [startLong floatValue]) addressName:startLocation];
      
            trip.endLocation = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([endLat floatValue], [endLong floatValue]) addressName:endLocation];
        trip.endLocation1 = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([endLat1 floatValue], [endLong1 floatValue]) addressName:endLocation1];
            
        trip.endLocation2 = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([endLat2 floatValue], [endLong2 floatValue]) addressName:endLocation2];
            
        trip.endLocation3 = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([endLat3 floatValue], [endLong3 floatValue]) addressName:endLocation3];
            
      trip.endLocation4 = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([endLat4 floatValue], [endLong4 floatValue]) addressName:endLocation4];
            
            
            [tripArr addObject:trip];
        }
    }
    
    
    return tripArr;
}

+(NSMutableArray *)parseTripFromDicArr1:(NSDictionary *)dic{
    if ([dic objectForKey:@"data"] != nil )
    {
    NSArray *arr = [dic objectForKey:@"data"];
//        if (arr.count >0)
//        {
//            arr = [[NSArray alloc]init];
//            arr = [[dic objectForKey:@"data"]allValues];
//
//        }
    NSMutableArray *tripArr = [[NSMutableArray alloc]init];
    if ([arr isKindOfClass:[NSArray class]]) {
        for (NSDictionary *dict in arr) {
            Trips *trip = [[Trips alloc]init];
            trip.tripId = [Validator getSafeString:dict[@"id"]];
            trip.passengerId = [Validator getSafeString:dict[@"passenger_id"]];
            trip.link = [Validator getSafeString:dict[@"link"]];
            trip.startTime = [Validator getSafeString:dict[@"startTime"]];
            trip.RequestTime = [Validator getSafeString:dict[@"requestTime"]];
            trip.distance = [Validator getSafeString:dict[@"distance"]];
            trip.status = [Validator getSafeInt:dict[@"status"]];
            trip.estimate_fare = [NSString stringWithFormat:@"%.2f", [Validator getSafeFloat:dict[@"estimate_fare"]]];
            trip.estimateFare = [Validator getSafeString:dict[@"estimate_fare"]];
            trip.actualFare = [Validator getSafeString:dict[@"actualFare"]];
            if (!trip.actualFare.length) {
                trip.actualFare = @"0.00";
            }
            trip.actualEarn = [Validator getSafeString:dict[@"actualReceive"]];
            if (!trip.actualEarn.length) {
                trip.actualEarn = @"0.00";
            }
            trip.driverRate = [Validator getSafeString:dict[@"driverRate"]];
            trip.passengerRate = [Validator getSafeString:dict[@"passengerRate"]];
            trip.dateCreated = [Validator getSafeString:dict[@"dateCreated"]];
            trip.totalTime = [Validator getSafeString:dict[@"totalTime"]];
            trip.driverId = [Validator getSafeString:dict[@"driverId"]];
            trip.estimateDistance = gTrip.estimateDistance;
            
            trip.passenger = [[User alloc]init];
            NSDictionary *passenger = dict[@"passenger"];
            if ([passenger isKindOfClass:[NSDictionary class]]){
                trip.passenger = [self parseUserFromdic:dict[@"passenger"]];
            }
            
            if (trip.status  ==  pendingPayment) {
                if ([trip.driverId isEqualToString:gUser.driver_id]) {
                    trip.status = finish;
                }
            }
            
            //prabhjot
            if ([dict valueForKey:@"driver_unique_id"] != nil) {
                trip.driver_unique_id = [Validator getSafeString:dict[@"driver_unique_id"]];
            }
            
            NSDictionary *driver = [dict objectForKey:@"driver"];
            if ([driver isKindOfClass:[NSDictionary class]]){
                trip.driver = [[Driver alloc]init];
                trip.driver.driverName = [Validator getSafeString:driver[@"fullName"]];
                trip.driver.driverRate = [Validator getSafeString:driver[@"rate"]];
                trip.driver.imageDriver = [Validator getSafeString:driver[@"imageDriver"]];
                if (trip.driver.imageDriver.length==0) {
                    trip.driver.imageDriver = [Validator getSafeString:driver[@"image"]];
                }
                NSDictionary *images = driver[@"carImage"];
                if ([images isKindOfClass:[NSDictionary class]]) {
                    trip.driver.carImage = [Validator getSafeString:driver[@"image1"]];
                }
                
                trip.driver.carPlate = [Validator getSafeString:driver[@"carPlate"]];
                trip.driver.phone = [Validator getSafeString:driver[@"phone"]];
            }
            
            NSString *startLat = [Validator getSafeString:dict[@"startLat"]];
            NSString *startLong = [Validator getSafeString:dict[@"startLong"]];
            NSString *endLat = [Validator getSafeString:dict[@"endLat"]];
            NSString *endLong = [Validator getSafeString:dict[@"endLong"]];
            NSString *startLocation = [Validator getSafeString:dict[@"startLocation"]];
            NSString *endLocation = [Validator getSafeString:dict[@"endLocation"]];
            
            trip.startLocation = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([startLat floatValue], [startLong floatValue]) addressName:startLocation];
            
            trip.endLocation = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([endLat floatValue], [endLong floatValue]) addressName:endLocation];
            [tripArr addObject:trip];
        }
    }
    
    
    return tripArr;
    }
    else
    {
        return 0;
    }
}



+(NSMutableArray *)parseTripFromDicDriver:(NSDictionary *)dic{
    if ([dic objectForKey:@"data"] != nil )
    {
        NSArray *arr = [dic objectForKey:@"data"];
        if (arr.count >0)
        {
            arr = [[NSArray alloc]init];
            arr = [dic objectForKey:@"data"];
            
        }
        NSMutableArray *tripArr = [[NSMutableArray alloc]init];
        if ([arr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dict in arr) {
                Trips *trip = [[Trips alloc]init];
                trip.tripId = [Validator getSafeString:dict[@"id"]];
                NSLog(@"%@",trip.tripId);
                trip.passengerId = [Validator getSafeString:dict[@"passengerId"]];
                NSLog(@"%@",trip.passengerId);
                trip.link = [Validator getSafeString:dict[@"link"]];
                NSLog(@"%@",trip.link);
                trip.startTime = [Validator getSafeString:dict[@"startTime"]];
                   NSLog(@"%@",trip.startTime);
                
                NSLog(@"%@",trip.startTime);
                trip.RequestTime = [Validator getSafeString:dict[@"requestTime"]];
                trip.distance = [Validator getSafeString:dict[@"distance"]];
                 NSLog(@"%@",trip.distance);
                trip.status = [Validator getSafeInt:dict[@"status"]];
                NSLog(@"%@",trip.status);
                trip.estimate_fare = [NSString stringWithFormat:@"%.2f", [Validator getSafeFloat:dict[@"estimate_fare"]]];
                trip.estimateFare = [Validator getSafeString:dict[@"estimate_fare"]];
                trip.actualFare = [Validator getSafeString:dict[@"actualFare"]];
                if (!trip.actualFare.length) {
                    trip.actualFare = @"0.00";
                }
                trip.actualEarn = [Validator getSafeString:dict[@"actualReceive"]];
                if (!trip.actualEarn.length) {
                    trip.actualEarn = @"0.00";
                }
                trip.driverRate = [Validator getSafeString:dict[@"driverRate"]];
                trip.passengerRate = [Validator getSafeString:dict[@"passengerRate"]];
                trip.dateCreated = [Validator getSafeString:dict[@"dateCreated"]];
                trip.totalTime = [Validator getSafeString:dict[@"totalTime"]];
                trip.driverId = [Validator getSafeString:dict[@"driverId"]];
                 NSLog(@"%@",trip.driverId);
                trip.estimateDistance = gTrip.estimateDistance;
                  NSLog(@"%@",trip.estimateDistance);
                trip.passenger = [[User alloc]init];
                NSDictionary *passenger = dict[@"passenger"];
                if ([passenger isKindOfClass:[NSDictionary class]]){
                    trip.passenger = [self parseUserFromdic:dict[@"passenger"]];
                }
                
                if (trip.status  ==  pendingPayment) {
                    if ([trip.driverId isEqualToString:gUser.driver_id]) {
                        trip.status = finish;
                    }
                }
                NSDictionary *driver = [dict objectForKey:@"driver"];
                if ([driver isKindOfClass:[NSDictionary class]]){
                    trip.driver = [[Driver alloc]init];
                    trip.driver.driverName = [Validator getSafeString:driver[@"fullName"]];
                    trip.driver.driverRate = [Validator getSafeString:driver[@"rate"]];
                    trip.driver.imageDriver = [Validator getSafeString:driver[@"imageDriver"]];
                    if (trip.driver.imageDriver.length==0) {
                        trip.driver.imageDriver = [Validator getSafeString:driver[@"image"]];
                    }
                    NSDictionary *images = driver[@"carImage"];
                    if ([images isKindOfClass:[NSDictionary class]]) {
                        trip.driver.carImage = [Validator getSafeString:driver[@"image1"]];
                    }
                    
                    trip.driver.carPlate = [Validator getSafeString:driver[@"carPlate"]];
                    trip.driver.phone = [Validator getSafeString:driver[@"phone"]];
                }
                
                NSString *startLat = [Validator getSafeString:dict[@"startLat"]];
                NSString *startLong = [Validator getSafeString:dict[@"startLong"]];
                NSString *endLat = [Validator getSafeString:dict[@"endLat"]];
                NSString *endLong = [Validator getSafeString:dict[@"endLong"]];
                NSString *startLocation = [Validator getSafeString:dict[@"startLocation"]];
                NSString *endLocation = [Validator getSafeString:dict[@"endLocation"]];
                
                trip.startLocation = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([startLat floatValue], [startLong floatValue]) addressName:startLocation];
                
                trip.endLocation = [[CustomPosition alloc] initWithPositon:CLLocationCoordinate2DMake([endLat floatValue], [endLong floatValue]) addressName:endLocation];
                [tripArr addObject:trip];
            }
        }
        
        
        return tripArr;
    }
    else
    {
        return 0;
    }
}




+ (void)getListStateWithSuccess:(void (^)(NSArray *))returnSuccess failure:(void (^)(NSString *))returnFail{
    NSDictionary *dic = @{};
    [self sendData:dic path:@"showStateCity" WithSuccess:^(NSDictionary *dict) {
        
        
        
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            NSArray *arrDictState = [dict objectForKey:@"data"];
            NSMutableArray *arrStateReturn = [[NSMutableArray alloc] init];
            for (NSDictionary *dictState in arrDictState) {
                StateObj* stateObj = [[StateObj alloc] initWithDict:dictState];
                [arrStateReturn addObject:stateObj];
            }
            returnSuccess(arrStateReturn);
            gArrState = arrStateReturn.copy;
        }
        else{
            returnFail(dict[@"message"]);
        }
        
    } failure:^(NSString *err) {
        returnFail(err);
    }];
        
    
}
     

+ (void)registerNormalWithUser:(User *)userObj avatar:(UIImage *)imgAvatar withSuccess:(void (^)(NSString *))returnSuccess failure:(void (^)(NSString *))failure{
    NSString *token = deviceTokenString;
    if (token.length == 0) {
        token = [Util objectForKey:@"Token"];
    }
    NSDictionary *dic = @{@"token" : token,
                          @"full_name" : userObj.name,
                          @"address":@"",
                          @"password":userObj.password,
                          @"post_code":@"",
                          @"country":@"",
                          @"city":@"",
                          @"email":userObj.email,
                          @"phone":userObj.phone,
                          @"account":@"",
                          @"image":@""};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BASE_URL,REGISTER_NOMAL] parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        if (imgAvatar) {
            NSData *document_car = UIImageJPEGRepresentation(imgAvatar,0.5);
            [formData appendPartWithFileData:document_car name:@"image" fileName:[NSString stringWithFormat:@"image%@.jpg",[Validator getSafeString:userObj.phone]] mimeType:@"image/jpeg"];
        }
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = responseObject;
        NSString *status = [dic objectForKey:@"status"];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            returnSuccess(dic[@"message"]);
        }
        else{
            failure([dic objectForKey:@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure([Util localized:@"msg_technical_error"]);
    }];
    
}

+ (void)forgotPasswordWithEmail:(NSString *)email withSuccess:(void (^)(NSString *))returnSuccess failure:(void (^)(NSString *))returnFailure{
    NSDictionary *dic = @{@"email":email};
    [self sendData:dic path:@"forgotPassword" WithSuccess:^(NSDictionary *dict) {
        NSString *status = [Validator getSafeString:dict[@"status"]];
        if ([status.lowercaseString isEqualToString:@"success"]) {
            returnSuccess(dic[@"message"]);
        }
        else{
            returnFailure([Validator getSafeString:dict[@"message"]]);
        }
        
    } failure:^(NSString *err) {
        returnFailure(err);
    }];
    
}

+ (void)addToAccountAmount:(NSString *)amount withStripToken:(NSString *)stripToken withSuccess:(void (^)(NSString *))callbackSuccess failure:(void (^)(NSString *))callbackFailure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%.2f",[amount floatValue]], @"amount", gUser.email, @"email", stripToken, @"token", nil];
    [manager GET:STRIPE_URL parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([[responseObject[@"status"] uppercaseString] isEqualToString:@"SUCCESS"]) {
            if (callbackSuccess) {
                callbackSuccess(responseObject[@"message"]);
            }
        }else{
            if (callbackFailure) {
                callbackFailure(responseObject[@"message"]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (callbackFailure) {
            callbackFailure(error.description);
        }
    }];
}

+ (void)getCurrentLocationWithDriverId:(NSString *)driverId withSuccess:(void (^)(float, float))callbackSuccess failure:(void (^)(NSString *))callbackFailure{
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:driverId, @"driverId", [NSString stringWithFormat:@"%.2f", [NSDate date].timeIntervalSince1970], @"timeNow", nil];
    
    if (dic != nil)
    {
    
    [self sendData:dic path:@"getDriverLocation" WithSuccess:^(NSDictionary *dic) {
        if ([[dic[@"status"] uppercaseString] isEqualToString:@"SUCCESS"]) {
            if (callbackSuccess) {
                callbackSuccess([Validator getSafeFloat:dic[@"data"][@"driverLat"]], [Validator getSafeFloat:dic[@"data"][@"driverLon"]]);
            }
        }else{
            if (callbackFailure) {
                callbackFailure(dic[@"message"]);
            }
        }
    } failure:^(NSString *err) {
        callbackFailure(err);
    }];
    }
}

+ (void)sendHelpRequestWithSuccess:(void (^)(NSString *))callbackSuccess failure:(void (^)(NSString *))callbackFailed{
    NSDictionary *dic = @{@"token" : gUser.token,
                          @"tripId" : gTrip.tripId};
    [self sendData:dic path:@"needHelpTrip" WithSuccess:^(NSDictionary *dic) {
        if ([Validator getSafeString:dic[@"status"]]) {
            callbackSuccess([Validator getSafeString:dic[@"message"]]);
        }else{
            callbackFailed([Validator getSafeString:dic[@"message"]]);
        }
    } failure:^(NSString *err) {
        callbackFailed(err);
    }];
}


+ (void)changeStatusOfTripToArrivedWithSuccess:(void (^)(NSString *))callbackSuccess failure:(void (^)(NSString *))callbackFailed{
    NSDictionary *dic = @{@"token": gUser.token,
                          @"tripId": gTrip.tripId};
    [self sendData:dic path:@"driverArrived" WithSuccess:^(NSDictionary *dic) {
        
    } failure:^(NSString *err) {
        
    }];
}

+ (void)getRoutingByGoogleWithStart:(CLLocationCoordinate2D)startLocation endLocation:(CLLocationCoordinate2D)endLocationl handlerSuccess:(void (^)(NSDictionary *))callbackSuccess handlerFailed:(void (^)(NSString *))callbackFalure{
    
    NSString *origin = [NSString stringWithFormat:@"%f,%f", startLocation.latitude, startLocation.longitude];
    NSString *destination = [NSString stringWithFormat:@"%f,%f", endLocationl.latitude, endLocationl.longitude];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:20];
    [manager POST:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=false&units=metric&mode=driving&api-key=%@", origin, destination, GOOGLE_API_KEY] parameters:nil progress:nil success:^
    
//         [manager POST:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=false&units=metric&mode=driving", origin, destination] parameters:nil progress:nil success:^
    
     (NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
        if ([responseObject[@"status"] isEqualToString:@"OK"]) {
            if (callbackSuccess) {
                callbackSuccess(responseObject);
            }
                       
        } else {
            if (callbackFalure) {
                callbackFalure(@"");
            }
        }
        
    }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error are %@",error);
        if (callbackFalure) {
            callbackFalure(@"");
        }
    }];
}

+ (void)getAddressNameByPosition:(CLLocationCoordinate2D)position handlerSuccess:(void (^)(NSString *))callbackSuccess handlerFailed:(void (^)(NSString *))callbackFailed{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:4];
    [manager POST:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true&api-key=AIzaSyBEVpmLQw9S2WzPQ89h8VchpboV47_TUYg",position.latitude, position.longitude] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([responseObject[@"status"] isEqualToString:@"OK"]) {
            if (callbackSuccess)
            {
                callbackSuccess(responseObject[@"results"][0][@"formatted_address"]);
            }
            
        }
        else
        {
            if (callbackFailed) {
                callbackFailed(@"");
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (callbackFailed) {
            callbackFailed(@"");
        }
    }];

}

@end
