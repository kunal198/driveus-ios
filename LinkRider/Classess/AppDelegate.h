

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "OrderConfirmViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <Google/SignIn.h>
#import <Stripe/Stripe.h>
#import "LocationManager.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate,UIAlertViewDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (assign, nonatomic) BOOL gotNotifcation;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

