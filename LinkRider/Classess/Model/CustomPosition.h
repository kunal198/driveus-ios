//
//  CustomPosition.h
//  LinkRider
//
//  Created by Pham Diep on 4/7/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomPosition : NSObject
@property (nonatomic) CLLocationCoordinate2D position;
@property (nonatomic, copy) NSString* address;

- (instancetype)initWithPositon:(CLLocationCoordinate2D)position addressName:(NSString*)address;
@end
