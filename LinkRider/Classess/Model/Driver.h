//
//  Driver.h
//  LinkRider
//
//  Created by Hicom on 7/15/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Driver : NSObject

@property(nonatomic,copy)NSString *document;
@property(nonatomic,copy)NSString *driverRate;
@property(nonatomic,copy)NSString *driverRateCount;
@property(nonatomic,copy)NSString *isActive;
@property(nonatomic,copy)NSString *isOnline;
@property(nonatomic,copy)NSString *rateup;
@property(nonatomic,copy)NSString *ratedown;
@property(nonatomic,copy)NSString *status;
@property(nonatomic,copy)NSString *driverName;
@property(nonatomic,copy)NSString *document1;
@property(nonatomic,copy)NSString *document2;
@property(nonatomic,copy)NSString *document3;
@property(nonatomic,copy)NSString *document4;
@property(nonatomic,copy)NSString *imageDriver;
@property(nonatomic,copy)NSString *carPlate;
@property(nonatomic,copy)NSString *carImage;
@property(nonatomic,copy)NSString *phone;
@property(nonatomic,copy)NSString *driver_unique_id;
@property(nonatomic,copy)NSString *update_pending;
@end
