//
//  CustomPosition.m
//  LinkRider
//
//  Created by Pham Diep on 4/7/17.
//  Copyright © 2017 Fruity Solution. All rights reserved.
//

#import "CustomPosition.h"

@implementation CustomPosition
- (instancetype)initWithPositon:(CLLocationCoordinate2D)position addressName:(NSString *)address{
    self = [super init];
    self.position = position;
    self.address = address;
    return self;
}
@end
