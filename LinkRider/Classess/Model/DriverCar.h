//
//  DriverCar.h
//  LinkRider
//
//  Created by Hicom on 7/15/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

#define carTypeDic  @{ @"SEDAN" : @"I", @"SUV" : @"II", @"LUX": @"III"}

#define carAvailableDic  @{ @"Yellow/Green Taxi" : @"I", @"Black": @"III"}


@interface DriverCar : NSObject
@property(nonatomic,copy)NSString *car_id;
@property(nonatomic,copy)NSString *brand;
@property(nonatomic,copy)NSString *carPlate;
@property(nonatomic,copy)NSString *dateCreated;
@property(nonatomic,copy)NSString *document;
@property(nonatomic,copy)NSString *document1;
@property(nonatomic,copy)NSString *document2;
@property(nonatomic,copy)NSString *document3;
@property(nonatomic,copy)NSString *document4;
@property(nonatomic,copy)NSString *image1;
@property(nonatomic,copy)NSString *image2;
@property(nonatomic,copy)NSString *model;
@property(nonatomic,copy)NSString *status;
@property(nonatomic,copy)NSString *year;
@property (nonatomic, copy) NSString *carType;

+ (NSString*)typeNameByTypeId:(NSString*)typeId;
@end
