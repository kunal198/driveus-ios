//
//  DriverCar.m
//  LinkRider
//
//  Created by Hicom on 7/15/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "DriverCar.h"

@implementation DriverCar
+ (NSString *)typeNameByTypeId:(NSString *)typeId{
    for (int i = 0; i < carTypeDic.allKeys.count; i++) {
        if ([typeId isEqualToString:carTypeDic.allValues[i]]) {
           return carTypeDic.allKeys[i];
        }
    }
    return @"";
}
@end
