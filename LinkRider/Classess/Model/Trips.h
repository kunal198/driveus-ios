//
//  Trips.h
//  LinkRider
//
//  Created by Hicom on 7/7/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomPosition.h"
#import "Driver.h"
#import "User.h"

typedef enum {
    approadching = 1,
    inprogress,
    pendingPayment,
    finish,
} TripStatus;

@interface Trips : NSObject
@property (strong, nonatomic) NSString *tripId;
@property (strong, nonatomic) NSString *link;
@property (nonatomic, strong)  CustomPosition *startLocation;
@property (strong, nonatomic) CustomPosition *endLocation;
@property (nonatomic, strong)  CustomPosition *endLocation1;
@property (strong, nonatomic) CustomPosition *startLocation1;
@property (nonatomic, strong)  CustomPosition *endLocation2;
@property (strong, nonatomic) CustomPosition *startLocation2;
@property (nonatomic, strong)  CustomPosition *endLocation3;
@property (strong, nonatomic) CustomPosition *startLocation3;
@property (nonatomic, strong)  CustomPosition *endLocation4;
@property (strong, nonatomic) CustomPosition *startLocation4;

@property (strong, nonatomic) NSString *passengerId;
@property (strong, nonatomic) NSString *driverId;
@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *startlat;
@property (strong, nonatomic) NSString *startlog;
@property (strong, nonatomic) NSString *endlat;
@property (strong, nonatomic) NSString *endlog;
@property (strong, nonatomic) NSString *driver_unique_id;
@property (strong, nonatomic) NSString *endTime;
@property (strong, nonatomic) NSString *RequestTime;
@property (strong, nonatomic) NSString *distance;
@property (assign, nonatomic) int status;
@property (strong, nonatomic) NSString *estimateFare;
@property (strong, nonatomic) NSString *futuretime;
@property (strong, nonatomic) NSString *actualFare;
@property (strong, nonatomic) NSString *actualEarn;
@property (strong, nonatomic) NSString *driverRate;
@property (strong, nonatomic) NSString *passengerRate;
@property (strong, nonatomic) NSString *dateCreated;
@property (strong, nonatomic) NSString *totalTime;
@property (strong, nonatomic) NSString *estimate_fare;
@property (strong, nonatomic) NSString *estimateDistance;
@property (strong, nonatomic) NSString *driver_UP;
@property (strong, nonatomic) NSString *driver_down;
@property (strong, nonatomic) NSString *passenger_UP;
@property (strong, nonatomic) NSString *passenger_down;

@property (strong, nonatomic) Driver *driver;
@property (strong, nonatomic) User *passenger;




//
//-(void)setStartLocationWithLatitude:(float)lat longitude:(float)lon andTitle:(NSString *)title;
//-(void)setEndLocationWithLatitude:(float)lat longitude:(float)lon andTitle:(NSString *)title;

@end
